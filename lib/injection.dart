import 'package:rassed_app/features/Constants/ApiConstants.dart';
import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:dio/dio.dart';
import 'package:get_it/get_it.dart';
import 'package:rassed_app/features/add_cards/component/Card/usecase/add_card_excel_usecase.dart';
import 'package:rassed_app/features/add_cards/component/Company/api/addCompany.dart';
import 'package:rassed_app/features/add_cards/component/Company/api/edit_company_remote.dart';
import 'package:rassed_app/features/add_cards/component/Company/api/get_categories.dart';
import 'package:rassed_app/features/add_cards/component/Company/api/get_copmany_remote.dart';
import 'package:rassed_app/features/add_cards/component/Company/bloc/companyBloc.dart';
import 'package:rassed_app/features/add_cards/component/Company/usecase/edit_company_usecae.dart';
import 'package:rassed_app/features/add_cards/component/Company/usecase/get_company_usecase.dart';
import 'package:rassed_app/features/add_cards/component/Company/usecase/get_categories_usecase.dart';
import 'package:rassed_app/features/home/api/home_remote.dart';
import 'package:rassed_app/features/home/bloc/HomeBloc.dart';
import 'package:rassed_app/features/home/usecase/home_usecase.dart';
import 'package:rassed_app/features/login/api/login_remote.dart';
import 'package:rassed_app/features/login/bloc/LoginBloc.dart';
import 'package:rassed_app/features/login/usecase/login_usecase.dart';
import 'package:rassed_app/features/profile/api/logout_api.dart';
import 'package:rassed_app/features/profile/api/user_profile_remote.dart';
import 'package:rassed_app/features/profile/bloc/ProfileBloc.dart';
import 'package:rassed_app/features/profile/usecase/logout_usecase.dart';
import 'package:rassed_app/features/profile/usecase/seen_notify_usecase.dart';
import 'package:rassed_app/features/profile/usecase/user_profile_usecase.dart';
import 'package:rassed_app/features/signup/api/sign_verify.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:rassed_app/features/signup/api/signup_remote.dart';
import 'package:rassed_app/features/signup/bloc/SignBloc.dart';
import 'package:rassed_app/features/signup/usecase/Signup_usecase.dart';
import 'core/network/network_info.dart';
import 'features/add_cards/component/Card/api/add_card_excel_remote.dart';
import 'features/add_cards/component/Card/api/get_cards_remote.dart';
import 'features/add_cards/component/Card/api/add_card_remote.dart';
import 'features/add_cards/component/Card/api/get_types_remote.dart';
import 'features/add_cards/component/Card/usecase/add_card_usecase.dart';
import 'features/add_cards/component/Card/usecase/get_types_usecase.dart';

import 'features/add_cards/component/Card/bloc/CardBloc.dart';
import 'features/add_cards/component/Card/usecase/card_usecase.dart';
import 'features/add_cards/component/CardType/api/get_compaies_remote.dart';
import 'features/add_cards/component/CardType/usecase/get_companies_usecase.dart';
import 'features/add_cards/component/Company/usecase/add_company_usecase.dart';
import 'features/add_cards/component/CardType/usecase/card_type_usecase.dart';
import 'features/add_cards/component/CardType/usecase/add_card_type_usecase.dart';
import 'features/add_cards/component/CardType/bloc/CardTypeBloc.dart';
import 'features/add_cards/component/CardType/api/get_card_type_remote.dart';
import 'features/add_cards/component/CardType/api/add_card_type_remote.dart';
import 'features/card_details/api/card_remote.dart';
import 'features/card_details/bloc/CardBloc.dart';
import 'features/card_details/usecase/card_case.dart';
import 'features/profile/api/charege_remote.dart';
import 'features/profile/api/notify_api.dart';
import 'features/profile/api/notify_refund_remote.dart';
import 'features/profile/api/seen_notify_remote.dart';
import 'features/profile/usecase/charge_usecase.dart';
import 'features/profile/usecase/notify_refund_usecase.dart';
import 'features/profile/usecase/notify_usecase.dart';
import 'features/signup/usecase/Signup_verify_usecase.dart';
import 'features/Shopping_basket/usecase/shop_basket_usecase.dart';
import 'features/Shopping_basket/bloc/ShopbasketBloc.dart';
import 'features/Shopping_basket/api/shop_basket_remote.dart';
import 'features/sales/usecase/sales_usecase.dart';
import 'features/sales/usecase/refund_all_invoice_usecase.dart';
import 'features/sales/usecase/get_invoice_usecase.dart';
import 'features/sales/api/sale_remote.dart';
import 'features/sales/api/get_invoices_remote.dart';
import 'features/sales/bloc/sale_bloc.dart';
import 'features/sales/api/refund_all_invoice_remote.dart';


import 'features/shop/bloc/shop_bloc.dart';
import 'features/shop/usecase/shops_usecase.dart';
import 'features/shop/api/shop_remote.dart';







final sl = GetIt.instance;

///
/// The [init] function is responsible for adding the instances to the graph
///
Future<void> init() async {
  //! External

  /// Adding the [SharedPreferences] instance to the graph to be later used by the local data sources
  final sharedPreferences = await SharedPreferences.getInstance();
  sl.registerLazySingleton(() => sharedPreferences);

  /// Adding the [Dio] instance to the graph to be later used by the local data sources
  sl.registerLazySingleton(
    () {
      final dio = Dio(
        BaseOptions(
          connectTimeout: 12000,
          receiveTimeout: 12000,
          sendTimeout: 2000,
          baseUrl: Url.BaseUrl,
          headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'Accept-Language': ""
          },
          responseType: ResponseType.plain,
        ),
      );
      dio.interceptors.add(
        LogInterceptor(
          responseBody: true,
          requestBody: true,
          responseHeader: true,
          requestHeader: true,
          request: true,
        ),
      );

      return dio;
    },
  );

  /// Adding the [DataConnectionChecker] instance to the graph to be later used by the [NetworkInfoImpl]
  sl.registerLazySingleton(() => DataConnectionChecker());
  sl.registerLazySingleton<NetworkInfo>(() => NetworkInfoImpl(sl()
  ));
  //! Use cases
  sl.registerLazySingleton<SignupInUseCase>(() => SignupInUseCase(signupApi: sl(),));
  sl.registerLazySingleton<SignupVerifyUseCase>(() => SignupVerifyUseCase(signupVerifyApi: sl(),));
  sl.registerLazySingleton<HomeUseCase>(() => HomeUseCase(homeApi: sl(),));
  sl.registerLazySingleton<CardUseCase>(() => CardUseCase(CardApi: sl(),));
  sl.registerLazySingleton<LoginUseCase>(() => LoginUseCase(loginApi: sl(),));
  sl.registerLazySingleton<LogoutUseCase>(() => LogoutUseCase(logoutApi: sl()));
  sl.registerLazySingleton<GetCompanysUseCase>(() => GetCompanysUseCase(getCompanyApi: sl()));
  sl.registerLazySingleton<GetCategoryUseCase>(() => GetCategoryUseCase(getCategories: sl()));
  sl.registerLazySingleton<AddCompanyUseCase>(() => AddCompanyUseCase(addCompanyDataSource: sl()));
  sl.registerLazySingleton<EditCompanyUseCase>(() => EditCompanyUseCase(editCompanyDataSource: sl()));
  sl.registerLazySingleton<CardTypeUseCase>(() => CardTypeUseCase(cardTypeDataSource: sl()));
  sl.registerLazySingleton<AddCardTypeUseCase>(() => AddCardTypeUseCase(addCardTypeDataSource: sl()));
  sl.registerLazySingleton<GetCompaniesUseCase>(() => GetCompaniesUseCase(getCampnies: sl()));
  sl.registerLazySingleton<GetCardsUseCase>(() => GetCardsUseCase(cardDataSource: sl()));
  sl.registerLazySingleton<GetTypesUseCase>(() => GetTypesUseCase(getTypes: sl()));
  sl.registerLazySingleton<AddCardUseCase>(() => AddCardUseCase(addCardDataSource: sl()));
  sl.registerLazySingleton<AddCardExcelUseCase>(() => AddCardExcelUseCase(addCardExcelDataSource: sl()));
  sl.registerLazySingleton<ShopBasketUseCase>(() => ShopBasketUseCase(shopBasketRemoteDataSource: sl()));
  sl.registerLazySingleton<SaleUseCase>(() => SaleUseCase(saleRemoteDataSource: sl()));
  sl.registerLazySingleton<GetInvoiceUseCase>(() => GetInvoiceUseCase(getInvoiceRemoteDataSource: sl()));
  sl.registerLazySingleton<ShopUseCase>(() => ShopUseCase(shopRemoteDataSource: sl()));
  sl.registerLazySingleton<RefundAllInvoiceUseCase>(() => RefundAllInvoiceUseCase(refundAllIncvoiceRemoteDataSource: sl()));
  sl.registerLazySingleton<UserProfileUseCase>(() => UserProfileUseCase(userProfileRemoteDataSource: sl()));
  sl.registerLazySingleton<NotifyUseCase>(() => NotifyUseCase(notifyRemoteDataSource: sl()));
  sl.registerLazySingleton<ChargeUseCase>(() => ChargeUseCase(chargeRemoteDataSource: sl()));
  sl.registerLazySingleton<NotifyRefundUseCase>(() => NotifyRefundUseCase(notifyRefundRemoteDataSource: sl()));
  sl.registerLazySingleton<SeenNotifyUseCase>(() => SeenNotifyUseCase(seenNotifyRemoteDataSource: sl()));


  //data source
  sl.registerLazySingleton<SignupVerifyRemoteDataSource>(
        () => SignupVerifyRemoteDataSourceImpl(dio: sl(), networkInfo: sl()),
  );
  sl.registerLazySingleton<SignupRemoteDataSource>(
    () => SignupRemoteDataSourceImpl(dio: sl(), networkInfo: sl()),
  );
  sl.registerLazySingleton<HomeRemoteDataSource>(
        () => HomeRemoteDataSourceImpl(dio: sl(), networkInfo: sl()),
  );
  sl.registerLazySingleton<CardRemoteDataSource>(
        () => CardRemoteDataSourceImpl(dio: sl(), networkInfo: sl()),
  );
  sl.registerLazySingleton<LoginRemoteDataSource>(
        () => LoginRemoteDataSourceImpl(dio: sl(), networkInfo: sl()),
  );
  sl.registerLazySingleton<LogoutRemoteDataSource>(
        () => LogoutRemoteDataSourceImpl(dio: sl(), networkInfo: sl()),);
    sl.registerLazySingleton<GetCompanyDataSource>(
          () => GetCompanyDataSourceImpl(dio: sl(), networkInfo: sl()),);
  sl.registerLazySingleton<GetCategoriesDataSource>(
        () => GetCategoriesDataSourceImpl(dio: sl(), networkInfo: sl()),);
  sl.registerLazySingleton<GetCardTypeDataSource>(
        () => GetCardTypeDataSourceImpl(dio: sl(), networkInfo: sl()),);
  sl.registerLazySingleton<AddCompanyDataSource>(
        () => AddCompanyDataSourceImpl(dio: sl(), networkInfo: sl()),);
  sl.registerLazySingleton<AddCardTypeDataSource>(
        () => AddCardTypeDataSourceImpl(dio: sl(), networkInfo: sl()),);
  sl.registerLazySingleton<AddCardDataSource>(
        () => AddCardDataSourceImpl(dio: sl(), networkInfo: sl()),);
  sl.registerLazySingleton<GetAllCompaniesDataSource>(
        () => GetAllCompaniesDataSourceImpl(dio: sl(), networkInfo: sl()),);
  sl.registerLazySingleton<GetCardDataSource>(
        () => GetCardDataSourceImpl(dio: sl(), networkInfo: sl()),);
  sl.registerLazySingleton<GetTypesDataSource>(
        () => GetTypesDataSourceImpl(dio: sl(), networkInfo: sl()),);
  sl.registerLazySingleton<AddCardExcelDataSource>(
        () => AddCardExcelDataSourceImpl(dio: sl(), networkInfo: sl()),);
  sl.registerLazySingleton<EditCompanyDataSource>(
        () => EditCompanyDataSourceImpl(dio: sl(), networkInfo: sl()),);
  sl.registerLazySingleton<ShopBasketRemoteDataSource>(
        () => ShopBasketRemoteDataSourceImpl(dio: sl(), networkInfo: sl()),);
  sl.registerLazySingleton<SaleRemoteDataSource>(
        () => SaleRemoteDataSourceImpl(dio: sl(), networkInfo: sl()),);
  sl.registerLazySingleton<GetInvoiceRemoteDataSource>(
        () => GetInvoiceRemoteDataSourceImpl(dio: sl(), networkInfo: sl()),);
  sl.registerLazySingleton<ShopRemoteDataSource>(
        () => ShopRemoteDataSourceImpl(dio: sl(), networkInfo: sl()),);
  sl.registerLazySingleton<RefundAllIncvoiceRemoteDataSource>(
        () => RefundAllIncvoiceRemoteDataSourceImpl(dio: sl(), networkInfo: sl()),);
  sl.registerLazySingleton<UserProfileRemoteDataSource>(
        () => UserProfileRemoteDataSourceImpl(dio: sl(), networkInfo: sl()),);
  sl.registerLazySingleton<NotifyRemoteDataSource>(
        () => NotifyRemoteDataSourceImpl(dio: sl(), networkInfo: sl()),);
  sl.registerLazySingleton<NotifyRefundRemoteDataSource>(
        () => NotifyRefundRemoteDataSourceImpl(dio: sl(), networkInfo: sl()),);
  sl.registerLazySingleton<ChargeRemoteDataSource>(
        () => ChargeRemoteDataSourceImpl(dio: sl(), networkInfo: sl()),);
  sl.registerLazySingleton<SeenNotifyRemoteDataSource>(
        () => SeenNotifyRemoteDataSourceImpl(dio: sl(), networkInfo: sl()),);

  // Bloc
  sl.registerLazySingleton(() => SignupBloc(signupInUseCase: sl(),signupVerifyUseCase: sl()));
  sl.registerLazySingleton(() => HomeBloc(homeUseCase: sl()));
  sl.registerLazySingleton(() => CardBloc(cardUseCase: sl()));
  sl.registerLazySingleton(() => LoginBloc(loginUseCase: sl()));
  sl.registerLazySingleton(() => ProfileBloc(logoutUseCase: sl(),userProfileUseCase: sl(),notifyUseCase: sl(),refundUseCase: sl(),chargeUseCase: sl(),seenNotifyUseCase: sl()));
  sl.registerLazySingleton(() => CardTypeBloc(cardTypeUseCase: sl(),addCardTypeUseCase: sl(),getCompaniesUseCase: sl()));
  sl.registerLazySingleton(() => CompanyBloc(getCompanysUseCase: sl(),addCompanyUseCase: sl(),getCategoriesUseCase: sl(),editCompanyUseCase: sl()));
  sl.registerLazySingleton(() => AddCardBloc(getcardUseCase: sl(),getTypesUseCase: sl(),addCardUseCase: sl(),addCardExcelUseCase: sl()));
  sl.registerLazySingleton(() => ShopBasketBloc(shopBasketUseCase: sl()));
  sl.registerLazySingleton(() => SaleBloc(saleUseCase: sl(),getInvoiceUseCase: sl(),refundAllInvoiceUseCase: sl()));
  sl.registerLazySingleton(() => ShopBloc(shopUseCase: sl()));
}
