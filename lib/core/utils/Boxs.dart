import 'package:hive/hive.dart';
import 'package:rassed_app/core/notifications/notify_database.dart';
import 'package:rassed_app/features/Shopping_basket/cards_basket_model.dart';


class Boxes {
  static Box<CardBasket> getTransactions() =>
      Hive.box<CardBasket>('CardBasket');
  static Box<Notify> getNotify() =>
      Hive.box<Notify>('Notify');
}
