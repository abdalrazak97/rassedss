import 'package:flutter/material.dart';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:rassed_app/features/widget/textauto.dart';
  showMyDialog(BuildContext context,String Msg1,String Msg)  {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return OrientationBuilder(
        builder: (context, orientation) {
          bool isPotrait = MediaQuery.of(context)
              .orientation == Orientation.portrait;
          return Dialog(
            insetPadding: EdgeInsets.symmetric(
              horizontal:isPotrait?ScreenUtil().setWidth(60): ScreenUtil().setWidth(100),

            ),
            child: SingleChildScrollView(
              child: ListBody(
                children: <Widget>[
                  SizedBox(height: ScreenUtil().screenHeight * 0.05,),
                  Container(
                      width: ScreenUtil().screenWidth ,
                      alignment: Alignment.center,
                      child: Text(Msg1.tr().toString(),
                        style: TextStyle(
                          color: Colors.black,
                          fontSize:isPotrait?  ScreenUtil().setSp(20):ScreenUtil().setSp(24),
                          fontWeight: FontWeight.w700,
                          fontFamily: 'Raleway',
                        ),)),
                  SizedBox(height: ScreenUtil().screenHeight * 0.02,),
                  Padding(
                    padding: EdgeInsets.symmetric(
                        horizontal: ScreenUtil().setWidth(34)),
                    child: Container(
                        width: ScreenUtil().screenWidth / 2,
                        alignment: Alignment.center,
                        child: Text(Msg.tr().toString(),
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: Colors.black,
                            fontSize:isPotrait? ScreenUtil().setSp(16):ScreenUtil().setSp(20),
                            fontWeight: FontWeight.w500,
                            fontFamily: 'Raleway',
                          ),)),
                  ),
                  SizedBox(height: ScreenUtil().screenHeight * 0.05,),
                  Row(
                    children: [
                      Expanded(
                        child: Container(
                          height: ScreenUtil().screenHeight * 0.1,
                          child: TextButton(
                              style: ButtonStyle(
                                backgroundColor: MaterialStateProperty.all(Colors.white)),
                              child: Padding(
                                padding: EdgeInsets.symmetric(vertical: ScreenUtil()
                                    .setHeight(12)),
                                child: Container(child:
                                AutoTextgrey(
                                    ScreenUtil().setSp(20), "Resume".tr().toString(),
                                      Colors.blueGrey)),
                              ),
                              onPressed: () {
                                Navigator.pop(context,);
                              }),
                        ),
                      ),
                      Expanded(
                        child: Container(
                          height: ScreenUtil().screenHeight * 0.1,
                          child: TextButton(

                              style: ButtonStyle(
                                  backgroundColor: MaterialStateProperty.all(Colors.blueGrey)),
                              child: Padding(
                                padding: EdgeInsets.symmetric(vertical: ScreenUtil()
                                    .setHeight(12)),
                                child: Container(child:
                                 AutoTextgrey(
                                    ScreenUtil().setSp(20),"Discard".tr().toString(),
                                    Colors.white)),
                              ),
                              onPressed: () {
                                Navigator.pop(context);
                                Navigator.pop(context);
                              }),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          );
        }
        );
      },
    );
  }

