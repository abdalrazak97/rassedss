// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'notify_database.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class NotifyAdapter extends TypeAdapter<Notify> {
  @override
  final int typeId = 1;

  @override
  Notify read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return Notify()
      ..id = fields[0] as int
      ..isAdd = fields[2] as bool;
  }

  @override
  void write(BinaryWriter writer, Notify obj) {
    writer
      ..writeByte(2)
      ..writeByte(0)
      ..write(obj.id)
      ..writeByte(2)
      ..write(obj.isAdd);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is NotifyAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
