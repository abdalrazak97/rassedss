import 'package:hive/hive.dart';

part 'notify_database.g.dart';

@HiveType(typeId: 1)
class Notify extends HiveObject {
  @HiveField(0)
  int id;

  @HiveField(2)
  bool isAdd=false;

}
