
import 'package:dio/dio.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:hive/hive.dart';
import 'package:rassed_app/core/notifications/notify_database.dart';
import 'package:rassed_app/core/utils/Boxs.dart';
import 'package:rassed_app/features/Constants/ApiConstants.dart';
import 'package:rassed_app/features/profile/model/notify_model.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:rassed_app/features/settings/Preference.dart';
import '../../injection.dart';
import '../../main.dart';
import '../network/network_info.dart';

Future<void>microTask() async{
  NetworkInfo networkInfo=sl();
  final Dio dio=sl();
  List<NotifyResponseModel>  notify;
  List<Notify> card=[];

  var box;
  Future addNotify(int id,bool isAdd) async {

    final notify = Notify()
      ..id=id
      ..isAdd=isAdd;

    final box = Boxes.getNotify();
    box.add(notify);


  }
  Future<void> showNotification(int i ,String ss,String name) async {
    const AndroidNotificationDetails androidPlatformChannelSpecifics =
    AndroidNotificationDetails(
        'your channel id', 'your channel name', 'your channel description',
        importance: Importance.max,
        channelShowBadge: false,
        priority: Priority.high,
        ticker: 'ticker');
    const NotificationDetails platformChannelSpecifics =
    NotificationDetails(android: androidPlatformChannelSpecifics);
    await flutterLocalNotificationsPlugin.show(
        i, name,ss , platformChannelSpecifics,
        payload: 'item x');
  }
  while(true){
    await Future.delayed( Duration(minutes:2,seconds:8), () async {


      if(await networkInfo.isConnected) {
        String apiToken=Preferences.getUserToken();
        final result = await dio.get(
          Url.notify,

          options: Options(
            headers: {
              'Authorization': 'Bearer $apiToken',
            },
            followRedirects: false,
            validateStatus: (status) {
              return status < 500;
            },
          ),
        );

        notify = NotifyResponseModelFromJson(result.data);
        for (int i = 0; i < notify.length; i++)
          if (notify[i].is_admin == 1) {
            box = Boxes.getNotify();
            Box<Notify> itemBox = Hive.box<Notify>("Notify");
            bool found= itemBox.values.where((item) => item.id==notify[i].id).isNotEmpty;
                 addNotify(notify[i].id,true);
                 if(!found)
                 await showNotification(i,"${'Want To'.tr().toString()} ${notify[i].type.tr().toString()}","${notify[i].first_name} ${notify[i].last_name} ${notify[i].phone}");

          }
        for (int i = 0; i < notify.length; i++)
          if (notify[i].is_admin == 0) {
            box = Boxes.getNotify();
            Box<Notify> itemBox = Hive.box<Notify>("Notify");
            bool found= itemBox.values.where((item) => item.id==notify[i].id).isNotEmpty;
            addNotify(notify[i].id,true);
            if(!found)
              if(notify[i].type=="Charged"||notify[i].type=='rejected Charged')
              await showNotification(i,"${notify[i].type.tr().toString()}","${'Admin'.tr().toString()} ${notify[i].type=="Charged"?"Accepted".tr().toString():"Rejected".tr().toString()}");
             else
                await showNotification(i,"${notify[i].type.tr().toString()}","${notify[i].type=="accept refund"?"Accepted".tr().toString():"Rejected".tr().toString()}");
          }
      }
    });
  }
}