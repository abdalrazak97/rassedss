import 'dart:io';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:rassed_app/features/home/home.dart';
import 'package:rassed_app/features/signup/continue_with_phone.dart';
import 'package:rassed_app/features/settings/Preference.dart';
import 'package:rxdart/rxdart.dart';
import 'core/notifications/notifications.dart';
import 'core/notifications/notify_database.dart';
import 'features/Shopping_basket/cards_basket_model.dart';
import 'features/signup/verifty_phone.dart';
import 'injection.dart';
import 'package:rassed_app/features/Settings/Globals.dart';
final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
FlutterLocalNotificationsPlugin();
class ReceivedNotification {
  ReceivedNotification({
    this.id,
     this.title,
     this.body,
     this.payload,
  });
  final int id;
  final String title;
  final String body;
  final String payload;
}
String selectedNotificationPayload;
final BehaviorSubject<String> selectNotificationSubject =
BehaviorSubject<String>();
/// Streams are created so that app can respond to notification-related events
/// since the plugin is initialised in the `main` function
final BehaviorSubject<ReceivedNotification> didReceiveLocalNotificationSubject =
BehaviorSubject<ReceivedNotification>();
void main() async{
  WidgetsFlutterBinding.ensureInitialized();
  final NotificationAppLaunchDetails notificationAppLaunchDetails =
  await flutterLocalNotificationsPlugin.getNotificationAppLaunchDetails();



  const AndroidInitializationSettings initializationSettingsAndroid =
  AndroidInitializationSettings('app_icon');

  /// Note: permissions aren't requested here just to demonstrate that can be
  /// done later
  final IOSInitializationSettings initializationSettingsIOS =
  IOSInitializationSettings(
      requestAlertPermission: false,
      requestBadgePermission: false,
      requestSoundPermission: false,
      onDidReceiveLocalNotification:
          (int id, String title, String body, String payload) async {
        didReceiveLocalNotificationSubject.add(ReceivedNotification(
            id: id, title: title, body: body, payload: payload));
      });
  const MacOSInitializationSettings initializationSettingsMacOS =
  MacOSInitializationSettings(
      requestAlertPermission: false,
      requestBadgePermission: false,
      requestSoundPermission: false);
  final InitializationSettings initializationSettings = InitializationSettings(
      android: initializationSettingsAndroid,
      iOS: initializationSettingsIOS,
      macOS: initializationSettingsMacOS);
  await flutterLocalNotificationsPlugin.initialize(initializationSettings,
      onSelectNotification: (String payload) async {
        if (payload != null) {
          debugPrint('notification payload: $payload');
        }
        selectedNotificationPayload = payload;
        selectNotificationSubject.add(payload);
      });
  await init();
  await Hive.initFlutter();
  await Preferences.init();
  Hive.registerAdapter(CardBasketAdapter());
  await Hive.openBox<CardBasket>('CardBasket');
  Hive.registerAdapter(NotifyAdapter());
  await Hive.openBox<Notify>('Notify');

  try {
    userToken = Preferences.getUserToken();
    if(userToken==null)
      userToken='0';
  } catch (e) {}
   isFirstTime = Preferences.getIsFirstTime();
  await EasyLocalization.ensureInitialized();
  var locale;
  if (Platform.localeName.substring(0, 2) == 'ar') {
    locale = Locale('ar', 'AE');
    Preferences.saveLang('ar');
  } else {
    locale = Locale('en', 'US');
    Preferences.saveLang('en');
  }
  try {
    lang = Preferences.getLang();
  } on Exception catch (e) {}
  runApp(
    EasyLocalization(
        supportedLocales: [Locale('en', 'US'), Locale('ar', 'AE')],
        path: 'assets/translations',
        saveLocale: true,
        fallbackLocale:locale ,
        child: MyApp()
    ),);
  await microTask();
}

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    print(" main  $userToken");
    return ScreenUtilInit(
      designSize: Size(360, 690),

      builder: () => MaterialApp(
        title: 'bitaqati',
        theme: ThemeData(
          primarySwatch: Colors.blueGrey,
          visualDensity: VisualDensity.adaptivePlatformDensity,
          textTheme: GoogleFonts.rubikTextTheme(),

        ),
        debugShowCheckedModeBanner: false,
        localizationsDelegates: context.localizationDelegates,
        supportedLocales: context.supportedLocales,
        locale: context.locale,
        home: userToken.length<5?ContinueWithPhone():Home(),

      ),
    );
  }
}