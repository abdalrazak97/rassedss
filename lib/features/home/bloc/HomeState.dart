library HomeState;

import 'package:built_value/built_value.dart';
import 'package:rassed_app/features/home/model/company_model.dart';
import 'package:rassed_app/features/home/model/home_model.dart';

part 'HomeState.g.dart';

abstract class HomeState implements Built<HomeState, HomeStateBuilder> {
  bool get isSuccess;
  bool get isLoading;
  String get Accept_Language;
  String get api_token;
  String get errorMessage;

  List<HomeResponseModel> get homeResponseModel;


  HomeState._();

  factory HomeState([updates(HomeStateBuilder b)]) = _$HomeState;

  factory HomeState.initial() {
    return HomeState((b) => b
      ..isLoading = false
      ..isSuccess = false
      ..api_token =''
      ..errorMessage = ''
      ..Accept_Language = ''
      ..homeResponseModel=[]
    );
  }
}
