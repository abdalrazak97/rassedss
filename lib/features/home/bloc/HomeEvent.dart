import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class HomeEvents extends Equatable {
  @override
  List<Object> get props => [];
}

class HomeEvent extends HomeEvents {}

class ChangeApiToken extends HomeEvents{
  final api_token;
  ChangeApiToken(this.api_token);
}

class ResetError extends HomeEvents {}

class Enable extends HomeEvents {}

class Disable extends HomeEvents {}
