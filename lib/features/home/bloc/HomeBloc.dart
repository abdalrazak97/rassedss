import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:rassed_app/features/home/bloc/HomeEvent.dart';
import 'package:rassed_app/features/home/model/home_model.dart';
import 'package:rassed_app/features/home/usecase/home_usecase.dart';

import 'HomeState.dart';


const String SERVER_FAILURE_MESSAGE = 'Server Failure';
const String CACHE_FAILURE_MESSAGE = 'Cache Failure';
const String INVALID_INPUT_FAILURE_MESSAGE =
    'Invalid Input - The number must be a positive integer or zero.';

class HomeBloc extends Bloc<HomeEvents, HomeState> {
  final HomeUseCase homeUseCase;

  HomeBloc({this.homeUseCase}) : super(HomeState.initial());

  void onChangeApiToken(String api_token){
    add( ChangeApiToken(api_token));
  }
  void onHomeEvent(){
    add( HomeEvent());
  }



  @override
  Stream<HomeState> mapEventToState(HomeEvents event) async* {
    if (event is HomeEvent) {
      yield state.rebuild((b) => b
        ..isLoading = true
        ..isSuccess = false

      );

      final result = await homeUseCase(
          homeParams(
              api_token: state.api_token
          )
      );

      yield* result.fold((l) async* {

        yield state.rebuild((b) => b
          ..isLoading = false
          ..isSuccess = false
          ..errorMessage = l);
      }, (r) async* {
        print('sucesss');
        yield state.rebuild((b) => b
          ..isLoading = false
          ..isSuccess = true
          ..errorMessage = ''
          ..homeResponseModel = r);
      });

    }

    else if (event is ChangeApiToken) {
      print('1111');
      yield state.rebuild((b) => b..api_token = event.api_token);

    }

  }
}
