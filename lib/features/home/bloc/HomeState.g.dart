// GENERATED CODE - DO NOT MODIFY BY HAND

part of HomeState;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$HomeState extends HomeState {
  @override
  final bool isSuccess;
  @override
  final bool isLoading;
  @override
  final String Accept_Language;
  @override
  final String api_token;
  @override
  final String errorMessage;
  @override
  final List<HomeResponseModel> homeResponseModel;

  factory _$HomeState([void Function(HomeStateBuilder) updates]) =>
      (new HomeStateBuilder()..update(updates)).build();

  _$HomeState._(
      {this.isSuccess,
      this.isLoading,
      this.Accept_Language,
      this.api_token,
      this.errorMessage,
      this.homeResponseModel})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(isSuccess, 'HomeState', 'isSuccess');
    BuiltValueNullFieldError.checkNotNull(isLoading, 'HomeState', 'isLoading');
    BuiltValueNullFieldError.checkNotNull(
        Accept_Language, 'HomeState', 'Accept_Language');
    BuiltValueNullFieldError.checkNotNull(api_token, 'HomeState', 'api_token');
    BuiltValueNullFieldError.checkNotNull(
        errorMessage, 'HomeState', 'errorMessage');
    BuiltValueNullFieldError.checkNotNull(
        homeResponseModel, 'HomeState', 'homeResponseModel');
  }

  @override
  HomeState rebuild(void Function(HomeStateBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  HomeStateBuilder toBuilder() => new HomeStateBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is HomeState &&
        isSuccess == other.isSuccess &&
        isLoading == other.isLoading &&
        Accept_Language == other.Accept_Language &&
        api_token == other.api_token &&
        errorMessage == other.errorMessage &&
        homeResponseModel == other.homeResponseModel;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc($jc($jc(0, isSuccess.hashCode), isLoading.hashCode),
                    Accept_Language.hashCode),
                api_token.hashCode),
            errorMessage.hashCode),
        homeResponseModel.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('HomeState')
          ..add('isSuccess', isSuccess)
          ..add('isLoading', isLoading)
          ..add('Accept_Language', Accept_Language)
          ..add('api_token', api_token)
          ..add('errorMessage', errorMessage)
          ..add('homeResponseModel', homeResponseModel))
        .toString();
  }
}

class HomeStateBuilder implements Builder<HomeState, HomeStateBuilder> {
  _$HomeState _$v;

  bool _isSuccess;
  bool get isSuccess => _$this._isSuccess;
  set isSuccess(bool isSuccess) => _$this._isSuccess = isSuccess;

  bool _isLoading;
  bool get isLoading => _$this._isLoading;
  set isLoading(bool isLoading) => _$this._isLoading = isLoading;

  String _Accept_Language;
  String get Accept_Language => _$this._Accept_Language;
  set Accept_Language(String Accept_Language) =>
      _$this._Accept_Language = Accept_Language;

  String _api_token;
  String get api_token => _$this._api_token;
  set api_token(String api_token) => _$this._api_token = api_token;

  String _errorMessage;
  String get errorMessage => _$this._errorMessage;
  set errorMessage(String errorMessage) => _$this._errorMessage = errorMessage;

  List<HomeResponseModel> _homeResponseModel;
  List<HomeResponseModel> get homeResponseModel => _$this._homeResponseModel;
  set homeResponseModel(List<HomeResponseModel> homeResponseModel) =>
      _$this._homeResponseModel = homeResponseModel;

  HomeStateBuilder();

  HomeStateBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _isSuccess = $v.isSuccess;
      _isLoading = $v.isLoading;
      _Accept_Language = $v.Accept_Language;
      _api_token = $v.api_token;
      _errorMessage = $v.errorMessage;
      _homeResponseModel = $v.homeResponseModel;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(HomeState other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$HomeState;
  }

  @override
  void update(void Function(HomeStateBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$HomeState build() {
    final _$result = _$v ??
        new _$HomeState._(
            isSuccess: BuiltValueNullFieldError.checkNotNull(
                isSuccess, 'HomeState', 'isSuccess'),
            isLoading: BuiltValueNullFieldError.checkNotNull(
                isLoading, 'HomeState', 'isLoading'),
            Accept_Language: BuiltValueNullFieldError.checkNotNull(
                Accept_Language, 'HomeState', 'Accept_Language'),
            api_token: BuiltValueNullFieldError.checkNotNull(
                api_token, 'HomeState', 'api_token'),
            errorMessage: BuiltValueNullFieldError.checkNotNull(
                errorMessage, 'HomeState', 'errorMessage'),
            homeResponseModel: BuiltValueNullFieldError.checkNotNull(
                homeResponseModel, 'HomeState', 'homeResponseModel'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
