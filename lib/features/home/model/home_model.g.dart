// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'home_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

HomeResponseModel _$HomeResponseModelFromJson(Map<String, dynamic> json) {
  return HomeResponseModel(
    id: json['id'] as int,
    name: json['name'] as String,
    company: (json['company'] as List)
        ?.map((e) => e == null
            ? null
            : CompanyResponseModel.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    success: json['success'] as bool,
    message: json['message'] as String,
  );
}

Map<String, dynamic> _$HomeResponseModelToJson(HomeResponseModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'company': instance.company,
      'success': instance.success,
      'message': instance.message,
    };
