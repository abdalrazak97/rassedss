import 'dart:convert';

import 'package:json_annotation/json_annotation.dart';
import 'package:rassed_app/features/home/model/company_model.dart';


part 'home_model.g.dart';

@JsonSerializable()
class HomeResponseModel {
  final int id;
  final String name;
  List<CompanyResponseModel> company;
  final bool success;
  final String message;


  Map<String, dynamic> toJson() => _$HomeResponseModelToJson(this);
  factory HomeResponseModel.fromJson(Map<String, dynamic> json) =>
      _$HomeResponseModelFromJson(json);

  HomeResponseModel({
      this.id, this.name, this.company, this.success, this.message});
}
List<HomeResponseModel> HomeResponseModelFromJson(String str) =>
    List<HomeResponseModel>.from(json.decode(str).map((x) => HomeResponseModel.fromJson(x)));
String HomeResponseModelToJson(List<HomeResponseModel> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));