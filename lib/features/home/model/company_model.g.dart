// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'company_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CompanyResponseModel _$CompanyResponseModelFromJson(Map<String, dynamic> json) {
  return CompanyResponseModel(
    id: json['id'] as int,
    category_id: json['category_id'] as int,
    name: json['name'] as String,
    image: json['image'] as String,
    success: json['success'] as bool,
    message: json['message'] as String,
  );
}

Map<String, dynamic> _$CompanyResponseModelToJson(
        CompanyResponseModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'category_id': instance.category_id,
      'name': instance.name,
      'image': instance.image,
      'success': instance.success,
      'message': instance.message,
    };
