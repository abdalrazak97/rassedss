import 'package:json_annotation/json_annotation.dart';


part 'company_model.g.dart';

@JsonSerializable()
class CompanyResponseModel {
  final int id;
  final int category_id;
  final String name;
  final String image;
  final bool success;
  final String message;


  Map<String, dynamic> toJson() => _$CompanyResponseModelToJson(this);
  factory CompanyResponseModel.fromJson(Map<String, dynamic> json) =>
      _$CompanyResponseModelFromJson(json);

  CompanyResponseModel({this.id, this.category_id, this.name, this.image,
      this.success, this.message});
}
