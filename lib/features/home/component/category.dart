import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'dart:ui' as ui;
import 'package:easy_localization/easy_localization.dart';
import 'package:google_fonts_arabic/fonts.dart';
class Category extends StatefulWidget {
  final int index1;
  final String name;

  Category({this.index1,this.name});

  @override
  _CategoryState createState() => _CategoryState();
}

class _CategoryState extends State<Category> {

  int selectedType =1;
  @override
  void initState() {

    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Container(
      width: ScreenUtil().screenWidth*0.4,

      padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(2)),
      child:  Stack(
        textDirection: ui.TextDirection.rtl,
          alignment: AlignmentDirectional.center,
          children:[
            Container(
              width: ScreenUtil().screenWidth * 0.4,
              height: ScreenUtil().screenHeight * 0.06,
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(5.0),
                  border: Border.all(
                      width: 2,
                      color: Colors.blueGrey,
                  )
              ),
              child: Center(


                child: Text(widget.name.tr().toString(),style: TextStyle(color:Colors.blueGrey,fontFamily: ArabicFonts.Cairo,
                    package: 'google_fonts_arabic',fontSize: ScreenUtil().setSp(14)),),
              ),
            ),
          ]
      ),
    );
  }
}
