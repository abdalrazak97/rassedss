import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class ValueItemCards extends StatelessWidget {

  final String image;
  final int id;
  final int selected;
  ValueItemCards({this.image,this.id,this.selected});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding:  EdgeInsets.all(0),
      child: Container(
              width: ScreenUtil().screenWidth * 0.4,
              height: ScreenUtil().screenHeight * 0.2,
              decoration: BoxDecoration(
                color: Colors.deepPurple,
                borderRadius: BorderRadius.circular(20),
              ),
              child: ClipRRect(
                  borderRadius: BorderRadius.circular(20.0),
                  child: Image.network(image,fit: BoxFit.cover)),),

    );
  }



}
