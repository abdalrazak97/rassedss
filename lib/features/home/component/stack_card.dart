import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class SpecialCard extends StatelessWidget {
  final int index1;
  final String image;

  SpecialCard({this.index1,this.image});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: ScreenUtil().screenWidth,
      padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(20)),
      child:  Stack(
          alignment: AlignmentDirectional.center,
          children:[
            Transform.translate(
              offset: Offset(0,ScreenUtil().setHeight(20)),
              child: Container(
                width: ScreenUtil().screenWidth * 0.9,
                height: ScreenUtil().screenHeight * 0.2,
                decoration: BoxDecoration(
                  color: Colors.blueGrey.shade200,
                  borderRadius: BorderRadius.circular(20),
                ),
                child: ClipRRect(
                    borderRadius: BorderRadius.circular(20.0),
                    child: Image.network('https://app.rasseed.com/files/stcf425d5.png',fit: BoxFit.cover)),),
            ),
          ]
      ),
    );
  }
}
