import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:rassed_app/features/Constants/ApiConstants.dart';
import 'package:rassed_app/features/home/model/home_model.dart';
class GridHome extends StatefulWidget {
   List<HomeResponseModel> homeResponseModel;


   GridHome(this.homeResponseModel);

  @override
  _GridHomeState createState() => _GridHomeState();
}

class _GridHomeState extends State<GridHome> {
  @override
  void initState() {

    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    for(int i=0;i<widget.homeResponseModel.length;i++)
      for(int j=0;j<widget.homeResponseModel[i].company.length;j++)
        print("ss ${widget.homeResponseModel[i].id}");

    return   Column(
      children: [
        for(int i=0;i<widget.homeResponseModel.length;i++)
          for(int j=0;j<widget.homeResponseModel[i].company.length;j+2)
             widget.homeResponseModel[i].id==0?Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Container(
                      height: ScreenUtil().screenHeight * 0.3,
                      width: ScreenUtil().screenWidth * 0.5,
                      padding: EdgeInsets.only(left: 16, right: 16, top: 0,),
                      child:
                      Image.network("${Url.BaseUrl}${widget.homeResponseModel[i]
                          .company[j].image.toString()}"),
                    ),
                  ]
              ):Container(),

      ],
    );
  }
}
