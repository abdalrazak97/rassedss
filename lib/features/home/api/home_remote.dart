import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:rassed_app/features/Constants/ApiConstants.dart';
import 'package:dartz/dartz.dart';
import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:dio/dio.dart';
import 'package:meta/meta.dart';
import 'package:rassed_app/features/home/model/home_model.dart';
import 'package:rassed_app/features/home/usecase/home_usecase.dart';


abstract class HomeRemoteDataSource {
  Future<Either<String, List<HomeResponseModel>>> home(homeParams params);
}

class HomeRemoteDataSourceImpl extends HomeRemoteDataSource {
  final Dio dio;
  final DataConnectionChecker networkInfo;

  HomeRemoteDataSourceImpl({@required this.dio, @required this.networkInfo});

  @override
  Future<Either<String, List<HomeResponseModel>>> home(homeParams params) async {
    List<HomeResponseModel> home;
    print("params ${params.api_token}");
    if (await networkInfo.hasConnection) {
      try {
        final result = await dio.post(
          Url.home ,

          options: Options(
            headers:{
              'Authorization': 'Bearer ${params.api_token}',
               } ,
            followRedirects: false,
            validateStatus: (status) {
              return status < 500;
            },
          ),
        );
        print('helllllllllllllllllllo');
        print(result.data);
        home=HomeResponseModelFromJson(result.data);

        return Right(home);
      } on DioError catch (ex) {
        if (ex.type == DioErrorType.connectTimeout) {
          return Left(Er.NetworkError);
        } else if (ex.type == DioErrorType.receiveTimeout) {
          return Left(Er.NetworkError);
        }
        else if (home == null) return Left(Er.Error);
      }
    } else {
      return Left(Er.NetworkError);
    }
  }
}
