import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:rassed_app/features/Constants/usecase.dart';
import 'package:rassed_app/features/home/api/home_remote.dart';
import 'package:rassed_app/features/home/model/home_model.dart';



class HomeUseCase extends UseCase<List<HomeResponseModel>, homeParams> {
  final HomeRemoteDataSource homeApi;

  HomeUseCase({this.homeApi});

  @override
  Future<Either<String, List<HomeResponseModel>>> call(homeParams params) async {
    return await homeApi.home(params);

  }
}

class homeParams extends Equatable {
  final String api_token;

  homeParams(
      {@required this.api_token,});

  @override
  // TODO: implement props
  List<Object> get props => [api_token];
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['api_token'] = this.api_token;

    return data;
  }
}
