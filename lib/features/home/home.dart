import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts_arabic/fonts.dart';
import 'package:rassed_app/features/Constants/ApiConstants.dart';
import 'package:rassed_app/features/card_details/card_details.dart';
import 'package:rassed_app/features/home/bloc/HomeBloc.dart';
import 'package:rassed_app/features/home/bloc/HomeState.dart';
import 'package:rassed_app/features/home/component/category.dart';
import 'dart:ui' as ui;
import 'package:easy_localization/easy_localization.dart';
import 'package:rassed_app/features/home/component/stack_card.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:rassed_app/features/profile/bloc/ProfileBloc.dart';
import 'package:rassed_app/features/profile/bloc/ProfileState.dart';
import 'package:rassed_app/features/profile/profile.dart';
import 'package:rassed_app/features/settings/Globals.dart';
import 'package:rassed_app/features/settings/Preference.dart';
import 'package:rassed_app/features/widget/drawer.dart';
import '../../injection.dart';
class Home extends StatefulWidget {
  const Home({Key key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  HomeBloc bloc = sl<HomeBloc>();
  ProfileBloc blocProfile = sl<ProfileBloc>();
  int selectedType=1;

   String Token ;
  @override
  void initState() {
    Token=Preferences.getUserToken();
    bloc.onChangeApiToken(Token);
    bloc.onHomeEvent();
    blocProfile.onChangeApiToken(Token);
    blocProfile.onUserProfile();
    super.initState();
  }
  @override
  Widget build(BuildContext context) {

    return Scaffold(
      key:_scaffoldKey ,
      drawer: drawer(),
      backgroundColor: Colors.grey.shade200,
      appBar: AppBar(
        elevation: 0,
        actions: [
          SizedBox(width: ScreenUtil().setWidth(40),
              child: Center(child: IconButton(icon:Icon(Icons.person_rounded,),
                onPressed: (){
                Navigator.push(context,  MaterialPageRoute(builder: (context) => Profile()),);
              },))),
              ],
        leading: SizedBox(
          width: ScreenUtil().setWidth(30),
          child: InkWell(child: Icon(Icons.menu),
          onTap: (){
            _scaffoldKey.currentState.openDrawer();
          },),
        ),
        backgroundColor: Colors.blueGrey,
        centerTitle: true,
        title: Text('Home'.tr().toString(),
          textDirection: ui.TextDirection.rtl,
          style: TextStyle(
            fontFamily: ArabicFonts.Cairo,
            package: 'google_fonts_arabic',
            fontSize: 18,
          ),),
      ),
      body: SafeArea(
        child:BlocBuilder<HomeBloc,HomeState>(
           bloc: bloc,
           builder: (context, state) {
             return BlocBuilder<ProfileBloc, ProfileState>(
                 bloc: blocProfile,
                 builder: (context, state2) {
                   return SingleChildScrollView(
                     child: Column(
                       crossAxisAlignment: CrossAxisAlignment.end,
                       mainAxisAlignment: MainAxisAlignment.end,
                       children: [
                         Stack(
                           children: [
                             Container(
                               width: ScreenUtil().screenWidth,
                               height: ScreenUtil().screenHeight * 0.2,
                               decoration: BoxDecoration(
                                   color: Colors.blueGrey,
                                   borderRadius: BorderRadius.only(
                                       bottomLeft: Radius.circular(20),
                                       bottomRight: Radius.circular(20))
                               ),
                             ),
                             SizedBox(
                               height: ScreenUtil().screenHeight * 0.26,
                               child: ListView.builder(
                                   scrollDirection: Axis.horizontal,
                                   physics: BouncingScrollPhysics(),
                                   itemCount: 4,
                                   itemBuilder: (context, index) {
                                     return SpecialCard(
                                       index1: index + 1,
                                       image: 'ss',
                                     );
                                   }),
                             ),
                           ],
                         ),
                         Stack(
                           textDirection: ui.TextDirection.rtl,
                           children: [
                             SizedBox(
                               height: ScreenUtil().screenHeight * 0.1,
                               child: ListView.builder(
                                   scrollDirection: Axis.horizontal,
                                   dragStartBehavior: DragStartBehavior.down,
                                   physics: BouncingScrollPhysics(),
                                   itemCount: state.homeResponseModel.length,
                                   itemBuilder: (context, index) {
                                     return InkWell(
                                       onTap: () {
                                         setState(() {
                                           if (index == 0)
                                             category = 0;
                                           else
                                             category = state.homeResponseModel
                                                 .length - index + 1;
                                           print(category);
                                         });
                                       },
                                       child: Category(
                                         index1: state.homeResponseModel
                                             .length - index,
                                         name: state.homeResponseModel[state
                                             .homeResponseModel.length - index -
                                             1].name,
                                       ),
                                     );
                                   }),
                             ),
                           ],
                         ),
                         if(state.isSuccess)
                           Stack(
                               children: [
                                 Column(
                                   children: [

                                     for(int i = 0; i <
                                         state.homeResponseModel.length; i++)
                                       for(int j = 0; j <
                                           state.homeResponseModel[i].company
                                               .length; j = j + 2)
                                         state.homeResponseModel[i].id ==
                                             category ? Row(
                                             textDirection: ui.TextDirection
                                                 .rtl,
                                             mainAxisAlignment: MainAxisAlignment
                                                 .spaceAround,
                                             children: [
                                               InkWell(
                                                 onTap: () {
                                                   print(state
                                                       .homeResponseModel[i]
                                                       .company[j].image);
                                                   Navigator.push(context,
                                                     MaterialPageRoute(
                                                         builder: (context) =>
                                                             CardDetails(state
                                                                 .homeResponseModel[i]
                                                                 .company[j]
                                                                 .id)),);
                                                 },
                                                 child: Container(
                                                   height: ScreenUtil()
                                                       .screenHeight * 0.2,
                                                   width: ScreenUtil()
                                                       .screenWidth * 0.45,
                                                   margin: EdgeInsets.all(8.0),
                                                   decoration: BoxDecoration(
                                                     color: Colors.blueGrey
                                                         .shade200,
                                                     borderRadius: BorderRadius
                                                         .circular(20),),
                                                   child:
                                                   ClipRRect(
                                                     borderRadius: BorderRadius
                                                         .circular(20.0),
                                                     child: Image.network(
                                                         "${Url.BaseUrl}${state
                                                             .homeResponseModel[i]
                                                             .company[j].image
                                                             .toString()}",
                                                         fit: BoxFit.cover),
                                                   ),
                                                 ),
                                               ),
                                               j + 1 <
                                                   state.homeResponseModel[i]
                                                       .company.length ? Center(
                                                 child: InkWell(
                                                   onTap: () {
                                                     Navigator.push(context,
                                                       MaterialPageRoute(
                                                           builder: (context) =>
                                                               CardDetails(state
                                                                   .homeResponseModel[i]
                                                                   .company[j +
                                                                   1].id)),);
                                                   },
                                                   child: Container(
                                                     margin: EdgeInsets.all(
                                                         8.0),
                                                     decoration: BoxDecoration(
                                                       color: Colors.blueGrey
                                                           .shade200,
                                                       borderRadius: BorderRadius
                                                           .circular(20),),
                                                     height: ScreenUtil()
                                                         .screenHeight * 0.2,
                                                     width: ScreenUtil()
                                                         .screenWidth * 0.45,
                                                     child:
                                                     ClipRRect(
                                                       borderRadius: BorderRadius
                                                           .circular(20.0),
                                                       child: Image.network(
                                                           "${Url
                                                               .BaseUrl}${state
                                                               .homeResponseModel[i]
                                                               .company[j + 1]
                                                               .image
                                                               .toString()}",
                                                           fit: BoxFit.cover),),
                                                   ),
                                                 ),
                                               ) : SizedBox(width: ScreenUtil()
                                                   .screenWidth * 0.5,),
                                             ]
                                         ) : Container()
                                   ],
                                 ),
                               ]
                           ),
                         if(state.isLoading)
                           Center(
                             child: CircularProgressIndicator(),
                           ),
                       ],
                     ),
                   );
                 }
             );
           }
        ),
      ),
    );
  }
}
/*
*
* ListView.builder(
                         reverse: true,
                           scrollDirection: Axis.horizontal,
                           physics: BouncingScrollPhysics(),
                           itemCount: state.homeResponseModel.length-1,
                           itemBuilder: (context, index) {
                             return Category(
                               index1: index + 1,
                               name: state.homeResponseModel[index].name,
                             );
                           }),
final
* */
/*
  box = Boxes.getTransactions();
final carddd=   box.values.where((item) => item.cardId == card.card_type_id).forEach((item) => print('All First Value Data Showing Result'));
*/