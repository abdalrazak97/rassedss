import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts_arabic/fonts.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:rassed_app/features/home/home.dart';
import 'package:rassed_app/features/signup/numeric_pad.dart';
import 'package:rassed_app/features/settings/Globals.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import '../../injection.dart';
import 'bloc/SignBloc.dart';
import 'bloc/signup_state.dart';
import 'model/sign_verify_model/Signup_verify_model.dart';
import 'dart:ui' as ui;
import 'package:easy_localization/easy_localization.dart';
class VerifyPhone extends StatefulWidget {
  final String phoneNumber;

  VerifyPhone({@required this.phoneNumber});

  @override
  _VerifyPhoneState createState() => _VerifyPhoneState();
}

class _VerifyPhoneState extends State<VerifyPhone> {
  String code = "";
  SignupBloc bloc = sl<SignupBloc>();
  SignupVerifyResponseModel signupVerifyResponseModel;
  bool requset=false;
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: GestureDetector(
          onTap: () {
            Navigator.pop(context);
          },
          child: Icon(
            Icons.arrow_back,
            size: 30,
            color: Colors.black,
          ),
        ),
        title: Text(
          "Verify phone".tr().toString(),
          style: TextStyle(
            fontFamily: ArabicFonts.Cairo,
            package: 'google_fonts_arabic',
            fontSize: ScreenUtil().setSp(18),
            fontWeight: FontWeight.bold,
            color: Colors.black,
          ),
        ),
        backgroundColor: Colors.white,
        elevation: 0,
        centerTitle: true,
        textTheme: Theme.of(context).textTheme,
      ),
      body: SafeArea(
        child: BlocListener<SignupBloc, SignupState>(
          bloc: bloc,
          listener: (context, state) {
            if(state.isSuccess && requset){
              setState(() {
                requset=false;
              });
              if (state.signupVerifyResponseModel.api_token != null && signupVerifyResponseModel.api_token.length>5)
                  Navigator.pushReplacement(context,
                    MaterialPageRoute(builder: (context) => Home()),);
              else{
                setState(() {
                  requset = false;
                });
                AwesomeDialog(
                    width: ScreenUtil().setWidth(400),
                    context: context,
                    buttonsTextStyle: TextStyle(
                        fontSize: ScreenUtil().setSp(20), color: Colors.white),
                    dialogType: DialogType.ERROR,
                    animType: AnimType.BOTTOMSLIDE,
                    title: "Alert".toString(),
                    desc: "${state.signupResponseModel.message.toString()}"
                        .toString(),
                    btnOkOnPress: () {},
                    btnOkColor: Colors.blueGrey)
                  ..show();
              }
            }

          },
          child: BlocBuilder<SignupBloc, SignupState>(
              bloc: bloc,
              builder: (context, state) {
                return ModalProgressHUD(
                  inAsyncCall: state.isLoading,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: <Widget>[
                      Expanded(
                        child: Container(
                          color: Colors.white,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Padding(
                                padding: EdgeInsets.symmetric(vertical: 14,horizontal: 10),
                                child: Row(
                                  children: [
                                    Text(
                                      "Code is sent to ".tr().toString(),

                                      style: TextStyle(

                                        fontSize: ScreenUtil().setSp(18),
                                        color: Color(0xFF818181),
                                      ),
                                    ),
                                    Text(
                                      widget.phoneNumber,
                                      textDirection: ui.TextDirection.ltr,
                                      style: TextStyle(

                                        fontSize: ScreenUtil().setSp(18),
                                        color: Color(0xFF818181),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              Expanded(
                                child: Row(
                                textDirection: ui.TextDirection.ltr,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    buildCodeNumberBox(code.length > 0
                                        ? code.substring(0, 1)
                                        : ""),
                                    buildCodeNumberBox(code.length > 1
                                        ? code.substring(1, 2)
                                        : ""),
                                    buildCodeNumberBox(code.length > 2
                                        ? code.substring(2, 3)
                                        : ""),
                                    buildCodeNumberBox(code.length > 3
                                        ? code.substring(3, 4)
                                        : ""),
                                    buildCodeNumberBox(code.length > 4
                                        ? code.substring(4, 5)
                                        : ""),
                                    buildCodeNumberBox(code.length > 5
                                        ? code.substring(5, 6)
                                        : ""),
                                  ],
                                ),
                              ),

                            ],
                          ),
                        ),
                      ),
                      Container(
                        height: MediaQuery.of(context).size.height * 0.13,
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.all(
                            Radius.circular(25),
                          ),
                        ),
                        child: Padding(
                          padding: EdgeInsets.all(16),
                          child: Row(
                            children: <Widget>[
                              Container(
                                width: ScreenUtil().screenWidth * 0.9,
                                height: ScreenUtil().screenHeight * 0.2,
                                child: GestureDetector(
                                  onTap: () {
                                    bloc.onChangeCode(code);
                                    bloc.onChangePhone(widget.phoneNumber);
                                    bloc.onSignupVerify();
                                    setState(() {
                                      requset=true;
                                    });
                                  },
                                  child: Container(
                                    decoration: BoxDecoration(
                                      color: Colors.blueGrey,
                                      borderRadius: BorderRadius.all(
                                        Radius.circular(15),
                                      ),
                                    ),
                                    child: Center(
                                      child: Text(
                                        "Verify and Create Account".tr().toString(),
                                        style: TextStyle(
                                          fontSize: ScreenUtil().setSp(18),
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      NumericPad(
                        onNumberSelected: (value) {
                          print(value);
                          setState(() {
                            if (value != -1) {
                              if (code.length < 6) {
                                code = code + value.toString();
                              }
                            } else {
                              code = code.substring(0, code.length - 1);
                            }
                            print(code);
                          });
                        },
                      ),
                    ],
                  ),
                );

              }),
        ),
      ),
    );
  }

  Widget buildCodeNumberBox(String codeNumber) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 8),
      child: SizedBox(
        width: ScreenUtil().screenWidth * 0.1,
        height: ScreenUtil().screenHeight * 0.05,
        child: Container(
          decoration: BoxDecoration(
            color: Color(0xFFF6F5FA),
            borderRadius: BorderRadius.all(
              Radius.circular(15),
            ),
            boxShadow: <BoxShadow>[
              BoxShadow(
                  color: Colors.black26,
                  blurRadius: 25.0,
                  spreadRadius: 1,
                  offset: Offset(0.0, 0.75))
            ],
          ),
          child: Center(
            child: Text(
              codeNumber,
              style: TextStyle(
                fontSize: ScreenUtil().setSp(20),
                fontWeight: FontWeight.bold,
                color: Color(0xFF1F1F1F),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
