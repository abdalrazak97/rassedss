import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:rassed_app/features/Constants/ApiConstants.dart';
import 'package:dartz/dartz.dart';
import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:dio/dio.dart';
import 'package:meta/meta.dart';
import 'package:rassed_app/features/signup/model/sign_model/Signup_model.dart';
import 'package:rassed_app/features/signup/usecase/Signup_usecase.dart';


abstract class SignupRemoteDataSource {
  Future<Either<String, SigupResponseModel>> Signup(SignupParams params);
}

class SignupRemoteDataSourceImpl extends SignupRemoteDataSource {
  final Dio dio;
  final DataConnectionChecker networkInfo;

  SignupRemoteDataSourceImpl({@required this.dio, @required this.networkInfo});

  @override
  Future<Either<String, SigupResponseModel>> Signup(SignupParams params) async {
    SigupResponseModel user;

    if (await networkInfo.hasConnection) {
      try {
        final result = await dio.post(
          Url.Signup ,
          data: params.toJson(),
          options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status < 500;
            },
          ),
        );
        user = SigupResponseModel.fromJson(json.decode(result.data));
        return Right(user);
      } on DioError catch (ex) {
        if (ex.type == DioErrorType.connectTimeout) {
          return Left(Er.NetworkError);
        } else if (ex.type == DioErrorType.receiveTimeout) {
          return Left(Er.NetworkError);
        } else if (user != null)
          return Left(user.message);
        else if (user == null) return Left(Er.Error);
      }
    } else {
      return Left(Er.NetworkError);
    }
  }
}
