import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:rassed_app/core/notifications/notifications.dart';
import 'package:rassed_app/features/Constants/ApiConstants.dart';
import 'package:dartz/dartz.dart';
import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:dio/dio.dart';
import 'package:meta/meta.dart';
import 'package:rassed_app/features/settings/Globals.dart';
import 'package:rassed_app/features/signup/model/sign_verify_model/Signup_verify_model.dart';
import 'package:rassed_app/features/signup/usecase/Signup_verify_usecase.dart';
import 'package:rassed_app/features/settings/Preference.dart';

abstract class SignupVerifyRemoteDataSource {
  Future<Either<String, SignupVerifyResponseModel>> SignupVerify(SingupVerifyParams params);
}

class SignupVerifyRemoteDataSourceImpl extends SignupVerifyRemoteDataSource {
  final Dio dio;
  final DataConnectionChecker networkInfo;
  SignupVerifyRemoteDataSourceImpl({@required this.dio, @required this.networkInfo});

  @override
  Future<Either<String, SignupVerifyResponseModel>> SignupVerify(SingupVerifyParams params) async {
    SignupVerifyResponseModel user;

    if (await networkInfo.hasConnection) {
      try {
        print(params.code);
        print(params.phone);
        final result = await dio.post(
          Url.verify,
          data: params.toJson(),
          options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status < 500;
            },
          ),
        );
        //tokenSave
        print("**********************");
        print(params.code);
        print(params.phone);
        print("**********************");
        user = SignupVerifyResponseModel.fromJson(json.decode(result.data));
        role=user.role;
        print(result.data);
        print("**********ttokken************");
        print(user.api_token);
        Preferences.saveUserToken(user.api_token.toString());
        return Right(user);
      } on DioError catch (ex) {
        if (ex.type == DioErrorType.connectTimeout) {
          return Left(Er.NetworkError);
        } else if (ex.type == DioErrorType.receiveTimeout) {
          return Left(Er.NetworkError);
        } else if (user != null)
          return Left(user.message);
        else if (user == null) return Left(Er.Error);
      }
    } else {
      return Left(Er.NetworkError);
    }
  }
}
