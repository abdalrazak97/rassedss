// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Signup_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SigupResponseModel _$SigupResponseModelFromJson(Map<String, dynamic> json) {
  return SigupResponseModel(
    code: json['code'] as int,
    success: json['success'] as bool,
    message: json['message'] as String,
    errors: (json['errors'] as List)
        ?.map((e) =>
            e == null ? null : ErrorModel.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$SigupResponseModelToJson(SigupResponseModel instance) =>
    <String, dynamic>{
      'code': instance.code,
      'success': instance.success,
      'message': instance.message,
      'errors': instance.errors,
    };
