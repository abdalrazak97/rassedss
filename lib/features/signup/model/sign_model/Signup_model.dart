import 'package:json_annotation/json_annotation.dart';


import 'error_model.dart';

part 'Signup_model.g.dart';

@JsonSerializable()
class SigupResponseModel {
  final int code;
  final bool success;
  final String message;
  final List<ErrorModel> errors;

  Map<String, dynamic> toJson() => _$SigupResponseModelToJson(this);
  factory SigupResponseModel.fromJson(Map<String, dynamic> json) =>
      _$SigupResponseModelFromJson(json);

  SigupResponseModel(
      {this.code, this.success, this.message, this.errors});
}
