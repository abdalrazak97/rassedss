import 'package:json_annotation/json_annotation.dart';

part 'error_model.g.dart';

@JsonSerializable()
class ErrorModel {
  final String message;
  final String field_name;

  Map<String, dynamic> toJson() => _$ErrorModelToJson(this);
  factory ErrorModel.fromJson(Map<String, dynamic> json) =>
      _$ErrorModelFromJson(json);

  ErrorModel({this.message, this.field_name});
}
