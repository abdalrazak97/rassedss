import 'package:json_annotation/json_annotation.dart';

part 'data_verify_model.g.dart';

@JsonSerializable()
class DataVerifyModel {
  final String api_token;

  Map<String, dynamic> toJson() => _$DataVerifyModelToJson(this);
  factory DataVerifyModel.fromJson(Map<String, dynamic> json) =>
      _$DataVerifyModelFromJson(json);

  DataVerifyModel({
    this.api_token,
  });
}
