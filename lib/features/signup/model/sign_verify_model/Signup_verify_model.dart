import 'package:json_annotation/json_annotation.dart';

import 'data_verify_model.dart';
import 'error_model.dart';

part 'Signup_verify_model.g.dart';

@JsonSerializable()
class SignupVerifyResponseModel {
  final int code;
  final bool success;
  final String message;
  final DataVerifyModel dataVerifyModel;
  final String api_token;
  final String first_name;
  final String last_name;
  final double balance;
  final String phone;
  final int role;
  final List<ErrorModel> errors;

  Map<String, dynamic> toJson() => _$SignupVerifyResponseModelToJson(this);
  factory SignupVerifyResponseModel.fromJson(Map<String, dynamic> json) =>
      _$SignupVerifyResponseModelFromJson(json);

  SignupVerifyResponseModel(
      {this.code, this.success, this.role,this.message,this.api_token,this.phone,
        this.dataVerifyModel, this.errors,this.balance,this.first_name,this.last_name});
}
