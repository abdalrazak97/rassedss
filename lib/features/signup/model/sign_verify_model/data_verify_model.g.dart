// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'data_verify_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DataVerifyModel _$DataVerifyModelFromJson(Map<String, dynamic> json) {
  return DataVerifyModel(
    api_token: json['api_token'] as String,
  );
}

Map<String, dynamic> _$DataVerifyModelToJson(DataVerifyModel instance) =>
    <String, dynamic>{
      'api_token': instance.api_token,
    };
