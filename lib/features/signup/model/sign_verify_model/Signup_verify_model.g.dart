// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Signup_verify_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SignupVerifyResponseModel _$SignupVerifyResponseModelFromJson(
    Map<String, dynamic> json) {
  return SignupVerifyResponseModel(
    code: json['code'] as int,
    success: json['success'] as bool,
    role: json['role'] as int,
    message: json['message'] as String,
    api_token: json['api_token'] as String,
    phone: json['phone'] as String,
    dataVerifyModel: json['dataVerifyModel'] == null
        ? null
        : DataVerifyModel.fromJson(
            json['dataVerifyModel'] as Map<String, dynamic>),
    errors: (json['errors'] as List)
        ?.map((e) =>
            e == null ? null : ErrorModel.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    balance: (json['balance'] as num)?.toDouble(),
    first_name: json['first_name'] as String,
    last_name: json['last_name'] as String,
  );
}

Map<String, dynamic> _$SignupVerifyResponseModelToJson(
        SignupVerifyResponseModel instance) =>
    <String, dynamic>{
      'code': instance.code,
      'success': instance.success,
      'message': instance.message,
      'dataVerifyModel': instance.dataVerifyModel,
      'api_token': instance.api_token,
      'first_name': instance.first_name,
      'last_name': instance.last_name,
      'balance': instance.balance,
      'phone': instance.phone,
      'role': instance.role,
      'errors': instance.errors,
    };
