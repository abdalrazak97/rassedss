
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:rassed_app/features/Constants/usecase.dart';
import 'package:rassed_app/features/signup/api/signup_remote.dart';
import 'package:rassed_app/features/signup/model/sign_model/Signup_model.dart';


class SignupInUseCase extends UseCase<SigupResponseModel, SignupParams> {
  final SignupRemoteDataSource signupApi;

  SignupInUseCase({this.signupApi});

  @override
  Future<Either<String, SigupResponseModel>> call(SignupParams params) async {
    return await signupApi.Signup(params);

  }
}

class SignupParams extends Equatable {
  final String phone;
  final String password;
  final String name;
  final String last;
  SignupParams(
      {@required this.phone,this.password,this.name,this.last});

  @override
  // TODO: implement props
  List<Object> get props => [phone,password,name,last];
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['phone'] = this.phone;
    data['password'] = this.password;
    data['name'] = this.name;
    data['last'] = this.last;
    return data;
  }
}
