import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:rassed_app/features/Constants/usecase.dart';
import 'package:rassed_app/features/signup/api/sign_verify.dart';
import 'package:rassed_app/features/signup/model/sign_verify_model/Signup_verify_model.dart';


class SignupVerifyUseCase extends UseCase<SignupVerifyResponseModel, SingupVerifyParams> {
  final SignupVerifyRemoteDataSource signupVerifyApi;

  SignupVerifyUseCase({this.signupVerifyApi});

  @override
  Future<Either<String, SignupVerifyResponseModel>> call(SingupVerifyParams params) async {
    return await signupVerifyApi.SignupVerify(params);
  }
}

class SingupVerifyParams extends Equatable {
  final String phone;
  final String code;
  SingupVerifyParams({@required this.phone,@required this.code});

  @override
  // TODO: implement props
  List<Object> get props => [phone,code];
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['phone'] = this.phone;
    data['verify_code'] = this.code;

    return data;
  }
}
