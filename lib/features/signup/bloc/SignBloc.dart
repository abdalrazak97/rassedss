import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:rassed_app/features/signup/bloc/SignupEvent.dart';
import 'package:rassed_app/features/signup/bloc/signup_state.dart';
import 'package:rassed_app/features/signup/model/sign_model/Signup_model.dart';
import 'package:rassed_app/features/signup/model/sign_verify_model/Signup_verify_model.dart';
import 'package:rassed_app/features/signup/model/sign_verify_model/data_verify_model.dart';

import '../usecase/Signup_usecase.dart';
import '../usecase/Signup_verify_usecase.dart';
const String SERVER_FAILURE_MESSAGE = 'Server Failure';
const String CACHE_FAILURE_MESSAGE = 'Cache Failure';
const String INVALID_INPUT_FAILURE_MESSAGE =
    'Invalid Input - The number must be a positive integer or zero.';

class SignupBloc extends Bloc<SignupEvents, SignupState> {
  final SignupInUseCase signupInUseCase;
  final SignupVerifyUseCase signupVerifyUseCase;
  SignupBloc({this.signupInUseCase,this.signupVerifyUseCase}) : super(SignupState.initial());

  void onSignup() {
    add(SignupEvent());
  }
  void onSignupVerify() {
    add(SignupVerifyEvent());
  }

  void onChangeName(String name) {
    add(ChangeName(name));
  }
  void onChangeLastName(String last) {
    add(ChangeLastName(last));
  }
  void onChangePassword(String password) {
    add(ChangePassword(password));
  }
  void onChangePhone(String phone) {
    add(ChangePhone(phone));
  }
  void onChangeCode(String code) {
    add(ChangeCode(code));
  }
  void onChangeLanguage(String lang) {
    add(ChangeLanguage(lang));
  }

  void resetError() {
    add(ResetError());
  }

  @override
  Stream<SignupState> mapEventToState(SignupEvents event) async* {
    if (event is SignupEvent) {
      yield state.rebuild((b) => b
        ..isLoading = true
        ..isSuccess = false);
      final result = await signupInUseCase(

          SignupParams(
          phone: state.Phone ,password: state.PassWord, name: state.name,last: state.last)
          );

      yield* result.fold((l) async* {
        yield state.rebuild((b) => b
          ..isLoading = false
          ..isSuccess = false
          ..errorMessage = l);
      }, (r) async* {
        print('sucesss');
        yield state.rebuild((b) => b
          ..isLoading = false
          ..isSuccess = true
          ..errorMessage = ''
          ..signupResponseModel = r);
      });
      yield state.rebuild((b) => b
        ..isLoading = false
        ..isSuccess = false
        ..errorMessage = ''
        ..signupResponseModel = SigupResponseModel(
            message: '',
            code: 0,
            success: false,
            errors: []));
    }
    else if (event is SignupVerifyEvent) {
      yield state.rebuild((b) => b
        ..isLoading = true
        ..isSuccess = false);
      final result = await signupVerifyUseCase(
          SingupVerifyParams(
            phone: state.Phone,
            code: state.code
          ));

      yield* result.fold((l) async* {
        yield state.rebuild((b) => b
          ..isLoading = false
          ..isSuccess = false
          ..errorMessage = l);
      }, (r) async* {
        print('sucesss');
        yield state.rebuild((b) => b
          ..isLoading = false
          ..isSuccess = true
          ..errorMessage = ''
          ..signupVerifyResponseModel = r);
      });
      yield state.rebuild((b) => b
        ..isLoading = false
        ..isSuccess = true
        ..errorMessage = ''
        ..signupVerifyResponseModel = SignupVerifyResponseModel(
            message: '',
            code: 0,
            success: false,
            role: 0,
            dataVerifyModel: DataVerifyModel(api_token: ''),
            errors: []));



    }
    else if (event is ChangePassword) {
      yield state.rebuild((b) => b..PassWord = event.password);

    }
    else if (event is ChangeCode) {

      yield state.rebuild((b) => b..code = event.code);

    }
    else if (event is ChangePhone) {

      yield state.rebuild((b) => b..Phone = event.phone);

    }
    else if (event is ChangeName) {
      yield state.rebuild((b) => b..name = event.name);
    } else if (event is ChangeLastName) {
      yield state.rebuild((b) => b..last = event.last);
    }

    else if (event is ChangeLanguage) {
      yield state.rebuild((b) => b..Accept_Language = event.lang);
    } else if (event is ResetError) {
      yield state.rebuild((b) => b..errorMessage = '');
    }
  }
}
