import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class SignupEvents extends Equatable {
  @override
  List<Object> get props => [];
}

class SignupEvent extends SignupEvents {}
class SignupVerifyEvent extends SignupEvents {}
class ChangeName extends SignupEvents {
  final name;
  ChangeName(this.name);
}
class ChangeLastName extends SignupEvents {
  final last;
  ChangeLastName(this.last);
}

class ChangeCode extends SignupEvents {
  final code;
  ChangeCode(this.code);
}

class ChangePhone extends SignupEvents {
  final phone;
  ChangePhone(this.phone);
}

class ChangePassword extends SignupEvents {
  final password;
  ChangePassword(this.password);
}

class ChangeLanguage extends SignupEvents {
  final lang;
  ChangeLanguage(this.lang);
}

class ResetError extends SignupEvents {}

class Enable extends SignupEvents {}

class Disable extends SignupEvents {}
