// GENERATED CODE - DO NOT MODIFY BY HAND

part of signup_state;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$SignupState extends SignupState {
  @override
  final bool isSuccess;
  @override
  final bool isLoading;
  @override
  final String Accept_Language;
  @override
  final String name;
  @override
  final String last;
  @override
  final String code;
  @override
  final String PassWord;
  @override
  final String Phone;
  @override
  final String errorMessage;
  @override
  final String username;
  @override
  final int role;
  @override
  final SigupResponseModel signupResponseModel;
  @override
  final SignupVerifyResponseModel signupVerifyResponseModel;

  factory _$SignupState([void Function(SignupStateBuilder) updates]) =>
      (new SignupStateBuilder()..update(updates)).build();

  _$SignupState._(
      {this.isSuccess,
      this.isLoading,
      this.Accept_Language,
      this.name,
      this.last,
      this.code,
      this.PassWord,
      this.Phone,
      this.errorMessage,
      this.username,
      this.role,
      this.signupResponseModel,
      this.signupVerifyResponseModel})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        isSuccess, 'SignupState', 'isSuccess');
    BuiltValueNullFieldError.checkNotNull(
        isLoading, 'SignupState', 'isLoading');
    BuiltValueNullFieldError.checkNotNull(
        Accept_Language, 'SignupState', 'Accept_Language');
    BuiltValueNullFieldError.checkNotNull(name, 'SignupState', 'name');
    BuiltValueNullFieldError.checkNotNull(last, 'SignupState', 'last');
    BuiltValueNullFieldError.checkNotNull(code, 'SignupState', 'code');
    BuiltValueNullFieldError.checkNotNull(PassWord, 'SignupState', 'PassWord');
    BuiltValueNullFieldError.checkNotNull(Phone, 'SignupState', 'Phone');
    BuiltValueNullFieldError.checkNotNull(
        errorMessage, 'SignupState', 'errorMessage');
    BuiltValueNullFieldError.checkNotNull(username, 'SignupState', 'username');
    BuiltValueNullFieldError.checkNotNull(role, 'SignupState', 'role');
    BuiltValueNullFieldError.checkNotNull(
        signupResponseModel, 'SignupState', 'signupResponseModel');
    BuiltValueNullFieldError.checkNotNull(
        signupVerifyResponseModel, 'SignupState', 'signupVerifyResponseModel');
  }

  @override
  SignupState rebuild(void Function(SignupStateBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  SignupStateBuilder toBuilder() => new SignupStateBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is SignupState &&
        isSuccess == other.isSuccess &&
        isLoading == other.isLoading &&
        Accept_Language == other.Accept_Language &&
        name == other.name &&
        last == other.last &&
        code == other.code &&
        PassWord == other.PassWord &&
        Phone == other.Phone &&
        errorMessage == other.errorMessage &&
        username == other.username &&
        role == other.role &&
        signupResponseModel == other.signupResponseModel &&
        signupVerifyResponseModel == other.signupVerifyResponseModel;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc(
                        $jc(
                            $jc(
                                $jc(
                                    $jc(
                                        $jc(
                                            $jc(
                                                $jc($jc(0, isSuccess.hashCode),
                                                    isLoading.hashCode),
                                                Accept_Language.hashCode),
                                            name.hashCode),
                                        last.hashCode),
                                    code.hashCode),
                                PassWord.hashCode),
                            Phone.hashCode),
                        errorMessage.hashCode),
                    username.hashCode),
                role.hashCode),
            signupResponseModel.hashCode),
        signupVerifyResponseModel.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('SignupState')
          ..add('isSuccess', isSuccess)
          ..add('isLoading', isLoading)
          ..add('Accept_Language', Accept_Language)
          ..add('name', name)
          ..add('last', last)
          ..add('code', code)
          ..add('PassWord', PassWord)
          ..add('Phone', Phone)
          ..add('errorMessage', errorMessage)
          ..add('username', username)
          ..add('role', role)
          ..add('signupResponseModel', signupResponseModel)
          ..add('signupVerifyResponseModel', signupVerifyResponseModel))
        .toString();
  }
}

class SignupStateBuilder implements Builder<SignupState, SignupStateBuilder> {
  _$SignupState _$v;

  bool _isSuccess;
  bool get isSuccess => _$this._isSuccess;
  set isSuccess(bool isSuccess) => _$this._isSuccess = isSuccess;

  bool _isLoading;
  bool get isLoading => _$this._isLoading;
  set isLoading(bool isLoading) => _$this._isLoading = isLoading;

  String _Accept_Language;
  String get Accept_Language => _$this._Accept_Language;
  set Accept_Language(String Accept_Language) =>
      _$this._Accept_Language = Accept_Language;

  String _name;
  String get name => _$this._name;
  set name(String name) => _$this._name = name;

  String _last;
  String get last => _$this._last;
  set last(String last) => _$this._last = last;

  String _code;
  String get code => _$this._code;
  set code(String code) => _$this._code = code;

  String _PassWord;
  String get PassWord => _$this._PassWord;
  set PassWord(String PassWord) => _$this._PassWord = PassWord;

  String _Phone;
  String get Phone => _$this._Phone;
  set Phone(String Phone) => _$this._Phone = Phone;

  String _errorMessage;
  String get errorMessage => _$this._errorMessage;
  set errorMessage(String errorMessage) => _$this._errorMessage = errorMessage;

  String _username;
  String get username => _$this._username;
  set username(String username) => _$this._username = username;

  int _role;
  int get role => _$this._role;
  set role(int role) => _$this._role = role;

  SigupResponseModel _signupResponseModel;
  SigupResponseModel get signupResponseModel => _$this._signupResponseModel;
  set signupResponseModel(SigupResponseModel signupResponseModel) =>
      _$this._signupResponseModel = signupResponseModel;

  SignupVerifyResponseModel _signupVerifyResponseModel;
  SignupVerifyResponseModel get signupVerifyResponseModel =>
      _$this._signupVerifyResponseModel;
  set signupVerifyResponseModel(
          SignupVerifyResponseModel signupVerifyResponseModel) =>
      _$this._signupVerifyResponseModel = signupVerifyResponseModel;

  SignupStateBuilder();

  SignupStateBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _isSuccess = $v.isSuccess;
      _isLoading = $v.isLoading;
      _Accept_Language = $v.Accept_Language;
      _name = $v.name;
      _last = $v.last;
      _code = $v.code;
      _PassWord = $v.PassWord;
      _Phone = $v.Phone;
      _errorMessage = $v.errorMessage;
      _username = $v.username;
      _role = $v.role;
      _signupResponseModel = $v.signupResponseModel;
      _signupVerifyResponseModel = $v.signupVerifyResponseModel;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(SignupState other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$SignupState;
  }

  @override
  void update(void Function(SignupStateBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$SignupState build() {
    final _$result = _$v ??
        new _$SignupState._(
            isSuccess: BuiltValueNullFieldError.checkNotNull(
                isSuccess, 'SignupState', 'isSuccess'),
            isLoading: BuiltValueNullFieldError.checkNotNull(
                isLoading, 'SignupState', 'isLoading'),
            Accept_Language: BuiltValueNullFieldError.checkNotNull(
                Accept_Language, 'SignupState', 'Accept_Language'),
            name: BuiltValueNullFieldError.checkNotNull(
                name, 'SignupState', 'name'),
            last: BuiltValueNullFieldError.checkNotNull(
                last, 'SignupState', 'last'),
            code: BuiltValueNullFieldError.checkNotNull(
                code, 'SignupState', 'code'),
            PassWord: BuiltValueNullFieldError.checkNotNull(
                PassWord, 'SignupState', 'PassWord'),
            Phone: BuiltValueNullFieldError.checkNotNull(
                Phone, 'SignupState', 'Phone'),
            errorMessage: BuiltValueNullFieldError.checkNotNull(
                errorMessage, 'SignupState', 'errorMessage'),
            username: BuiltValueNullFieldError.checkNotNull(
                username, 'SignupState', 'username'),
            role:
                BuiltValueNullFieldError.checkNotNull(role, 'SignupState', 'role'),
            signupResponseModel: BuiltValueNullFieldError.checkNotNull(signupResponseModel, 'SignupState', 'signupResponseModel'),
            signupVerifyResponseModel: BuiltValueNullFieldError.checkNotNull(signupVerifyResponseModel, 'SignupState', 'signupVerifyResponseModel'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
