library signup_state;

import 'package:built_value/built_value.dart';
import 'package:rassed_app/features/signup/model/sign_model/Signup_model.dart';
import 'package:rassed_app/features/signup/model/sign_verify_model/data_verify_model.dart';
import 'package:rassed_app/features/signup/model/sign_verify_model/Signup_verify_model.dart';

part 'signup_state.g.dart';

abstract class SignupState implements Built<SignupState, SignupStateBuilder> {
  bool get isSuccess;
  bool get isLoading;
  String get Accept_Language;
  String get name;
  String get last;
  String get code;
  String get PassWord;
  String get Phone;
  String get errorMessage;
  String get username;

  int get role;
  SigupResponseModel get signupResponseModel;
  SignupVerifyResponseModel get signupVerifyResponseModel;
  SignupState._();

  factory SignupState([updates(SignupStateBuilder b)]) = _$SignupState;

  factory SignupState.initial() {
    return SignupState((b) => b
      ..isLoading = false
      ..isSuccess = false
      ..name = ''
      ..last=''
      ..Phone=''
      ..code=''
      ..username=''
      ..role=0
      ..PassWord = ''
      ..errorMessage = ''
      ..Accept_Language = ''
      ..signupVerifyResponseModel=
      SignupVerifyResponseModel(
              message: '',
              code: 0,
              success: false,
              role: 0,
              dataVerifyModel: DataVerifyModel(api_token: ''),
              errors: [])
      ..signupResponseModel = SigupResponseModel(
          message: '',
          code: 0,
          success: false,
          errors: []));
  }
}
