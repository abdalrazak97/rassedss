import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:country_code_picker/country_code_picker.dart';
import 'package:flutter/material.dart';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:google_fonts_arabic/fonts.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:rassed_app/features/login/login_page.dart';
import 'package:rassed_app/features/signup/verifty_phone.dart';
import '../../injection.dart';
import 'bloc/SignBloc.dart';
import 'bloc/signup_state.dart';
import 'dart:ui' as ui;

class ContinueWithPhone extends StatefulWidget {
  @override
  _ContinueWithPhoneState createState() => _ContinueWithPhoneState();
}

class _ContinueWithPhoneState extends State<ContinueWithPhone> {
  String phoneNumber = "";
  String phoneCode = '+966';
  TextEditingController passwordController = TextEditingController();
  TextEditingController phoneController = TextEditingController();
  TextEditingController nameController = TextEditingController();
  TextEditingController lastController = TextEditingController();
  SignupBloc bloc = sl<SignupBloc>();
  String password;
  String phone;
  bool first = false;

  @override
  void initState() {
    super.initState();
    phoneController.text = '+966';
  }

  @override
  void dispose() {
    passwordController.dispose();
    phoneController.dispose();
    nameController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: SizedBox(),
        title: Text(
          "Signup with phone".tr().toString(),
          style: TextStyle(
            fontSize: 18,
            fontFamily: ArabicFonts.Cairo,
            package: 'google_fonts_arabic',
            fontWeight: FontWeight.bold,
            color: Colors.black,
          ),
        ),
        backgroundColor: Colors.grey.shade50,
        elevation: 0,
        centerTitle: true,
        textTheme: Theme.of(context).textTheme,
      ),
      body: SafeArea(
        child: BlocListener<SignupBloc, SignupState>(
          bloc: bloc,
          listener: (context, state) {
            if (state.isSuccess && first) {
              if (state.signupResponseModel.message.contains('code sent to Number:')&& phone.length>5) {
                setState(() {
                  first = false;
                });
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => VerifyPhone(phoneNumber: phone)),
                );
              } else{
                setState(() {
                  first = false;
                });
                AwesomeDialog(
                    width: ScreenUtil().setWidth(400),
                    context: context,
                    buttonsTextStyle: TextStyle(
                        fontSize: ScreenUtil().setSp(20), color: Colors.white),
                    dialogType: DialogType.ERROR,
                    animType: AnimType.BOTTOMSLIDE,
                    title: "Alert".toString(),
                    desc: "${state.signupResponseModel.message.toString()}"
                        .toString(),
                    btnOkOnPress: () {},
                    btnOkColor: Colors.blueGrey)
                  ..show();
              }

            }
          },
          child: BlocBuilder<SignupBloc, SignupState>(
              bloc: bloc,
              builder: (context, state) {
                return ModalProgressHUD(
                  inAsyncCall: state.isLoading,
                  child: SingleChildScrollView(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            SizedBox(
                              height: ScreenUtil().screenHeight * 0.05,
                            ),
                            SizedBox(
                              height: ScreenUtil().screenHeight * 0.2,
                              child:
                                  Image.asset('assets/images/holding-phone.png'),
                            ),
                            Padding(
                              padding: EdgeInsets.symmetric(
                                  vertical: 14, horizontal: 20),
                              child: Text(
                                "You'll receive a 6 digit code to verify next."
                                    .tr()
                                    .toString(),
                                style: TextStyle(
                                  fontFamily: ArabicFonts.Cairo,
                                  package: 'google_fonts_arabic',
                                  fontSize: ScreenUtil().setSp(16),
                                  color: Color(0xFF818181),
                                ),
                              ),
                            ),
                            Container(
                              height: ScreenUtil().screenHeight * 0.6,
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.all(
                                  Radius.circular(25),
                                ),
                              ),
                              child: Padding(
                                padding: EdgeInsets.all(16),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Container(
                                        width: ScreenUtil().screenWidth * 0.95,
                                        color: Colors.grey.shade50,
                                        padding: EdgeInsets.symmetric(
                                            horizontal: ScreenUtil().setWidth(5)),
                                        child: TextField(

                                          style: TextStyle(
                                            fontFamily: ArabicFonts.Cairo,
                                            package: 'google_fonts_arabic',
                                            fontSize: ScreenUtil().setSp(14),
                                          ),
                                          controller: nameController,
                                          decoration: InputDecoration(
                                              icon: Icon(
                                                Icons.person_rounded,
                                                size: ScreenUtil().setWidth(20),
                                                color: Colors.grey,
                                              ),
                                              border: InputBorder.none,
                                              hintText:
                                                  'First Name'.tr().toString(),
                                              hintStyle: TextStyle(
                                                fontSize: ScreenUtil().setSp(14),
                                              ),
                                              labelStyle: TextStyle(
                                                fontSize: ScreenUtil().setSp(14),
                                              )),
                                          onChanged: (text) {},
                                        )),
                                    SizedBox(
                                      height: ScreenUtil().screenHeight * 0.02,
                                    ),
                                    Container(
                                        width: ScreenUtil().screenWidth * 0.95,
                                        color: Colors.grey.shade50,
                                        padding: EdgeInsets.symmetric(
                                            horizontal: ScreenUtil().setWidth(5)),
                                        child: TextField(

                                          style: TextStyle(
                                            fontFamily: ArabicFonts.Cairo,
                                            package: 'google_fonts_arabic',
                                            fontSize: ScreenUtil().setSp(14),
                                          ),
                                          controller: lastController,
                                          decoration: InputDecoration(
                                              icon: Icon(
                                                Icons.person_rounded,
                                                size: ScreenUtil().setWidth(20),
                                                color: Colors.grey,
                                              ),
                                              border: InputBorder.none,
                                              hintText:
                                                  'Last Name'.tr().toString(),
                                              hintStyle: TextStyle(
                                                fontSize: ScreenUtil().setSp(14),
                                              ),
                                              labelStyle: TextStyle(
                                                fontSize: ScreenUtil().setSp(14),
                                              )),
                                          onChanged: (text) {},
                                        )),
                                    SizedBox(
                                      height: ScreenUtil().screenHeight * 0.02,
                                    ),
                                    Container(
                                        width: ScreenUtil().screenWidth * 0.95,
                                        color: Colors.grey.shade50,
                                        padding: EdgeInsets.symmetric(
                                            horizontal: ScreenUtil().setWidth(5)),
                                        child: Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            Expanded(
                                              child: TextField(
                                                keyboardType: TextInputType.phone,
                                                textDirection:
                                                    ui.TextDirection.ltr,
                                                style: TextStyle(
                                                  fontFamily: ArabicFonts.Cairo,
                                                  package: 'google_fonts_arabic',
                                                  fontSize:
                                                      ScreenUtil().setSp(14),
                                                ),
                                                controller: phoneController,
                                                decoration: InputDecoration(
                                                    icon: Icon(
                                                      Icons.phone,
                                                      size: ScreenUtil()
                                                          .setWidth(20),
                                                      color: Colors.grey,
                                                    ),
                                                    border: InputBorder.none,
                                                    hintText: 'Phone'.toString(),
                                                    hintStyle: TextStyle(
                                                      fontSize:
                                                          ScreenUtil().setSp(14),
                                                    ),
                                                    labelStyle: TextStyle(
                                                      fontSize:
                                                          ScreenUtil().setSp(14),
                                                    )),
                                                onChanged: (text) {
                                                  phoneNumber = text;
                                                },
                                              ),
                                            ),
                                            Expanded(
                                              child: CountryCodePicker(
                                                onChanged: (value) {
                                                  phoneController.text =
                                                      value.dialCode;
                                                },
                                                searchStyle: TextStyle(
                                                  fontFamily: ArabicFonts.Cairo,
                                                  package: 'google_fonts_arabic',
                                                  fontSize:
                                                      ScreenUtil().setSp(12),
                                                ),
                                                textStyle: TextStyle(fontFamily: ArabicFonts.Cairo,
                                                  package: 'google_fonts_arabic',
                                                  fontSize:
                                                      ScreenUtil().setSp(12),
                                                ),
                                                dialogTextStyle: TextStyle(
                                                  fontFamily: ArabicFonts.Cairo,
                                                  package: 'google_fonts_arabic',
                                                  fontSize:
                                                      ScreenUtil().setSp(14),
                                                ),
                                                // Initial selection and favorite can be one of code ('IT') OR dial_code('+39')
                                                initialSelection: 'SA',
                                                textOverflow:
                                                    TextOverflow.visible,
                                                favorite: ['+963', 'SA'],
                                                showFlag: false,
                                                showFlagDialog: true,
                                                showFlagMain: true,
                                                // optional. Shows only country name and flag
                                                showCountryOnly: false,
                                                // optional. Shows only country name and flag when popup is closed.
                                                showOnlyCountryWhenClosed: true,
                                                // optional. aligns the flag and the Text left
                                                alignLeft: false,
                                                showDropDownButton: true,
                                              ),
                                            ),
                                          ],
                                        )),
                                    SizedBox(
                                      height: ScreenUtil().screenHeight * 0.02,
                                    ),
                                    Container(
                                        width: ScreenUtil().screenWidth * 0.95,
                                        color: Colors.grey.shade50,
                                        padding: EdgeInsets.symmetric(
                                            horizontal: ScreenUtil().setWidth(5)),
                                        child: TextField(

                                          style: TextStyle(
                                            fontFamily: ArabicFonts.Cairo,
                                            package: 'google_fonts_arabic',
                                            fontSize: ScreenUtil().setSp(14),
                                          ),
                                          controller: passwordController,
                                          decoration: InputDecoration(
                                              icon: Icon(
                                                Icons.lock_outlined,
                                                size: ScreenUtil().setWidth(20),
                                                color: Colors.grey,
                                              ),
                                              border: InputBorder.none,
                                              hintText:
                                                  'Password'.tr().toString(),
                                              hintStyle: TextStyle(
                                                fontSize: ScreenUtil().setSp(14),
                                              ),
                                              labelStyle: TextStyle(
                                                fontSize: ScreenUtil().setSp(14),
                                              )),
                                          onChanged: (text) {
                                            password = text;
                                          },
                                        )),
                                    Padding(
                                        padding: EdgeInsets.symmetric(
                                            vertical: 14, horizontal: 0),
                                        child: InkWell(
                                          onTap: (){
                                            Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (context) => Login()),
                                            );
                                          },
                                          child: RichText(
                                            text: TextSpan(
                                              text: 'Already have an account? '.tr().toString(),
                                              style: TextStyle(
                                                fontFamily: ArabicFonts.Cairo,
                                                package: 'google_fonts_arabic',
                                                fontSize: ScreenUtil().setSp(12),
                                                color: Color(0xFF818181),
                                              ),
                                              children:  <TextSpan>[
                                                TextSpan(
                                                    text: 'Log in'.tr().toString(),

                                                    style: TextStyle(
                                                      fontFamily: ArabicFonts.Cairo,
                                                      package: 'google_fonts_arabic',
                                                        fontWeight:
                                                            FontWeight.bold,
                                                      color: Colors.blue,),
                                                ),
                                              ],
                                            ),
                                          ),
                                        )),
                                    Center(
                                      child: Container(
                                        width: ScreenUtil().screenWidth * 0.4,
                                        height: ScreenUtil().screenHeight * 0.08,
                                        child: GestureDetector(
                                          onTap: () {
                                            phone = phoneController.text;
                                            String pass = passwordController.text;
                                            String name = nameController.text;
                                            bloc.onChangeName(name);
                                            bloc.onChangeLastName(
                                                lastController.text);
                                            bloc.onChangePassword(pass);
                                            bloc.onChangePhone(phone);
                                            bloc.onSignup();
                                            setState(() {
                                              first = true;
                                            });
                                          },
                                          child: Container(
                                            decoration: BoxDecoration(
                                              color: Colors.blueGrey,
                                              borderRadius: BorderRadius.all(
                                                Radius.circular(15),
                                              ),
                                            ),
                                            child: Center(
                                              child: Text(
                                                "Continue".tr().toString(),
                                                style: TextStyle(
                                                  fontFamily: ArabicFonts.Cairo,
                                                  package: 'google_fonts_arabic',
                                                  fontSize:
                                                      ScreenUtil().setSp(14),
                                                  fontWeight: FontWeight.bold,
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                );
              }),
        ),
      ),
    );
  }
}
