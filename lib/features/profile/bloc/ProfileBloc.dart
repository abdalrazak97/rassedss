import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:rassed_app/features/profile/usecase/charge_usecase.dart';
import 'package:rassed_app/features/profile/usecase/logout_usecase.dart';
import 'package:rassed_app/features/profile/usecase/notify_refund_usecase.dart';
import 'package:rassed_app/features/profile/usecase/notify_usecase.dart';
import 'package:rassed_app/features/profile/usecase/seen_notify_usecase.dart';
import 'package:rassed_app/features/profile/usecase/user_profile_usecase.dart';
import 'package:rassed_app/features/profile/bloc/ProfileEvent.dart';
import 'package:rassed_app/features/profile/bloc/ProfileState.dart';
const String SERVER_FAILURE_MESSAGE = 'Server Failure';
const String CACHE_FAILURE_MESSAGE = 'Cache Failure';
const String INVALID_INPUT_FAILURE_MESSAGE = 'Invalid Input - The number must be a positive integer or zero.';

class ProfileBloc extends Bloc<ProfileEvents, ProfileState> {
  final LogoutUseCase logoutUseCase;
  final UserProfileUseCase userProfileUseCase;
  final NotifyUseCase notifyUseCase;
  final NotifyRefundUseCase refundUseCase;
  final ChargeUseCase chargeUseCase;
  final SeenNotifyUseCase seenNotifyUseCase;
  ProfileBloc({this.logoutUseCase,this.userProfileUseCase,this.notifyUseCase,this.refundUseCase,this.chargeUseCase,this.seenNotifyUseCase}) : super(ProfileState.initial());

  void onLogout() {
    add(LogoutEvent());
  }

void  onUserProfile(){
    add(UserProfile());
}


  void  onNotifyRefund(){
    add(NotifyRefund());
  }
  void  onCharge(){
    add(Charge());
  }
  void  onSeenNotify(){
    add(SeenNotify());
  }
  void  onNotifyProfile(){
    add(NotifyProfile());
  }
  void onChangeApiToken(String api_token) {
    add(ChangeApiToken(api_token));
  }
  void onChangeNotificationId(int notification_id) {
    add(NotificationId(notification_id));
  }
  void onChangeBalance(double balance) {
    add(ChangeBalance(balance));
  }
  void onChangeAccept(int accept) {
    add(ChangeAccept(accept));
  }
  void onChangeIds(List<int> ids) {
    add(ChangeIds(ids));
  }


  @override
  Stream<ProfileState> mapEventToState(ProfileEvents event) async* {
    if (event is LogoutEvent) {
      yield state.rebuild((b) => b
        ..isLoading = true
        ..isSuccess = false);
      final result = await logoutUseCase(
          LogoutParams(
              api_token: state.api_token
          )
      );

      yield* result.fold((l) async* {
        yield state.rebuild((b) => b
          ..isLoading = false
          ..isSuccess = false
          ..errorMessage = l);
      }, (r) async* {
        print('sucesss');
        yield state.rebuild((b) => b
          ..isLoading = false
          ..isSuccess = true
          ..errorMessage = ''
          ..logoutResponseModel = r);
      });
    }
else if(event is UserProfile) {
      yield state.rebuild((b) => b
        ..isLoading = true
        ..isSuccess = false);
      final result = await userProfileUseCase(
          UserProfileParams(
              api_token: state.api_token
          )
      );

      yield* result.fold((l) async* {
        yield state.rebuild((b) => b
          ..isLoading = false
          ..isSuccess = false
          ..errorMessage = l);
      }, (r) async* {
        yield state.rebuild((b) => b
          ..isLoading = false
          ..isSuccess = true
          ..errorMessage = ''
          ..userProfileResponseModel = r);
      });
    }
    else if(event is NotifyProfile) {
      yield state.rebuild((b) => b
        ..isLoading = true
        ..isSuccess = false);
      final result = await notifyUseCase(
          NotifyParams(
              api_token: state.api_token
          )
      );

      yield* result.fold((l) async* {
        yield state.rebuild((b) => b
          ..isLoading = false
          ..isSuccess = false
          ..errorMessage = l);
      }, (r) async* {
        yield state.rebuild((b) => b
          ..isLoading = false
          ..isSuccess = true
          ..errorMessage = ''
          ..notifyResponseModel = r);
      });
    }
    else if(event is NotifyRefund) {
      yield state.rebuild((b) => b
        ..isLoading = true
        ..isSuccess = false);
      final result = await refundUseCase(
          NotifyRefundParams(
              api_token: state.api_token,
            accept: state.accept,
            notification_id: state.notification_id

          )
      );

      yield* result.fold((l) async* {
        yield state.rebuild((b) => b
          ..isLoading = false
          ..isSuccess = false
          ..errorMessage = l);
      }, (r) async* {
        yield state.rebuild((b) => b
          ..isLoading = false
          ..isSuccess = true
          ..errorMessage = ''
          ..notifyRefundResponseModel = r);
      });
    }
    else if(event is SeenNotify) {
      yield state.rebuild((b) => b
        ..isLoading = true
        ..isSuccess = false);
      final result = await seenNotifyUseCase(
          SeenNotifyParams(
              api_token: state.api_token,
              ids: state.ids

          )
      );

      yield* result.fold((l) async* {
        yield state.rebuild((b) => b
          ..isLoading = false
          ..isSuccess = false
          ..errorMessage = l);
      }, (r) async* {
        yield state.rebuild((b) => b
          ..isLoading = false
          ..isSuccess = true
          ..errorMessage = ''
          ..seenNotifyResponseModel = r);
      });
    }
    else if(event is Charge) {
      yield state.rebuild((b) => b
        ..isLoading = true
        ..isSuccess = false);
      final result = await chargeUseCase(
          ChargeParams(
              api_token: state.api_token,
              balance: state.balance

          )
      );

      yield* result.fold((l) async* {
        yield state.rebuild((b) => b
          ..isLoading = false
          ..isSuccess = false
          ..errorMessage = l);
      }, (r) async* {
        yield state.rebuild((b) => b
          ..isLoading = false
          ..isSuccess = true
          ..errorMessage = ''
          ..chargeResponseModel = r);
      });
    }
    else if (event is ChangeApiToken) {
      yield state.rebuild((b) => b..api_token = event.api_token);

    }
    else if (event is ChangeAccept) {
      yield state.rebuild((b) => b..accept = event.accept);

    }
    else if (event is NotificationId) {
      yield state.rebuild((b) => b..notification_id = event.notification_id);

    }
    else if (event is ChangeBalance) {
      yield state.rebuild((b) => b..balance = event.balance);

    }
    else if (event is ChangeIds) {
      print('ids in bloc');
      yield state.rebuild((b) => b..ids = event.ids);

    }


  }
}
