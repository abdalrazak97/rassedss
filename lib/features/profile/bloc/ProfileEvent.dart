import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class ProfileEvents extends Equatable {
  @override
  List<Object> get props => [];
}

class LogoutEvent extends ProfileEvents {}


class UserProfile extends ProfileEvents {}
class NotifyProfile extends ProfileEvents {}
class NotifyRefund extends ProfileEvents {}
class Charge extends ProfileEvents {}
class SeenNotify extends ProfileEvents {}
class ChangeApiToken extends ProfileEvents {
  final api_token;
  ChangeApiToken(this.api_token);
}
class ChangeBalance extends ProfileEvents {
  final balance;
  ChangeBalance(this.balance);
}
class ChangeIds extends ProfileEvents {
  final ids;
  ChangeIds(this.ids);
}
class NotificationId extends ProfileEvents {
  final notification_id;
  NotificationId(this.notification_id);
}

class ChangeAccept extends ProfileEvents {
  final accept;
  ChangeAccept(this.accept);
}






class ResetError extends ProfileEvents {}

class Enable extends ProfileEvents {}

class Disable extends ProfileEvents {}
