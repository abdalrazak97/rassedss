library ProfileState;

import 'package:built_value/built_value.dart';
import 'package:rassed_app/features/profile/model/charge_model.dart';
import 'package:rassed_app/features/profile/model/logout_model.dart';
import 'package:rassed_app/features/profile/model/notify_model.dart';
import 'package:rassed_app/features/profile/model/notify_refund_model.dart';
import 'package:rassed_app/features/profile/model/seen_notify_model.dart';
import 'package:rassed_app/features/profile/model/user_profile_model.dart';
part 'ProfileState.g.dart';

abstract class ProfileState implements Built<ProfileState, ProfileStateBuilder> {
  bool get isSuccess;
  bool get isLoading;
  String get api_token;
  String get errorMessage;
  int get accept;
  int get notification_id;
  double get balance;
  List<int> get ids;

  LogoutResponseModel get logoutResponseModel;
  UserProfileResponseModel get userProfileResponseModel;
  List<NotifyResponseModel> get notifyResponseModel;
  NotifyRefundResponseModel get notifyRefundResponseModel;
  ChargeResponseModel get chargeResponseModel;
  SeenNotifyResponseModel get seenNotifyResponseModel;
  ProfileState._();

  factory ProfileState([updates(ProfileStateBuilder b)]) = _$ProfileState;

  factory ProfileState.initial() {
    return ProfileState((b) => b
      ..isLoading = false
      ..isSuccess = false
      ..api_token='0'
      ..accept=0
      ..balance=0
      ..notification_id=0
      ..errorMessage = ''
      ..ids=[0]
      ..seenNotifyResponseModel=SeenNotifyResponseModel(
        message: '',
      )
      ..notifyResponseModel=[]
      ..notifyRefundResponseModel=NotifyRefundResponseModel(
        message: '',

      )
      ..chargeResponseModel=ChargeResponseModel(
        message: '',
      )
      ..logoutResponseModel=
      LogoutResponseModel(
          message: '',
      )
        ..userProfileResponseModel=UserProfileResponseModel(
          message: '',
          balance: 0,
          first_name: '',
          last_name: '',
          email: "",
          phone: '',
          role_id: 0
        )
    );
  }
}
