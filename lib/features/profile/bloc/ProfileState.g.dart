// GENERATED CODE - DO NOT MODIFY BY HAND

part of ProfileState;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$ProfileState extends ProfileState {
  @override
  final bool isSuccess;
  @override
  final bool isLoading;
  @override
  final String api_token;
  @override
  final String errorMessage;
  @override
  final int accept;
  @override
  final int notification_id;
  @override
  final double balance;
  @override
  final List<int> ids;
  @override
  final LogoutResponseModel logoutResponseModel;
  @override
  final UserProfileResponseModel userProfileResponseModel;
  @override
  final List<NotifyResponseModel> notifyResponseModel;
  @override
  final NotifyRefundResponseModel notifyRefundResponseModel;
  @override
  final ChargeResponseModel chargeResponseModel;
  @override
  final SeenNotifyResponseModel seenNotifyResponseModel;

  factory _$ProfileState([void Function(ProfileStateBuilder) updates]) =>
      (new ProfileStateBuilder()..update(updates)).build();

  _$ProfileState._(
      {this.isSuccess,
      this.isLoading,
      this.api_token,
      this.errorMessage,
      this.accept,
      this.notification_id,
      this.balance,
      this.ids,
      this.logoutResponseModel,
      this.userProfileResponseModel,
      this.notifyResponseModel,
      this.notifyRefundResponseModel,
      this.chargeResponseModel,
      this.seenNotifyResponseModel})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        isSuccess, 'ProfileState', 'isSuccess');
    BuiltValueNullFieldError.checkNotNull(
        isLoading, 'ProfileState', 'isLoading');
    BuiltValueNullFieldError.checkNotNull(
        api_token, 'ProfileState', 'api_token');
    BuiltValueNullFieldError.checkNotNull(
        errorMessage, 'ProfileState', 'errorMessage');
    BuiltValueNullFieldError.checkNotNull(accept, 'ProfileState', 'accept');
    BuiltValueNullFieldError.checkNotNull(
        notification_id, 'ProfileState', 'notification_id');
    BuiltValueNullFieldError.checkNotNull(balance, 'ProfileState', 'balance');
    BuiltValueNullFieldError.checkNotNull(ids, 'ProfileState', 'ids');
    BuiltValueNullFieldError.checkNotNull(
        logoutResponseModel, 'ProfileState', 'logoutResponseModel');
    BuiltValueNullFieldError.checkNotNull(
        userProfileResponseModel, 'ProfileState', 'userProfileResponseModel');
    BuiltValueNullFieldError.checkNotNull(
        notifyResponseModel, 'ProfileState', 'notifyResponseModel');
    BuiltValueNullFieldError.checkNotNull(
        notifyRefundResponseModel, 'ProfileState', 'notifyRefundResponseModel');
    BuiltValueNullFieldError.checkNotNull(
        chargeResponseModel, 'ProfileState', 'chargeResponseModel');
    BuiltValueNullFieldError.checkNotNull(
        seenNotifyResponseModel, 'ProfileState', 'seenNotifyResponseModel');
  }

  @override
  ProfileState rebuild(void Function(ProfileStateBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ProfileStateBuilder toBuilder() => new ProfileStateBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ProfileState &&
        isSuccess == other.isSuccess &&
        isLoading == other.isLoading &&
        api_token == other.api_token &&
        errorMessage == other.errorMessage &&
        accept == other.accept &&
        notification_id == other.notification_id &&
        balance == other.balance &&
        ids == other.ids &&
        logoutResponseModel == other.logoutResponseModel &&
        userProfileResponseModel == other.userProfileResponseModel &&
        notifyResponseModel == other.notifyResponseModel &&
        notifyRefundResponseModel == other.notifyRefundResponseModel &&
        chargeResponseModel == other.chargeResponseModel &&
        seenNotifyResponseModel == other.seenNotifyResponseModel;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc(
                        $jc(
                            $jc(
                                $jc(
                                    $jc(
                                        $jc(
                                            $jc(
                                                $jc(
                                                    $jc(
                                                        $jc(0,
                                                            isSuccess.hashCode),
                                                        isLoading.hashCode),
                                                    api_token.hashCode),
                                                errorMessage.hashCode),
                                            accept.hashCode),
                                        notification_id.hashCode),
                                    balance.hashCode),
                                ids.hashCode),
                            logoutResponseModel.hashCode),
                        userProfileResponseModel.hashCode),
                    notifyResponseModel.hashCode),
                notifyRefundResponseModel.hashCode),
            chargeResponseModel.hashCode),
        seenNotifyResponseModel.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('ProfileState')
          ..add('isSuccess', isSuccess)
          ..add('isLoading', isLoading)
          ..add('api_token', api_token)
          ..add('errorMessage', errorMessage)
          ..add('accept', accept)
          ..add('notification_id', notification_id)
          ..add('balance', balance)
          ..add('ids', ids)
          ..add('logoutResponseModel', logoutResponseModel)
          ..add('userProfileResponseModel', userProfileResponseModel)
          ..add('notifyResponseModel', notifyResponseModel)
          ..add('notifyRefundResponseModel', notifyRefundResponseModel)
          ..add('chargeResponseModel', chargeResponseModel)
          ..add('seenNotifyResponseModel', seenNotifyResponseModel))
        .toString();
  }
}

class ProfileStateBuilder
    implements Builder<ProfileState, ProfileStateBuilder> {
  _$ProfileState _$v;

  bool _isSuccess;
  bool get isSuccess => _$this._isSuccess;
  set isSuccess(bool isSuccess) => _$this._isSuccess = isSuccess;

  bool _isLoading;
  bool get isLoading => _$this._isLoading;
  set isLoading(bool isLoading) => _$this._isLoading = isLoading;

  String _api_token;
  String get api_token => _$this._api_token;
  set api_token(String api_token) => _$this._api_token = api_token;

  String _errorMessage;
  String get errorMessage => _$this._errorMessage;
  set errorMessage(String errorMessage) => _$this._errorMessage = errorMessage;

  int _accept;
  int get accept => _$this._accept;
  set accept(int accept) => _$this._accept = accept;

  int _notification_id;
  int get notification_id => _$this._notification_id;
  set notification_id(int notification_id) =>
      _$this._notification_id = notification_id;

  double _balance;
  double get balance => _$this._balance;
  set balance(double balance) => _$this._balance = balance;

  List<int> _ids;
  List<int> get ids => _$this._ids;
  set ids(List<int> ids) => _$this._ids = ids;

  LogoutResponseModel _logoutResponseModel;
  LogoutResponseModel get logoutResponseModel => _$this._logoutResponseModel;
  set logoutResponseModel(LogoutResponseModel logoutResponseModel) =>
      _$this._logoutResponseModel = logoutResponseModel;

  UserProfileResponseModel _userProfileResponseModel;
  UserProfileResponseModel get userProfileResponseModel =>
      _$this._userProfileResponseModel;
  set userProfileResponseModel(
          UserProfileResponseModel userProfileResponseModel) =>
      _$this._userProfileResponseModel = userProfileResponseModel;

  List<NotifyResponseModel> _notifyResponseModel;
  List<NotifyResponseModel> get notifyResponseModel =>
      _$this._notifyResponseModel;
  set notifyResponseModel(List<NotifyResponseModel> notifyResponseModel) =>
      _$this._notifyResponseModel = notifyResponseModel;

  NotifyRefundResponseModel _notifyRefundResponseModel;
  NotifyRefundResponseModel get notifyRefundResponseModel =>
      _$this._notifyRefundResponseModel;
  set notifyRefundResponseModel(
          NotifyRefundResponseModel notifyRefundResponseModel) =>
      _$this._notifyRefundResponseModel = notifyRefundResponseModel;

  ChargeResponseModel _chargeResponseModel;
  ChargeResponseModel get chargeResponseModel => _$this._chargeResponseModel;
  set chargeResponseModel(ChargeResponseModel chargeResponseModel) =>
      _$this._chargeResponseModel = chargeResponseModel;

  SeenNotifyResponseModel _seenNotifyResponseModel;
  SeenNotifyResponseModel get seenNotifyResponseModel =>
      _$this._seenNotifyResponseModel;
  set seenNotifyResponseModel(
          SeenNotifyResponseModel seenNotifyResponseModel) =>
      _$this._seenNotifyResponseModel = seenNotifyResponseModel;

  ProfileStateBuilder();

  ProfileStateBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _isSuccess = $v.isSuccess;
      _isLoading = $v.isLoading;
      _api_token = $v.api_token;
      _errorMessage = $v.errorMessage;
      _accept = $v.accept;
      _notification_id = $v.notification_id;
      _balance = $v.balance;
      _ids = $v.ids;
      _logoutResponseModel = $v.logoutResponseModel;
      _userProfileResponseModel = $v.userProfileResponseModel;
      _notifyResponseModel = $v.notifyResponseModel;
      _notifyRefundResponseModel = $v.notifyRefundResponseModel;
      _chargeResponseModel = $v.chargeResponseModel;
      _seenNotifyResponseModel = $v.seenNotifyResponseModel;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ProfileState other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$ProfileState;
  }

  @override
  void update(void Function(ProfileStateBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$ProfileState build() {
    final _$result = _$v ??
        new _$ProfileState._(
            isSuccess: BuiltValueNullFieldError.checkNotNull(
                isSuccess, 'ProfileState', 'isSuccess'),
            isLoading: BuiltValueNullFieldError.checkNotNull(
                isLoading, 'ProfileState', 'isLoading'),
            api_token: BuiltValueNullFieldError.checkNotNull(
                api_token, 'ProfileState', 'api_token'),
            errorMessage: BuiltValueNullFieldError.checkNotNull(
                errorMessage, 'ProfileState', 'errorMessage'),
            accept: BuiltValueNullFieldError.checkNotNull(
                accept, 'ProfileState', 'accept'),
            notification_id: BuiltValueNullFieldError.checkNotNull(
                notification_id, 'ProfileState', 'notification_id'),
            balance: BuiltValueNullFieldError.checkNotNull(
                balance, 'ProfileState', 'balance'),
            ids: BuiltValueNullFieldError.checkNotNull(
                ids, 'ProfileState', 'ids'),
            logoutResponseModel: BuiltValueNullFieldError.checkNotNull(
                logoutResponseModel, 'ProfileState', 'logoutResponseModel'),
            userProfileResponseModel:
                BuiltValueNullFieldError.checkNotNull(userProfileResponseModel, 'ProfileState', 'userProfileResponseModel'),
            notifyResponseModel: BuiltValueNullFieldError.checkNotNull(notifyResponseModel, 'ProfileState', 'notifyResponseModel'),
            notifyRefundResponseModel: BuiltValueNullFieldError.checkNotNull(notifyRefundResponseModel, 'ProfileState', 'notifyRefundResponseModel'),
            chargeResponseModel: BuiltValueNullFieldError.checkNotNull(chargeResponseModel, 'ProfileState', 'chargeResponseModel'),
            seenNotifyResponseModel: BuiltValueNullFieldError.checkNotNull(seenNotifyResponseModel, 'ProfileState', 'seenNotifyResponseModel'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
