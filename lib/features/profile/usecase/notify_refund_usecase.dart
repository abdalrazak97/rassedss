
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:rassed_app/features/Constants/usecase.dart';

import 'package:rassed_app/features/profile/api/notify_refund_remote.dart';
import 'package:rassed_app/features/profile/model/notify_refund_model.dart';



class NotifyRefundUseCase extends UseCase<NotifyRefundResponseModel, NotifyRefundParams> {
  final NotifyRefundRemoteDataSource notifyRefundRemoteDataSource;

  NotifyRefundUseCase({this.notifyRefundRemoteDataSource});

  @override
  Future<Either<String, NotifyRefundResponseModel>> call(NotifyRefundParams params) async {
    return await notifyRefundRemoteDataSource.NotifyRefundApi(params);

  }
}

class NotifyRefundParams extends Equatable {
  final String api_token;
  final int notification_id;
  final int accept;
  NotifyRefundParams(
      {@required this.api_token,this.notification_id,this.accept});

  @override
  // TODO: implement props
  List<Object> get props => [api_token];
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['api_token'] = this.api_token;
    data['notification_id'] = this.notification_id;
    data['accept'] = this.accept;

    return data;
  }
}
