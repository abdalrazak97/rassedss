
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:rassed_app/features/Constants/usecase.dart';
import 'package:rassed_app/features/profile/api/logout_api.dart';
import 'package:rassed_app/features/profile/model/logout_model.dart';


class LogoutUseCase extends UseCase<LogoutResponseModel, LogoutParams> {
  final LogoutRemoteDataSource logoutApi;

  LogoutUseCase({this.logoutApi});

  @override
  Future<Either<String, LogoutResponseModel>> call(LogoutParams params) async {
    return await logoutApi.Logout(params);

  }
}

class LogoutParams extends Equatable {
  final String api_token;

  LogoutParams(
      {@required this.api_token,});

  @override
  // TODO: implement props
  List<Object> get props => [api_token];
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['api_token'] = this.api_token;

    return data;
  }
}
