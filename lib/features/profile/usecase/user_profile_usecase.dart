
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:rassed_app/features/Constants/usecase.dart';
import 'package:rassed_app/features/profile/api/user_profile_remote.dart';
import 'package:rassed_app/features/profile/model/user_profile_model.dart';


class UserProfileUseCase extends UseCase<UserProfileResponseModel, UserProfileParams> {
  final UserProfileRemoteDataSource userProfileRemoteDataSource;

  UserProfileUseCase({this.userProfileRemoteDataSource});

  @override
  Future<Either<String, UserProfileResponseModel>> call(UserProfileParams params) async {
    return await userProfileRemoteDataSource.UserProfile(params);

  }
}

class UserProfileParams extends Equatable {
  final String api_token;

  UserProfileParams(
      {@required this.api_token,});

  @override
  // TODO: implement props
  List<Object> get props => [api_token];
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['api_token'] = this.api_token;

    return data;
  }
}
