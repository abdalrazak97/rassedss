
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:rassed_app/features/Constants/usecase.dart';
import 'package:rassed_app/features/profile/api/charege_remote.dart';

import 'package:rassed_app/features/profile/model/charge_model.dart';



class ChargeUseCase extends UseCase<ChargeResponseModel, ChargeParams> {
  final ChargeRemoteDataSource chargeRemoteDataSource;

  ChargeUseCase({this.chargeRemoteDataSource});

  @override
  Future<Either<String, ChargeResponseModel>> call(ChargeParams params) async {
    return await chargeRemoteDataSource.chargeApi(params);

  }
}

class ChargeParams extends Equatable {
  final String api_token;
  final double balance;
  ChargeParams(
      {@required this.api_token,this.balance});

  @override
  // TODO: implement props
  List<Object> get props => [api_token];
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['api_token'] = this.api_token;
    data['balance'] = this.balance;
    return data;
  }
}
