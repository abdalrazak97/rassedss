
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:rassed_app/features/Constants/usecase.dart';
import 'package:rassed_app/features/profile/api/seen_notify_remote.dart';
import 'package:rassed_app/features/profile/model/seen_notify_model.dart';


class SeenNotifyUseCase extends UseCase<SeenNotifyResponseModel, SeenNotifyParams> {
  final SeenNotifyRemoteDataSource seenNotifyRemoteDataSource;

  SeenNotifyUseCase({this.seenNotifyRemoteDataSource});

  @override
  Future<Either<String, SeenNotifyResponseModel>> call(SeenNotifyParams params) async {
    return await seenNotifyRemoteDataSource.SeenNotifyApi(params);

  }
}

class SeenNotifyParams extends Equatable {
  final String api_token;
  final List<int> ids;
  SeenNotifyParams(
      {@required this.api_token,this.ids});

  @override
  // TODO: implement props
  List<Object> get props => [api_token,ids];
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['api_token'] = this.api_token;
    data['ids'] = this.ids;
    return data;
  }
}
