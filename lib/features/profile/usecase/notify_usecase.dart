
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:rassed_app/features/Constants/usecase.dart';
import 'package:rassed_app/features/profile/api/logout_api.dart';
import 'package:rassed_app/features/profile/api/notify_api.dart';
import 'package:rassed_app/features/profile/model/logout_model.dart';
import 'package:rassed_app/features/profile/model/notify_model.dart';


class NotifyUseCase extends UseCase<List<NotifyResponseModel>, NotifyParams> {
  final NotifyRemoteDataSource notifyRemoteDataSource;

  NotifyUseCase({this.notifyRemoteDataSource});

  @override
  Future<Either<String, List<NotifyResponseModel>>> call(NotifyParams params) async {
    return await notifyRemoteDataSource.NotifyApi(params);

  }
}

class NotifyParams extends Equatable {
  final String api_token;

  NotifyParams(
      {@required this.api_token,});

  @override
  // TODO: implement props
  List<Object> get props => [api_token];
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['api_token'] = this.api_token;

    return data;
  }
}
