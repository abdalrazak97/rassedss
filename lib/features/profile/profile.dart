import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:google_fonts_arabic/fonts.dart';
import 'package:rassed_app/features/home/home.dart';
import 'package:rassed_app/features/login/bloc/LoginBloc.dart';
import 'package:rassed_app/features/login/bloc/LoginState.dart';
import 'package:rassed_app/features/login/login_page.dart';
import 'package:rassed_app/features/profile/bloc/ProfileBloc.dart';
import 'package:rassed_app/features/profile/bloc/ProfileState.dart';
import 'dart:ui' as ui;
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter_svg/svg.dart';
import 'package:rassed_app/injection.dart';
import 'package:rassed_app/features/profile/component/setteing_profile.dart';
import 'package:rassed_app/features/settings/Globals.dart';
import 'package:rassed_app/features/settings/Preference.dart';
import 'package:rassed_app/features/widget/drawer.dart';
import 'package:rassed_app/main.dart';

import 'component/charge_balanc.dart';
import 'component/notify.dart';

class Profile extends StatefulWidget {
  const Profile({Key key}) : super(key: key);

  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  bool requset = false;
  ProfileBloc bloc = sl<ProfileBloc>();
  LoginBloc blocLogin = sl<LoginBloc>();
  String token;

  @override
  void initState() {
    // token = Preferences.getUserToken();
    // bloc.onChangeApiToken(token);
     bloc.onNotifyProfile();
    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    return BlocListener<ProfileBloc, ProfileState>(
        bloc: bloc,
        listener: (context, state) {
          if (state.isSuccess && requset) {
            if (state.logoutResponseModel.message == 'logged out' ||
                state.logoutResponseModel.message == 'Unauthenticated.') {
              setState(() {
                requset=false;
                Preferences.saveUserToken(null);
              });
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => Login()),
              );
            } else {
              AwesomeDialog(
                  width: ScreenUtil().setWidth(400),
                  context: context,
                  buttonsTextStyle: TextStyle(
                      fontSize: ScreenUtil().setSp(fontSize),
                      color: Colors.white),
                  dialogType: DialogType.ERROR,
                  animType: AnimType.BOTTOMSLIDE,
                  title: "Alert".toString(),
                  desc: "${state.logoutResponseModel.message.toString()}"
                      .toString(),
                  btnOkOnPress: () {},
                  btnOkColor: Colors.blueGrey)
                ..show();
            }
          }
        },
        child: Scaffold(
          drawer: drawer(),
          appBar: AppBar(
            elevation: 0,
            actions: [
              SizedBox(
                width: ScreenUtil().setWidth(40),
                child: Center(
                  child: TextButton(
                    style: ButtonStyle(
                      backgroundColor:
                      MaterialStateProperty.all(Colors.blueGrey),
                    ),
                    onPressed: () {
                      Navigator.pushReplacement(context, MaterialPageRoute(builder: (BuildContext context) => Home()));
                    },
                    child: SvgPicture.asset(
                      "assets/icons/close3.svg",
                      width: ScreenUtil().screenWidth * 0.1,
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
            ],
            leading: SizedBox(
                width: ScreenUtil().setWidth(30),
                child: Center(
                    child: IconButton(
                      icon: Icon(Icons.logout),
                      onPressed: () {
                        String token = Preferences.getUserToken();
                        bloc.onChangeApiToken(token);
                        bloc.onLogout();
                        setState(() {
                          requset = true;
                        });
                      },
                    ))),
            backgroundColor: Colors.blueGrey,
            centerTitle: true,
            title: Text(
              'Profile'.tr().toString(),
              textDirection: ui.TextDirection.rtl,
              style: TextStyle(
                fontFamily: ArabicFonts.Cairo,
                package: 'google_fonts_arabic',
                fontSize: 18,
              ),
            ),
          ),
          body: BlocBuilder<ProfileBloc, ProfileState>(
              bloc: bloc,
              builder: (context, state) {
                return BlocBuilder<LoginBloc, LoginState>(
                    bloc: blocLogin,
                    builder: (context, state2) {

                      if (state.isLoading)
                      return Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Center(child: CircularProgressIndicator(),),
                                  ],
                          );
                        return SingleChildScrollView(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                width: ScreenUtil().screenWidth,
                                height: ScreenUtil().screenHeight * 0.22,
                                color: Colors.blueGrey,
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment
                                      .spaceBetween,
                                  children: [
                                    Container(
                                      height: ScreenUtil().screenHeight * 0.1,
                                      padding: EdgeInsets.symmetric(
                                          horizontal: ScreenUtil().setWidth(
                                              20)),
                                      child: Row(
                                        textDirection: ui.TextDirection.rtl,
                                        mainAxisAlignment: MainAxisAlignment
                                            .start,
                                        crossAxisAlignment: CrossAxisAlignment
                                            .start,
                                        children: [
                                          CircleAvatar(
                                            backgroundColor: Colors.white,
                                            child: Icon(
                                              Icons.person_rounded,
                                              color: Colors.blueGrey,
                                            ),
                                            minRadius: 25,
                                            maxRadius: 25,
                                          ),
                                          SizedBox(
                                            width: ScreenUtil().screenWidth *
                                                0.05,
                                          ),
                                          Column(
                                            mainAxisAlignment: MainAxisAlignment
                                                .start,
                                            crossAxisAlignment: CrossAxisAlignment
                                                .start,
                                            children: [
                                              state.userProfileResponseModel.first_name!=null?Text(
                                                state.userProfileResponseModel.first_name+ " " + state.userProfileResponseModel
                                                        .last_name,
                                                style: TextStyle(
                                                    fontFamily: ArabicFonts
                                                        .Cairo,
                                                    package: 'google_fonts_arabic',
                                                    fontSize: ScreenUtil()
                                                        .setSp(
                                                        18),
                                                    color: Colors.white),
                                              ):Text("user name",
                                                style: TextStyle(
                                                    fontFamily: ArabicFonts
                                                        .Cairo,
                                                    package: 'google_fonts_arabic',
                                                    fontSize: ScreenUtil()
                                                        .setSp(
                                                        18),
                                                    color: Colors.white),
                                              ),
                                              Text(
                                                state.userProfileResponseModel
                                                    .phone,
                                                textDirection: ui.TextDirection
                                                    .ltr,
                                                style: TextStyle(
                                                    fontFamily: ArabicFonts
                                                        .Cairo,
                                                    package: 'google_fonts_arabic',
                                                    fontSize: ScreenUtil()
                                                        .setSp(
                                                        18),
                                                    color: Colors.white),
                                              ),
                                            ],
                                          ),
                                        ],
                                      ),
                                    ),
                                    Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Container(
                                          height: ScreenUtil().screenHeight * 0.1,
                                          width:ScreenUtil().screenWidth*0.4,
                                          decoration: BoxDecoration(
                                            color: Colors.blueGrey,
                                            borderRadius: BorderRadius.circular(
                                                8),
                                            border: Border.all(
                                              color: Colors.blueGrey.shade200,
                                              width: 2,
                                            ),
                                          ),

                                          child: Column(
                                            children: [
                                              Text(
                                                'Balance'.tr().toString(),
                                                style: TextStyle(
                                                    fontFamily: ArabicFonts.Cairo,
                                                    package: 'google_fonts_arabic',
                                                    fontSize: ScreenUtil().setSp(
                                                        14),
                                                    color: Colors.white),
                                              ),
                                              Text(
                                                state.userProfileResponseModel
                                                    .balance
                                                    .toString(),
                                                style: TextStyle(
                                                    fontFamily: ArabicFonts.Cairo,
                                                    package: 'google_fonts_arabic',
                                                    fontSize: ScreenUtil().setSp(18),
                                                    fontWeight: FontWeight.w700,
                                                    color: Colors.white),
                                              ),
                                            ],
                                          ),
                                        ),
                                        Container(
                                          width: ScreenUtil().screenWidth * 0.4,
                                          height: ScreenUtil().screenHeight * 0.1,
                                          decoration: BoxDecoration(
                                            color: Colors.blueGrey,
                                            borderRadius: BorderRadius.circular(
                                                8),
                                            border: Border.all(
                                              color: Colors.red.shade200,
                                              width: 2,
                                            ),
                                          ),

                                          child: Column(
                                            children: [
                                              Text(
                                                'Waiting Balance'.tr().toString(),
                                                style: TextStyle(
                                                    fontFamily: ArabicFonts.Cairo,
                                                    package: 'google_fonts_arabic',
                                                    fontSize: ScreenUtil().setSp(
                                                        14),
                                                    color: Colors.white),
                                              ),
                                              Text(
                                                state.userProfileResponseModel.amount!=null? state.userProfileResponseModel.amount.toString():"0",
                                                style: TextStyle(
                                                    fontFamily: ArabicFonts.Cairo,
                                                    package: 'google_fonts_arabic',
                                                    fontWeight: FontWeight.w700,
                                                    fontSize: ScreenUtil().setSp(18),
                                                    color: Colors.white),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                    SizedBox(
                                      height: ScreenUtil().screenHeight * 0.02,
                                    ),
                                  ],
                                ),
                              ),
                              InkWell(
                                onTap: () {
                                  Navigator.push(context,
                                      MaterialPageRoute(
                                          builder: (context) => Setting()));
                                },
                                child: Container(
                                  decoration: BoxDecoration(
                                      border: Border(
                                        bottom: BorderSide(
                                          color: Colors.grey,
                                          width: 2.0,
                                        ),
                                      )),
                                  padding: EdgeInsets.symmetric(
                                      vertical: ScreenUtil().setHeight(20),
                                      horizontal: ScreenUtil().setWidth(20)),
                                  child: Row(
                                    textDirection: ui.TextDirection.rtl,
                                    mainAxisAlignment: MainAxisAlignment
                                        .spaceBetween,
                                    crossAxisAlignment: CrossAxisAlignment
                                        .start,
                                    children: [
                                      Row(
                                        textDirection: ui.TextDirection.rtl,
                                        mainAxisAlignment: MainAxisAlignment
                                            .start,
                                        crossAxisAlignment: CrossAxisAlignment
                                            .start,
                                        children: [
                                          Center(
                                              child: Icon(
                                                Icons.settings,
                                                size: 30,
                                                color: Colors.blueGrey,
                                              )),
                                          SizedBox(
                                              width: ScreenUtil().screenWidth *
                                                  0.1),
                                          Center(
                                            child: Text(
                                              'Setting'.tr().toString(),
                                              style: TextStyle(
                                                  fontSize: ScreenUtil().setSp(
                                                      22),
                                                  fontFamily: ArabicFonts.Cairo,
                                                  package: 'google_fonts_arabic',
                                                  color: Colors.blueGrey),
                                            ),
                                          ),
                                        ],
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Center(
                                          child: Icon(
                                            Icons.arrow_back_ios_outlined,
                                            size: 25,
                                            color: Colors.blueGrey,
                                            textDirection: ui.TextDirection.ltr,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                             role==20?InkWell(
                                onTap: () {
                                  Navigator.push(context,
                                      MaterialPageRoute(
                                          builder: (context) => ChargeBalance()));
                                },
                                child: Container(
                                  decoration: BoxDecoration(
                                      border: Border(
                                        bottom: BorderSide(
                                          color: Colors.grey,
                                          width: 2.0,
                                        ),
                                      )),
                                  padding: EdgeInsets.symmetric(
                                      vertical: ScreenUtil().setHeight(20),
                                      horizontal: ScreenUtil().setWidth(20)),
                                  child: Row(
                                    textDirection: ui.TextDirection.rtl,
                                    mainAxisAlignment: MainAxisAlignment
                                        .spaceBetween,
                                    crossAxisAlignment: CrossAxisAlignment
                                        .start,
                                    children: [
                                      Row(
                                        textDirection: ui.TextDirection.rtl,
                                        mainAxisAlignment: MainAxisAlignment
                                            .start,
                                        crossAxisAlignment: CrossAxisAlignment
                                            .start,
                                        children: [
                                          Center(
                                              child: Icon(
                                                Icons.monetization_on_outlined,
                                                size: 30,
                                                color: Colors.blueGrey,
                                              )),
                                          SizedBox(
                                              width: ScreenUtil().screenWidth *
                                                  0.1),
                                          Center(
                                            child: Text(
                                              'Charge Balance'.tr().toString(),
                                              style: TextStyle(
                                                  fontSize: ScreenUtil().setSp(
                                                      22),
                                                  fontFamily: ArabicFonts.Cairo,
                                                  package: 'google_fonts_arabic',
                                                  color: Colors.blueGrey),
                                            ),
                                          ),
                                        ],
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Center(
                                          child: Icon(
                                            Icons.arrow_back_ios_outlined,
                                            size: 25,
                                            color: Colors.blueGrey,
                                            textDirection: ui.TextDirection.ltr,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ):SizedBox(),

                            ],
                          ),
                        );

                    }
                );
              }
          ),
        )

    );
  }
}

class PaddedElevatedButton extends StatelessWidget {
  const PaddedElevatedButton({
    this.buttonText,
    this.onPressed,
    Key key,
  }) : super(key: key);

  final String buttonText;
  final VoidCallback onPressed;

  @override
  Widget build(BuildContext context) =>
      Padding(
        padding: const EdgeInsets.fromLTRB(0, 0, 0, 8),
        child: ElevatedButton(
          onPressed: onPressed,
          child: Text(buttonText),
        ),
      );
}

Future<void> _showNotification() async {
  const AndroidNotificationDetails androidPlatformChannelSpecifics =
  AndroidNotificationDetails(
      'your channel id', 'your channel name', 'your channel description',
      importance: Importance.max,
      channelShowBadge: false,
      priority: Priority.high,
      ticker: 'ticker');
  const NotificationDetails platformChannelSpecifics =
  NotificationDetails(android: androidPlatformChannelSpecifics);
  await flutterLocalNotificationsPlugin.show(
      0, 'Hello Msms', 'I love you', platformChannelSpecifics,
      payload: 'item x');
}