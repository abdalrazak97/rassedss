import 'dart:convert';

import 'package:json_annotation/json_annotation.dart';

import 'action_model.dart';




part 'notify_model.g.dart';

@JsonSerializable()
class NotifyResponseModel {

  final int id;
  final int user_id;
  final int is_admin;
  final String type;
  final String first_name;
  final String last_name;
  final String phone;
  ActionResponseModel object;
  final int is_done;
  final int is_accepted;



  Map<String, dynamic> toJson() => _$NotifyResponseModelToJson(this);
  factory NotifyResponseModel.fromJson(Map<String, dynamic> json) =>
      _$NotifyResponseModelFromJson(json);

  NotifyResponseModel({this.id, this.user_id, this.is_admin, this.type,this.object,
    this.is_done, this.is_accepted,this.last_name,this.first_name,this.phone});
}
List<NotifyResponseModel> NotifyResponseModelFromJson(String str) =>
    List<NotifyResponseModel>.from(json.decode(str).map((x) => NotifyResponseModel.fromJson(x)));
String NotifyResponseModelToJson(List<NotifyResponseModel> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));