import 'package:json_annotation/json_annotation.dart';


part 'notify_refund_model.g.dart';

@JsonSerializable()
class NotifyRefundResponseModel {

  final bool success;
  final String message;


  Map<String, dynamic> toJson() => _$NotifyRefundResponseModelToJson(this);
  factory NotifyRefundResponseModel.fromJson(Map<String, dynamic> json) =>
      _$NotifyRefundResponseModelFromJson(json);

  NotifyRefundResponseModel(
      {this.success, this.message,});
}
