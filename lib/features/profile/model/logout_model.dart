import 'package:json_annotation/json_annotation.dart';




part 'logout_model.g.dart';

@JsonSerializable()
class LogoutResponseModel {

  final bool success;
  final String message;


  Map<String, dynamic> toJson() => _$LogoutResponseModelToJson(this);
  factory LogoutResponseModel.fromJson(Map<String, dynamic> json) =>
      _$LogoutResponseModelFromJson(json);

  LogoutResponseModel(
      {this.success, this.message,});
}
