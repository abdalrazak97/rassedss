// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'notify_refund_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

NotifyRefundResponseModel _$NotifyRefundResponseModelFromJson(
    Map<String, dynamic> json) {
  return NotifyRefundResponseModel(
    success: json['success'] as bool,
    message: json['message'] as String,
  );
}

Map<String, dynamic> _$NotifyRefundResponseModelToJson(
        NotifyRefundResponseModel instance) =>
    <String, dynamic>{
      'success': instance.success,
      'message': instance.message,
    };
