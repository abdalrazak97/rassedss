import 'package:json_annotation/json_annotation.dart';




part 'charge_model.g.dart';

@JsonSerializable()
class ChargeResponseModel {

  final bool success;
  final String message;


  Map<String, dynamic> toJson() => _$ChargeResponseModelToJson(this);
  factory ChargeResponseModel.fromJson(Map<String, dynamic> json) =>
      _$ChargeResponseModelFromJson(json);

  ChargeResponseModel({this.success, this.message,});
}
