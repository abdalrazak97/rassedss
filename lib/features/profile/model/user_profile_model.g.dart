// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_profile_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UserProfileResponseModel _$UserProfileResponseModelFromJson(
    Map<String, dynamic> json) {
  return UserProfileResponseModel(
    success: json['success'] as bool,
    message: json['message'] as String,
    first_name: json['first_name'] as String,
    last_name: json['last_name'] as String,
    balance: (json['balance'] as num)?.toDouble(),
    phone: json['phone'] as String,
    email: json['email'] as String,
    role_id: json['role_id'] as int,
    amount: (json['amount'] as num)?.toDouble(),
  );
}

Map<String, dynamic> _$UserProfileResponseModelToJson(
        UserProfileResponseModel instance) =>
    <String, dynamic>{
      'success': instance.success,
      'message': instance.message,
      'first_name': instance.first_name,
      'last_name': instance.last_name,
      'balance': instance.balance,
      'amount': instance.amount,
      'phone': instance.phone,
      'email': instance.email,
      'role_id': instance.role_id,
    };
