import 'package:json_annotation/json_annotation.dart';




part 'user_profile_model.g.dart';

@JsonSerializable()
class UserProfileResponseModel {

  final bool success;
  final String message;
  final String first_name;
  final String last_name;
  final double balance;
  final double amount;
  final String phone;
  final String email;
  final int role_id;

  Map<String, dynamic> toJson() => _$UserProfileResponseModelToJson(this);
  factory UserProfileResponseModel.fromJson(Map<String, dynamic> json) =>
      _$UserProfileResponseModelFromJson(json);

  UserProfileResponseModel({this.success, this.message, this.first_name,
      this.last_name, this.balance, this.phone, this.email, this.role_id,this.amount});
}
