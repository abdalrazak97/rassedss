// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'action_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ActionResponseModel _$ActionResponseModelFromJson(Map<String, dynamic> json) {
  return ActionResponseModel(
    invoice_id: json['invoice_id'] as int,
    user: json['user'] as int,
    ids: (json['ids'] as List)?.map((e) => e as int)?.toList(),
    amount: (json['amount'] as num)?.toDouble(),
  );
}

Map<String, dynamic> _$ActionResponseModelToJson(
        ActionResponseModel instance) =>
    <String, dynamic>{
      'invoice_id': instance.invoice_id,
      'user': instance.user,
      'ids': instance.ids,
      'amount': instance.amount,
    };
