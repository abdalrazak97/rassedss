import 'package:json_annotation/json_annotation.dart';




part 'seen_notify_model.g.dart';

@JsonSerializable()
class SeenNotifyResponseModel {

  final bool success;
  final String message;


  Map<String, dynamic> toJson() => _$SeenNotifyResponseModelToJson(this);
  factory SeenNotifyResponseModel.fromJson(Map<String, dynamic> json) =>
      _$SeenNotifyResponseModelFromJson(json);

  SeenNotifyResponseModel({this.success, this.message,});
}
