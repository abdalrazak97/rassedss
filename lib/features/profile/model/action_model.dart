import 'package:json_annotation/json_annotation.dart';

part 'action_model.g.dart';

@JsonSerializable()
class ActionResponseModel {

  final int invoice_id;
  final int user;
  final List<int> ids;
  final double amount;

  Map<String, dynamic> toJson() => _$ActionResponseModelToJson(this);
  factory ActionResponseModel.fromJson(Map<String, dynamic> json) =>
      _$ActionResponseModelFromJson(json);

   ActionResponseModel({this.invoice_id, this.user,this.ids,this.amount});

}
