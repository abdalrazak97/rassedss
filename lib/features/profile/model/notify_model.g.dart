// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'notify_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

NotifyResponseModel _$NotifyResponseModelFromJson(Map<String, dynamic> json) {
  return NotifyResponseModel(
    id: json['id'] as int,
    user_id: json['user_id'] as int,
    is_admin: json['is_admin'] as int,
    type: json['type'] as String,
    object: json['object'] == null
        ? null
        : ActionResponseModel.fromJson(json['object'] as Map<String, dynamic>),
    is_done: json['is_done'] as int,
    is_accepted: json['is_accepted'] as int,
    last_name: json['last_name'] as String,
    first_name: json['first_name'] as String,
    phone: json['phone'] as String,
  );
}

Map<String, dynamic> _$NotifyResponseModelToJson(
        NotifyResponseModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'user_id': instance.user_id,
      'is_admin': instance.is_admin,
      'type': instance.type,
      'first_name': instance.first_name,
      'last_name': instance.last_name,
      'phone': instance.phone,
      'object': instance.object,
      'is_done': instance.is_done,
      'is_accepted': instance.is_accepted,
    };
