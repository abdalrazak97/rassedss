import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:rassed_app/features/Constants/ApiConstants.dart';
import 'package:dartz/dartz.dart';
import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:dio/dio.dart';
import 'package:meta/meta.dart';
import 'package:rassed_app/features/profile/usecase/logout_usecase.dart';
import 'package:rassed_app/features/profile/model/logout_model.dart';

abstract class LogoutRemoteDataSource {
  Future<Either<String, LogoutResponseModel>> Logout(LogoutParams params);
}

class LogoutRemoteDataSourceImpl extends LogoutRemoteDataSource {
  final Dio dio;
  final DataConnectionChecker networkInfo;
  LogoutRemoteDataSourceImpl({@required this.dio, @required this.networkInfo});

  @override
  Future<Either<String, LogoutResponseModel>> Logout(LogoutParams params) async {
    LogoutResponseModel user;

    if (await networkInfo.hasConnection) {
      try {

        final result = await dio.post(
          Url.logout,

          options: Options(
            headers:{
              'Authorization': 'Bearer ${params.api_token}',
            } ,
            followRedirects: false,
            validateStatus: (status) {
              return status < 500;
            },
          ),
        );

        user = LogoutResponseModel.fromJson(json.decode(result.data));

        return Right(user);
      } on DioError catch (ex) {
        if (ex.type == DioErrorType.connectTimeout) {
          return Left(Er.NetworkError);
        } else if (ex.type == DioErrorType.receiveTimeout) {
          return Left(Er.NetworkError);
        } else if (user != null)
          return Left(user.message);
        else if (user == null) return Left(Er.Error);
      }
    } else {
      return Left(Er.NetworkError);
    }
  }
}
