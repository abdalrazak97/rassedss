import 'package:flutter/material.dart';
import 'package:rassed_app/features/Constants/ApiConstants.dart';
import 'package:dartz/dartz.dart';
import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:dio/dio.dart';
import 'package:meta/meta.dart';
import 'package:rassed_app/features/profile/model/notify_model.dart';
import 'package:rassed_app/features/profile/usecase/notify_usecase.dart';


abstract class NotifyRemoteDataSource {
  Future<Either<String, List<NotifyResponseModel>>> NotifyApi(NotifyParams params);
}

class NotifyRemoteDataSourceImpl extends NotifyRemoteDataSource {
  final Dio dio;
  final DataConnectionChecker networkInfo;
  NotifyRemoteDataSourceImpl({@required this.dio, @required this.networkInfo});

  @override
  Future<Either<String, List<NotifyResponseModel>>> NotifyApi(NotifyParams params) async {
   List<NotifyResponseModel>  notify;

    if (await networkInfo.hasConnection) {
      try {

        final result = await dio.get(
          Url.notify,

          options: Options(
            headers:{
              'Authorization': 'Bearer ${params.api_token}',
            } ,
            followRedirects: false,
            validateStatus: (status) {
              return status < 500;
            },
          ),
        );

        notify = NotifyResponseModelFromJson(result.data);

        return Right(notify);
      } on DioError catch (ex) {
        if (ex.type == DioErrorType.connectTimeout) {
          return Left(Er.NetworkError);
        } else if (ex.type == DioErrorType.receiveTimeout) {
          return Left(Er.NetworkError);
        }
        else if (notify == null) return Left(Er.Error);
      }
    } else {
      return Left(Er.NetworkError);
    }
  }
}
