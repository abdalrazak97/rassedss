import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:rassed_app/features/Constants/ApiConstants.dart';
import 'package:dartz/dartz.dart';
import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:dio/dio.dart';
import 'package:meta/meta.dart';
import 'package:rassed_app/features/profile/model/seen_notify_model.dart';
import 'package:rassed_app/features/profile/usecase/seen_notify_usecase.dart';



abstract class SeenNotifyRemoteDataSource {
  Future<Either<String, SeenNotifyResponseModel>> SeenNotifyApi(SeenNotifyParams params);
}

class SeenNotifyRemoteDataSourceImpl extends SeenNotifyRemoteDataSource {
  final Dio dio;
  final DataConnectionChecker networkInfo;
  SeenNotifyRemoteDataSourceImpl({@required this.dio, @required this.networkInfo});

  @override
  Future<Either<String, SeenNotifyResponseModel>> SeenNotifyApi(SeenNotifyParams params) async {
    SeenNotifyResponseModel  notifySeen;

    if (await networkInfo.hasConnection) {
      try {
       print(params.ids);
        final result = await dio.post(
          Url.seenNotify,
          data:jsonEncode(params.toJson()),
          options: Options(
            headers:{
              'Authorization': 'Bearer ${params.api_token}',
            } ,
            followRedirects: false,
            validateStatus: (status) {
              return status < 500;
            },
          ),
        );

        notifySeen = SeenNotifyResponseModel.fromJson(json.decode(result.data));

        return Right(notifySeen);
      } on DioError catch (ex) {
        if (ex.type == DioErrorType.connectTimeout) {
          return Left(Er.NetworkError);
        } else if (ex.type == DioErrorType.receiveTimeout) {
          return Left(Er.NetworkError);
        }
        else if (notifySeen == null) return Left(Er.Error);
      }
    } else {
      return Left(Er.NetworkError);
    }
  }
}
