import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:rassed_app/features/Constants/ApiConstants.dart';
import 'package:dartz/dartz.dart';
import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:dio/dio.dart';
import 'package:meta/meta.dart';
import 'package:rassed_app/features/profile/usecase/user_profile_usecase.dart';
import 'package:rassed_app/features/profile/model/user_profile_model.dart';
import 'package:rassed_app/features/settings/Globals.dart';

abstract class UserProfileRemoteDataSource {
  Future<Either<String, UserProfileResponseModel>> UserProfile(UserProfileParams params);
}

class UserProfileRemoteDataSourceImpl extends UserProfileRemoteDataSource {
  final Dio dio;
  final DataConnectionChecker networkInfo;
  UserProfileRemoteDataSourceImpl({@required this.dio, @required this.networkInfo});

  @override
  Future<Either<String, UserProfileResponseModel>> UserProfile(UserProfileParams params) async {
    UserProfileResponseModel user;

    if (await networkInfo.hasConnection) {
      try {

        final result = await dio.get(
          Url.profile,

          options: Options(
            headers:{
              'Authorization': 'Bearer ${params.api_token}',
            } ,
            followRedirects: false,
            validateStatus: (status) {
              return status < 500;
            },
          ),
        );

        user = UserProfileResponseModel.fromJson(json.decode(result.data));
        role=user.role_id;
        print(role);
        return Right(user);
      } on DioError catch (ex) {
        if (ex.type == DioErrorType.connectTimeout) {
          return Left(Er.NetworkError);
        } else if (ex.type == DioErrorType.receiveTimeout) {
          return Left(Er.NetworkError);
        } else if (user != null)
          return Left(user.message);
        else if (user == null) return Left(Er.Error);
      }
    } else {
      return Left(Er.NetworkError);
    }
  }
}
