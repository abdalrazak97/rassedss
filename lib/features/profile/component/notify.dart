import 'package:auto_size_text/auto_size_text.dart';
import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_fonts_arabic/fonts.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:rassed_app/core/utils/Boxs.dart';
import 'package:rassed_app/features/home/home.dart';
import 'package:rassed_app/features/profile/bloc/ProfileBloc.dart';
import 'package:rassed_app/features/profile/bloc/ProfileState.dart';
import 'package:rassed_app/features/settings/Preference.dart';
import 'package:hive/hive.dart';
import 'dart:ui' as ui;
import 'package:rassed_app/features/widget/textauto.dart';
import '../../../injection.dart';
class NotifyPage extends StatefulWidget {
  const NotifyPage({Key key}) : super(key: key);
  @override
  _NotifyPageState createState() => _NotifyPageState();
}




class _NotifyPageState extends State<NotifyPage> {
  ProfileBloc bloc = sl<ProfileBloc>();
  String token;
  bool switchBool=true;
  bool first=false;
  var box;
  @override
  void initState() {
    token = Preferences.getUserToken();
    bloc.onChangeApiToken(token);
    bloc.onNotifyProfile();
    Hive.openBox('Notify');
    first=false;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blueGrey,
      appBar: AppBar(
        elevation: 0,
        actions: [
          SizedBox(width: ScreenUtil().setWidth(40),
            child: Center(child: TextButton(
              style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all(
                    Colors.blueGrey),
              ),
              onPressed: () {
                Navigator.pushReplacement(context, MaterialPageRoute(builder: (BuildContext context) => Home()));
              },
              child: SvgPicture.asset(
                "assets/icons/close3.svg",
                width: ScreenUtil().screenWidth * 0.1, color: Colors.white,
              ),
            ),),),
        ],
        leading: SizedBox(width: 10,),

        backgroundColor: Colors.blueGrey,
        centerTitle: true,
        title: Text('Notify'.tr().toString(),

          style: TextStyle(
            fontFamily: ArabicFonts.Cairo,
            package: 'google_fonts_arabic',
            fontSize: 20,
          ),),
      ),
      body: BlocListener<ProfileBloc, ProfileState>(
          bloc: bloc,
          listener: (context, state) {
            if(state.isSuccess && first){
              if (state.notifyRefundResponseModel.message.contains("Charged Done to :Abd And his balance Now:"))
              {
                setState(() {
                  first=false;
                });
                AwesomeDialog(
                    width: ScreenUtil()
                        .setWidth(400),
                    context: context,
                    buttonsTextStyle: TextStyle(
                        fontSize:
                        ScreenUtil()
                            .setSp(20),
                        color: Colors
                            .white),
                    dialogType: DialogType.SUCCES,
                    animType: AnimType.BOTTOMSLIDE,
                    title: "Alert"
                        .toString(),
                    desc:
                    "${state.notifyRefundResponseModel.message.toString()}"

                        .toString(),
                    btnOkOnPress: () {
                      Navigator.push(context,
                        MaterialPageRoute(builder: (context) => Home()),);
                    },
                    btnOkColor:
                    Colors.blueGrey)..show();

              }
             if(state.seenNotifyResponseModel.message=='Notifications Is Seen')
               {
               setState(() {
               first=false;
               });
               bloc.onNotifyProfile();
               }
            }
          },
        child: BlocBuilder<ProfileBloc, ProfileState>(
            bloc: bloc,
            builder: (context, state) {

              if(state.isSuccess)
              return SingleChildScrollView(
                child: Column(
                  children: [
                    Padding(
                      padding: EdgeInsets.symmetric(
                          horizontal: ScreenUtil().setWidth(10),vertical: ScreenUtil().setHeight(20)),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Expanded(
                            child: TextButton(
                                style: ButtonStyle(
                                  backgroundColor:
                                  MaterialStateProperty.all(switchBool?Colors.blueGrey:Colors.white),
                                  shape: MaterialStateProperty.all(
                                    RoundedRectangleBorder(
                                      side: BorderSide(
                                          color: Colors.grey,
                                          width: 1,
                                          style: BorderStyle.solid),
                                    ),
                                  ),
                                ),
                                child: Padding(
                                  padding: EdgeInsets.symmetric(
                                      vertical: ScreenUtil().setHeight(12)),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Container(
                                          child: AutoTextgrey(
                                              ScreenUtil().setSp(16),
                                              "Refunds".tr().toString(),
                                              switchBool?Colors.white:Colors.blueGrey)),
                                      switchBool?Icon(Icons.check_rounded,color: Colors.green,):SizedBox()
                                    ],
                                  ),
                                ),
                                onPressed: () {
                                  setState(() {
                                    switchBool=true;
                                  });
                                }),
                          ),
                          Expanded(
                            child: TextButton(
                                style: ButtonStyle(
                                  backgroundColor:
                                  MaterialStateProperty.all(
                                      switchBool?Colors.white:Colors.blueGrey),
                                  shape: MaterialStateProperty.all(
                                    RoundedRectangleBorder(
                                      side: BorderSide(
                                          color: Colors.grey,
                                          width: 1,
                                          style: BorderStyle.solid),
                                    ),
                                  ),
                                ),
                                child: Padding(
                                  padding: EdgeInsets.symmetric(
                                      vertical: ScreenUtil().setHeight(12)),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Container(
                                          child: AutoTextgrey(
                                              ScreenUtil().setSp(16),
                                              "Charge".tr().toString(),
                                              switchBool?Colors.blueGrey:Colors.white)),
                                      !switchBool?Icon(Icons.check_rounded,color: Colors.green,):SizedBox()
                                    ],
                                  ),
                                ),
                                onPressed: () {
                                  setState(() {
                                    switchBool=false;
                                  });
                                }),
                          ),
                        ],
                      ),
                    ),
                    switchBool? Column(
                        children: [
                          for(int i = 0; i < state.notifyResponseModel.length; i++)
                            if(state.notifyResponseModel[i].is_admin==1 &&state.notifyResponseModel[i].type=='Make Refund')
                          Center(
                            child: Container(
                              margin: EdgeInsets.symmetric(
                                  vertical: ScreenUtil().setHeight(20)),
                              width: ScreenUtil().screenWidth * 0.9,
                              height: ScreenUtil().screenHeight * 0.25,
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(8.0),
                              ),
                              child: Column(

                                children: [
                                  SizedBox(height: ScreenUtil().screenHeight * 0.01,),
                                  Padding(
                                    padding: EdgeInsets.symmetric(
                                        horizontal: ScreenUtil().setWidth(20),
                                        vertical: ScreenUtil().setHeight(2)),
                                    child: Row(

                                      children: [
                                        AutoSizeText('Notify ID'.tr().toString()+": ",

                                          style: TextStyle(
                                              fontFamily: ArabicFonts.Cairo,
                                              package: 'google_fonts_arabic',
                                              fontSize: ScreenUtil().setSp(16)),),
                                        AutoSizeText("${i+1}".toString(),style: TextStyle(
                                            fontFamily: ArabicFonts.Cairo,
                                            package: 'google_fonts_arabic',
                                            fontSize: ScreenUtil().setSp(14)),)
                                      ],
                                    ),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.symmetric(
                                        horizontal: ScreenUtil().setWidth(20),
                                        vertical: ScreenUtil().setHeight(2)),
                                    child: Row(

                                      children: [
                                        AutoSizeText('User name'.tr().toString()+": ",

                                          style: TextStyle(
                                              fontFamily: ArabicFonts.Cairo,
                                              package: 'google_fonts_arabic',
                                              fontSize: ScreenUtil().setSp(16)),),
                                        AutoSizeText(state.notifyResponseModel[i].first_name.toString()+" ",style: TextStyle(
                                            fontFamily: ArabicFonts.Cairo,
                                            package: 'google_fonts_arabic',
                                            fontSize: ScreenUtil().setSp(14)),),
                                        AutoSizeText(state.notifyResponseModel[i].phone.toString(),
                                          textDirection: ui.TextDirection.ltr,
                                          style: TextStyle(
                                              fontFamily: ArabicFonts.Cairo,
                                              package: 'google_fonts_arabic',
                                              fontSize: ScreenUtil().setSp(14)),),
                                      ],
                                    ),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.symmetric(
                                        horizontal: ScreenUtil().setWidth(20),
                                        vertical: ScreenUtil().setHeight(2)),
                                    child: Row(

                                      children: [
                                        AutoSizeText('Notify type'.tr().toString()+": ",

                                          style: TextStyle(
                                              fontFamily: ArabicFonts.Cairo,
                                              package: 'google_fonts_arabic',
                                              fontSize: ScreenUtil().setSp(16)),),
                                        AutoSizeText(state.notifyResponseModel[i].type.tr().toString(),style: TextStyle(
                                            fontFamily: ArabicFonts.Cairo,
                                            package: 'google_fonts_arabic',
                                            fontSize: ScreenUtil().setSp(14)),),
                                        AutoSizeText(state.notifyResponseModel[i].object.invoice_id.toString(),style: TextStyle(
                                            fontFamily: ArabicFonts.Cairo,
                                            package: 'google_fonts_arabic',
                                            fontSize: ScreenUtil().setSp(14)),),
                                      ],
                                    ),
                                  ),
                                  Align(
                                    alignment: Alignment.bottomLeft,
                                    child: Padding(
                                      padding: EdgeInsets.symmetric(
                                          horizontal: ScreenUtil().setWidth(5),
                                          vertical: ScreenUtil().setHeight(10)),
                                      child: Row(
                                        children: [

                                          Container(
                                            height: ScreenUtil().screenHeight * 0.07,
                                            width: ScreenUtil().screenWidth * 0.4,
                                            color: Colors.white,
                                            child: Container(
                                              decoration: BoxDecoration(
                                                borderRadius: BorderRadius.circular(8.0),
                                              ),
                                              padding: EdgeInsets.symmetric(
                                                  horizontal: ScreenUtil().setWidth(20)),

                                              child: InkWell(
                                                onTap: () async {
                                                  bloc.onChangeNotificationId(state.notifyResponseModel[i].id);
                                                  bloc.onChangeAccept(1);
                                                  bloc.onNotifyRefund();
                                                  bloc.onNotifyProfile();
                                                },
                                                child: Container(
                                                  decoration: BoxDecoration(
                                                    color: Colors.green,
                                                    borderRadius: BorderRadius.circular(8.0),
                                                  ),

                                                  width: ScreenUtil().screenWidth,
                                                  child: Center(child: Text('Accept'.tr().toString(),
                                                    style: TextStyle(
                                                        fontSize: ScreenUtil().setSp(14),
                                                        color: Colors.white,
                                                      fontFamily: ArabicFonts.Cairo,
                                                      package: 'google_fonts_arabic',),)),),
                                              ),

                                            ),
                                          ),
                                          Container(
                                            height: ScreenUtil().screenHeight * 0.07,
                                            width: ScreenUtil().screenWidth * 0.4,
                                            color: Colors.white,
                                            child: Container(
                                              decoration: BoxDecoration(
                                                borderRadius: BorderRadius.circular(8.0),
                                              ),
                                              padding: EdgeInsets.symmetric(
                                                  horizontal: ScreenUtil().setWidth(20)),

                                              child: InkWell(
                                                onTap: () async {
                                                  bloc.onChangeNotificationId(state.notifyResponseModel[i].id);
                                                  bloc.onChangeAccept(0);
                                                  bloc.onNotifyRefund();
                                                  bloc.onNotifyProfile();
                                                  box = Boxes.getNotify();
                                                  box.clear();
                                                },
                                                child: Container(
                                                  decoration: BoxDecoration(
                                                    color: Colors.red,
                                                    borderRadius: BorderRadius.circular(8.0),
                                                  ),

                                                  width: ScreenUtil().screenWidth,
                                                  child: Center(child: Text('Refusal'.tr().toString(),
                                                    style: TextStyle(
                                                      fontSize: ScreenUtil().setSp(14),
                                                      color: Colors.white,
                                                      fontFamily: ArabicFonts.Cairo,
                                                      package: 'google_fonts_arabic',),)),),
                                              ),

                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          for(int i = 0; i < state.notifyResponseModel.length; i++)
                          if(state.notifyResponseModel[i].is_admin==0 && state.notifyResponseModel[i].type=='accept refund' ||state.notifyResponseModel[i].type== 'rejected refund')
                            Center(
                              child: Container(
                                margin: EdgeInsets.symmetric(
                                    vertical: ScreenUtil().setHeight(20)),
                                width: ScreenUtil().screenWidth * 0.9,
                                height: ScreenUtil().screenHeight * 0.27,
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(8.0),
                                ),
                                child: Column(

                                  children: [
                                    SizedBox(height: ScreenUtil().screenHeight * 0.01,),
                                    Padding(
                                      padding: EdgeInsets.symmetric(
                                          horizontal: ScreenUtil().setWidth(30),
                                          vertical: ScreenUtil().setHeight(2)),
                                      child: Row(

                                        children: [
                                          AutoSizeText('Notify ID'.tr().toString()+": ",

                                            style: TextStyle(
                                                fontFamily: ArabicFonts.Cairo,
                                                package: 'google_fonts_arabic',
                                                fontSize: ScreenUtil().setSp(16)),),
                                          AutoSizeText("${i+1}".toString(),style: TextStyle(
                                              fontFamily: ArabicFonts.Cairo,
                                              package: 'google_fonts_arabic',
                                              fontSize: ScreenUtil().setSp(14)),)
                                        ],
                                      ),
                                    ),
                                    Padding(
                                      padding: EdgeInsets.symmetric(
                                          horizontal: ScreenUtil().setWidth(30),
                                          vertical: ScreenUtil().setHeight(2)),
                                      child: Row(

                                        children: [
                                          AutoSizeText('Notify Status'.tr().toString()+": ",

                                            style: TextStyle(
                                                fontFamily: ArabicFonts.Cairo,
                                                package: 'google_fonts_arabic',
                                                fontSize: ScreenUtil().setSp(16)),),
                                          AutoSizeText(state.notifyResponseModel[i].type=="accept refund"?"Accepted".tr().toString():"Rejected".tr().toString(),style: TextStyle(

                                              fontFamily: ArabicFonts.Cairo,
                                              package: 'google_fonts_arabic',
                                              fontSize: ScreenUtil().setSp(14)),),
                                        ],
                                      ),
                                    ),
                                    Padding(
                                      padding: EdgeInsets.symmetric(
                                          horizontal: ScreenUtil().setWidth(30),
                                          vertical: ScreenUtil().setHeight(2)),
                                      child: Row(

                                        children: [
                                          AutoSizeText('Notify type'.tr().toString()+": ",

                                            style: TextStyle(
                                                fontFamily: ArabicFonts.Cairo,
                                                package: 'google_fonts_arabic',
                                                fontSize: ScreenUtil().setSp(16)),),
                                          AutoSizeText('Admin'.tr().toString() +' '+ state.notifyResponseModel[i].type.tr().toString(),style: TextStyle(
                                              fontFamily: ArabicFonts.Cairo,
                                              package: 'google_fonts_arabic',
                                              fontSize: ScreenUtil().setSp(14)),),
                                        ],
                                      ),
                                    ),
                                    Align(
                                      alignment: Alignment.bottomLeft,
                                      child: Padding(
                                        padding: EdgeInsets.symmetric(
                                            horizontal: ScreenUtil().setWidth(5),
                                            vertical: ScreenUtil().setHeight(10)),
                                        child: Row(
                                          children: [

                                            Container(
                                              height: ScreenUtil().screenHeight * 0.07,
                                              width: ScreenUtil().screenWidth * 0.4,
                                              color: Colors.white,
                                              child: Container(
                                                decoration: BoxDecoration(
                                                  borderRadius: BorderRadius.circular(8.0),
                                                ),
                                                padding: EdgeInsets.symmetric(
                                                    horizontal: ScreenUtil().setWidth(20)),



                                              ),
                                            ),
                                            Container(
                                              height: ScreenUtil().screenHeight * 0.07,
                                              width: ScreenUtil().screenWidth * 0.4,
                                              color: Colors.white,
                                              child: Container(
                                                decoration: BoxDecoration(
                                                  borderRadius: BorderRadius.circular(8.0),
                                                ),
                                                padding: EdgeInsets.symmetric(
                                                    horizontal: ScreenUtil().setWidth(20)),

                                                child: InkWell(
                                                  onTap: () async {
                                                    List<int> ids=[state.notifyResponseModel[i].id];
                                                    bloc.onChangeIds(ids);
                                                    bloc.onChangeApiToken(token);
                                                    bloc.onSeenNotify();
                                                    setState(() {
                                                      first=true;
                                                    });
                                                  },
                                                  child: Container(
                                                    decoration: BoxDecoration(
                                                      color: Colors.blueGrey,
                                                      borderRadius: BorderRadius.circular(8.0),
                                                    ),

                                                    width: ScreenUtil().screenWidth,
                                                    child: Center(child: Text('Seen'.tr().toString(),
                                                      style: TextStyle(
                                                        fontSize: ScreenUtil().setSp(14),
                                                        color: Colors.white,
                                                        fontFamily: ArabicFonts.Cairo,
                                                        package: 'google_fonts_arabic',),)),),
                                                ),

                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),

                        ],
                      ):Column(children: [
                       for(int i = 0; i < state.notifyResponseModel.length; i++)
                        if(state.notifyResponseModel[i].is_admin==1 &&state.notifyResponseModel[i].type=='Charge')
                          Center(
                            child: Container(
                              margin: EdgeInsets.symmetric(
                                  vertical: ScreenUtil().setHeight(20)),
                              width: ScreenUtil().screenWidth * 0.9,
                              height: ScreenUtil().screenHeight * 0.25,
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(8.0),
                              ),
                              child: Column(

                                children: [
                                  SizedBox(height: ScreenUtil().screenHeight * 0.01,),
                                  Padding(
                                    padding: EdgeInsets.symmetric(
                                        horizontal: ScreenUtil().setWidth(20),
                                        vertical: ScreenUtil().setHeight(2)),
                                    child: Row(

                                      children: [
                                        AutoSizeText('Notify ID'.tr().toString()+": ",

                                          style: TextStyle(
                                              fontFamily: ArabicFonts.Cairo,
                                              package: 'google_fonts_arabic',
                                              fontSize: ScreenUtil().setSp(16)),),
                                        AutoSizeText("${i+1}".toString(),style: TextStyle(
                                            fontFamily: ArabicFonts.Cairo,
                                            package: 'google_fonts_arabic',
                                            fontSize: ScreenUtil().setSp(14)),)
                                      ],
                                    ),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.symmetric(
                                        horizontal: ScreenUtil().setWidth(20),
                                        vertical: ScreenUtil().setHeight(2)),
                                    child: Row(

                                      children: [
                                        AutoSizeText('User name'.tr().toString()+": ",

                                          style: TextStyle(
                                              fontFamily: ArabicFonts.Cairo,
                                              package: 'google_fonts_arabic',
                                              fontSize: ScreenUtil().setSp(16)),),
                                        AutoSizeText(state.notifyResponseModel[i].first_name.toString()+" ",style: TextStyle(
                                            fontFamily: ArabicFonts.Cairo,
                                            package: 'google_fonts_arabic',
                                            fontSize: ScreenUtil().setSp(14)),),
                                        AutoSizeText(state.notifyResponseModel[i].phone.toString(),
                                          textDirection: ui.TextDirection.ltr,
                                          style: TextStyle(
                                            fontFamily: ArabicFonts.Cairo,
                                            package: 'google_fonts_arabic',
                                            fontSize: ScreenUtil().setSp(14)),),
                                      ],
                                    ),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.symmetric(
                                        horizontal: ScreenUtil().setWidth(20),
                                        vertical: ScreenUtil().setHeight(2)),
                                    child: Row(

                                      children: [
                                        AutoSizeText('Notify type'.tr().toString()+": ",

                                          style: TextStyle(
                                              fontFamily: ArabicFonts.Cairo,
                                              package: 'google_fonts_arabic',
                                              fontSize: ScreenUtil().setSp(16)),),
                                        AutoSizeText(state.notifyResponseModel[i].type.tr().toString()+"  ",style: TextStyle(
                                            fontFamily: ArabicFonts.Cairo,
                                            package: 'google_fonts_arabic',
                                            fontSize: ScreenUtil().setSp(14)),),
                                        AutoSizeText(state.notifyResponseModel[i].object.amount.toString(),style: TextStyle(
                                            fontFamily: ArabicFonts.Cairo,
                                            package: 'google_fonts_arabic',
                                            fontSize: ScreenUtil().setSp(14)),),
                                      ],
                                    ),
                                  ),
                                  Align(
                                    alignment: Alignment.bottomLeft,
                                    child: Padding(
                                      padding: EdgeInsets.symmetric(
                                          horizontal: ScreenUtil().setWidth(5),
                                          vertical: ScreenUtil().setHeight(10)),
                                      child: Row(
                                        children: [

                                          Container(
                                            height: ScreenUtil().screenHeight * 0.07,
                                            width: ScreenUtil().screenWidth * 0.4,
                                            color: Colors.white,
                                            child: Container(
                                              decoration: BoxDecoration(
                                                borderRadius: BorderRadius.circular(8.0),
                                              ),
                                              padding: EdgeInsets.symmetric(
                                                  horizontal: ScreenUtil().setWidth(20)),

                                              child: InkWell(
                                                onTap: () async {
                                                  bloc.onChangeNotificationId(state.notifyResponseModel[i].id);
                                                  bloc.onChangeAccept(1);
                                                  bloc.onNotifyRefund();
                                                  bloc.onNotifyProfile();
                                                },
                                                child: Container(
                                                  decoration: BoxDecoration(
                                                    color: Colors.green,
                                                    borderRadius: BorderRadius.circular(8.0),
                                                  ),

                                                  width: ScreenUtil().screenWidth,
                                                  child: Center(child: Text('Accept'.tr().toString(),
                                                    style: TextStyle(
                                                      fontSize: ScreenUtil().setSp(14),
                                                      color: Colors.white,
                                                      fontFamily: ArabicFonts.Cairo,
                                                      package: 'google_fonts_arabic',),)),),
                                              ),

                                            ),
                                          ),
                                          Container(
                                            height: ScreenUtil().screenHeight * 0.07,
                                            width: ScreenUtil().screenWidth * 0.4,
                                            color: Colors.white,
                                            child: Container(
                                              decoration: BoxDecoration(
                                                borderRadius: BorderRadius.circular(8.0),
                                              ),
                                              padding: EdgeInsets.symmetric(
                                                  horizontal: ScreenUtil().setWidth(20)),

                                              child: InkWell(
                                                onTap: () async {
                                                  bloc.onChangeNotificationId(state.notifyResponseModel[i].id);
                                                  bloc.onChangeAccept(0);
                                                  bloc.onNotifyRefund();
                                                  bloc.onNotifyProfile();
                                                  box = Boxes.getNotify();
                                                  box.clear();
                                                },
                                                child: Container(
                                                  decoration: BoxDecoration(
                                                    color: Colors.red,
                                                    borderRadius: BorderRadius.circular(8.0),
                                                  ),

                                                  width: ScreenUtil().screenWidth,
                                                  child: Center(child: Text('Refusal'.tr().toString(),
                                                    style: TextStyle(
                                                      fontSize: ScreenUtil().setSp(14),
                                                      color: Colors.white,
                                                      fontFamily: ArabicFonts.Cairo,
                                                      package: 'google_fonts_arabic',),)),),
                                              ),

                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                       for(int i = 0; i < state.notifyResponseModel.length; i++)
                        if(state.notifyResponseModel[i].is_admin==0 && state.notifyResponseModel[i].type=='Charged'||state.notifyResponseModel[i].type=='rejected Charged')
                          Center(
                            child: Container(
                              margin: EdgeInsets.symmetric(
                                  vertical: ScreenUtil().setHeight(20)),
                              width: ScreenUtil().screenWidth * 0.9,
                              height: ScreenUtil().screenHeight * 0.27,
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(8.0),
                              ),
                              child: Column(

                                children: [
                                  SizedBox(height: ScreenUtil().screenHeight * 0.01,),
                                  Padding(
                                    padding: EdgeInsets.symmetric(
                                        horizontal: ScreenUtil().setWidth(30),
                                        vertical: ScreenUtil().setHeight(2)),
                                    child: Row(

                                      children: [
                                        AutoSizeText('Notify ID'.tr().toString()+": ",

                                          style: TextStyle(
                                              fontFamily: ArabicFonts.Cairo,
                                              package: 'google_fonts_arabic',
                                              fontSize: ScreenUtil().setSp(16)),),
                                        AutoSizeText("${i+1}".toString(),style: TextStyle(
                                            fontFamily: ArabicFonts.Cairo,
                                            package: 'google_fonts_arabic',
                                            fontSize: ScreenUtil().setSp(14)),)
                                      ],
                                    ),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.symmetric(
                                        horizontal: ScreenUtil().setWidth(30),
                                        vertical: ScreenUtil().setHeight(2)),
                                    child: Row(

                                      children: [
                                        AutoSizeText('Notify Status'.tr().toString()+": ",

                                          style: TextStyle(
                                              fontFamily: ArabicFonts.Cairo,
                                              package: 'google_fonts_arabic',
                                              fontSize: ScreenUtil().setSp(16)),),
                                        AutoSizeText(state.notifyResponseModel[i].type=="Charged"?"Accepted".tr().toString():"Rejected".tr().toString(),style: TextStyle(

                                            fontFamily: ArabicFonts.Cairo,
                                            package: 'google_fonts_arabic',
                                            fontSize: ScreenUtil().setSp(14)),),
                                      ],
                                    ),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.symmetric(
                                        horizontal: ScreenUtil().setWidth(30),
                                        vertical: ScreenUtil().setHeight(2)),
                                    child: Row(

                                      children: [
                                        AutoSizeText('Notify type'.tr().toString()+": ",

                                          style: TextStyle(
                                              fontFamily: ArabicFonts.Cairo,
                                              package: 'google_fonts_arabic',
                                              fontSize: ScreenUtil().setSp(16)),),
                                        AutoSizeText(state.notifyResponseModel[i].type,style: TextStyle(
                                            fontFamily: ArabicFonts.Cairo,
                                            package: 'google_fonts_arabic',
                                            fontSize: ScreenUtil().setSp(14)),),
                                      ],
                                    ),
                                  ),
                                  Align(
                                    alignment: Alignment.bottomLeft,
                                    child: Padding(
                                      padding: EdgeInsets.symmetric(
                                          horizontal: ScreenUtil().setWidth(5),
                                          vertical: ScreenUtil().setHeight(10)),
                                      child: Row(
                                        children: [

                                          Container(
                                            height: ScreenUtil().screenHeight * 0.07,
                                            width: ScreenUtil().screenWidth * 0.4,
                                            color: Colors.white,
                                            child: Container(
                                              decoration: BoxDecoration(
                                                borderRadius: BorderRadius.circular(8.0),
                                              ),
                                              padding: EdgeInsets.symmetric(
                                                  horizontal: ScreenUtil().setWidth(20)),



                                            ),
                                          ),
                                          Container(
                                            height: ScreenUtil().screenHeight * 0.07,
                                            width: ScreenUtil().screenWidth * 0.4,
                                            color: Colors.white,
                                            child: Container(
                                              decoration: BoxDecoration(
                                                borderRadius: BorderRadius.circular(8.0),
                                              ),
                                              padding: EdgeInsets.symmetric(
                                                  horizontal: ScreenUtil().setWidth(20)),

                                              child: InkWell(
                                                onTap: () async {
                                                  List<int> ids=[state.notifyResponseModel[i].id];
                                                  print("ids in state ${state.notification_id}");
                                                  bloc.onChangeIds(ids);
                                                  bloc.onChangeApiToken(token);
                                                  bloc.onSeenNotify();
                                                  setState(() {
                                                    first=true;
                                                  });
                                                },
                                                child: Container(
                                                  decoration: BoxDecoration(
                                                    color: Colors.blueGrey,
                                                    borderRadius: BorderRadius.circular(8.0),
                                                  ),

                                                  width: ScreenUtil().screenWidth,
                                                  child: Center(child: Text('Seen'.tr().toString(),
                                                    style: TextStyle(
                                                      fontSize: ScreenUtil().setSp(14),
                                                      color: Colors.white,
                                                      fontFamily: ArabicFonts.Cairo,
                                                      package: 'google_fonts_arabic',),)),),
                                              ),

                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                    ],),
                  ],
                ),
              );
              return Center(child: CircularProgressIndicator(  valueColor: new AlwaysStoppedAnimation<Color>(Colors.white),),);
            }
        ),
      ),
    );
  }


}