import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'dart:ui' as ui;
import 'package:easy_localization/easy_localization.dart';
import 'package:google_fonts_arabic/fonts.dart';

import 'package:rassed_app/features/profile/component/Personal_Data.dart';
class Setting extends StatefulWidget {


  @override
  _SettingState createState() => _SettingState();
}

class _SettingState extends State<Setting> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        actions: [
          SizedBox(width: ScreenUtil().setWidth(40),
            child: Center(child: TextButton(
              style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all(
                    Colors.blueGrey),
              ),
              onPressed: () {
                Navigator.pop(context);
              },
              child: SvgPicture.asset(
                "assets/icons/close3.svg",width: ScreenUtil().screenWidth*0.1,color: Colors.white,
              ),
            ),),),
        ],
        leading: SizedBox(width: 10,),

        backgroundColor: Colors.blueGrey,
        centerTitle: true,
        title: Text('Setting'.tr().toString(),

          style: TextStyle(
            fontFamily: ArabicFonts.Cairo,
            package: 'google_fonts_arabic',
            fontSize: 20,
          ),),
      ),
      body: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            InkWell(
              onTap: (){
      Navigator.push(context, MaterialPageRoute(builder: (context) =>  PersonalData()));
              },
              child: Container(
                padding: EdgeInsets.symmetric(vertical: ScreenUtil().setHeight(10),horizontal: ScreenUtil().setWidth(20)),
                child: Row(
                  textDirection: ui.TextDirection.rtl,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      textDirection: ui.TextDirection.rtl,
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Center(child: Icon(Icons.person_rounded,size: 30,color:Colors.blueGrey,)),
                        SizedBox(width: ScreenUtil().screenWidth*0.1),
                        Center(
                          child: Text('Personal Data'.tr().toString(),style: TextStyle(
                              fontSize:ScreenUtil()
                                  .setSp(22),
                              fontFamily: ArabicFonts.Cairo,
                              package: 'google_fonts_arabic',
                              color: Colors
                                  .blueGrey),


                          ),
                        ),
                      ],
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Center(child: Icon(Icons.arrow_back_ios_outlined,size: 25,color:Colors.blueGrey ,textDirection: ui.TextDirection.ltr,)),
                    ),
                  ],
                ),
              ),
            ),
            Divider(),
            Container(
              padding: EdgeInsets.symmetric(vertical: ScreenUtil().setHeight(10),horizontal: ScreenUtil().setWidth(20)),
              child: Row(
                textDirection: ui.TextDirection.rtl,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    textDirection: ui.TextDirection.rtl,
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Center(child: Icon(Icons.language,size: 30,color:Colors.blueGrey,)),
                      SizedBox(width: ScreenUtil().screenWidth*0.1),
                      Center(
                        child: Text('language'.tr().toString(),style: TextStyle(
                            fontSize:ScreenUtil()
                                .setSp(22),
                            fontFamily: ArabicFonts.Cairo,
                            package: 'google_fonts_arabic',
                            color: Colors
                                .blueGrey),
                        ),
                      ),
                    ],
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Center(child: Icon(Icons.arrow_back_ios_outlined,size: 25,color:Colors.blueGrey ,textDirection: ui.TextDirection.ltr,)),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
