import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'dart:ui' as ui;

import 'package:google_fonts_arabic/fonts.dart';

class PersonalData extends StatefulWidget {
  @override
  _PersonalDataState createState() => _PersonalDataState();
}

class _PersonalDataState extends State<PersonalData> {
  TextEditingController nameController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController phoneController = TextEditingController();

  //store
  TextEditingController nameStoreController = TextEditingController();
  TextEditingController nameOwnerStoreController = TextEditingController();
  TextEditingController nameWorkerStoreController = TextEditingController();
  TextEditingController emailStoreController = TextEditingController();
  TextEditingController phoneStoreController = TextEditingController();
  TextEditingController locationStoreController = TextEditingController();
 int role=10;
  @override
  void initState() {
    // TODO: implement initState
    nameController.text = 'abdo';
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        actions: [
          SizedBox(
            width: ScreenUtil().setWidth(40),
            child: Center(
              child: TextButton(
                style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.all(Colors.blueGrey),
                ),
                onPressed: () {
                  Navigator.pop(context);
                },
                child: SvgPicture.asset(
                  "assets/icons/close3.svg",
                  width: ScreenUtil().screenWidth * 0.1,
                  color: Colors.white,
                ),
              ),
            ),
          ),
        ],
        leading: SizedBox(width: 5,),
        backgroundColor: Colors.blueGrey,
        centerTitle: true,
        title: Text(
          'البيانات الشخصية',
          textDirection: ui.TextDirection.rtl,
          style: TextStyle(
            fontFamily: ArabicFonts.Cairo,
            package: 'google_fonts_arabic',
            fontSize: 20,
          ),
        ),
      ),
      body: SingleChildScrollView(
        child: role<9?Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            textDirection: ui.TextDirection.rtl,
            children: [
              Padding(
                padding: EdgeInsets.symmetric(
                    vertical: ScreenUtil().setHeight(10),
                    horizontal: ScreenUtil().setWidth(30)),
                child: Text(
                  'الأسم',
                  style: TextStyle(
                      fontFamily: ArabicFonts.Cairo,
                      package: 'google_fonts_arabic',

                      fontSize: ScreenUtil().setSp(18), color: Colors.black),
                ),
              ),
              Container(
                  width: ScreenUtil().screenWidth * 0.95,
                  color: Colors.grey.shade50,
                  padding: EdgeInsets.symmetric(
                      horizontal: ScreenUtil().setWidth(5)),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    textDirection: ui.TextDirection.rtl,
                    children: [
                      Expanded(
                        child: TextField(
                          textDirection: ui.TextDirection.rtl,
                          style: TextStyle(
                            fontSize: ScreenUtil().setSp(14),
                          ),
                          controller: nameController,
                          decoration: InputDecoration(
                              focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Colors.blueGrey, width: 2.0),
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderSide:
                                    BorderSide(color: Colors.grey, width: 2.0),
                              ),
                              suffixIcon: Icon(
                                Icons.person_rounded,
                                size: ScreenUtil().setWidth(20),
                                color: Colors.grey,
                                textDirection: ui.TextDirection.ltr,
                              ),
                              hintTextDirection: ui.TextDirection.rtl,
                              hintText: 'Phone'.toString(),
                              hintStyle: TextStyle(
                                fontSize: ScreenUtil().setSp(14),
                              ),
                              labelStyle: TextStyle(
                                fontSize: ScreenUtil().setSp(14),
                              )),
                          onChanged: (text) {},
                        ),
                      ),
                    ],
                  )),
              Padding(
                padding: EdgeInsets.symmetric(
                    vertical: ScreenUtil().setHeight(10),
                    horizontal: ScreenUtil().setWidth(30)),
                child: Text(
                  'البريد الالكتروني',
                  style: TextStyle(
                      fontFamily: ArabicFonts.Cairo,
                      package: 'google_fonts_arabic',
                      fontSize: ScreenUtil().setSp(18), color: Colors.black),
                ),
              ),
              Container(
                  width: ScreenUtil().screenWidth * 0.95,
                  color: Colors.grey.shade50,
                  padding: EdgeInsets.symmetric(
                      horizontal: ScreenUtil().setWidth(5)),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    textDirection: ui.TextDirection.rtl,
                    children: [
                      Expanded(
                        child: TextField(

                          textDirection: ui.TextDirection.rtl,
                          style: TextStyle(
                            fontSize: ScreenUtil().setSp(14),
                          ),
                          controller: emailController,
                          decoration: InputDecoration(
                              focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Colors.blueGrey, width: 2.0),
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderSide:
                                BorderSide(color: Colors.grey, width: 2.0),
                              ),
                              suffixIcon: Icon(
                                Icons.email,
                                size: ScreenUtil().setWidth(20),
                                color: Colors.grey,
                                textDirection: ui.TextDirection.ltr,
                              ),
                              hintTextDirection: ui.TextDirection.rtl,
                              hintText: 'email@examble.com'.toString(),
                              hintStyle: TextStyle(
                                fontSize: ScreenUtil().setSp(14),
                              ),
                              labelStyle: TextStyle(
                                fontSize: ScreenUtil().setSp(14),
                              )),
                          onChanged: (text) {},
                        ),
                      ),
                    ],
                  )),
              Padding(
                padding: EdgeInsets.symmetric(
                    vertical: ScreenUtil().setHeight(10),
                    horizontal: ScreenUtil().setWidth(30)),
                child: Text(
                  'الرقم',
                  style: TextStyle(
                      fontFamily: ArabicFonts.Cairo,
                      package: 'google_fonts_arabic',
                      fontSize: ScreenUtil().setSp(18), color: Colors.black),
                ),
              ),
              Container(
                  width: ScreenUtil().screenWidth * 0.95,
                  color: Colors.grey.shade50,
                  padding: EdgeInsets.symmetric(
                      horizontal: ScreenUtil().setWidth(5)),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    textDirection: ui.TextDirection.rtl,
                    children: [
                      Expanded(
                        child: TextField(
                          keyboardType: TextInputType.phone,
                          textDirection: ui.TextDirection.rtl,
                          style: TextStyle(
                            fontSize: ScreenUtil().setSp(14),
                          ),
                          controller: phoneController,
                          decoration: InputDecoration(
                              focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Colors.blueGrey, width: 2.0),
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderSide:
                                BorderSide(color: Colors.grey, width: 2.0),
                              ),
                              suffixIcon: Icon(
                                Icons.phone,
                                size: ScreenUtil().setWidth(20),
                                color: Colors.grey,
                                textDirection: ui.TextDirection.ltr,
                              ),
                              hintTextDirection: ui.TextDirection.rtl,
                              hintText: 'Number phone'.toString(),
                              hintStyle: TextStyle(
                                fontSize: ScreenUtil().setSp(14),
                              ),
                              labelStyle: TextStyle(
                                fontSize: ScreenUtil().setSp(14),
                              )),
                          onChanged: (text) {},
                        ),
                      ),
                    ],
                  )),
              SizedBox(
                height: ScreenUtil().screenHeight * 0.02,
              ),
            ]):
        Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            textDirection: ui.TextDirection.rtl,
            children: [
              Padding(
                padding: EdgeInsets.symmetric(
                    vertical: ScreenUtil().setHeight(10),
                    horizontal: ScreenUtil().setWidth(30)),
                child: Text(
                  'اسم المحل',
                  style: TextStyle(
                      fontFamily: ArabicFonts.Cairo,
                      package: 'google_fonts_arabic',

                      fontSize: ScreenUtil().setSp(18), color: Colors.black),
                ),
              ),
              Container(
                  width: ScreenUtil().screenWidth * 0.95,
                  color: Colors.grey.shade50,
                  padding: EdgeInsets.symmetric(
                      horizontal: ScreenUtil().setWidth(5)),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    textDirection: ui.TextDirection.rtl,
                    children: [
                      Expanded(
                        child: TextField(
                          textDirection: ui.TextDirection.rtl,
                          style: TextStyle(
                            fontSize: ScreenUtil().setSp(14),
                          ),
                          controller: nameStoreController,
                          decoration: InputDecoration(
                              focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Colors.blueGrey, width: 2.0),
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderSide:
                                BorderSide(color: Colors.grey, width: 2.0),
                              ),
                              suffixIcon: Icon(
                                Icons.person_rounded,
                                size: ScreenUtil().setWidth(20),
                                color: Colors.grey,

                                textDirection: ui.TextDirection.ltr,
                              ),
                              hintTextDirection: ui.TextDirection.rtl,
                              hintText: 'Store name'.toString(),
                              hintStyle: TextStyle(
                                fontSize: ScreenUtil().setSp(14),
                              ),
                              labelStyle: TextStyle(
                                fontSize: ScreenUtil().setSp(14),
                              )),
                          onChanged: (text) {},
                        ),
                      ),
                    ],
                  )),
              Padding(
                padding: EdgeInsets.symmetric(
                    vertical: ScreenUtil().setHeight(10),
                    horizontal: ScreenUtil().setWidth(30)),
                child: Text(
                  'اسم صاحب المحل',
                  style: TextStyle(
                      fontFamily: ArabicFonts.Cairo,
                      package: 'google_fonts_arabic',

                      fontSize: ScreenUtil().setSp(18), color: Colors.black),
                ),
              ),
              Container(
                  width: ScreenUtil().screenWidth * 0.95,
                  color: Colors.grey.shade50,
                  padding: EdgeInsets.symmetric(
                      horizontal: ScreenUtil().setWidth(5)),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    textDirection: ui.TextDirection.rtl,
                    children: [
                      Expanded(
                        child: TextField(
                          textDirection: ui.TextDirection.rtl,
                          style: TextStyle(
                            fontSize: ScreenUtil().setSp(14),
                          ),
                          controller: nameOwnerStoreController,
                          decoration: InputDecoration(
                              focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Colors.blueGrey, width: 2.0),
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderSide:
                                BorderSide(color: Colors.grey, width: 2.0),
                              ),
                              suffixIcon: Icon(
                                Icons.person_rounded,
                                size: ScreenUtil().setWidth(20),
                                color: Colors.grey,
                                textDirection: ui.TextDirection.ltr,
                              ),
                              hintTextDirection: ui.TextDirection.rtl,
                              hintText: 'Owner name'.toString(),
                              hintStyle: TextStyle(
                                fontSize: ScreenUtil().setSp(14),
                              ),
                              labelStyle: TextStyle(
                                fontSize: ScreenUtil().setSp(14),
                              )),
                          onChanged: (text) {},
                        ),
                      ),
                    ],
                  )),
              Padding(
                padding: EdgeInsets.symmetric(
                    vertical: ScreenUtil().setHeight(10),
                    horizontal: ScreenUtil().setWidth(30)),
                child: Text(
                  'اسم عامل',
                  style: TextStyle(
                      fontFamily: ArabicFonts.Cairo,
                      package: 'google_fonts_arabic',

                      fontSize: ScreenUtil().setSp(18), color: Colors.black),
                ),
              ),
              Container(
                  width: ScreenUtil().screenWidth * 0.95,
                  color: Colors.grey.shade50,
                  padding: EdgeInsets.symmetric(
                      horizontal: ScreenUtil().setWidth(5)),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    textDirection: ui.TextDirection.rtl,
                    children: [
                      Expanded(
                        child: TextField(
                          textDirection: ui.TextDirection.rtl,
                          style: TextStyle(
                            fontSize: ScreenUtil().setSp(14),
                          ),
                          controller: nameWorkerStoreController,
                          decoration: InputDecoration(
                              focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Colors.blueGrey, width: 2.0),
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderSide:
                                BorderSide(color: Colors.grey, width: 2.0),
                              ),
                              suffixIcon: Icon(
                                Icons.person_rounded,
                                size: ScreenUtil().setWidth(20),
                                color: Colors.grey,
                                textDirection: ui.TextDirection.ltr,
                              ),
                              hintTextDirection: ui.TextDirection.rtl,
                              hintText: 'Worker name'.toString(),
                              hintStyle: TextStyle(
                                fontSize: ScreenUtil().setSp(14),
                              ),
                              labelStyle: TextStyle(
                                fontSize: ScreenUtil().setSp(14),
                              )),
                          onChanged: (text) {},
                        ),
                      ),
                    ],
                  )),
              Padding(
                padding: EdgeInsets.symmetric(
                    vertical: ScreenUtil().setHeight(10),
                    horizontal: ScreenUtil().setWidth(30)),
                child: Text(
                  'البريد الالكتروني',
                  style: TextStyle(
                      fontFamily: ArabicFonts.Cairo,
                      package: 'google_fonts_arabic',
                      fontSize: ScreenUtil().setSp(18), color: Colors.black),
                ),
              ),
              Container(
                  width: ScreenUtil().screenWidth * 0.95,
                  color: Colors.grey.shade50,
                  padding: EdgeInsets.symmetric(
                      horizontal: ScreenUtil().setWidth(5)),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    textDirection: ui.TextDirection.rtl,
                    children: [
                      Expanded(
                        child: TextField(

                          textDirection: ui.TextDirection.rtl,
                          style: TextStyle(
                            fontSize: ScreenUtil().setSp(14),
                          ),
                          controller: emailStoreController,
                          decoration: InputDecoration(
                              focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Colors.blueGrey, width: 2.0),
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderSide:
                                BorderSide(color: Colors.grey, width: 2.0),
                              ),
                              suffixIcon: Icon(
                                Icons.email,
                                size: ScreenUtil().setWidth(20),
                                color: Colors.grey,
                                textDirection: ui.TextDirection.ltr,
                              ),
                              hintTextDirection: ui.TextDirection.rtl,
                              hintText: 'email@examble.com'.toString(),
                              hintStyle: TextStyle(
                                fontSize: ScreenUtil().setSp(14),
                              ),
                              labelStyle: TextStyle(
                                fontSize: ScreenUtil().setSp(14),
                              )),
                          onChanged: (text) {},
                        ),
                      ),
                    ],
                  )),
              Padding(
                padding: EdgeInsets.symmetric(
                    vertical: ScreenUtil().setHeight(10),
                    horizontal: ScreenUtil().setWidth(30)),
                child: Text(
                  'الرقم',
                  style: TextStyle(
                      fontFamily: ArabicFonts.Cairo,
                      package: 'google_fonts_arabic',
                      fontSize: ScreenUtil().setSp(18), color: Colors.black),
                ),
              ),
              Container(
                  width: ScreenUtil().screenWidth * 0.95,
                  color: Colors.grey.shade50,
                  padding: EdgeInsets.symmetric(
                      horizontal: ScreenUtil().setWidth(5)),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    textDirection: ui.TextDirection.rtl,
                    children: [
                      Expanded(
                        child: TextField(
                          keyboardType: TextInputType.phone,
                          textDirection: ui.TextDirection.rtl,
                          style: TextStyle(
                            fontSize: ScreenUtil().setSp(14),
                          ),
                          controller: phoneStoreController,
                          decoration: InputDecoration(
                              focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Colors.blueGrey, width: 2.0),
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderSide:
                                BorderSide(color: Colors.grey, width: 2.0),
                              ),
                              suffixIcon: Icon(
                                Icons.phone,
                                size: ScreenUtil().setWidth(20),
                                color: Colors.grey,
                                textDirection: ui.TextDirection.ltr,
                              ),
                              hintTextDirection: ui.TextDirection.rtl,
                              hintText: 'Number phone'.toString(),
                              hintStyle: TextStyle(
                                fontSize: ScreenUtil().setSp(14),
                              ),
                              labelStyle: TextStyle(
                                fontSize: ScreenUtil().setSp(14),
                              )),
                          onChanged: (text) {},
                        ),
                      ),
                    ],
                  )),
              Padding(
                padding: EdgeInsets.symmetric(
                    vertical: ScreenUtil().setHeight(10),
                    horizontal: ScreenUtil().setWidth(30)),
                child: Text(
                  'الموقع',
                  style: TextStyle(
                      fontFamily: ArabicFonts.Cairo,
                      package: 'google_fonts_arabic',
                      fontSize: ScreenUtil().setSp(18), color: Colors.black),
                ),
              ),
              Container(
                  width: ScreenUtil().screenWidth * 0.95,
                  color: Colors.grey.shade50,
                  padding: EdgeInsets.symmetric(
                      horizontal: ScreenUtil().setWidth(5)),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    textDirection: ui.TextDirection.rtl,
                    children: [
                      Expanded(
                        child: TextField(

                          textDirection: ui.TextDirection.rtl,
                          style: TextStyle(
                            fontSize: ScreenUtil().setSp(14),
                          ),
                          controller: locationStoreController,
                          decoration: InputDecoration(
                              focusedBorder: OutlineInputBorder(

                                borderSide: BorderSide(
                                    color: Colors.blueGrey, width: 2.0),

                              ),
                              enabledBorder: OutlineInputBorder(
                                borderSide:
                                BorderSide(color: Colors.grey, width: 2.0),
                              ),
                              suffixIcon: Icon(
                                Icons.location_on,
                                size: ScreenUtil().setWidth(20),
                                color: Colors.grey,
                                textDirection: ui.TextDirection.ltr,
                              ),
                              hintTextDirection: ui.TextDirection.rtl,
                              hintText: 'Add your location'.toString(),
                              hintStyle: TextStyle(
                                fontSize: ScreenUtil().setSp(14),
                                 ),
                              labelStyle: TextStyle(
                                fontSize: ScreenUtil().setSp(14),
                              )),
                          onChanged: (text) {},
                        ),
                      ),
                    ],
                  )),
              SizedBox(
                height: ScreenUtil().screenHeight * 0.1,
              ),
            ]),      ),
    );
  }
}
