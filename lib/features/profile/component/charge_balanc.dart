import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_fonts_arabic/fonts.dart';
import 'package:rassed_app/features/home/home.dart';
import 'package:rassed_app/features/profile/bloc/ProfileBloc.dart';
import 'package:rassed_app/features/profile/bloc/ProfileState.dart';
import 'package:rassed_app/features/settings/Preference.dart';
import 'package:easy_localization/easy_localization.dart';
import '../../../injection.dart';
import 'dart:ui'as ui;
class ChargeBalance extends StatefulWidget {
  const ChargeBalance({Key key}) : super(key: key);

  @override
  _ChargeBalanceState createState() => _ChargeBalanceState();
}

class _ChargeBalanceState extends State<ChargeBalance> {
  ProfileBloc bloc = sl<ProfileBloc>();
  String token;
  bool first=false;
  double balance=0;
 TextEditingController nameController=TextEditingController();
  @override
  void initState() {
    token = Preferences.getUserToken();
    bloc.onChangeApiToken(token);
    first=false;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<ProfileBloc, ProfileState>(
      bloc: bloc,
      listener: (context, state) {
        if(state.isSuccess && first){
          if (state.chargeResponseModel.message.contains("wait for Admin to Make Charge"))
          {
            setState(() {
              first=false;
              nameController.text='0';
            });
            AwesomeDialog(
                width: ScreenUtil()
                    .setWidth(400),
                context: context,
                buttonsTextStyle: TextStyle(
                    fontSize:
                    ScreenUtil()
                        .setSp(20),
                    color: Colors
                        .white),
                dialogType: DialogType.SUCCES,
                animType: AnimType.BOTTOMSLIDE,
                title: "Alert"
                    .toString(),
                desc:
                "wait for Admin to Make Charge".tr().toString()

                    .toString(),
                btnOkOnPress: () {
                  Navigator.push(context,
                    MaterialPageRoute(builder: (context) => Home()),);
                },
                btnOkColor:
                Colors.blueGrey)..show();

          }
        }
      },
      child: Scaffold(
        appBar: AppBar(
          elevation: 0,
          actions: [
            SizedBox(width: ScreenUtil().setWidth(40),
              child: Center(child: TextButton(
                style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.all(
                      Colors.blueGrey),
                ),
                onPressed: () {
                  Navigator.push(context,MaterialPageRoute(builder: (context)=>Home()));
                },
                child: SvgPicture.asset(
                  "assets/icons/close3.svg",
                  width: ScreenUtil().screenWidth * 0.1, color: Colors.white,
                ),
              ),),),
          ],
          leading: SizedBox(width: 10,),

          backgroundColor: Colors.blueGrey,
          centerTitle: true,
          title: Text('Charge Balance'.tr().toString(),

            style: TextStyle(
              fontFamily: ArabicFonts.Cairo,
              package: 'google_fonts_arabic',
              fontSize: 20,
            ),),
        ),
        body: BlocBuilder<ProfileBloc, ProfileState>(
            bloc: bloc,
            builder: (context, state) {
              if (state.isSuccess) {
                return SingleChildScrollView(
                  child: Column (

                   mainAxisAlignment: MainAxisAlignment.start,
                   crossAxisAlignment: CrossAxisAlignment.center,
                   children: [
                    SizedBox(height: ScreenUtil().screenHeight * 0.1,),
                   Padding(
                     padding: EdgeInsets.symmetric(
                         vertical: ScreenUtil().setHeight(10),
                         horizontal: ScreenUtil().setWidth(30)),
                     child: Container(
                       width: ScreenUtil().screenWidth,
                       child: Text(
                         'Charge Balance with value: '.tr().toString(),
                         style: TextStyle(
                             fontFamily: ArabicFonts.Cairo,
                             package: 'google_fonts_arabic',
                             fontSize: ScreenUtil().setSp(16),
                             color: Colors.black),
                       ),
                     ),
                   ),


                     Container(
                         width: ScreenUtil().screenWidth * 0.7,
                         color: Colors.grey.shade50,
                         padding: EdgeInsets.symmetric(
                             horizontal: ScreenUtil().setWidth(30)),
                         child: Row(
                           crossAxisAlignment: CrossAxisAlignment.start,
                           mainAxisAlignment: MainAxisAlignment.start,

                           children: [
                             Expanded(
                               child: TextField(
                                 keyboardType: TextInputType.numberWithOptions(),
                                 style: TextStyle(
                                   fontSize: ScreenUtil().setSp(14),
                                 ),
                                 controller: nameController,
                                 decoration: InputDecoration(
                                     focusedBorder: OutlineInputBorder(
                                       borderSide: BorderSide(
                                           color: Colors.blueGrey,
                                           width: 2.0),
                                     ),
                                     enabledBorder: OutlineInputBorder(
                                       borderSide: BorderSide(
                                           color: Colors.grey, width: 2.0),
                                     ),

                                     hintText: 'Balance'.toString(),
                                     hintStyle: TextStyle(
                                       fontSize: ScreenUtil().setSp(14),
                                     ),
                                     labelStyle: TextStyle(
                                       fontSize: ScreenUtil().setSp(14),
                                     )),
                                 onChanged: (text) {},
                                 onSubmitted: (string) {
                                   FocusScope.of(context)
                                       .requestFocus(new FocusNode());
                                 },
                               ),
                             ),
                           ],
                         )),
                     Padding(
                       padding: EdgeInsets.symmetric(vertical: ScreenUtil().setHeight(20), horizontal: ScreenUtil().setWidth(30)),
                       child: Container(

                         width: ScreenUtil().screenWidth *0.5,

                         child: Container(
                           height: ScreenUtil().screenHeight * 0.07,
                           width: ScreenUtil().screenWidth * 0.4,
                           decoration: BoxDecoration(
                             borderRadius: BorderRadius.circular(8.0),
                           ),
                           padding: EdgeInsets.symmetric(
                               horizontal: ScreenUtil().setWidth(20)),

                           child: InkWell(
                             onTap: ()  {
                               balance=double.parse(nameController.text);
                                bloc.onChangeBalance(balance);
                                setState(() {
                                  first=true;
                                });
                                bloc.onCharge();
                             },
                             child: Container(
                               decoration: BoxDecoration(
                                 color: Colors.blueGrey,
                                 borderRadius: BorderRadius.circular(8.0),
                               ),

                               width: ScreenUtil().screenWidth,
                               child: Center(child: Text('Request'.tr().toString(),
                                 style: TextStyle(
                                   fontSize: ScreenUtil().setSp(14),
                                   color: Colors.white,
                                   fontFamily: ArabicFonts.Cairo,
                                   package: 'google_fonts_arabic',),)),),
                           ),

                         ),
                       ),
                     ),
                      ]),
                );
              }
              return Center(
                child: CircularProgressIndicator(
                  valueColor: new AlwaysStoppedAnimation<Color>(Colors.white),
                ),
              );
            }),
      ),
    );
  }
}
