import 'package:easy_localization/easy_localization.dart';

class Url {
  static const String BaseUrl = "https://pocpal.net/rased/public";
  static const String Signup = '/api/signup';
  static const String verify ='/api/verify';
  static const String home='/api/show_card';
  static const String Get_Banner = '/api/home/banner';
  static const String show_company_card ='/api/show_company_card';
  static const String logout ='/api/logout';
  static const String login ='/api/login';
  static const String getCompany='/api/sendcompany';
  static const String getCategories='/api/sendcategory';
  static const String getCardType='/api/sendtype';
  static const String getCard='/api/sendcard';
  static const String addCompany='/api/addcompany';
  static const String addCardType='/api/addtype';
  static const String addCard='/api/addcard';
  static const String addCardExcel='/api/import';
  static const String editCompany='/api/CompanyEdit';
  static const String sale='/api/sale';
  static const String showInvoice='/api/show_invoice';
  static const String showShop='/api/show_shop';
  static const String profile='/api/profile';
  static const String showInvoiceDetails='/api/show_invoice_card';
  static const String refundAllInvoice='/api/refund';
  static const String notify='/api/notify';
  static const String charge='/api/charge';
  static const String seenNotify='/api/seen';
  static const String notifyRefund='/api/notify_refund';
}

class Er {
  static String Error = "SomethingWentWrong".tr().toString();
  static String NetworkError = "ThereIsNoInternetConnection".tr().toString();
  static String EmailError = "EmailAddressIsNotValid".tr().toString();
}
///
/// The [ErrorCode] enum is responsible of stating all the possible issues that can be retrieved from the API
///
/// Values:
///
/// * [ErrorCode.SERVER_ERROR] indicates that the server returned an undefined error
/// * [ErrorCode.WRONG_INPUT] returned when the server returns error 422 UnProcessable Entity
/// * [ErrorCode.NO_INTERNET_CONNECTION] returned when the device is not connected to a network
/// * [ErrorCode.FORBIDDEN] returned when the server returns 403 Forbidden
/// * [ErrorCode.TIMEOUT] returned when the request has timed out
/// * [ErrorCode.UNAUTHENTICATED] returned when the request returns that the user is unauthenticated
///
///
enum ErrorCode {
  SERVER_ERROR,
  UNAUTHENTICATED,
  TIMEOUT,
  NO_INTERNET_CONNECTION,
  WRONG_INPUT,
  FORBIDDEN,
}