import 'package:flutter/material.dart';

const girlColor = Color(0xffE71D73);
const girlColorLight = Color(0xffFFC9E0);
const boyColor = Color(0xff2B8FCE);
const boyColorLight = Color(0xffB6E1FE);
const girlColorExtraLight = Color(0xffFFECF4);
const boyColorExtraLight = Color(0xffEBF7FC);
