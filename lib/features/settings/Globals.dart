library globals;

import 'package:flutter/material.dart';
import 'package:rassed_app/features/card_details/model/cards_type_model.dart';

String userToken="0";
String lang;
int fontSize = 50;
int category=0;
int role;
bool isFirstTime = true;
ValueNotifier catButtonsVisible;
ValueNotifier homeButtonsVisible;
bool fruitSelected = false;
bool storiesSelected = false;
bool animalsSelected = false;
bool funSelected = false;
bool cultSelected = false;
bool eduSelected = false;
List<CardsTypeResponseModel> cardsBasket=[];