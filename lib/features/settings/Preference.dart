import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';

class Preferences {
  static SharedPreferences preferences;
  static const String KEY_IS_First_Time = 'key_is_first_time';
  static const String KEY_Gender = 'key_gender';
  static const String KEY_Anonymous = 'key_Anonymous';
  static const String KEY_UserToken = 'key_UserToken';
  static const String KEY_Lang = 'key_Lang';

  static init() async {
    preferences = await SharedPreferences.getInstance();
  }

  static void saveIsFirstTime(bool isFirstTime) async {
    String isFirstTimeString = jsonEncode(isFirstTime.toString());
    preferences.setString(KEY_IS_First_Time, isFirstTimeString);
  }

  static bool getIsFirstTime() {
    String isFirstTimeString = preferences.getString(KEY_IS_First_Time);

    if (null == isFirstTimeString) {
      return true;
    }
    isFirstTimeString = jsonDecode(isFirstTimeString);
    if (isFirstTimeString == 'false') {
      return false;
    }
    return true;
  }

  static void saveGender(String gender) async {
    String genderEncoded = jsonEncode(gender);
    preferences.setString(KEY_Gender, genderEncoded);
  }

  static String getGender() {
    String gender = preferences.getString(KEY_Gender);

    if (null == gender) {
      return 'Boy';
    }
    String genderDecoded = jsonDecode(gender);
    return genderDecoded;
  }

  static void saveAnonymous(bool isAnonymous) async {
    String isAnonymousEncoded = jsonEncode(isAnonymous);
    await preferences.setString(KEY_Anonymous, isAnonymousEncoded);
  }

  static bool getAnonymous() {
    String isAnonymous = preferences.getString(KEY_Anonymous);

    if (null == isAnonymous) {
      return true;
    }

    bool isAnonymousbool = jsonDecode(isAnonymous);
    if (!isAnonymousbool) {
      return false;
    }
    return true;
  }

  static void saveUserToken(String userToken) async {
    String userTokenEncoded = jsonEncode(userToken);
    preferences.setString(KEY_UserToken, userTokenEncoded);
  }

  static String getUserToken() {
    String userToken = preferences.getString(KEY_UserToken);
    if (null == userToken) {
      return '0';
    }
    String userTokenDecoded = jsonDecode(userToken);
    return userTokenDecoded;
  }

  static void saveLang(String lang) async {
    String langEncoded = jsonEncode(lang);
    preferences.setString(KEY_Lang, langEncoded);
  }

  static String getLang() {
    String lang = preferences.getString(KEY_Lang);

    if (null == lang) {
      return 'en';
    }
    String langDecoded = jsonDecode(lang);
    return langDecoded;
  }
}
