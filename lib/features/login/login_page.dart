import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:country_code_picker/country_code_picker.dart';
import 'package:flutter/material.dart';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:google_fonts_arabic/fonts.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:rassed_app/features/home/home.dart';
import 'package:rassed_app/features/login/bloc/LoginState.dart';
import 'package:rassed_app/features/settings/Globals.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:rassed_app/features/signup/continue_with_phone.dart';

import '../../injection.dart';

import 'dart:ui' as ui;

import 'bloc/LoginBloc.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  String phoneNumber = "";
  String phoneCode = '+966';
  TextEditingController passwordController = TextEditingController();
  TextEditingController nameController = TextEditingController();
  String password;
  bool requset =false;
  LoginBloc bloc = sl<LoginBloc>();
  @override
  void initState() {
    super.initState();
    nameController.text='+966';
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: SizedBox(),
        title: Text(
          "Login".tr().toString(),
          style: TextStyle(
            fontSize: 18,
            fontFamily: ArabicFonts.Cairo,
            package: 'google_fonts_arabic',
            fontWeight: FontWeight.bold,
            color: Colors.black,
          ),
        ),
        backgroundColor:  Colors.grey.shade50,
        elevation: 0,
        centerTitle: true,
        textTheme: Theme.of(context).textTheme,
      ),
      body: SafeArea(
        child: BlocListener<LoginBloc, LoginState>(
          bloc: bloc,
          listener: (context, state) {
            print("before ${state.reciveResponse}");
            if(state.reciveResponse && !state.isLoading && requset){

              if (state.loginResponseModel.api_token != null&& state.loginResponseModel.api_token.length >5)
              {
                setState(() {
                  requset=false;
                });
                Navigator.pushReplacement(context,
                  MaterialPageRoute(builder: (context) => Home()),);
              }
              else if(requset && state.loginResponseModel.message=='The password field is required.')
              {
                setState(() {
                  requset=false;
                });
                AwesomeDialog(
                    width: ScreenUtil()
                        .setWidth(400),
                    context: context,
                    buttonsTextStyle: TextStyle(
                        fontSize:
                        ScreenUtil()
                            .setSp(fontSize),
                        color: Colors
                            .white),
                    dialogType: DialogType.ERROR,
                    animType: AnimType.BOTTOMSLIDE,
                    title: "Alert"
                        .toString(),
                    desc:
                    "${state.loginResponseModel.message.toString()}"

                        .toString(),
                    btnOkOnPress: () {},
                    btnOkColor:
                    Colors.blueGrey)..show();
              }
              else if(requset && state.loginResponseModel.error=='Unauthorised')
              {
                setState(() {
                  requset=false;
                });
                AwesomeDialog(
                    width: ScreenUtil()
                        .setWidth(400),
                    context: context,
                    buttonsTextStyle: TextStyle(
                        fontSize:
                        ScreenUtil()
                            .setSp(fontSize),
                        color: Colors
                            .white),
                    dialogType: DialogType.ERROR,
                    animType: AnimType.BOTTOMSLIDE,
                    title: "Alert"
                        .toString(),
                    desc:
                    "${state.loginResponseModel.error.toString()}"

                        .toString(),
                    btnOkOnPress: () {},
                    btnOkColor:
                    Colors.blueGrey)..show();
              }
            }

          },
          child: BlocBuilder<LoginBloc,LoginState>(
            bloc: bloc,
             builder: (context, state) {
               return  ModalProgressHUD(
                 inAsyncCall: state.isLoading,
                 child: SingleChildScrollView(
                   child: Column(
                       mainAxisAlignment: MainAxisAlignment.center,
                       crossAxisAlignment: CrossAxisAlignment.center,
                       children: <Widget>[
                         Column(
                           mainAxisAlignment: MainAxisAlignment.center,
                           children: <Widget>[
                             SizedBox(height: ScreenUtil().screenHeight * 0.1,),
                             SizedBox(
                               height: ScreenUtil().screenHeight * 0.2,
                               child: Image.asset('assets/images/holding-phone.png'),
                             ),

                             Container(
                               height: ScreenUtil().screenHeight * 0.35,
                               decoration: BoxDecoration(
                                 color: Colors.white,
                                 borderRadius: BorderRadius.all(
                                   Radius.circular(25),
                                 ),
                               ),
                               child: Padding(
                                 padding: EdgeInsets.all(16),
                                 child: Column(
                                   crossAxisAlignment: CrossAxisAlignment.start,
                                   children: [
                                     Container(
                                         width: ScreenUtil().screenWidth * 0.95,
                                         color: Colors.grey.shade50,
                                         padding: EdgeInsets.symmetric(
                                             horizontal: ScreenUtil().setWidth(5)),
                                         child: Row(
                                           crossAxisAlignment: CrossAxisAlignment
                                               .start,
                                           mainAxisAlignment: MainAxisAlignment
                                               .spaceBetween,
                                           textDirection: ui.TextDirection.rtl,
                                           children: [
                                             Expanded(
                                               child: TextField(
                                                 keyboardType: TextInputType.phone,
                                                 textDirection: ui.TextDirection.ltr,
                                                 style: TextStyle(
                                                   fontSize: ScreenUtil().setSp(14),
                                                   fontFamily: ArabicFonts.Cairo,
                                                   package: 'google_fonts_arabic',
                                                 ),
                                                 controller: nameController,
                                                 decoration: InputDecoration(

                                                     prefixIcon: Icon(
                                                       Icons.phone,
                                                       size: ScreenUtil().setWidth(
                                                           20),
                                                       color: Colors.grey, textDirection: ui.TextDirection.ltr,
                                                     ),
                                                     border: InputBorder.none,
                                                     hintText: 'Phone'.toString(),

                                                     hintStyle: TextStyle(
                                                       fontSize:
                                                       ScreenUtil().setSp(14),
                                                     ),
                                                     labelStyle: TextStyle(
                                                       fontSize:
                                                       ScreenUtil().setSp(14),
                                                     )),
                                                 onChanged: (text) {
                                                   phoneNumber = text;
                                                 },
                                               ),
                                             ),
                                             Expanded(
                                               child: CountryCodePicker(
                                                 onChanged: (value) {
                                                   nameController.text =
                                                       value.dialCode;
                                                 },
                                                 searchStyle: TextStyle(
                                                   fontSize: ScreenUtil().setSp(12),
                                                 ),
                                                 textStyle: TextStyle(
                                                   fontSize: ScreenUtil().setSp(12),
                                                 ),
                                                 dialogTextStyle: TextStyle(
                                                   fontSize: ScreenUtil().setSp(14),
                                                 ),
                                                 // Initial selection and favorite can be one of code ('IT') OR dial_code('+39')
                                                 initialSelection: 'SA',
                                                 textOverflow: TextOverflow.visible,
                                                 favorite: ['+963', 'SA'],
                                                 showFlag: false,
                                                 showFlagDialog: true,
                                                 showFlagMain: true,
                                                 // optional. Shows only country name and flag
                                                 showCountryOnly: false,
                                                 // optional. Shows only country name and flag when popup is closed.
                                                 showOnlyCountryWhenClosed: true,
                                                 // optional. aligns the flag and the Text left
                                                 alignLeft: false,
                                                 showDropDownButton: true,
                                               ),
                                             ),
                                           ],
                                         )),
                                     SizedBox(
                                       height: ScreenUtil().screenHeight * 0.02,
                                     ),

                                     Container(
                                         width: ScreenUtil().screenWidth * 0.95,
                                         color: Colors.grey.shade50,
                                         padding: EdgeInsets.symmetric(
                                             horizontal: ScreenUtil().setWidth(5)),
                                         child: TextField(
                                           textDirection: ui.TextDirection.rtl,
                                           style: TextStyle(
                                             fontSize: ScreenUtil().setSp(14),
                                           ),
                                           controller: passwordController,
                                           decoration: InputDecoration(
                                               prefixIcon: Icon(
                                                 Icons.lock_outlined,
                                                 size: ScreenUtil().setWidth(20),
                                                 color: Colors.grey,
                                               ),
                                               border: InputBorder.none,
                                               hintText: 'Password'.tr().toString(),
                                               hintStyle: TextStyle(
                                                 fontSize: ScreenUtil().setSp(14),
                                               ),
                                               labelStyle: TextStyle(
                                                 fontSize: ScreenUtil().setSp(14),
                                               )),
                                           onChanged: (text) {
                                             password = text;
                                           },
                                         )),

                                     Padding(
                                         padding: EdgeInsets.symmetric(
                                             vertical: 14, horizontal: 0),
                                         child: InkWell(
                                           onTap: (){
                                             Navigator.push(
                                               context,
                                               MaterialPageRoute(
                                                   builder: (context) => ContinueWithPhone()),
                                             );
                                           },
                                           child: RichText(
                                             text: TextSpan(
                                               text: 'Don’t have an account yet? '.tr().toString(),
                                               style: TextStyle(
                                                 fontFamily: ArabicFonts.Cairo,
                                                 package: 'google_fonts_arabic',
                                                 fontSize: ScreenUtil().setSp(12),
                                                 color: Color(0xFF818181),
                                               ),
                                               children:  <TextSpan>[
                                                 TextSpan(
                                                   text: 'Sign up'.tr().toString(),

                                                   style: TextStyle(
                                                     fontFamily: ArabicFonts.Cairo,
                                                     package: 'google_fonts_arabic',
                                                     fontWeight:
                                                     FontWeight.bold,
                                                     color: Colors.blue,),
                                                 ),
                                               ],
                                             ),
                                           ),
                                         )),
                                     Center(
                                       child: Container(
                                         width: ScreenUtil().screenWidth * 0.4,
                                         height: ScreenUtil().screenHeight * 0.08,
                                         child: GestureDetector(
                                           onTap: () {
                                             String phone = nameController.text;
                                             String pass = passwordController.text;
                                             bloc.onChangePhone(phone);
                                             bloc.onChangePassword(pass);
                                             bloc.onLogin();
                                             setState(() {
                                               requset=true;
                                             });

                                           },
                                           child: Container(
                                             decoration: BoxDecoration(
                                               color: Colors.blueGrey,
                                               borderRadius: BorderRadius.all(
                                                 Radius.circular(15),
                                               ),
                                             ),
                                             child: Center(
                                               child: Text(
                                                 "Continue".tr().toString(),
                                                 style: TextStyle(
                                                   fontFamily: ArabicFonts.Cairo,
                                                   package: 'google_fonts_arabic',
                                                   fontSize: ScreenUtil().setSp(14),
                                                   fontWeight: FontWeight.bold,
                                                 ),
                                               ),
                                             ),
                                           ),
                                         ),
                                       ),
                                     ),
                                   ],
                                 ),
                               ),
                             ),
                           ],
                         ),
                       ],
                     ),

                 ),
               );
             }
          ),
        ),),
              );

  }
}
