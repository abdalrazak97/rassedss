import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class LoginEvents extends Equatable {
  @override
  List<Object> get props => [];
}

class LoginEvent extends LoginEvents {}



class ChangePhone extends LoginEvents {
  final phone;
  ChangePhone(this.phone);
}

class ChangePassword extends LoginEvents {
  final password;
  ChangePassword(this.password);
}

class ChangeLanguage extends LoginEvents {
  final lang;
  ChangeLanguage(this.lang);
}

class ResetError extends LoginEvents {}

class Enable extends LoginEvents {}

class Disable extends LoginEvents {}
