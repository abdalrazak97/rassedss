import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:rassed_app/features/login/bloc/LoginEvent.dart';
import 'package:rassed_app/features/login/bloc/LoginState.dart';
import 'package:rassed_app/features/login/model/login_model.dart';
import 'package:rassed_app/features/login/usecase/login_usecase.dart';
const String SERVER_FAILURE_MESSAGE = 'Server Failure';
const String CACHE_FAILURE_MESSAGE = 'Cache Failure';
const String INVALID_INPUT_FAILURE_MESSAGE =
    'Invalid Input - The number must be a positive integer or zero.';

class LoginBloc extends Bloc<LoginEvents, LoginState> {
  final LoginUseCase loginUseCase;

  LoginBloc({this.loginUseCase,}) : super(LoginState.initial());

  void onLogin() {
    add(LoginEvent());
  }


  void onChangePassword(String password) {
    add(ChangePassword(password));
  }
  void onChangePhone(String phone) {
    add(ChangePhone(phone));
  }



  @override
  Stream<LoginState> mapEventToState(LoginEvents event) async* {
    if (event is LoginEvent) {
      yield state.rebuild((b) => b
        ..isLoading = true
        ..isSuccess = false
        ..reciveResponse=false
        ..loginResponseModel= LoginResponseModel(
            message: '',
            api_token: null,
            firstname: '',
            lastname: '',
            balance: 0,
            email: '',
            error: '',
            role: 0
        )
      );
      final result = await loginUseCase(
          LoginParams(
            phone: state.Phone,
            password: state.PassWord
          )
      );

      yield* result.fold((l) async* {
        yield state.rebuild((b) => b
          ..isLoading = false
          ..isSuccess = false
          ..reciveResponse=false
          ..errorMessage = l
        ..loginResponseModel= LoginResponseModel(
            message: '',
            api_token: null,
            firstname: '',
            lastname: '',
            balance: 0,
            error: '',
            email: '',
            role: 0
        ));
      }, (r) async* {
        print('sucesss');
        yield state.rebuild((b) => b
          ..isLoading = false
          ..isSuccess = true
          ..reciveResponse=true
          ..errorMessage = ''
          ..loginResponseModel = r);
      });
      yield state.rebuild((b) => b
        ..isLoading = false
        ..isSuccess = false
        ..reciveResponse=false
        ..errorMessage = ''
        ..loginResponseModel= LoginResponseModel(
            message: '',
            api_token: null,
            firstname: '',
            lastname: '',
            balance: 0,
            error: '',
            email: '',
            role: 0
        ));
    }

    else if (event is ChangePassword) {
      yield state.rebuild((b) => b..PassWord = event.password);
      print('1111');
    }

    else if (event is ChangePhone) {
      print('2222');
      yield state.rebuild((b) => b..Phone = event.phone);

    }

  }
}
