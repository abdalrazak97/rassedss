library LoginState;

import 'package:built_value/built_value.dart';
import 'package:rassed_app/features/login/model/login_model.dart';
part 'LoginState.g.dart';

abstract class LoginState implements Built<LoginState, LoginStateBuilder> {
  bool get isSuccess;
  bool get isLoading;
  bool get reciveResponse;
  String get Email;
  String get code;
  String get PassWord;
  String get Phone;
  String get username;
  String get errorMessage;

  int get role;
  LoginResponseModel get loginResponseModel;

  LoginState._();

  factory LoginState([updates(LoginStateBuilder b)]) = _$LoginState;

  factory LoginState.initial() {
    return LoginState((b) => b
      ..isLoading = false
      ..isSuccess = false
      ..reciveResponse=false
      ..Email = ''
      ..Phone=''
      ..code=''
      ..role=0
      ..username=''
      ..PassWord = ''
      ..errorMessage = ''
      ..loginResponseModel=
      LoginResponseModel(
          message: '',
          error: '',
          api_token: null,
          firstname: '',
          lastname: '',
          balance: 0,
         email: '',
         role: 0
      )
     );
  }
}
