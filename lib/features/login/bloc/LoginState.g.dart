// GENERATED CODE - DO NOT MODIFY BY HAND

part of LoginState;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$LoginState extends LoginState {
  @override
  final bool isSuccess;
  @override
  final bool isLoading;
  @override
  final bool reciveResponse;
  @override
  final String Email;
  @override
  final String code;
  @override
  final String PassWord;
  @override
  final String Phone;
  @override
  final String username;
  @override
  final String errorMessage;
  @override
  final int role;
  @override
  final LoginResponseModel loginResponseModel;

  factory _$LoginState([void Function(LoginStateBuilder) updates]) =>
      (new LoginStateBuilder()..update(updates)).build();

  _$LoginState._(
      {this.isSuccess,
      this.isLoading,
      this.reciveResponse,
      this.Email,
      this.code,
      this.PassWord,
      this.Phone,
      this.username,
      this.errorMessage,
      this.role,
      this.loginResponseModel})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(isSuccess, 'LoginState', 'isSuccess');
    BuiltValueNullFieldError.checkNotNull(isLoading, 'LoginState', 'isLoading');
    BuiltValueNullFieldError.checkNotNull(
        reciveResponse, 'LoginState', 'reciveResponse');
    BuiltValueNullFieldError.checkNotNull(Email, 'LoginState', 'Email');
    BuiltValueNullFieldError.checkNotNull(code, 'LoginState', 'code');
    BuiltValueNullFieldError.checkNotNull(PassWord, 'LoginState', 'PassWord');
    BuiltValueNullFieldError.checkNotNull(Phone, 'LoginState', 'Phone');
    BuiltValueNullFieldError.checkNotNull(username, 'LoginState', 'username');
    BuiltValueNullFieldError.checkNotNull(
        errorMessage, 'LoginState', 'errorMessage');
    BuiltValueNullFieldError.checkNotNull(role, 'LoginState', 'role');
    BuiltValueNullFieldError.checkNotNull(
        loginResponseModel, 'LoginState', 'loginResponseModel');
  }

  @override
  LoginState rebuild(void Function(LoginStateBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  LoginStateBuilder toBuilder() => new LoginStateBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is LoginState &&
        isSuccess == other.isSuccess &&
        isLoading == other.isLoading &&
        reciveResponse == other.reciveResponse &&
        Email == other.Email &&
        code == other.code &&
        PassWord == other.PassWord &&
        Phone == other.Phone &&
        username == other.username &&
        errorMessage == other.errorMessage &&
        role == other.role &&
        loginResponseModel == other.loginResponseModel;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc(
                        $jc(
                            $jc(
                                $jc(
                                    $jc(
                                        $jc($jc(0, isSuccess.hashCode),
                                            isLoading.hashCode),
                                        reciveResponse.hashCode),
                                    Email.hashCode),
                                code.hashCode),
                            PassWord.hashCode),
                        Phone.hashCode),
                    username.hashCode),
                errorMessage.hashCode),
            role.hashCode),
        loginResponseModel.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('LoginState')
          ..add('isSuccess', isSuccess)
          ..add('isLoading', isLoading)
          ..add('reciveResponse', reciveResponse)
          ..add('Email', Email)
          ..add('code', code)
          ..add('PassWord', PassWord)
          ..add('Phone', Phone)
          ..add('username', username)
          ..add('errorMessage', errorMessage)
          ..add('role', role)
          ..add('loginResponseModel', loginResponseModel))
        .toString();
  }
}

class LoginStateBuilder implements Builder<LoginState, LoginStateBuilder> {
  _$LoginState _$v;

  bool _isSuccess;
  bool get isSuccess => _$this._isSuccess;
  set isSuccess(bool isSuccess) => _$this._isSuccess = isSuccess;

  bool _isLoading;
  bool get isLoading => _$this._isLoading;
  set isLoading(bool isLoading) => _$this._isLoading = isLoading;

  bool _reciveResponse;
  bool get reciveResponse => _$this._reciveResponse;
  set reciveResponse(bool reciveResponse) =>
      _$this._reciveResponse = reciveResponse;

  String _Email;
  String get Email => _$this._Email;
  set Email(String Email) => _$this._Email = Email;

  String _code;
  String get code => _$this._code;
  set code(String code) => _$this._code = code;

  String _PassWord;
  String get PassWord => _$this._PassWord;
  set PassWord(String PassWord) => _$this._PassWord = PassWord;

  String _Phone;
  String get Phone => _$this._Phone;
  set Phone(String Phone) => _$this._Phone = Phone;

  String _username;
  String get username => _$this._username;
  set username(String username) => _$this._username = username;

  String _errorMessage;
  String get errorMessage => _$this._errorMessage;
  set errorMessage(String errorMessage) => _$this._errorMessage = errorMessage;

  int _role;
  int get role => _$this._role;
  set role(int role) => _$this._role = role;

  LoginResponseModel _loginResponseModel;
  LoginResponseModel get loginResponseModel => _$this._loginResponseModel;
  set loginResponseModel(LoginResponseModel loginResponseModel) =>
      _$this._loginResponseModel = loginResponseModel;

  LoginStateBuilder();

  LoginStateBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _isSuccess = $v.isSuccess;
      _isLoading = $v.isLoading;
      _reciveResponse = $v.reciveResponse;
      _Email = $v.Email;
      _code = $v.code;
      _PassWord = $v.PassWord;
      _Phone = $v.Phone;
      _username = $v.username;
      _errorMessage = $v.errorMessage;
      _role = $v.role;
      _loginResponseModel = $v.loginResponseModel;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(LoginState other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$LoginState;
  }

  @override
  void update(void Function(LoginStateBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$LoginState build() {
    final _$result = _$v ??
        new _$LoginState._(
            isSuccess: BuiltValueNullFieldError.checkNotNull(
                isSuccess, 'LoginState', 'isSuccess'),
            isLoading: BuiltValueNullFieldError.checkNotNull(
                isLoading, 'LoginState', 'isLoading'),
            reciveResponse: BuiltValueNullFieldError.checkNotNull(
                reciveResponse, 'LoginState', 'reciveResponse'),
            Email: BuiltValueNullFieldError.checkNotNull(
                Email, 'LoginState', 'Email'),
            code: BuiltValueNullFieldError.checkNotNull(
                code, 'LoginState', 'code'),
            PassWord: BuiltValueNullFieldError.checkNotNull(
                PassWord, 'LoginState', 'PassWord'),
            Phone: BuiltValueNullFieldError.checkNotNull(
                Phone, 'LoginState', 'Phone'),
            username: BuiltValueNullFieldError.checkNotNull(
                username, 'LoginState', 'username'),
            errorMessage: BuiltValueNullFieldError.checkNotNull(
                errorMessage, 'LoginState', 'errorMessage'),
            role: BuiltValueNullFieldError.checkNotNull(
                role, 'LoginState', 'role'),
            loginResponseModel: BuiltValueNullFieldError.checkNotNull(loginResponseModel, 'LoginState', 'loginResponseModel'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
