
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:rassed_app/features/Constants/usecase.dart';
import 'package:rassed_app/features/login/api/login_remote.dart';
import 'package:rassed_app/features/login/model/login_model.dart';


class LoginUseCase extends UseCase<LoginResponseModel, LoginParams> {
  final LoginRemoteDataSource loginApi;
  LoginUseCase({this.loginApi});

  @override
  Future<Either<String, LoginResponseModel>> call(LoginParams params) async {
    return await loginApi.login( params);

  }
}

class LoginParams extends Equatable {
  final String phone;
  final String password;
  LoginParams(
      {@required this.phone,@required this.password});

  @override
  // TODO: implement props
  List<Object> get props => [phone,password];
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['phone'] = this.phone;
    data['password'] = this.password;
    return data;
  }
}
