import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:rassed_app/core/notifications/notifications.dart';
import 'package:rassed_app/features/Constants/ApiConstants.dart';
import 'package:dartz/dartz.dart';
import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:dio/dio.dart';
import 'package:meta/meta.dart';
import 'package:rassed_app/features/login/model/login_model.dart';
import 'package:rassed_app/features/login/usecase/login_usecase.dart';
import 'package:rassed_app/features/settings/Globals.dart';
import 'package:rassed_app/features/settings/Preference.dart';

abstract class LoginRemoteDataSource {
  Future<Either<String, LoginResponseModel>> login(LoginParams params);
}

class LoginRemoteDataSourceImpl extends LoginRemoteDataSource {
  final Dio dio;
  final DataConnectionChecker networkInfo;
  LoginRemoteDataSourceImpl({@required this.dio, @required this.networkInfo});

  @override
  Future<Either<String, LoginResponseModel>> login(LoginParams params) async {
    LoginResponseModel user;

    if (await networkInfo.hasConnection) {
      try {
        print(params.phone);
        final result = await dio.post(
          Url.login,
          data: params.toJson(),
          options: Options(

            followRedirects: false,
            validateStatus: (status) {
              return status < 500;
            },
          ),
        );
        //tokenSave
        print("**********************");
        print(params.phone);
        print("**********************");
        user = LoginResponseModel.fromJson(json.decode(result.data));
        role=user.role;
        Preferences.saveUserToken(user.api_token.toString());
        return Right(user);
      } on DioError catch (ex) {
        if (ex.type == DioErrorType.connectTimeout) {
          return Left(Er.NetworkError);
        } else if (ex.type == DioErrorType.receiveTimeout) {
          return Left(Er.NetworkError);
        } else if (user != null)
          return Left(user.message);
        else if (user == null) return Left(Er.Error);
      }
    } else {
      return Left(Er.NetworkError);
    }
  }
}
