// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'login_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LoginResponseModel _$LoginResponseModelFromJson(Map<String, dynamic> json) {
  return LoginResponseModel(
    api_token: json['api_token'] as String,
    firstname: json['firstname'] as String,
    lastname: json['lastname'] as String,
    balance: (json['balance'] as num)?.toDouble(),
    phone: json['phone'] as String,
    email: json['email'] as String,
    role: json['role'] as int,
    message: json['message'] as String,
    error: json['error'] as String,
  );
}

Map<String, dynamic> _$LoginResponseModelToJson(LoginResponseModel instance) =>
    <String, dynamic>{
      'api_token': instance.api_token,
      'firstname': instance.firstname,
      'lastname': instance.lastname,
      'balance': instance.balance,
      'phone': instance.phone,
      'email': instance.email,
      'role': instance.role,
      'message': instance.message,
      'error': instance.error,
    };
