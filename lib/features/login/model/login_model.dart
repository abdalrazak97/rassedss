import 'package:json_annotation/json_annotation.dart';



part 'login_model.g.dart';

@JsonSerializable()
class LoginResponseModel {
  final String api_token;
  final String firstname;
  final String lastname;
  final double balance;
  final String phone;
  final String email;
  final int role;
  final String message;
  final String error;



  Map<String, dynamic> toJson() => _$LoginResponseModelToJson(this);
  factory LoginResponseModel.fromJson(Map<String, dynamic> json) =>
      _$LoginResponseModelFromJson(json);

  LoginResponseModel({this.api_token, this.firstname, this.lastname,
      this.balance, this.phone, this.email, this.role, this.message,this.error});
}
