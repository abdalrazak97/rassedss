// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'cards_basket_model.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class CardBasketAdapter extends TypeAdapter<CardBasket> {
  @override
  final int typeId = 0;

  @override
  CardBasket read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return CardBasket()
      ..name = fields[0] as String
      ..price = fields[1] as double
      ..count = fields[2] as int
      ..image = fields[3] as String
      ..cardId = fields[4] as int
      ..isAdd = fields[5] as bool
      ..maxCount = fields[6] as int;
  }

  @override
  void write(BinaryWriter writer, CardBasket obj) {
    writer
      ..writeByte(7)
      ..writeByte(0)
      ..write(obj.name)
      ..writeByte(1)
      ..write(obj.price)
      ..writeByte(2)
      ..write(obj.count)
      ..writeByte(3)
      ..write(obj.image)
      ..writeByte(4)
      ..write(obj.cardId)
      ..writeByte(5)
      ..write(obj.isAdd)
      ..writeByte(6)
      ..write(obj.maxCount);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is CardBasketAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
