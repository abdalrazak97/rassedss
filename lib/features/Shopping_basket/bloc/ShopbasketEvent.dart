import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class ShopBasketEvents extends Equatable {
  @override
  List<Object> get props => [];
}

class SaleEvent extends ShopBasketEvents {}


class ChangeApiToken extends ShopBasketEvents {
  final api_token;
  ChangeApiToken(this.api_token);
}


class ChangeIdsList extends ShopBasketEvents {
  final ids;
  ChangeIdsList(this.ids);
}


