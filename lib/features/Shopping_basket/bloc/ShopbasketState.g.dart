// GENERATED CODE - DO NOT MODIFY BY HAND

part of ProfileState;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$ShopBasketState extends ShopBasketState {
  @override
  final bool isSuccess;
  @override
  final bool isLoading;
  @override
  final String api_token;
  @override
  final String errorMessage;
  @override
  final List<idsClassModel> ids;
  @override
  final ShopBasketResponseModel shopBasketResponseModel;

  factory _$ShopBasketState([void Function(ShopBasketStateBuilder) updates]) =>
      (new ShopBasketStateBuilder()..update(updates)).build();

  _$ShopBasketState._(
      {this.isSuccess,
      this.isLoading,
      this.api_token,
      this.errorMessage,
      this.ids,
      this.shopBasketResponseModel})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        isSuccess, 'ShopBasketState', 'isSuccess');
    BuiltValueNullFieldError.checkNotNull(
        isLoading, 'ShopBasketState', 'isLoading');
    BuiltValueNullFieldError.checkNotNull(
        api_token, 'ShopBasketState', 'api_token');
    BuiltValueNullFieldError.checkNotNull(
        errorMessage, 'ShopBasketState', 'errorMessage');
    BuiltValueNullFieldError.checkNotNull(ids, 'ShopBasketState', 'ids');
    BuiltValueNullFieldError.checkNotNull(
        shopBasketResponseModel, 'ShopBasketState', 'shopBasketResponseModel');
  }

  @override
  ShopBasketState rebuild(void Function(ShopBasketStateBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ShopBasketStateBuilder toBuilder() =>
      new ShopBasketStateBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ShopBasketState &&
        isSuccess == other.isSuccess &&
        isLoading == other.isLoading &&
        api_token == other.api_token &&
        errorMessage == other.errorMessage &&
        ids == other.ids &&
        shopBasketResponseModel == other.shopBasketResponseModel;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc($jc($jc(0, isSuccess.hashCode), isLoading.hashCode),
                    api_token.hashCode),
                errorMessage.hashCode),
            ids.hashCode),
        shopBasketResponseModel.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('ShopBasketState')
          ..add('isSuccess', isSuccess)
          ..add('isLoading', isLoading)
          ..add('api_token', api_token)
          ..add('errorMessage', errorMessage)
          ..add('ids', ids)
          ..add('shopBasketResponseModel', shopBasketResponseModel))
        .toString();
  }
}

class ShopBasketStateBuilder
    implements Builder<ShopBasketState, ShopBasketStateBuilder> {
  _$ShopBasketState _$v;

  bool _isSuccess;
  bool get isSuccess => _$this._isSuccess;
  set isSuccess(bool isSuccess) => _$this._isSuccess = isSuccess;

  bool _isLoading;
  bool get isLoading => _$this._isLoading;
  set isLoading(bool isLoading) => _$this._isLoading = isLoading;

  String _api_token;
  String get api_token => _$this._api_token;
  set api_token(String api_token) => _$this._api_token = api_token;

  String _errorMessage;
  String get errorMessage => _$this._errorMessage;
  set errorMessage(String errorMessage) => _$this._errorMessage = errorMessage;

  List<idsClassModel> _ids;
  List<idsClassModel> get ids => _$this._ids;
  set ids(List<idsClassModel> ids) => _$this._ids = ids;

  ShopBasketResponseModel _shopBasketResponseModel;
  ShopBasketResponseModel get shopBasketResponseModel =>
      _$this._shopBasketResponseModel;
  set shopBasketResponseModel(
          ShopBasketResponseModel shopBasketResponseModel) =>
      _$this._shopBasketResponseModel = shopBasketResponseModel;

  ShopBasketStateBuilder();

  ShopBasketStateBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _isSuccess = $v.isSuccess;
      _isLoading = $v.isLoading;
      _api_token = $v.api_token;
      _errorMessage = $v.errorMessage;
      _ids = $v.ids;
      _shopBasketResponseModel = $v.shopBasketResponseModel;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ShopBasketState other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$ShopBasketState;
  }

  @override
  void update(void Function(ShopBasketStateBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$ShopBasketState build() {
    final _$result = _$v ??
        new _$ShopBasketState._(
            isSuccess: BuiltValueNullFieldError.checkNotNull(
                isSuccess, 'ShopBasketState', 'isSuccess'),
            isLoading: BuiltValueNullFieldError.checkNotNull(
                isLoading, 'ShopBasketState', 'isLoading'),
            api_token: BuiltValueNullFieldError.checkNotNull(
                api_token, 'ShopBasketState', 'api_token'),
            errorMessage: BuiltValueNullFieldError.checkNotNull(
                errorMessage, 'ShopBasketState', 'errorMessage'),
            ids: BuiltValueNullFieldError.checkNotNull(
                ids, 'ShopBasketState', 'ids'),
            shopBasketResponseModel: BuiltValueNullFieldError.checkNotNull(
                shopBasketResponseModel,
                'ShopBasketState',
                'shopBasketResponseModel'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
