library ProfileState;

import 'package:built_value/built_value.dart';
import 'package:rassed_app/features/Shopping_basket/model/shop_basket_model.dart';
import 'package:rassed_app/features/Shopping_basket/model/Ids_class_model.dart';

 part 'ShopbasketState.g.dart';

abstract class ShopBasketState implements Built<ShopBasketState, ShopBasketStateBuilder> {
  bool get isSuccess;
  bool get isLoading;
  String get api_token;
  String get errorMessage;
  List<idsClassModel> get ids;
  ShopBasketResponseModel get shopBasketResponseModel;

  ShopBasketState._();

  factory ShopBasketState([updates(ShopBasketStateBuilder b)]) = _$ShopBasketState;

  factory ShopBasketState.initial() {
    return ShopBasketState((b) => b
      ..isLoading = false
      ..isSuccess = false
      ..api_token='0'
      ..ids=[]
      ..errorMessage = ''
      ..shopBasketResponseModel=
      ShopBasketResponseModel(
        message: '',
      )
    );
  }
}
