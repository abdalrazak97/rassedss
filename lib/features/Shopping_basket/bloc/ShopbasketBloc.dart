import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:rassed_app/features/Shopping_basket/model/Ids_class_model.dart';
import '../usecase/shop_basket_usecase.dart';
import '../bloc/ShopbasketEvent.dart';
import '../bloc/ShopbasketState.dart';
const String SERVER_FAILURE_MESSAGE = 'Server Failure';
const String CACHE_FAILURE_MESSAGE = 'Cache Failure';
const String INVALID_INPUT_FAILURE_MESSAGE = 'Invalid Input - The number must be a positive integer or zero.';

class ShopBasketBloc extends Bloc<ShopBasketEvents, ShopBasketState> {
  final ShopBasketUseCase shopBasketUseCase;

  ShopBasketBloc({this.shopBasketUseCase,}) : super(ShopBasketState.initial());

  void onSaleEvent() {
    add(SaleEvent());
  }


  void onChangeApiToken(String api_token) {
    add(ChangeApiToken(api_token));
  }

  void onChangeIdsList(List<idsClassModel> ids) {
    add(ChangeIdsList(ids));
  }


  @override
  Stream<ShopBasketState> mapEventToState(ShopBasketEvents event) async* {
    if (event is SaleEvent) {
      yield state.rebuild((b) => b
        ..isLoading = true
        ..isSuccess = false);
      final result = await shopBasketUseCase(
          SaleParams(
              api_token: state.api_token,
              ids:state.ids,
          )
      );

      yield* result.fold((l) async* {
        yield state.rebuild((b) => b
          ..isLoading = false
          ..isSuccess = false
          ..errorMessage = l);
      }, (r) async* {
        print('sucesss');
        yield state.rebuild((b) => b
          ..isLoading = false
          ..isSuccess = true
          ..errorMessage = ''
          ..shopBasketResponseModel = r);
      });
    }

    else if (event is ChangeApiToken) {
      yield state.rebuild((b) => b..api_token = event.api_token);
      print('1111');
    }
    else if (event is ChangeIdsList) {
      yield state.rebuild((b) => b..ids = event.ids);
      print('222');
    }


  }
}
