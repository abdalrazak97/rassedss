import 'dart:convert';

import 'package:json_annotation/json_annotation.dart';




part 'shop_basket_model.g.dart';

@JsonSerializable()
class ShopBasketResponseModel {

  final bool success;
  final String message;


  Map<String, dynamic> toJson() => _$ShopBasketResponseModelToJson(this);
  factory ShopBasketResponseModel.fromJson(Map<String, dynamic> json) =>
      _$ShopBasketResponseModelFromJson(json);

  ShopBasketResponseModel({this.success, this.message,});
}
