import 'package:json_annotation/json_annotation.dart';




part 'Ids_class_model.g.dart';

@JsonSerializable()
class idsClassModel{
  int id;
  int count;
  Map<String, dynamic> toJson() => _$idsClassModelToJson(this);
  factory idsClassModel.fromJson(Map<String, dynamic> json) =>
      _$idsClassModelFromJson(json);
  idsClassModel({this.id, this.count});
}
