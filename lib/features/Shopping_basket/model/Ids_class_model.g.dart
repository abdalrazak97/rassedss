// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Ids_class_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

idsClassModel _$idsClassModelFromJson(Map<String, dynamic> json) {
  return idsClassModel(
    id: json['id'] as int,
    count: json['count'] as int,
  );
}

Map<String, dynamic> _$idsClassModelToJson(idsClassModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'count': instance.count,
    };
