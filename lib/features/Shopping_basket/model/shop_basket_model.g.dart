// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'shop_basket_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ShopBasketResponseModel _$ShopBasketResponseModelFromJson(
    Map<String, dynamic> json) {
  return ShopBasketResponseModel(
    success: json['success'] as bool,
    message: json['message'] as String,
  );
}

Map<String, dynamic> _$ShopBasketResponseModelToJson(
        ShopBasketResponseModel instance) =>
    <String, dynamic>{
      'success': instance.success,
      'message': instance.message,
    };
