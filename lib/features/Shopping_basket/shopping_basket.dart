import 'package:easy_localization/easy_localization.dart';
import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:google_fonts_arabic/fonts.dart';
import 'dart:ui' as ui;
import 'package:hive_flutter/hive_flutter.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:hive/hive.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:rassed_app/core/utils/Boxs.dart';
import 'package:rassed_app/features/Shopping_basket/bloc/ShopbasketBloc.dart';
import 'package:rassed_app/features/Shopping_basket/bloc/ShopbasketState.dart';
import 'package:rassed_app/features/home/home.dart';
import 'package:rassed_app/features/Shopping_basket/model/Ids_class_model.dart';
import 'package:rassed_app/features/profile/bloc/ProfileBloc.dart';
import 'package:rassed_app/features/profile/bloc/ProfileState.dart';
import 'package:rassed_app/features/settings/Preference.dart';

import '../../injection.dart';
import 'cards_basket_model.dart';
class ShoppingBasket extends StatefulWidget {

  @override
  _ShoppingBasketState createState() => _ShoppingBasketState();

}

List<double> sum=[0];
List<int> totalCount=[0];
double Total=0;
class _ShoppingBasketState extends State<ShoppingBasket> {
  int numberCards=0;

  bool first =true;
  ProfileBloc bloc = sl<ProfileBloc>();
  ShopBasketBloc blocSale = sl<ShopBasketBloc>();
  List<idsClassModel> ids=[];
  List<CardBasket> card=[];
  String Token;
  @override
  void initState() {
    Token=Preferences.getUserToken();
    bloc.onChangeApiToken(Token);
    bloc.onUserProfile();
    editTotal();
    super.initState();
  }
  @override
  Widget build(BuildContext context) {

    return BlocListener<ShopBasketBloc, ShopBasketState>(
      bloc: blocSale,
      listener: (context, state) {
        if(state.isSuccess  && state.shopBasketResponseModel.success && first){
          if (state.shopBasketResponseModel.message.contains("Sale Completed And Your Balance:"))
            {
              setState(() {
                first=false;
              });

              AwesomeDialog(
                  width: ScreenUtil()
                      .setWidth(400),
                  context: context,
                  buttonsTextStyle: TextStyle(
                      fontSize:
                      ScreenUtil()
                          .setSp(20),
                      color: Colors
                          .white),
                  dialogType: DialogType.SUCCES,
                  animType: AnimType.BOTTOMSLIDE,
                  title: "Alert"
                      .toString(),
                  desc:
                  "${state.shopBasketResponseModel.message.toString()}"

                      .toString(),
                  btnOkOnPress: () {
                    Navigator.push(context,
                      MaterialPageRoute(builder: (context) => Home()),);
                  },
                  btnOkColor:
                  Colors.blueGrey)..show();

              final box = Boxes.getTransactions();
              box.clear();

            }

        }
        else if (state.shopBasketResponseModel.message.contains("Your Balance is less than The Invoice Price")){
          AwesomeDialog(
              width: ScreenUtil()
                  .setWidth(400),
              context: context,
              buttonsTextStyle: TextStyle(
                  fontSize:
                  ScreenUtil()
                      .setSp(20),
                  color: Colors
                      .white),
              dialogType: DialogType.SUCCES,
              animType: AnimType.BOTTOMSLIDE,
              title: "Alert"
                  .toString(),
              desc:
              "${state.shopBasketResponseModel.message.toString()}"

                  .toString(),
              btnOkOnPress: () {

              },
              btnOkColor:
              Colors.blueGrey)..show();
        }
      },
      child:BlocBuilder<ShopBasketBloc, ShopBasketState>(
          bloc: blocSale,
          builder: (context, state) {
            return BlocBuilder<ProfileBloc, ProfileState>(
                bloc: bloc,
                builder: (context, state2) {
                  return Scaffold(
                    appBar: AppBar(
                      centerTitle: true,
                      elevation: 0,
                      leading: SizedBox(),
                      actions: [
                        SizedBox(
                          width: ScreenUtil().setWidth(40),
                          child: Center(
                            child: TextButton(
                              style: ButtonStyle(
                                backgroundColor:
                                MaterialStateProperty.all(Colors.blueGrey),
                              ),
                              onPressed: () {
                                Navigator.push(context, MaterialPageRoute(
                                    builder: (context) => Home()));
                              },
                              child: SvgPicture.asset(
                                "assets/icons/close3.svg",
                                width: ScreenUtil().screenWidth * 0.1,
                                color: Colors.white,
                              ),
                            ),
                          ),
                        ),
                      ],
                      title: Text('Shop Cart'.tr().toString(),

                        style: TextStyle(
                          fontFamily: ArabicFonts.Cairo,
                          package: 'google_fonts_arabic',
                          fontSize: ScreenUtil().setSp(18),
                        ),),
                    ),
                    body: ModalProgressHUD(
                      inAsyncCall: state.isLoading,
                      child: Stack(
                          children: [
                            Padding(
                              padding: EdgeInsets.symmetric(horizontal:ScreenUtil().setWidth(30),vertical: ScreenUtil().setHeight(5)),
                              child: Row(children: [
                                Text('Balance'.tr().toString()+":  ",
                                  style: TextStyle(
                                    fontSize: ScreenUtil().setSp(22),
                                    color: Colors.black,
                                    fontFamily: ArabicFonts.Cairo,
                                    package: 'google_fonts_arabic',),),
                                Text(state2.userProfileResponseModel.balance.toString(),
                                  style: TextStyle(
                                    fontSize: ScreenUtil().setSp(20),
                                    color: Colors.black,
                                    fontFamily: ArabicFonts.Cairo,
                                    package: 'google_fonts_arabic',),)
                              ],),
                            ),
                            ValueListenableBuilder<Box<CardBasket>>(
                              valueListenable: Boxes.getTransactions().listenable(),
                              builder: (context, box, _,) {
                                final Cards = box.values.toList().cast<CardBasket>();
                                return buildContent(Cards);
                              },
                            ),
                            Align(
                              alignment: Alignment.bottomLeft,
                              child: Container(
                                color: Colors.white,
                                child: Container(
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(8.0),
                                  ),
                                  padding: EdgeInsets.symmetric(
                                      horizontal: ScreenUtil().setWidth(20)),
                                  height: ScreenUtil().screenHeight * 0.1,
                                  width: ScreenUtil().screenWidth * 0.65,
                                  child: InkWell(
                                    onTap: () async {
                                      final box = Boxes.getTransactions();
                                      card = box.values.toList();
                                      ids = [];
                                      for (int i = 0; i < card.length; i++)
                                        ids.add(idsClassModel(id: card[i].cardId, count: card[i].count));
                                      print(ids);
                                      blocSale.onChangeApiToken(Token);
                                      blocSale.onChangeIdsList(ids);
                                      blocSale.onSaleEvent();
                                    },
                                    child: Container(
                                      decoration: BoxDecoration(
                                        color: Colors.blueGrey,
                                        borderRadius: BorderRadius.circular(8.0),
                                      ),

                                      width: ScreenUtil().screenWidth,
                                      child: Center(child: Text('Buy'.tr().toString(),
                                        style: TextStyle(
                                            fontSize: ScreenUtil().setSp(22),
                                            color: Colors.white,
                                          fontFamily: ArabicFonts.Cairo,
                                          package: 'google_fonts_arabic',),)),),
                                  ),

                                ),
                              ),
                            ),
                            Align(
                              alignment: Alignment.bottomRight,
                              child: Container(
                                color: Colors.white,
                                height: ScreenUtil().screenHeight * 0.1,
                                width: ScreenUtil().screenWidth * 0.4,
                                child: Column(
                                  children: [
                                    Text('Total amount'.tr().toString(),
                                      style: TextStyle(fontSize: ScreenUtil()
                                          .setSp(22),
                                          color: Colors.black,
                                        fontFamily: ArabicFonts.Cairo,
                                        package: 'google_fonts_arabic',),),
                                    Text('${Total}', style: TextStyle(fontSize: ScreenUtil()
                                        .setSp(14),
                              color: Colors.black,
                              fontFamily: ArabicFonts.Cairo,
                              package: 'google_fonts_arabic',),),
                                  ],
                                ),

                              ),
                            ),
                          ]
                      ),
                    ),
                  );
                }
            );
          }
      ),
    );
  }
  Widget buildContent(List<CardBasket> Cards,) {
    if (Cards.isEmpty) {
      return Center(
        child: Text(
          'No Cards yet!'.tr().toString(),
          style: TextStyle(fontSize: 24 ,fontFamily: ArabicFonts.Cairo,
            package: 'google_fonts_arabic',),
        ),
      );
    } else {


      return Column(
        children: [
          SizedBox(height: 50),
          Expanded(
            child: ListView.builder(
              padding: EdgeInsets.symmetric(vertical: 5,horizontal: 8),
              itemCount: Cards.length,
              itemBuilder: (BuildContext context, int index) {
                final transaction = Cards[index];
                return buildTransaction(context, transaction,);
              },
            ),
          ),
          SizedBox(height: ScreenUtil().screenHeight*0.1),
        ],
      );
    }
  }
  Widget buildTransaction(BuildContext context, CardBasket cardBasket,)
  {
    final name = cardBasket.name;
    final price = cardBasket.price;
    var count = cardBasket.count;
    final cardId=cardBasket.cardId;
    var image=cardBasket.image;
    var maxCount =cardBasket.maxCount;

    return Card(
      color: Colors.grey.shade50,
      child: Row(
        children: [
          SizedBox(width: ScreenUtil().screenWidth*0.25,
          height: ScreenUtil().screenHeight*0.1,
          child: Image.network(image,fit: BoxFit.cover,),),
          SizedBox(width:  ScreenUtil().screenWidth*0.25,child:
            Column(
              children: [
                Text(
                  name,
                  maxLines: 2,
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
                ),
                Text(price.toString()),
              ],
            ),),
          SizedBox(width:  ScreenUtil().screenWidth*0.4,
            child:Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(0)),
                  child: SizedBox(
                    width: ScreenUtil().screenWidth * 0.1,
                    child: TextButton(
                      style: ButtonStyle(

                      ),
                      // foreground
                      onPressed: () {
                        if(count==0){
                          deleteTransaction(cardBasket);
                          editTotal();
                        }
                        else
                        {
                          count--;
                          if(count==0)
                          {
                            deleteTransaction(cardBasket);
                            editTotal();
                          }

                          editTransaction(cardBasket, count);
                          editTotal();

                        }


                      },
                      child:Icon(Icons.remove,size: ScreenUtil().setWidth(20),color: Colors.black,),
                    ),
                  ),
                ),
                SizedBox(
                    width: ScreenUtil().screenWidth * 0.1,
                    child: Center(child: Text(count.toString(),style: TextStyle(fontSize: ScreenUtil().setSp(16),color: Colors.black,fontFamily: 'Raleway-Regular'),))),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(0)),
                  child: SizedBox(
                    width: ScreenUtil().screenWidth * 0.1,
                    child: TextButton(
                        style: ButtonStyle(),
                        // foreground
                        onPressed: () {
                          if(maxCount>count)
                          {
                            count++;
                            editTransaction(cardBasket, count,);
                            editTotal();
                          }
                          else
                          {
                            ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                              backgroundColor: Colors.redAccent,
                              content: Text("No more Cards".tr().toString(),style: TextStyle(
                                color: Colors
                                    .white,
                                fontWeight:
                                FontWeight
                                    .w500,
                                fontFamily: ArabicFonts.Cairo,
                                package: 'google_fonts_arabic',
                                fontSize: ScreenUtil().setSp(18),
                              ),
                            )));
                          }


                        },
                        child:Icon(Icons.add,size: ScreenUtil().setWidth(20),color: Colors.black,)
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(0)),
                  child: SizedBox(
                    width: ScreenUtil().screenWidth * 0.1,
                    child: IconButton(
                        icon:  Icon(Icons.delete,size: ScreenUtil().setWidth(20),color: Colors.red,),
                      onPressed: (){
                        deleteTransaction(cardBasket);
                        editTotal();
                      },
                    ),
                  ),
                ),
              ],
            ),),
        ],

      ),
    );
  }
  void editTotal() {
    final box = Boxes.getTransactions();
    final list = box.values.toList();
    List<double>sum=[];
    List<int>totalCount=[];
    Total=0;
    if(list.isEmpty)
      setState(() {
        Total=0;
      });
    for(int i=0;i<list.length;i++)
    {
        sum.add(list[i].price);
        totalCount.add(list[i].count);

    }
    for(int i=0;i<list.length;i++)
     Total+=sum[i]*totalCount[i];
    setState(() {
      Total;
    });
  }
 }


void editTransaction(CardBasket card, int count,) {
  card.count = count;
  card.save();
}

void deleteTransaction(CardBasket card) {

  card.delete();
}

