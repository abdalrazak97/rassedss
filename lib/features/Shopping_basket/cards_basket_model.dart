import 'package:hive/hive.dart';

part 'cards_basket_model.g.dart';

@HiveType(typeId: 0)
class CardBasket extends HiveObject {
  @HiveField(0)
   String name;

  @HiveField(1)
   double price;

  @HiveField(2)
   int count;

  @HiveField(3)
   String image;

  @HiveField(4)
  int cardId;

  @HiveField(5)
  bool isAdd=false;

  @HiveField(6)
  int maxCount;

}
