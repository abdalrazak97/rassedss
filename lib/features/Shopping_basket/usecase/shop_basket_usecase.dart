
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:rassed_app/features/Constants/usecase.dart';
import 'package:rassed_app/features/Shopping_basket/api/shop_basket_remote.dart';
import 'package:rassed_app/features/Shopping_basket/model/shop_basket_model.dart';

import '../model/Ids_class_model.dart';


class ShopBasketUseCase extends UseCase<ShopBasketResponseModel, SaleParams> {
  final ShopBasketRemoteDataSource shopBasketRemoteDataSource;

  ShopBasketUseCase({this.shopBasketRemoteDataSource});

  @override
  Future<Either<String, ShopBasketResponseModel>> call(SaleParams params) async {
    return await shopBasketRemoteDataSource.sale(params);

  }
}

class SaleParams extends Equatable {
  final String api_token;
  final List<idsClassModel> ids;

  SaleParams(
      {@required this.api_token,this.ids});

  @override
  // TODO: implement props
  List<Object> get props => [api_token];
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['api_token'] = this.api_token;
    data['ids'] = this.ids;
    return data;
  }
}
