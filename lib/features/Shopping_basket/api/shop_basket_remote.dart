import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:rassed_app/features/Constants/ApiConstants.dart';
import 'package:dartz/dartz.dart';
import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:dio/dio.dart';
import 'package:meta/meta.dart';
import 'package:rassed_app/features/Shopping_basket/model/shop_basket_model.dart';
import 'package:rassed_app/features/Shopping_basket/usecase/shop_basket_usecase.dart';

abstract class ShopBasketRemoteDataSource {
  Future<Either<String, ShopBasketResponseModel>> sale(SaleParams params);
}

class ShopBasketRemoteDataSourceImpl extends ShopBasketRemoteDataSource {
  final Dio dio;
  final DataConnectionChecker networkInfo;
  ShopBasketRemoteDataSourceImpl({@required this.dio, @required this.networkInfo});

  @override
  Future<Either<String, ShopBasketResponseModel>> sale(SaleParams params) async {
    ShopBasketResponseModel sale;

    if (await networkInfo.hasConnection) {
      try {

        final result = await dio.post(
          Url.sale,
             data:jsonEncode(params.toJson()),
          options: Options(
            headers:{
              'Authorization': 'Bearer ${params.api_token}',
            } ,
            followRedirects: false,
            validateStatus: (status) {
              return status < 500;
            },
          ),
        );

        sale = ShopBasketResponseModel.fromJson(json.decode(result.data));

        return Right(sale);
      } on DioError catch (ex) {
        if (ex.type == DioErrorType.connectTimeout) {
          return Left(Er.NetworkError);
        } else if (ex.type == DioErrorType.receiveTimeout) {
          return Left(Er.NetworkError);
        } else if (sale != null)
          return Left(sale.message);
        else if (sale == null) return Left(Er.Error);
      }
    } else {
      return Left(Er.NetworkError);
    }
  }
}
