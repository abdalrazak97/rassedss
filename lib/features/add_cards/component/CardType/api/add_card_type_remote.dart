
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:rassed_app/features/Constants/ApiConstants.dart';
import 'package:dartz/dartz.dart';
import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:dio/dio.dart';
import 'package:meta/meta.dart';
import 'package:rassed_app/features/error/exceptions.dart';
import 'dart:core';
import 'package:rassed_app/features/error/failures.dart';
import 'package:rassed_app/features/add_cards/component/CardType/model/add_card_type_model.dart';
import 'package:rassed_app/features/add_cards/component/CardType/usecase/add_card_type_usecase.dart';
abstract class AddCardTypeDataSource {
  Future<Either<String, AddCardTypeResponseModel>> addCardType(addCardTypeParams params);
}

class AddCardTypeDataSourceImpl extends AddCardTypeDataSource {
  final Dio dio;
  final DataConnectionChecker networkInfo;

  AddCardTypeDataSourceImpl({@required this.dio, @required this.networkInfo});

  @override
  Future<Either<String, AddCardTypeResponseModel>> addCardType(addCardTypeParams params) async {
    AddCardTypeResponseModel addCardTypeResponseModel;
    print("params ${params.api_token}");
    print("params ${params.name}");
    print("params ${params.company_id}");
    print("params ${params.api_token}");
    String fileName = params.image.path
        .split('/')
        .last;
    FormData formData = FormData.fromMap({
      'company_id': params.company_id,
      'description':params.description,
      'name': params.name,
      "image": await MultipartFile.fromFile(
          params.image.path, filename: fileName),
    });
    if (await networkInfo.hasConnection) {
      try {
        final result = await dio.post(
          Url.addCardType,
          data: formData,
          options: Options(
            headers: {
              'Authorization': 'Bearer ${params.api_token}',
            },
            followRedirects: false,
            validateStatus: (status) {
              return status < 500;
            },
          ),
        );
        print(result.data);
        addCardTypeResponseModel=AddCardTypeResponseModel.fromJson(json.decode(result.data));
        return Right(addCardTypeResponseModel);
      } on DioError catch (ex) {
        if (ex.type == DioErrorType.connectTimeout) {
          return Left(Er.NetworkError);
        } else if (ex.type == DioErrorType.receiveTimeout) {
          return Left(Er.NetworkError);
        }
        else if (addCardTypeResponseModel == null) return Left(Er.Error);
      }
    } else {
      return Left(Er.NetworkError);
    }
  }
}