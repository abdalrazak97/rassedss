import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:rassed_app/features/Constants/ApiConstants.dart';
import 'package:dartz/dartz.dart';
import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:dio/dio.dart';
import 'package:meta/meta.dart';

import 'package:rassed_app/features/add_cards/component/CardType/usecase/get_companies_usecase.dart';
import 'package:rassed_app/features/add_cards/component/CardType/model/get_compnaies_model.dart';
abstract class GetAllCompaniesDataSource {
  Future<Either<String, List<GetAllCompaniesResponseModel>>> getAllCompanies(getCompaniesParams params);
}

class GetAllCompaniesDataSourceImpl extends GetAllCompaniesDataSource {
  final Dio dio;
  final DataConnectionChecker networkInfo;

  GetAllCompaniesDataSourceImpl({@required this.dio, @required this.networkInfo});

  @override
  Future<Either<String, List<GetAllCompaniesResponseModel>>> getAllCompanies(getCompaniesParams params) async {
    List<GetAllCompaniesResponseModel> campanies;
    print("params ${params.api_token}");
    if (await networkInfo.hasConnection) {
      try {
        final result = await dio.get(
          Url.getCompany ,

          options: Options(
            headers:{
              'Authorization': 'Bearer ${params.api_token}',
            } ,
            followRedirects: false,
            validateStatus: (status) {
              return status < 500;
            },
          ),
        );

        print(result.data);
        campanies=GetCompanyResponseModelFromJson(result.data);

        return Right(campanies);
      } on DioError catch (ex) {
        if (ex.type == DioErrorType.connectTimeout) {
          return Left(Er.NetworkError);
        } else if (ex.type == DioErrorType.receiveTimeout) {
          return Left(Er.NetworkError);
        }
        else if (campanies == null) return Left(Er.Error);
      }
    } else {
      return Left(Er.NetworkError);
    }
  }
}
