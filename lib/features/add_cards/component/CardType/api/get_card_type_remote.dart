import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:rassed_app/features/Constants/ApiConstants.dart';
import 'package:dartz/dartz.dart';
import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:dio/dio.dart';
import 'package:meta/meta.dart';

import 'package:rassed_app/features/add_cards/component/CardType/usecase/card_type_usecase.dart';
import 'package:rassed_app/features/add_cards/component/CardType/model/card_type_model.dart';

abstract class GetCardTypeDataSource {
  Future<Either<String, List<GetCardTypeResponseModel>>> getCardType(cardTypeParams params);
}

class GetCardTypeDataSourceImpl extends GetCardTypeDataSource {
  final Dio dio;
  final DataConnectionChecker networkInfo;

  GetCardTypeDataSourceImpl({@required this.dio, @required this.networkInfo});

  @override
  Future<Either<String, List<GetCardTypeResponseModel>>> getCardType(cardTypeParams params) async {
    List<GetCardTypeResponseModel> cardType;

    if (await networkInfo.hasConnection) {
      try {
        final result = await dio.get(
          Url.getCardType ,

          options: Options(
            headers:{
              'Authorization': 'Bearer ${params.api_token}',
            } ,
            followRedirects: false,
            validateStatus: (status) {
              return status < 500;
            },
          ),
        );

        print(result.data);
        cardType=GetCardTypeResponseModelFromJson(result.data);

        return Right(cardType);
      } on DioError catch (ex) {
        if (ex.type == DioErrorType.connectTimeout) {
          return Left(Er.NetworkError);
        } else if (ex.type == DioErrorType.receiveTimeout) {
          return Left(Er.NetworkError);
        }
        else if (cardType == null) return Left(Er.Error);
      }
    } else {
      return Left(Er.NetworkError);
    }
  }
}
