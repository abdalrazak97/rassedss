import 'dart:convert';

import 'package:json_annotation/json_annotation.dart';



part 'get_compnaies_model.g.dart';

@JsonSerializable()
class GetAllCompaniesResponseModel {
  final int id;
  final String name;
  final int category_id;
  final String categoryname;
  final String image;
  final bool success;
  final String message;


  Map<String, dynamic> toJson() => _$GetAllCompaniesResponseModelToJson(this);
  factory GetAllCompaniesResponseModel.fromJson(Map<String, dynamic> json) =>
      _$GetAllCompaniesResponseModelFromJson(json);

  GetAllCompaniesResponseModel({
    this.id, this.name,this.categoryname, this.category_id,this.image, this.success, this.message});
}
List<GetAllCompaniesResponseModel> GetCompanyResponseModelFromJson(String str) =>
    List<GetAllCompaniesResponseModel>.from(json.decode(str).map((x) => GetAllCompaniesResponseModel.fromJson(x)));
String GetAllCompaniesResponseModelToJson(List<GetAllCompaniesResponseModel> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));