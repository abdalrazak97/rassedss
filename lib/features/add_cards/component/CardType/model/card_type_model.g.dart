// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'card_type_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

GetCardTypeResponseModel _$GetCardTypeResponseModelFromJson(
    Map<String, dynamic> json) {
  return GetCardTypeResponseModel(
    id: json['id'] as int,
    name: json['name'] as String,
    company_id: json['company_id'] as int,
    company: json['company'] as String,
    description: json['description'] as String,
    image: json['image'] as String,
    success: json['success'] as bool,
    message: json['message'] as String,
  );
}

Map<String, dynamic> _$GetCardTypeResponseModelToJson(
        GetCardTypeResponseModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'company_id': instance.company_id,
      'company': instance.company,
      'description': instance.description,
      'image': instance.image,
      'success': instance.success,
      'message': instance.message,
    };
