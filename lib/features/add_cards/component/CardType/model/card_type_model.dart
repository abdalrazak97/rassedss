import 'dart:convert';

import 'package:json_annotation/json_annotation.dart';



part 'card_type_model.g.dart';

@JsonSerializable()
class GetCardTypeResponseModel {
  final int id;
  final String name;
  final int company_id;
  final String company;
  final String description;
  final String image;
  final bool success;
  final String message;


  Map<String, dynamic> toJson() => _$GetCardTypeResponseModelToJson(this);
  factory GetCardTypeResponseModel.fromJson(Map<String, dynamic> json) =>
      _$GetCardTypeResponseModelFromJson(json);

  GetCardTypeResponseModel({this.id, this.name, this.company_id, this.company,
      this.description, this.image, this.success, this.message});
}
List<GetCardTypeResponseModel> GetCardTypeResponseModelFromJson(String str) =>
    List<GetCardTypeResponseModel>.from(json.decode(str).map((x) => GetCardTypeResponseModel.fromJson(x)));
String GetCardTypeResponseModelToJson(List<GetCardTypeResponseModel> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));