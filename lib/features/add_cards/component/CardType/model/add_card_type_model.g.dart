// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'add_card_type_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AddCardTypeResponseModel _$AddCardTypeResponseModelFromJson(
    Map<String, dynamic> json) {
  return AddCardTypeResponseModel(
    id: json['id'] as int,
    name: json['name'] as String,
    success: json['success'] as bool,
    message: json['message'] as String,
  );
}

Map<String, dynamic> _$AddCardTypeResponseModelToJson(
        AddCardTypeResponseModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'success': instance.success,
      'message': instance.message,
    };
