// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'get_compnaies_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

GetAllCompaniesResponseModel _$GetAllCompaniesResponseModelFromJson(
    Map<String, dynamic> json) {
  return GetAllCompaniesResponseModel(
    id: json['id'] as int,
    name: json['name'] as String,
    categoryname: json['categoryname'] as String,
    category_id: json['category_id'] as int,
    image: json['image'] as String,
    success: json['success'] as bool,
    message: json['message'] as String,
  );
}

Map<String, dynamic> _$GetAllCompaniesResponseModelToJson(
        GetAllCompaniesResponseModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'category_id': instance.category_id,
      'categoryname': instance.categoryname,
      'image': instance.image,
      'success': instance.success,
      'message': instance.message,
    };
