import 'dart:convert';

import 'package:json_annotation/json_annotation.dart';



part 'add_card_type_model.g.dart';

@JsonSerializable()
class AddCardTypeResponseModel {
  final int id;
  final String name;
  final bool success;
  final String message;


  Map<String, dynamic> toJson() => _$AddCardTypeResponseModelToJson(this);
  factory AddCardTypeResponseModel.fromJson(Map<String, dynamic> json) =>
      _$AddCardTypeResponseModelFromJson(json);

  AddCardTypeResponseModel({
    this.id, this.name,this.success, this.message});
}
List<AddCardTypeResponseModel> AddCardTypeResponseModelFromJson(String str) =>
    List<AddCardTypeResponseModel>.from(json.decode(str).map((x) => AddCardTypeResponseModel.fromJson(x)));
String AddCardTypeResponseModelToJson(List<AddCardTypeResponseModel> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));
