import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_fonts_arabic/fonts.dart';
import 'package:easy_localization/easy_localization.dart';
import 'dart:ui' as ui;
import 'package:rassed_app/features/add_cards/component/CardType/bloc/CardTypeBloc.dart';
import 'package:rassed_app/features/add_cards/component/CardType/bloc/CardTypeState.dart';
import 'package:rassed_app/features/add_cards/component/CardType/card_type_info.dart';
import 'package:rassed_app/features/add_cards/component/CardType/add_card_type.dart';

import 'package:rassed_app/features/settings/Preference.dart';
import 'package:rassed_app/features/widget/textauto.dart';

import '../../../../injection.dart';


class CardType extends StatefulWidget {
  @override
  _CardTypeState createState() => _CardTypeState();
}

class _CardTypeState extends State<CardType> {


  CardTypeBloc bloc = sl<CardTypeBloc>();
  String Token ;
  @override
  void initState() {

    Token=Preferences.getUserToken();
    bloc.onChangeApiToken(Token);
    bloc.onCompaniesEvent();
     bloc.onCardTypeEvent();

    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        actions: [
          SizedBox(
            width: ScreenUtil().setWidth(40),
            child: Center(
              child: TextButton(
                style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.all(Colors.blueGrey),
                ),
                onPressed: () {
                  Navigator.pop(context);
                },
                child: SvgPicture.asset(
                  "assets/icons/close3.svg",
                  width: ScreenUtil().screenWidth * 0.1,
                  color: Colors.white,
                ),
              ),
            ),
          ),
        ],
        leading: SizedBox(
          width: 5,
        ),
        backgroundColor: Colors.blueGrey,
        centerTitle: true,
        title: Text(
          'Card Type'.tr().toString(),
          textDirection: ui.TextDirection.rtl,
          style: TextStyle(
            fontFamily: ArabicFonts.Cairo,
            package: 'google_fonts_arabic',
            fontSize: 20,
          ),
        ),
      ),
      body:  BlocBuilder<CardTypeBloc, CardTypeState>(
          bloc: bloc,
          builder: (context, state) {
            if (state.isLoading)
              return Center(child: CircularProgressIndicator(),);
            return SingleChildScrollView(
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  textDirection: ui.TextDirection.rtl,
                  children: [
                    SizedBox(height: ScreenUtil().screenHeight * 0.05,),
                    Center(
                      child: SizedBox(
                        height: ScreenUtil().screenHeight * 0.1,
                        width: ScreenUtil().screenWidth * 0.8,
                        child: TextButton(
                            style: ButtonStyle(
                              backgroundColor: MaterialStateProperty.all(
                                  Colors.blueGrey),
                              shape: MaterialStateProperty.all(
                                  RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(15.0),
                                      side:
                                      BorderSide(color: Colors.grey.shade50))),
                            ),
                            child: Padding(
                              padding: EdgeInsets.symmetric(
                                  vertical: ScreenUtil().setHeight(12)),
                              child: Container(
                                  child: AutoTextgrey(
                                      ScreenUtil().setSp(16),
                                      "Add Card Type".tr().toString(),
                                      Colors.grey.shade50)),
                            ),
                            onPressed: () {
                              Navigator.push(context, MaterialPageRoute(
                                  builder: (context) =>
                                      AddCardType(
                                        getCardTypeResponseModel: state.getCardTypeResponseModel,getAllCompaniesResponseModel:  state.getAllCompaniesResponseModel,)));
                            }),
                      ),
                    ),

                    Container(
                      padding: EdgeInsets.symmetric(
                          vertical: ScreenUtil().setHeight(20)),
                      color: Colors.grey.shade50,
                      height: ScreenUtil().screenHeight * 0.8,
                      width: ScreenUtil().screenWidth,
                      child: ListView.builder(
                        itemCount: state.getCardTypeResponseModel.length,
                        itemBuilder: (context, index) =>
                            GestureDetector(
                              onTap: () {
                                /* Route route = MaterialPageRoute(builder: (context) =>
                                  EditItem(prdouctEntity: state2.Prdoucts[index],));
                              Navigator.push(context, route).then(onGoBack);*/
                              },
                              child: CardTypeInfo(
                                count: state.getCardTypeResponseModel.length,
                                index: index
                                ,
                                name: state.getCardTypeResponseModel[index]
                                    .name,
                                url: state.getCardTypeResponseModel[index]
                                    .image,
                                value: state.getCardTypeResponseModel[index]
                                    .company,),
                            ),
                      ),
                    ),

                    SizedBox(
                      height: ScreenUtil().screenHeight * 0.02,
                    ),
                  ]),

      );
      }
      ),
        );


  }
}
