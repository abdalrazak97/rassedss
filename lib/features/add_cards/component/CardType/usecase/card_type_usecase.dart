import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:rassed_app/features/Constants/usecase.dart';
import 'package:rassed_app/features/add_cards/component/CardType/api/get_card_type_remote.dart';
import 'package:rassed_app/features/add_cards/component/CardType/model/card_type_model.dart';



class CardTypeUseCase extends UseCase<List<GetCardTypeResponseModel>, cardTypeParams> {
  final GetCardTypeDataSource cardTypeDataSource;

  CardTypeUseCase({this.cardTypeDataSource});

  @override
  Future<Either<String, List<GetCardTypeResponseModel>>> call(cardTypeParams params) async {
    return await cardTypeDataSource.getCardType(params);

  }
}

class cardTypeParams extends Equatable {
  final String api_token;

  cardTypeParams(
      {@required this.api_token,});

  @override
  // TODO: implement props
  List<Object> get props => [api_token];
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['api_token'] = this.api_token;

    return data;
  }
}
