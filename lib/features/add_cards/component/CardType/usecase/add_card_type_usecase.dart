
import 'dart:io';
import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:rassed_app/features/Constants/usecase.dart';
import 'package:rassed_app/features/add_cards/component/CardType/api/add_card_type_remote.dart';
import 'package:rassed_app/features/add_cards/component/CardType/model/add_card_type_model.dart';
import 'package:rassed_app/features/add_cards/component/CardType/usecase/add_card_type_usecase.dart';
class AddCardTypeUseCase extends UseCase<AddCardTypeResponseModel, addCardTypeParams> {
  final AddCardTypeDataSource addCardTypeDataSource;
  AddCardTypeUseCase({this.addCardTypeDataSource});

  @override
  Future<Either<String, AddCardTypeResponseModel>> call(addCardTypeParams params) async {
    return await addCardTypeDataSource.addCardType(params);
  }
}


class addCardTypeParams extends Equatable {
  final String api_token;
  final String name;
  final String description;
  final int company_id;
  final File image;

  addCardTypeParams(
      {@required this.api_token,@required this.company_id,@required this.name,this.image,this.description,});

  @override
  // TODO: implement props
  List<Object> get props => [api_token];
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['api_token'] = this.api_token;
    data['company_id'] = this.company_id;
    data['description'] = this.description;
    data['name'] = this.name;
    data['image'] = this.image;

    return data;
  }
}
