import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:rassed_app/features/Constants/usecase.dart';
import 'package:rassed_app/features/add_cards/component/CardType/api/get_compaies_remote.dart';
import 'package:rassed_app/features/add_cards/component/CardType/model/get_compnaies_model.dart';


class GetCompaniesUseCase extends UseCase<List<GetAllCompaniesResponseModel>,getCompaniesParams > {
  final GetAllCompaniesDataSource getCampnies;

  GetCompaniesUseCase({this.getCampnies});

  @override
  Future<Either<String, List<GetAllCompaniesResponseModel>>> call(getCompaniesParams params) async {
    return await getCampnies.getAllCompanies(params);

  }
}

class getCompaniesParams extends Equatable {
  final String api_token;

  getCompaniesParams(
      {@required this.api_token,});

  @override
  // TODO: implement props
  List<Object> get props => [api_token];
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['api_token'] = this.api_token;

    return data;
  }
}
