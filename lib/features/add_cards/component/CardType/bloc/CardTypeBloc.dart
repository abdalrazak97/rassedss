import 'dart:async';
import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:rassed_app/features/add_cards/component/CardType/bloc/CardTypeEvent.dart';
import 'package:rassed_app/features/add_cards/component/CardType/bloc/CardTypeState.dart';
import 'package:rassed_app/features/add_cards/component/CardType/usecase/card_type_usecase.dart';
import 'package:rassed_app/features/add_cards/component/CardType/usecase/add_card_type_usecase.dart';
import 'package:rassed_app/features/add_cards/component/CardType/usecase/get_companies_usecase.dart';
import 'package:rassed_app/features/error/failures.dart';
import 'package:rassed_app/features/add_cards/component/CardType/model/add_card_type_model.dart';


const String SERVER_FAILURE_MESSAGE = 'Server Failure';
const String CACHE_FAILURE_MESSAGE = 'Cache Failure';
const String INVALID_INPUT_FAILURE_MESSAGE =
    'Invalid Input - The number must be a positive integer or zero.';

class CardTypeBloc extends Bloc<CardTypeEvents, CardTypeState> {
  final CardTypeUseCase cardTypeUseCase;
  final GetCompaniesUseCase getCompaniesUseCase;
  final AddCardTypeUseCase addCardTypeUseCase;
  CardTypeBloc({this.cardTypeUseCase,this.addCardTypeUseCase,this.getCompaniesUseCase}) : super(CardTypeState.initial());

  void onChangeApiToken(String api_token){
    add( ChangeApiToken(api_token));
  }

  void onCardTypeEvent(){
    add(CardTypeEvent());
  }
  void onAddCardTypeEvent(){
    add(AddCardTypeEvent());
  }
  void onCompaniesEvent(){
    add(CompaniesEvent());
  }

  void onChangeDescriptionCardType(String name){
    add(ChangeDescriptionCardType(name));
  }

  void onChangeImageNetwork(File im) {
    add(ChangeImageNetwork(im));
  }
  void onChangeNameCardType(String name){
    add(ChangeNameCardType(name));
  }
  void onChangeIdCompany(int company_id){
    add(ChangeIdCompany(company_id));
  }

  @override
  Stream<CardTypeState> mapEventToState(CardTypeEvents event) async* {
    if (event is CardTypeEvent) {
      yield state.rebuild((b) => b
        ..isLoading = true
        ..isSuccess = false

      );
      final result = await cardTypeUseCase(
          cardTypeParams(
              api_token: state.api_token
          )
      );

      yield* result.fold((l) async* {

        yield state.rebuild((b) => b
          ..isLoading = false
          ..isSuccess = false
          ..errorMessage = l);
      }, (r) async* {
        print('sucesss');
        yield state.rebuild((b) => b
          ..isLoading = false
          ..isSuccess = true
          ..errorMessage = ''
          ..getCardTypeResponseModel = r);
      });

    }
    else if (event is CompaniesEvent) {
      yield state.rebuild((b) => b
        ..isLoading = true
        ..isSuccess = false

      );
      final result = await getCompaniesUseCase(
          getCompaniesParams(
              api_token: state.api_token
          )
      );

      yield* result.fold((l) async* {

        yield state.rebuild((b) => b
          ..isLoading = false
          ..isSuccess = false
          ..errorMessage = l);
      }, (r) async* {
        print('sucesss');
        yield state.rebuild((b) => b
          ..isLoading = false
          ..isSuccess = true
          ..errorMessage = ''
          ..getAllCompaniesResponseModel = r);
      });
    }
    else if (event is AddCardTypeEvent) {

      yield* _onAddCardTypeInitiated(
        addCardTypeUseCase(
          addCardTypeParams(
              name: state.name,
              image: state.image,
              company_id: state.company_id,
              description: state.description,
              api_token: state.api_token
          ),
        ),
        state,
      );
    }
    else if (event is ChangeApiToken) {
      print('1111');
      yield state.rebuild((b) => b..api_token = event.api_token);

    }
    else if (event is ChangeImageNetwork) {
      print('image change ');
      yield state.rebuild((b) => b..image = event.img);

    }
    else if (event is ChangeNameCardType) {
      print('222 name');
      yield state.rebuild((b) => b..name = event.name);

    }
    else if (event is ChangeDescriptionCardType) {
      print('222 description');
      yield state.rebuild((b) => b..description = event.description);

    }
    else if (event is ChangeIdCompany) {
      print('333 category_id');
      yield state.rebuild((b) => b..company_id = event.company_id);

    }

  }
  Stream<CardTypeState> _onAddCardTypeInitiated(
      Future<Either<String, AddCardTypeResponseModel>> AddPrdouct,
      CardTypeState state,
      ) async* {
    yield state.rebuild(
          (builder) => builder
        ..isLoading = true
        ..isSuccess = false
        ..error = null,

    );

    final result = await AddPrdouct;

    yield* result.fold((l) async* {
      print('===========1');
      if (l is ServerFailure) {
        yield state.rebuild((builder) => builder

          ..isSuccess = false
          ..isLoading = false
          ..errorMessage= l);
      } else {
        print('===========2');
        yield state.rebuild((builder) => builder

          ..isSuccess = false
          ..isLoading = false);
      }
    }, (r) async* {
      print('===========3');
      // the best case
      yield state.rebuild(
            (builder) => builder
          ..isLoading = false
          ..isSuccess = true
          ..receive=true
          ..addCardTypeResponseModel=r,
      );

      // navigate to next screen
    });
  }
}
