// GENERATED CODE - DO NOT MODIFY BY HAND

part of CardTypeState;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$CardTypeState extends CardTypeState {
  @override
  final bool isSuccess;
  @override
  final bool isLoading;
  @override
  final bool receive;
  @override
  final String Accept_Language;
  @override
  final String api_token;
  @override
  final String name;
  @override
  final String description;
  @override
  final int category_id;
  @override
  final int company_id;
  @override
  final ErrorCode error;
  @override
  final File image;
  @override
  final String errorMessage;
  @override
  final List<GetCardTypeResponseModel> getCardTypeResponseModel;
  @override
  final List<GetAllCompaniesResponseModel> getAllCompaniesResponseModel;
  @override
  final AddCardTypeResponseModel addCardTypeResponseModel;

  factory _$CardTypeState([void Function(CardTypeStateBuilder) updates]) =>
      (new CardTypeStateBuilder()..update(updates)).build();

  _$CardTypeState._(
      {this.isSuccess,
      this.isLoading,
      this.receive,
      this.Accept_Language,
      this.api_token,
      this.name,
      this.description,
      this.category_id,
      this.company_id,
      this.error,
      this.image,
      this.errorMessage,
      this.getCardTypeResponseModel,
      this.getAllCompaniesResponseModel,
      this.addCardTypeResponseModel})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        isSuccess, 'CardTypeState', 'isSuccess');
    BuiltValueNullFieldError.checkNotNull(
        isLoading, 'CardTypeState', 'isLoading');
    BuiltValueNullFieldError.checkNotNull(receive, 'CardTypeState', 'receive');
    BuiltValueNullFieldError.checkNotNull(
        Accept_Language, 'CardTypeState', 'Accept_Language');
    BuiltValueNullFieldError.checkNotNull(
        api_token, 'CardTypeState', 'api_token');
    BuiltValueNullFieldError.checkNotNull(name, 'CardTypeState', 'name');
    BuiltValueNullFieldError.checkNotNull(
        description, 'CardTypeState', 'description');
    BuiltValueNullFieldError.checkNotNull(
        category_id, 'CardTypeState', 'category_id');
    BuiltValueNullFieldError.checkNotNull(
        company_id, 'CardTypeState', 'company_id');
    BuiltValueNullFieldError.checkNotNull(
        errorMessage, 'CardTypeState', 'errorMessage');
    BuiltValueNullFieldError.checkNotNull(
        getCardTypeResponseModel, 'CardTypeState', 'getCardTypeResponseModel');
    BuiltValueNullFieldError.checkNotNull(getAllCompaniesResponseModel,
        'CardTypeState', 'getAllCompaniesResponseModel');
    BuiltValueNullFieldError.checkNotNull(
        addCardTypeResponseModel, 'CardTypeState', 'addCardTypeResponseModel');
  }

  @override
  CardTypeState rebuild(void Function(CardTypeStateBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  CardTypeStateBuilder toBuilder() => new CardTypeStateBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is CardTypeState &&
        isSuccess == other.isSuccess &&
        isLoading == other.isLoading &&
        receive == other.receive &&
        Accept_Language == other.Accept_Language &&
        api_token == other.api_token &&
        name == other.name &&
        description == other.description &&
        category_id == other.category_id &&
        company_id == other.company_id &&
        error == other.error &&
        image == other.image &&
        errorMessage == other.errorMessage &&
        getCardTypeResponseModel == other.getCardTypeResponseModel &&
        getAllCompaniesResponseModel == other.getAllCompaniesResponseModel &&
        addCardTypeResponseModel == other.addCardTypeResponseModel;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc(
                        $jc(
                            $jc(
                                $jc(
                                    $jc(
                                        $jc(
                                            $jc(
                                                $jc(
                                                    $jc(
                                                        $jc(
                                                            $jc(
                                                                0,
                                                                isSuccess
                                                                    .hashCode),
                                                            isLoading.hashCode),
                                                        receive.hashCode),
                                                    Accept_Language.hashCode),
                                                api_token.hashCode),
                                            name.hashCode),
                                        description.hashCode),
                                    category_id.hashCode),
                                company_id.hashCode),
                            error.hashCode),
                        image.hashCode),
                    errorMessage.hashCode),
                getCardTypeResponseModel.hashCode),
            getAllCompaniesResponseModel.hashCode),
        addCardTypeResponseModel.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('CardTypeState')
          ..add('isSuccess', isSuccess)
          ..add('isLoading', isLoading)
          ..add('receive', receive)
          ..add('Accept_Language', Accept_Language)
          ..add('api_token', api_token)
          ..add('name', name)
          ..add('description', description)
          ..add('category_id', category_id)
          ..add('company_id', company_id)
          ..add('error', error)
          ..add('image', image)
          ..add('errorMessage', errorMessage)
          ..add('getCardTypeResponseModel', getCardTypeResponseModel)
          ..add('getAllCompaniesResponseModel', getAllCompaniesResponseModel)
          ..add('addCardTypeResponseModel', addCardTypeResponseModel))
        .toString();
  }
}

class CardTypeStateBuilder
    implements Builder<CardTypeState, CardTypeStateBuilder> {
  _$CardTypeState _$v;

  bool _isSuccess;
  bool get isSuccess => _$this._isSuccess;
  set isSuccess(bool isSuccess) => _$this._isSuccess = isSuccess;

  bool _isLoading;
  bool get isLoading => _$this._isLoading;
  set isLoading(bool isLoading) => _$this._isLoading = isLoading;

  bool _receive;
  bool get receive => _$this._receive;
  set receive(bool receive) => _$this._receive = receive;

  String _Accept_Language;
  String get Accept_Language => _$this._Accept_Language;
  set Accept_Language(String Accept_Language) =>
      _$this._Accept_Language = Accept_Language;

  String _api_token;
  String get api_token => _$this._api_token;
  set api_token(String api_token) => _$this._api_token = api_token;

  String _name;
  String get name => _$this._name;
  set name(String name) => _$this._name = name;

  String _description;
  String get description => _$this._description;
  set description(String description) => _$this._description = description;

  int _category_id;
  int get category_id => _$this._category_id;
  set category_id(int category_id) => _$this._category_id = category_id;

  int _company_id;
  int get company_id => _$this._company_id;
  set company_id(int company_id) => _$this._company_id = company_id;

  ErrorCode _error;
  ErrorCode get error => _$this._error;
  set error(ErrorCode error) => _$this._error = error;

  File _image;
  File get image => _$this._image;
  set image(File image) => _$this._image = image;

  String _errorMessage;
  String get errorMessage => _$this._errorMessage;
  set errorMessage(String errorMessage) => _$this._errorMessage = errorMessage;

  List<GetCardTypeResponseModel> _getCardTypeResponseModel;
  List<GetCardTypeResponseModel> get getCardTypeResponseModel =>
      _$this._getCardTypeResponseModel;
  set getCardTypeResponseModel(
          List<GetCardTypeResponseModel> getCardTypeResponseModel) =>
      _$this._getCardTypeResponseModel = getCardTypeResponseModel;

  List<GetAllCompaniesResponseModel> _getAllCompaniesResponseModel;
  List<GetAllCompaniesResponseModel> get getAllCompaniesResponseModel =>
      _$this._getAllCompaniesResponseModel;
  set getAllCompaniesResponseModel(
          List<GetAllCompaniesResponseModel> getAllCompaniesResponseModel) =>
      _$this._getAllCompaniesResponseModel = getAllCompaniesResponseModel;

  AddCardTypeResponseModel _addCardTypeResponseModel;
  AddCardTypeResponseModel get addCardTypeResponseModel =>
      _$this._addCardTypeResponseModel;
  set addCardTypeResponseModel(
          AddCardTypeResponseModel addCardTypeResponseModel) =>
      _$this._addCardTypeResponseModel = addCardTypeResponseModel;

  CardTypeStateBuilder();

  CardTypeStateBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _isSuccess = $v.isSuccess;
      _isLoading = $v.isLoading;
      _receive = $v.receive;
      _Accept_Language = $v.Accept_Language;
      _api_token = $v.api_token;
      _name = $v.name;
      _description = $v.description;
      _category_id = $v.category_id;
      _company_id = $v.company_id;
      _error = $v.error;
      _image = $v.image;
      _errorMessage = $v.errorMessage;
      _getCardTypeResponseModel = $v.getCardTypeResponseModel;
      _getAllCompaniesResponseModel = $v.getAllCompaniesResponseModel;
      _addCardTypeResponseModel = $v.addCardTypeResponseModel;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(CardTypeState other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$CardTypeState;
  }

  @override
  void update(void Function(CardTypeStateBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$CardTypeState build() {
    final _$result = _$v ??
        new _$CardTypeState._(
            isSuccess: BuiltValueNullFieldError.checkNotNull(
                isSuccess, 'CardTypeState', 'isSuccess'),
            isLoading: BuiltValueNullFieldError.checkNotNull(
                isLoading, 'CardTypeState', 'isLoading'),
            receive: BuiltValueNullFieldError.checkNotNull(
                receive, 'CardTypeState', 'receive'),
            Accept_Language: BuiltValueNullFieldError.checkNotNull(
                Accept_Language, 'CardTypeState', 'Accept_Language'),
            api_token: BuiltValueNullFieldError.checkNotNull(
                api_token, 'CardTypeState', 'api_token'),
            name: BuiltValueNullFieldError.checkNotNull(
                name, 'CardTypeState', 'name'),
            description: BuiltValueNullFieldError.checkNotNull(
                description, 'CardTypeState', 'description'),
            category_id: BuiltValueNullFieldError.checkNotNull(
                category_id, 'CardTypeState', 'category_id'),
            company_id: BuiltValueNullFieldError.checkNotNull(
                company_id, 'CardTypeState', 'company_id'),
            error: error,
            image: image,
            errorMessage: BuiltValueNullFieldError.checkNotNull(errorMessage, 'CardTypeState', 'errorMessage'),
            getCardTypeResponseModel: BuiltValueNullFieldError.checkNotNull(getCardTypeResponseModel, 'CardTypeState', 'getCardTypeResponseModel'),
            getAllCompaniesResponseModel: BuiltValueNullFieldError.checkNotNull(getAllCompaniesResponseModel, 'CardTypeState', 'getAllCompaniesResponseModel'),
            addCardTypeResponseModel: BuiltValueNullFieldError.checkNotNull(addCardTypeResponseModel, 'CardTypeState', 'addCardTypeResponseModel'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
