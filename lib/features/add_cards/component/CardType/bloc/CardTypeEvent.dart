import 'dart:io';

import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class CardTypeEvents extends Equatable {
  @override
  List<Object> get props => [];
}

class CardTypeEvent extends CardTypeEvents {}
class CompaniesEvent extends CardTypeEvents {}
class AddCardTypeEvent extends CardTypeEvents {}
class ChangeApiToken extends CardTypeEvents{
  final api_token;
  ChangeApiToken(this.api_token);
}
class ChangeImageNetwork extends CardTypeEvents {
  final File img;
  ChangeImageNetwork(this.img);
}
class ChangeNameCardType extends CardTypeEvents{
  final name;
  ChangeNameCardType(this.name);
}
class ChangeDescriptionCardType extends CardTypeEvents{
  final description;
  ChangeDescriptionCardType(this.description);
}
class ChangeIdCompany extends CardTypeEvents{
  final company_id;
  ChangeIdCompany(this.company_id);
}
class ResetError extends CardTypeEvents {}

class Enable extends CardTypeEvents {}

class Disable extends CardTypeEvents {}
