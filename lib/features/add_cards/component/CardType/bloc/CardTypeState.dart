library CardTypeState;
import 'dart:io';
import 'package:built_value/built_value.dart';
import 'package:rassed_app/features/Constants/ApiConstants.dart';
import 'package:rassed_app/features/add_cards/component/CardType/model/card_type_model.dart';
import 'package:rassed_app/features/add_cards/component/CardType/model/add_card_type_model.dart';
import 'package:rassed_app/features/add_cards/component/CardType/model/get_compnaies_model.dart';

part 'CardTypeState.g.dart';
abstract class CardTypeState implements Built<CardTypeState, CardTypeStateBuilder> {
  bool get isSuccess;
  bool get isLoading;
  bool get receive;
  String get Accept_Language;
  String get api_token;
  String get name;
  String get description;
  int get category_id;
   int get company_id;
  @nullable
  ErrorCode get error;
  @nullable
  File get image;
  String get errorMessage;
  List<GetCardTypeResponseModel> get getCardTypeResponseModel;
  List<GetAllCompaniesResponseModel> get getAllCompaniesResponseModel;
  AddCardTypeResponseModel get addCardTypeResponseModel;

  CardTypeState._();

  factory CardTypeState([updates(CardTypeStateBuilder b)]) = _$CardTypeState;

  factory CardTypeState.initial() {
    return CardTypeState((b) => b
      ..isLoading = false
      ..isSuccess = false
      ..receive=false
      ..description=''
      ..api_token =''
      ..name=''
      ..category_id=0
      ..company_id=0
      ..errorMessage = ''
      ..Accept_Language = ''
      ..getCardTypeResponseModel=[]
      ..getAllCompaniesResponseModel=[]
        ..addCardTypeResponseModel=AddCardTypeResponseModel(
          message: ''
        )

    );
  }
}
