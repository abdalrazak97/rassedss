
import 'package:flutter/material.dart';
import 'package:rassed_app/features/Constants/ApiConstants.dart';
import 'package:dartz/dartz.dart';
import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:dio/dio.dart';
import 'package:meta/meta.dart';
import 'package:rassed_app/features/add_cards/component/Card/usecase/card_usecase.dart';
import 'package:rassed_app/features/add_cards/component/Card/model/card_model.dart';

abstract class GetCardDataSource {
  Future<Either<String, List<GetCardResponseModel>>> getCard(getCardParams params);
}

class GetCardDataSourceImpl extends GetCardDataSource {
  final Dio dio;
  final DataConnectionChecker networkInfo;

  GetCardDataSourceImpl({@required this.dio, @required this.networkInfo});

  @override
  Future<Either<String, List<GetCardResponseModel>>> getCard(getCardParams params) async {
    List<GetCardResponseModel> card;

    if (await networkInfo.hasConnection) {
      try {
        final result = await dio.get(
          Url.getCard ,
          options: Options(
            headers:{
              'Authorization': 'Bearer ${params.api_token}',
            } ,
            followRedirects: false,
            validateStatus: (status) {
              return status < 500;
            },
          ),
        );

        print(result.data);
        card=GetCardResponseModelFromJson(result.data);

        return Right(card);
      } on DioError catch (ex) {
        if (ex.type == DioErrorType.connectTimeout) {
          return Left(Er.NetworkError);
        } else if (ex.type == DioErrorType.receiveTimeout) {
          return Left(Er.NetworkError);
        }
        else if (card == null) return Left(Er.Error);
      }
    } else {
      return Left(Er.NetworkError);
    }
  }
}
