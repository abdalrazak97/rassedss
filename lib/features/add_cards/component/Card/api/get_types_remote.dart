
import 'package:flutter/material.dart';
import 'package:rassed_app/features/Constants/ApiConstants.dart';
import 'package:dartz/dartz.dart';
import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:dio/dio.dart';
import 'package:meta/meta.dart';

import 'package:rassed_app/features/add_cards/component/Card/usecase/get_types_usecase.dart';
import 'package:rassed_app/features/add_cards/component/Card/model/get_types_model.dart';

abstract class GetTypesDataSource {
  Future<Either<String, List<GetAllTypesResponseModel>>> getTypes(getTypesParams params);
}

class GetTypesDataSourceImpl extends GetTypesDataSource {
  final Dio dio;
  final DataConnectionChecker networkInfo;

  GetTypesDataSourceImpl({@required this.dio, @required this.networkInfo});

  @override
  Future<Either<String, List<GetAllTypesResponseModel>>> getTypes(getTypesParams params) async {
    List<GetAllTypesResponseModel> type;

    if (await networkInfo.hasConnection) {
      try {
        final result = await dio.get(
          Url.getCardType ,

          options: Options(
            headers:{
              'Authorization': 'Bearer ${params.api_token}',
            } ,
            followRedirects: false,
            validateStatus: (status) {
              return status < 500;
            },
          ),
        );

        print(result.data);
        type=GetAllTypesResponseModelFromJson(result.data);

        return Right(type);
      } on DioError catch (ex) {
        if (ex.type == DioErrorType.connectTimeout) {
          return Left(Er.NetworkError);
        } else if (ex.type == DioErrorType.receiveTimeout) {
          return Left(Er.NetworkError);
        }
        else if (type == null) return Left(Er.Error);
      }
    } else {
      return Left(Er.NetworkError);
    }
  }
}
