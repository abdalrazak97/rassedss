
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:rassed_app/features/Constants/ApiConstants.dart';
import 'package:dartz/dartz.dart';
import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:dio/dio.dart';
import 'package:meta/meta.dart';
import 'dart:core';
import 'package:path/path.dart';
import 'package:rassed_app/features/add_cards/component/Card/usecase/add_card_excel_usecase.dart';
import 'package:rassed_app/features/add_cards/component/Card/model/add_card_excel_model.dart';
abstract class AddCardExcelDataSource {
  Future<Either<String, AddCardExcelResponseModel>> addCardExcel(addCardExcelParams params);
}

class AddCardExcelDataSourceImpl extends AddCardExcelDataSource {
  final Dio dio;
  final DataConnectionChecker networkInfo;

  AddCardExcelDataSourceImpl({@required this.dio, @required this.networkInfo});

  @override
  Future<Either<String, AddCardExcelResponseModel>> addCardExcel(addCardExcelParams params) async {
    AddCardExcelResponseModel addCardTypeResponseModel;
    String fileName =params.file.path;
    print(fileName);
    FormData formData = FormData.fromMap({
      'type_select': params.type_select,
      "file": await MultipartFile.fromFile(params.file.path, filename: fileName,),
    });
    print(formData.files);
    if (await networkInfo.hasConnection) {
      try {
        final result = await dio.post(
          Url.addCardExcel,
          data: formData,
          options: Options(
            headers: {
              'Authorization': 'Bearer ${params.api_token}',
            },
            followRedirects: false,
            validateStatus: (status) {
              return status < 500;
            },
          ),
        );
        print(result.data);
        addCardTypeResponseModel=AddCardExcelResponseModel.fromJson(json.decode(result.data));
        return Right(addCardTypeResponseModel);
      } on DioError catch (ex) {
        if (ex.type == DioErrorType.connectTimeout) {
          return Left(Er.NetworkError);
        } else if (ex.type == DioErrorType.receiveTimeout) {
          return Left(Er.NetworkError);
        }
        else if (addCardTypeResponseModel == null) return Left(Er.Error);
      }
    } else {
      return Left(Er.NetworkError);
    }
  }
}