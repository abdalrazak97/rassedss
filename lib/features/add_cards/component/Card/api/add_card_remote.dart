
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:rassed_app/features/Constants/ApiConstants.dart';
import 'package:dartz/dartz.dart';
import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:dio/dio.dart';
import 'package:meta/meta.dart';
import 'dart:core';

import 'package:rassed_app/features/add_cards/component/Card/model/add_card_model.dart';
import 'package:rassed_app/features/add_cards/component/Card/usecase/add_card_usecase.dart';
abstract class AddCardDataSource {
  Future<Either<String, AddCardResponseModel>> addCard(addCardParams params);
}

class AddCardDataSourceImpl extends AddCardDataSource {
  final Dio dio;
  final DataConnectionChecker networkInfo;

  AddCardDataSourceImpl({@required this.dio, @required this.networkInfo});

  @override
  Future<Either<String, AddCardResponseModel>> addCard(addCardParams params) async {
    AddCardResponseModel addCardResponseModel;
    print('${params.name}  4');
    if (await networkInfo.hasConnection) {
      try {
        final result = await dio.post(
          Url.addCard,
          data: params.toJson(),
          options: Options(
            headers: {
              'Authorization': 'Bearer ${params.api_token}',
            },
            followRedirects: false,
            validateStatus: (status) {
              return status < 500;
            },
          ),
        );
        print(result.data);
        addCardResponseModel=AddCardResponseModel.fromJson(json.decode(result.data));
        return Right(addCardResponseModel);
      } on DioError catch (ex) {
        if (ex.type == DioErrorType.connectTimeout) {
          return Left(Er.NetworkError);
        } else if (ex.type == DioErrorType.receiveTimeout) {
          return Left(Er.NetworkError);
        }
        else if (addCardResponseModel == null) return Left(Er.Error);
      }
    } else {
      return Left(Er.NetworkError);
    }
  }
}