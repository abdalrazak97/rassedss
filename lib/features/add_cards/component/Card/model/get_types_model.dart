import 'dart:convert';

import 'package:json_annotation/json_annotation.dart';



part 'get_types_model.g.dart';

@JsonSerializable()
class GetAllTypesResponseModel {
  final int id;
  final String name;
  final int company_id;
  final String company;
  final String description;
  final String image;
  final bool success;
  final String message;

  Map<String, dynamic> toJson() => _$GetAllTypesResponseModelToJson(this);
  factory GetAllTypesResponseModel.fromJson(Map<String, dynamic> json) =>
      _$GetAllTypesResponseModelFromJson(json);

  GetAllTypesResponseModel({this.id, this.name, this.company_id, this.company,
      this.description, this.image, this.success, this.message});
}
List<GetAllTypesResponseModel> GetAllTypesResponseModelFromJson(String str) =>
    List<GetAllTypesResponseModel>.from(json.decode(str).map((x) => GetAllTypesResponseModel.fromJson(x)));
String GetAllTypesResponseModelToJson(List<GetAllTypesResponseModel> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));