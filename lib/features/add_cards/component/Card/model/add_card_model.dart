import 'dart:convert';

import 'package:json_annotation/json_annotation.dart';



part 'add_card_model.g.dart';

@JsonSerializable()
class AddCardResponseModel {
  final int id;
  final String name;
  final bool success;
  final String message;


  Map<String, dynamic> toJson() => _$AddCardResponseModelToJson(this);
  factory AddCardResponseModel.fromJson(Map<String, dynamic> json) =>
      _$AddCardResponseModelFromJson(json);

  AddCardResponseModel({
    this.id, this.name,this.success, this.message});
}
List<AddCardResponseModel> AddCardResponseModelFromJson(String str) =>
    List<AddCardResponseModel>.from(json.decode(str).map((x) => AddCardResponseModel.fromJson(x)));
String AddCardResponseModelToJson(List<AddCardResponseModel> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));
