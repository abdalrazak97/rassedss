// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'get_types_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

GetAllTypesResponseModel _$GetAllTypesResponseModelFromJson(
    Map<String, dynamic> json) {
  return GetAllTypesResponseModel(
    id: json['id'] as int,
    name: json['name'] as String,
    company_id: json['company_id'] as int,
    company: json['company'] as String,
    description: json['description'] as String,
    image: json['image'] as String,
    success: json['success'] as bool,
    message: json['message'] as String,
  );
}

Map<String, dynamic> _$GetAllTypesResponseModelToJson(
        GetAllTypesResponseModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'company_id': instance.company_id,
      'company': instance.company,
      'description': instance.description,
      'image': instance.image,
      'success': instance.success,
      'message': instance.message,
    };
