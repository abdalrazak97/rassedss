// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'card_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

GetCardResponseModel _$GetCardResponseModelFromJson(Map<String, dynamic> json) {
  return GetCardResponseModel(
    id: json['id'] as int,
    name: json['name'] as String,
    card_type_id: json['card_type_id'] as int,
    type: json['type'] as String,
    price: json['price'] as String,
    pinserial: json['pinserial'] as String,
    facevalue: json['facevalue'] as String,
    pinnumber: json['pinnumber'] as String,
    pinexpiry: json['pinexpiry'] as String,
    is_sale: json['is_sale'] as int,
    success: json['success'] as bool,
    message: json['message'] as String,
  );
}

Map<String, dynamic> _$GetCardResponseModelToJson(
        GetCardResponseModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'card_type_id': instance.card_type_id,
      'type': instance.type,
      'price': instance.price,
      'pinserial': instance.pinserial,
      'facevalue': instance.facevalue,
      'pinnumber': instance.pinnumber,
      'pinexpiry': instance.pinexpiry,
      'is_sale': instance.is_sale,
      'success': instance.success,
      'message': instance.message,
    };
