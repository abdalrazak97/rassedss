// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'add_card_excel_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AddCardExcelResponseModel _$AddCardExcelResponseModelFromJson(
    Map<String, dynamic> json) {
  return AddCardExcelResponseModel(
    id: json['id'] as int,
    name: json['name'] as String,
    success: json['success'] as bool,
    message: json['message'] as String,
  );
}

Map<String, dynamic> _$AddCardExcelResponseModelToJson(
        AddCardExcelResponseModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'success': instance.success,
      'message': instance.message,
    };
