import 'dart:convert';

import 'package:json_annotation/json_annotation.dart';



part 'card_model.g.dart';

@JsonSerializable()
class GetCardResponseModel {
  final int id;
  final String name;
  final int card_type_id;
  final String type;
  final String price;
  final String pinserial;
  final String facevalue;
  final String pinnumber;
  final String pinexpiry;
  final int is_sale;
  final bool success;
  final String message;


  Map<String, dynamic> toJson() => _$GetCardResponseModelToJson(this);
  factory GetCardResponseModel.fromJson(Map<String, dynamic> json) =>
      _$GetCardResponseModelFromJson(json);

  GetCardResponseModel({
      this.id,
      this.name,
      this.card_type_id,
      this.type,
      this.price,
      this.pinserial,
      this.facevalue,
      this.pinnumber,
      this.pinexpiry,
      this.is_sale,
      this.success,
      this.message});
}
List<GetCardResponseModel> GetCardResponseModelFromJson(String str) =>
    List<GetCardResponseModel>.from(json.decode(str).map((x) => GetCardResponseModel.fromJson(x)));
String GetCardResponseModelToJson(List<GetCardResponseModel> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));