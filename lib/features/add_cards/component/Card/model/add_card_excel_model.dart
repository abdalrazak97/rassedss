import 'dart:convert';

import 'package:json_annotation/json_annotation.dart';



part 'add_card_excel_model.g.dart';

@JsonSerializable()
class AddCardExcelResponseModel {
  final int id;
  final String name;
  final bool success;
  final String message;


  Map<String, dynamic> toJson() => _$AddCardExcelResponseModelToJson(this);
  factory AddCardExcelResponseModel.fromJson(Map<String, dynamic> json) =>
      _$AddCardExcelResponseModelFromJson(json);

  AddCardExcelResponseModel({
    this.id, this.name,this.success, this.message});
}

