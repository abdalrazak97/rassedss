// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'add_card_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AddCardResponseModel _$AddCardResponseModelFromJson(Map<String, dynamic> json) {
  return AddCardResponseModel(
    id: json['id'] as int,
    name: json['name'] as String,
    success: json['success'] as bool,
    message: json['message'] as String,
  );
}

Map<String, dynamic> _$AddCardResponseModelToJson(
        AddCardResponseModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'success': instance.success,
      'message': instance.message,
    };
