import 'dart:io';

import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:excel/excel.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts_arabic/fonts.dart';
import 'dart:ui' as ui;
import 'package:easy_localization/easy_localization.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:rassed_app/features/add_cards/component/Card/model/get_types_model.dart';
import 'package:rassed_app/features/settings/Preference.dart';
import 'package:rassed_app/features/widget/textauto.dart';
import '../../../../injection.dart';
import 'bloc/CardBloc.dart';
import 'bloc/CardState.dart';

class AddInfoCardType extends StatefulWidget {
  List<GetAllTypesResponseModel> getAllTypesResponseModel;

  AddInfoCardType({this.getAllTypesResponseModel});

  @override
  _AddInfoCardTypeState createState() => _AddInfoCardTypeState();
}

class copmanyClass {
  int id;
  String name;

  copmanyClass({this.id, this.name});
}
class excelClass {
  int id;
   String price;
   String pin_serial;
   String face_value;
   String pin_number;
   String pin_expiry;

  excelClass(this.id, this.price, this.pin_serial, this.face_value,
      this.pin_number, this.pin_expiry);
}


class _AddInfoCardTypeState extends State<AddInfoCardType> {
  TextEditingController priceController = TextEditingController();
  TextEditingController pinSerial = TextEditingController();
  TextEditingController faceValueController = TextEditingController();
  TextEditingController pinNumberController = TextEditingController();
  TextEditingController nameController = TextEditingController();
  TextEditingController pinExpiryController = TextEditingController();
  TextEditingController supplierController = TextEditingController();
  AddCardBloc bloc = sl<AddCardBloc>();
  String dropdownValue;
  bool switchBooll=true;
  bool addBooll=false;
  File _excel;
  File file;
  int _user;
  int p;
  String Token;
  List<copmanyClass> Companies = [];
  copmanyClass selectedComany;
  List<Data> excelRead=[];
  List<excelClass> ff=[];

  Future getImage() async {
    final result = await FilePicker.platform.pickFiles();
    PlatformFile d=result.files.first;
    print("ssss ${d.path}");
      if (result != null)
      {
        _excel=File(d.path);
        }
      else {
        print('No image selected.');
      }
  }
  @override
  void initState() {
    for (int i = 0; i < widget.getAllTypesResponseModel.length; i++)
      Companies.add(copmanyClass(
          name: widget.getAllTypesResponseModel[i].name,
          id: widget.getAllTypesResponseModel[i].id));
    print(Companies);
    Token = Preferences.getUserToken();
    _user = 0;
    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    priceController.dispose();
    pinSerial.dispose();
    pinExpiryController.dispose();
    pinNumberController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        actions: [
          SizedBox(
            width: ScreenUtil().setWidth(40),
            child: Center(
              child: TextButton(
                style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.all(Colors.blueGrey),
                ),
                onPressed: () {
                  Navigator.pop(context);
                },
                child: SvgPicture.asset(
                  "assets/icons/close3.svg",
                  width: ScreenUtil().screenWidth * 0.1,
                  color: Colors.white,
                ),
              ),
            ),
          ),
        ],
        leading: SizedBox(
          width: 5,
        ),
        backgroundColor: Colors.blueGrey,
        centerTitle: true,
        title: Text(
          'Card Type'.tr().toString(),
          textDirection: ui.TextDirection.rtl,
          style: TextStyle(
            fontFamily: ArabicFonts.Cairo,
            package: 'google_fonts_arabic',
            fontSize: 18,
          ),
        ),
      ),
      body: BlocListener<AddCardBloc, AddCardState>(
        bloc: bloc,
        listener: (context, state) {
          if (state.isSuccess && state.receive) {
            print(state.receive);
            if (state.addCardResponseModel.message == 'Card Added Successfully'|| state.addCardExcelResponseModel.message =='hatta') {
              bloc.onCardEvent();
              Navigator.pop(
                context,
              );
            } else
              AwesomeDialog(
                  width: ScreenUtil().setWidth(400),
                  context: context,
                  buttonsTextStyle: TextStyle(
                      fontSize: ScreenUtil().setSp(20), color: Colors.white),
                  dialogType: DialogType.ERROR,
                  animType: AnimType.BOTTOMSLIDE,
                  title: "Alert".toString(),
                  desc: "${state.addCardResponseModel.message.toString()}"
                      .toString(),
                  btnOkOnPress: () {},
                  btnOkColor: Colors.blueGrey)
                ..show();
          } else if (state.error != null) {
            AwesomeDialog(
                width: ScreenUtil().setWidth(400),
                context: context,
                buttonsTextStyle: TextStyle(
                    fontSize: ScreenUtil().setSp(20), color: Colors.white),
                dialogType: DialogType.ERROR,
                animType: AnimType.BOTTOMSLIDE,
                title: "Alert".toString(),
                desc: "${state.addCardResponseModel.message.toString()}"
                    .toString(),
                btnOkOnPress: () {},
                btnOkColor: Colors.blueGrey)
              ..show();
          }
        },
        child: BlocBuilder<AddCardBloc, AddCardState>(
            bloc: bloc,
            builder: (context, state) {
              return ModalProgressHUD(
                inAsyncCall: state.isLoading,
                child: SingleChildScrollView(
                  child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      textDirection: ui.TextDirection.rtl,
                      children: [
                        Padding(
                          padding: EdgeInsets.symmetric(
                              horizontal: ScreenUtil().setWidth(10),vertical: ScreenUtil().setHeight(20)),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              Expanded(
                                child: TextButton(
                                    style: ButtonStyle(
                                      backgroundColor:
                                          MaterialStateProperty.all(switchBooll?Colors.blueGrey:Colors.white),
                                      shape: MaterialStateProperty.all(
                                        RoundedRectangleBorder(
                                          side: BorderSide(
                                              color: Colors.grey,
                                              width: 1,
                                              style: BorderStyle.solid),
                                        ),
                                      ),
                                    ),
                                    child: Padding(
                                      padding: EdgeInsets.symmetric(
                                          vertical: ScreenUtil().setHeight(12)),
                                      child: Container(
                                          child: AutoTextgrey(
                                              ScreenUtil().setSp(16),
                                              "Manual".tr().toString(),
                                              switchBooll?Colors.white:Colors.blueGrey)),
                                    ),
                                    onPressed: () {
                                      setState(() {
                                        switchBooll=true;
                                      });
                                    }),
                              ),
                              Expanded(
                                child: TextButton(
                                    style: ButtonStyle(
                                      backgroundColor:
                                          MaterialStateProperty.all(
                                              switchBooll?Colors.white:Colors.blueGrey),
                                      shape: MaterialStateProperty.all(
                                        RoundedRectangleBorder(
                                          side: BorderSide(
                                              color: Colors.grey,
                                              width: 1,
                                              style: BorderStyle.solid),
                                        ),
                                      ),
                                    ),
                                    child: Padding(
                                      padding: EdgeInsets.symmetric(
                                          vertical: ScreenUtil().setHeight(12)),
                                      child: Container(
                                          child: AutoTextgrey(
                                              ScreenUtil().setSp(16),
                                              "Import Excel".tr().toString(),
                                              switchBooll?Colors.blueGrey:Colors.white)),
                                    ),
                                    onPressed: () {
                                      setState(() {
                                        switchBooll=false;
                                      });
                                    }),
                              ),
                            ],
                          ),
                        ),
                        switchBooll?Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          textDirection: ui.TextDirection.rtl,
                          children: [
                            Padding(
                              padding: EdgeInsets.symmetric(
                                  vertical: ScreenUtil().setHeight(10),
                                  horizontal: ScreenUtil().setWidth(30)),
                              child: Text(
                                'Name Card'.tr().toString(),
                                style: TextStyle(
                                    fontFamily: ArabicFonts.Cairo,
                                    package: 'google_fonts_arabic',
                                    fontSize: ScreenUtil().setSp(18),
                                    color: Colors.black),
                              ),
                            ),
                            Container(
                                width: ScreenUtil().screenWidth * 0.95,
                                color: Colors.grey.shade50,
                                padding: EdgeInsets.symmetric(
                                    horizontal: ScreenUtil().setWidth(5)),
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  textDirection: ui.TextDirection.rtl,
                                  children: [
                                    Expanded(
                                      child: TextField(
                                        textDirection: ui.TextDirection.rtl,
                                        style: TextStyle(
                                          fontSize: ScreenUtil().setSp(14),
                                        ),
                                        controller: nameController,
                                        decoration: InputDecoration(
                                            focusedBorder: OutlineInputBorder(
                                              borderSide: BorderSide(
                                                  color: Colors.blueGrey,
                                                  width: 2.0),
                                            ),
                                            enabledBorder: OutlineInputBorder(
                                              borderSide: BorderSide(
                                                  color: Colors.grey, width: 2.0),
                                            ),
                                            prefixIcon: Icon(
                                              Icons.credit_card,
                                              size: ScreenUtil().setWidth(20),
                                              color: Colors.grey,
                                              textDirection: ui.TextDirection.ltr,
                                            ),
                                            hintTextDirection: ui.TextDirection.rtl,
                                            hintText: 'Name'.toString(),
                                            hintStyle: TextStyle(
                                              fontSize: ScreenUtil().setSp(14),
                                            ),
                                            labelStyle: TextStyle(
                                              fontSize: ScreenUtil().setSp(14),
                                            )),
                                        onChanged: (text) {},
                                        onSubmitted: (string) {
                                          FocusScope.of(context)
                                              .requestFocus(new FocusNode());
                                        },
                                      ),
                                    ),
                                  ],
                                )),
                            Padding(
                              padding: EdgeInsets.symmetric(
                                  vertical: ScreenUtil().setHeight(10),
                                  horizontal: ScreenUtil().setWidth(30)),
                              child: Text(
                                'Price'.tr().toString(),
                                style: TextStyle(
                                    fontFamily: ArabicFonts.Cairo,
                                    package: 'google_fonts_arabic',
                                    fontSize: ScreenUtil().setSp(18),
                                    color: Colors.black),
                              ),
                            ),
                            Container(
                                width: ScreenUtil().screenWidth * 0.95,
                                color: Colors.grey.shade50,
                                padding: EdgeInsets.symmetric(
                                    horizontal: ScreenUtil().setWidth(5)),
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  textDirection: ui.TextDirection.rtl,
                                  children: [
                                    Expanded(
                                      child: TextField(
                                        textDirection: ui.TextDirection.rtl,
                                        style: TextStyle(
                                          fontSize: ScreenUtil().setSp(14),
                                        ),
                                        controller: priceController,
                                        decoration: InputDecoration(
                                            focusedBorder: OutlineInputBorder(
                                              borderSide: BorderSide(
                                                  color: Colors.blueGrey,
                                                  width: 2.0),
                                            ),
                                            enabledBorder: OutlineInputBorder(
                                              borderSide: BorderSide(
                                                  color: Colors.grey, width: 2.0),
                                            ),
                                            prefixIcon: Icon(
                                              Icons.credit_card,
                                              size: ScreenUtil().setWidth(20),
                                              color: Colors.grey,
                                              textDirection: ui.TextDirection.ltr,
                                            ),
                                            hintTextDirection: ui.TextDirection.rtl,
                                            hintText: 'Price'.toString(),
                                            hintStyle: TextStyle(
                                              fontSize: ScreenUtil().setSp(14),
                                            ),
                                            labelStyle: TextStyle(
                                              fontSize: ScreenUtil().setSp(14),
                                            )),
                                        onChanged: (text) {},
                                        onSubmitted: (string) {
                                          FocusScope.of(context)
                                              .requestFocus(new FocusNode());
                                        },
                                      ),
                                    ),
                                  ],
                                )),
                            Padding(
                              padding: EdgeInsets.symmetric(
                                  vertical: ScreenUtil().setHeight(10),
                                  horizontal: ScreenUtil().setWidth(30)),
                              child: Text(
                                'Pin Serial'.tr().toString(),
                                style: TextStyle(
                                    fontFamily: ArabicFonts.Cairo,
                                    package: 'google_fonts_arabic',
                                    fontSize: ScreenUtil().setSp(18),
                                    color: Colors.black),
                              ),
                            ),
                            Container(
                                width: ScreenUtil().screenWidth * 0.95,
                                color: Colors.grey.shade50,
                                padding: EdgeInsets.symmetric(
                                    horizontal: ScreenUtil().setWidth(5)),
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  textDirection: ui.TextDirection.rtl,
                                  children: [
                                    Expanded(
                                      child: TextField(
                                        textDirection: ui.TextDirection.rtl,
                                        style: TextStyle(
                                          fontSize: ScreenUtil().setSp(14),
                                        ),
                                        controller: pinSerial,
                                        decoration: InputDecoration(
                                            focusedBorder: OutlineInputBorder(
                                              borderSide: BorderSide(
                                                  color: Colors.blueGrey,
                                                  width: 2.0),
                                            ),
                                            enabledBorder: OutlineInputBorder(
                                              borderSide: BorderSide(
                                                  color: Colors.grey, width: 2.0),
                                            ),
                                            hintTextDirection: ui.TextDirection.rtl,
                                            hintText: 'Pin Serial'.toString(),
                                            hintStyle: TextStyle(
                                              fontSize: ScreenUtil().setSp(14),
                                            ),
                                            labelStyle: TextStyle(
                                              fontSize: ScreenUtil().setSp(14),
                                            )),
                                        onChanged: (text) {},
                                        onSubmitted: (string) {
                                          FocusScope.of(context)
                                              .requestFocus(new FocusNode());
                                        },
                                      ),
                                    ),
                                  ],
                                )),
                            Padding(
                              padding: EdgeInsets.symmetric(
                                  vertical: ScreenUtil().setHeight(10),
                                  horizontal: ScreenUtil().setWidth(30)),
                              child: Text(
                                'Face Value'.tr().toString(),
                                style: TextStyle(
                                    fontFamily: ArabicFonts.Cairo,
                                    package: 'google_fonts_arabic',
                                    fontSize: ScreenUtil().setSp(18),
                                    color: Colors.black),
                              ),
                            ),
                            Container(
                                width: ScreenUtil().screenWidth * 0.95,
                                color: Colors.grey.shade50,
                                padding: EdgeInsets.symmetric(
                                    horizontal: ScreenUtil().setWidth(5)),
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  textDirection: ui.TextDirection.rtl,
                                  children: [
                                    Expanded(
                                      child: TextField(
                                        textDirection: ui.TextDirection.rtl,
                                        style: TextStyle(
                                          fontSize: ScreenUtil().setSp(14),
                                        ),
                                        controller: faceValueController,
                                        decoration: InputDecoration(
                                            focusedBorder: OutlineInputBorder(
                                              borderSide: BorderSide(
                                                  color: Colors.blueGrey,
                                                  width: 2.0),
                                            ),
                                            enabledBorder: OutlineInputBorder(
                                              borderSide: BorderSide(
                                                  color: Colors.grey, width: 2.0),
                                            ),
                                            hintTextDirection: ui.TextDirection.rtl,
                                            hintText: 'Face Value'.toString(),
                                            hintStyle: TextStyle(
                                              fontSize: ScreenUtil().setSp(14),
                                            ),
                                            labelStyle: TextStyle(
                                              fontSize: ScreenUtil().setSp(14),
                                            )),
                                        onChanged: (text) {},
                                        onSubmitted: (string) {
                                          FocusScope.of(context)
                                              .requestFocus(new FocusNode());
                                        },
                                      ),
                                    ),
                                  ],
                                )),
                            Padding(
                              padding: EdgeInsets.symmetric(
                                  vertical: ScreenUtil().setHeight(10),
                                  horizontal: ScreenUtil().setWidth(30)),
                              child: Text(
                                'Pin number'.tr().toString(),
                                style: TextStyle(
                                    fontFamily: ArabicFonts.Cairo,
                                    package: 'google_fonts_arabic',
                                    fontSize: ScreenUtil().setSp(18),
                                    color: Colors.black),
                              ),
                            ),
                            Container(
                                width: ScreenUtil().screenWidth * 0.95,
                                color: Colors.grey.shade50,
                                padding: EdgeInsets.symmetric(
                                    horizontal: ScreenUtil().setWidth(5)),
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  textDirection: ui.TextDirection.rtl,
                                  children: [
                                    Expanded(
                                      child: TextField(
                                        textDirection: ui.TextDirection.rtl,
                                        style: TextStyle(
                                          fontSize: ScreenUtil().setSp(14),
                                        ),
                                        controller: pinNumberController,
                                        decoration: InputDecoration(
                                            focusedBorder: OutlineInputBorder(
                                              borderSide: BorderSide(
                                                  color: Colors.blueGrey,
                                                  width: 2.0),
                                            ),
                                            enabledBorder: OutlineInputBorder(
                                              borderSide: BorderSide(
                                                  color: Colors.grey, width: 2.0),
                                            ),
                                            hintTextDirection: ui.TextDirection.rtl,
                                            hintText: 'Pin number'.toString(),
                                            hintStyle: TextStyle(
                                              fontSize: ScreenUtil().setSp(14),
                                            ),
                                            labelStyle: TextStyle(
                                              fontSize: ScreenUtil().setSp(14),
                                            )),
                                        onChanged: (text) {},
                                        onSubmitted: (string) {
                                          FocusScope.of(context)
                                              .requestFocus(new FocusNode());
                                        },
                                      ),
                                    ),
                                  ],
                                )),
                            Padding(
                              padding: EdgeInsets.symmetric(
                                  vertical: ScreenUtil().setHeight(10),
                                  horizontal: ScreenUtil().setWidth(30)),
                              child: Text(
                                'Pin expiry'.tr().toString(),
                                style: TextStyle(
                                    fontFamily: ArabicFonts.Cairo,
                                    package: 'google_fonts_arabic',
                                    fontSize: ScreenUtil().setSp(18),
                                    color: Colors.black),
                              ),
                            ),
                            Container(
                                width: ScreenUtil().screenWidth * 0.95,
                                color: Colors.grey.shade50,
                                padding: EdgeInsets.symmetric(
                                    horizontal: ScreenUtil().setWidth(5)),
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  textDirection: ui.TextDirection.rtl,
                                  children: [
                                    Expanded(
                                      child: TextField(
                                        textDirection: ui.TextDirection.rtl,
                                        style: TextStyle(
                                          fontSize: ScreenUtil().setSp(14),
                                        ),
                                        controller: pinExpiryController,
                                        decoration: InputDecoration(
                                            focusedBorder: OutlineInputBorder(
                                              borderSide: BorderSide(
                                                  color: Colors.blueGrey,
                                                  width: 2.0),
                                            ),
                                            enabledBorder: OutlineInputBorder(
                                              borderSide: BorderSide(
                                                  color: Colors.grey, width: 2.0),
                                            ),
                                            hintTextDirection: ui.TextDirection.rtl,
                                            hintText: 'Pin expiry'.toString(),
                                            hintStyle: TextStyle(
                                              fontSize: ScreenUtil().setSp(14),
                                            ),
                                            labelStyle: TextStyle(
                                              fontSize: ScreenUtil().setSp(14),
                                            )),
                                        onChanged: (text) {},
                                        onSubmitted: (string) {
                                          FocusScope.of(context)
                                              .requestFocus(new FocusNode());
                                        },
                                      ),
                                    ),
                                  ],
                                )),
                            Padding(
                              padding: EdgeInsets.symmetric(
                                  vertical: ScreenUtil().setHeight(10),
                                  horizontal: ScreenUtil().setWidth(30)),
                              child: Text(
                                'Supplier number'.tr().toString(),
                                style: TextStyle(
                                    fontFamily: ArabicFonts.Cairo,
                                    package: 'google_fonts_arabic',
                                    fontSize: ScreenUtil().setSp(18),
                                    color: Colors.black),
                              ),
                            ),
                            Container(
                                width: ScreenUtil().screenWidth * 0.95,
                                color: Colors.grey.shade50,
                                padding: EdgeInsets.symmetric(
                                    horizontal: ScreenUtil().setWidth(5)),
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  textDirection: ui.TextDirection.rtl,
                                  children: [
                                    Expanded(
                                      child: TextField(
                                        textDirection: ui.TextDirection.rtl,
                                        style: TextStyle(
                                          fontSize: ScreenUtil().setSp(14),
                                        ),
                                        controller: supplierController,
                                        decoration: InputDecoration(
                                            focusedBorder: OutlineInputBorder(
                                              borderSide: BorderSide(
                                                  color: Colors.blueGrey,
                                                  width: 2.0),
                                            ),
                                            enabledBorder: OutlineInputBorder(
                                              borderSide: BorderSide(
                                                  color: Colors.grey, width: 2.0),
                                            ),
                                            hintTextDirection: ui.TextDirection.rtl,
                                            hintText: 'Supplier number'.toString(),
                                            hintStyle: TextStyle(
                                              fontSize: ScreenUtil().setSp(14),
                                            ),
                                            labelStyle: TextStyle(
                                              fontSize: ScreenUtil().setSp(14),
                                            )),
                                        onChanged: (text) {},
                                        onSubmitted: (string) {
                                          FocusScope.of(context)
                                              .requestFocus(new FocusNode());
                                        },
                                      ),
                                    ),
                                  ],
                                )),
                            Padding(
                              padding: EdgeInsets.symmetric(
                                  vertical: ScreenUtil().setHeight(10),
                                  horizontal: ScreenUtil().setWidth(30)),
                              child: Text(
                                'Card Type Category'.tr().toString(),
                                style: TextStyle(
                                    fontFamily: ArabicFonts.Cairo,
                                    package: 'google_fonts_arabic',
                                    fontSize: ScreenUtil().setSp(18),
                                    color: Colors.black),
                              ),
                            ),
                            Container(
                                width: ScreenUtil().screenWidth * 0.95,
                                color: Colors.grey.shade50,
                                padding: EdgeInsets.symmetric(
                                    horizontal: ScreenUtil().setWidth(5)),
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  textDirection: ui.TextDirection.rtl,
                                  children: [
                                    SizedBox(
                                      width: ScreenUtil().screenWidth * 0.8,
                                      child: Row(
                                        textDirection: ui.TextDirection.rtl,
                                        children: [
                                          Container(
                                            padding: EdgeInsets.symmetric(
                                                horizontal: 10.0),
                                            decoration: BoxDecoration(
                                              borderRadius:
                                              BorderRadius.circular(15.0),
                                              border: Border.all(
                                                  color: Colors.blueGrey,
                                                  style: BorderStyle.solid,
                                                  width: 0.80),
                                            ),
                                            child: DropdownButton<copmanyClass>(
                                              style:
                                              TextStyle(color: Colors.blueGrey),
                                              value: selectedComany,
                                              elevation: 2,
                                              onChanged: (copmanyClass pp) {
                                                FocusScope.of(context)
                                                    .requestFocus(new FocusNode());

                                                setState(() {
                                                  _user = pp.id;
                                                  selectedComany = pp;
                                                });
                                              },
                                              items:
                                              Companies.map((copmanyClass p) {
                                                return DropdownMenuItem<
                                                    copmanyClass>(
                                                    value: p,
                                                    child: Center(
                                                        child: Text(
                                                          p.name,
                                                        )));
                                              }).toList(),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                )),
                            SizedBox(
                              height: ScreenUtil().screenHeight * 0.05,
                            ),
                            Center(
                              child: SizedBox(
                                height: ScreenUtil().screenHeight * 0.1,
                                width: ScreenUtil().screenWidth * 0.8,
                                child: TextButton(
                                    style: ButtonStyle(
                                      backgroundColor: MaterialStateProperty.all(
                                          Colors.blueGrey),
                                      shape: MaterialStateProperty.all(
                                          RoundedRectangleBorder(
                                              borderRadius:
                                              BorderRadius.circular(15.0),
                                              side: BorderSide(
                                                  color: Colors.grey.shade50))),
                                    ),
                                    child: Padding(
                                      padding: EdgeInsets.symmetric(
                                          vertical: ScreenUtil().setHeight(12)),
                                      child: Container(
                                          child: AutoTextgrey(
                                              ScreenUtil().setSp(16),
                                              "Save".tr().toString(),
                                              Colors.grey.shade50)),
                                    ),
                                    onPressed: () {
                                      bloc.onChangeApiToken(Token);
                                      bloc.onChangePrice(priceController.text);
                                      bloc.onChangeFaceValue(faceValueController.text);
                                      bloc.onChangePinSerial(pinSerial.text);
                                      bloc.onChangeTypeSelect(_user);
                                      bloc.onChangePinExpiry(pinExpiryController.text);
                                      bloc.onChangePinNumber(pinNumberController.text);
                                      bloc.onChangeNameCard(nameController.text);
                                      bloc.onChangeSupplireNumber(supplierController.text);
                                      bloc.onAddCardEvent();
                                    }),
                              ),
                            ),
                        ],):Column(
                          textDirection: ui.TextDirection.rtl,
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                          Padding(
                            padding: EdgeInsets.symmetric(
                                vertical: ScreenUtil().setHeight(10),
                                horizontal: ScreenUtil().setWidth(30)),
                            child: Text(
                              'Select Excel file'.tr().toString(),
                              style: TextStyle(
                                  fontFamily: ArabicFonts.Cairo,
                                  package: 'google_fonts_arabic',
                                  fontSize: ScreenUtil().setSp(18),
                                  color: Colors.black),
                            ),
                          ),
                          Container(
                              width: ScreenUtil().screenWidth * 0.95,
                              height: ScreenUtil().screenHeight * 0.15,
                              color: Colors.grey.shade50,
                              padding: EdgeInsets.symmetric(
                                  horizontal: ScreenUtil().setWidth(5)),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.start,
                                textDirection: ui.TextDirection.rtl,
                                children: [
                                  SizedBox(
                                    height: ScreenUtil().screenHeight * 0.1,
                                    width: ScreenUtil().screenWidth * 0.4,
                                    child: TextButton(
                                        style: ButtonStyle(
                                          backgroundColor: MaterialStateProperty.all(
                                              Colors.grey.shade50),
                                          shape: MaterialStateProperty.all(
                                              RoundedRectangleBorder(
                                                  borderRadius: BorderRadius.circular(
                                                      15.0),
                                                  side:
                                                  BorderSide(color: Colors.blueGrey))),
                                        ),
                                        child: Padding(
                                          padding: EdgeInsets.symmetric(
                                              vertical: ScreenUtil().setHeight(12)),
                                          child: Container(
                                              child: AutoTextgrey(
                                                  ScreenUtil().setSp(16),
                                                  "Select Excel".tr().toString(),
                                                  Colors.blueGrey)),
                                        ),
                                        onPressed: ()  {
                                           getImage();
                                        }),
                                  ),

                                  ],
                              )),

                          Padding(
                            padding: EdgeInsets.symmetric(
                                vertical: ScreenUtil().setHeight(0),
                                horizontal: ScreenUtil().setWidth(30)),
                            child: Text(
                              'Card Type Category'.tr().toString(),
                              style: TextStyle(
                                  fontFamily: ArabicFonts.Cairo,
                                  package: 'google_fonts_arabic',
                                  fontSize: ScreenUtil().setSp(18),
                                  color: Colors.black),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.symmetric(
                                vertical: ScreenUtil().setHeight(10),
                                horizontal: ScreenUtil().setWidth(30)),
                            child: Container(
                                width: ScreenUtil().screenWidth * 0.95,
                                color: Colors.grey.shade50,
                                padding: EdgeInsets.symmetric(
                                    horizontal: ScreenUtil().setWidth(5)),
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  textDirection: ui.TextDirection.ltr,
                                  children: [
                                    SizedBox(
                                      width: ScreenUtil().screenWidth * 0.8,
                                      child: Row(
                                        textDirection: ui.TextDirection.rtl,
                                        children: [
                                          Container(
                                            padding: EdgeInsets.symmetric(
                                                horizontal: 10.0),
                                            decoration: BoxDecoration(
                                              borderRadius:
                                              BorderRadius.circular(15.0),
                                              border: Border.all(
                                                  color: Colors.blueGrey,
                                                  style: BorderStyle.solid,
                                                  width: 0.80),
                                            ),
                                            child: DropdownButton<copmanyClass>(
                                              style:
                                              TextStyle(color: Colors.blueGrey),
                                              value: selectedComany,
                                              elevation: 2,
                                              onChanged: (copmanyClass pp) {
                                                FocusScope.of(context)
                                                    .requestFocus(new FocusNode());

                                                setState(() {
                                                  _user = pp.id;
                                                  print(_user);
                                                  selectedComany = pp;
                                                });
                                              },
                                              items:
                                              Companies.map((copmanyClass p) {
                                                return DropdownMenuItem<
                                                    copmanyClass>(
                                                    value: p,
                                                    child: Center(
                                                        child: Text(
                                                          p.name,
                                                        )));
                                              }).toList(),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                )),
                          ),
                            SizedBox(height: ScreenUtil().screenHeight*0.2,),
                            Center(
                              child: SizedBox(
                                height: ScreenUtil().screenHeight * 0.1,
                                width: ScreenUtil().screenWidth * 0.8,
                                child: TextButton(
                                    style: ButtonStyle(
                                      backgroundColor: MaterialStateProperty.all(
                                          Colors.blueGrey),
                                      shape: MaterialStateProperty.all(
                                          RoundedRectangleBorder(
                                              borderRadius:
                                              BorderRadius.circular(15.0),
                                              side: BorderSide(
                                                  color: Colors.grey.shade50))),
                                    ),
                                    child: Padding(
                                      padding: EdgeInsets.symmetric(
                                          vertical: ScreenUtil().setHeight(12)),
                                      child: Container(
                                          child: AutoTextgrey(
                                              ScreenUtil().setSp(16),
                                              "Save".tr().toString(),
                                              Colors.grey.shade50)),
                                    ),
                                    onPressed: () {
                                      bloc.onChangeApiToken(Token);
                                      bloc.onChangeTypeSelect(_user);
                                      bloc.onChangeExcelFile(_excel);
                                      bloc.onAddCardExcelEvent();
                                    }),
                              ),
                            ),
                        ],),

                      ]),
                ),
              );
            }),
      ),
    );
  }
}
