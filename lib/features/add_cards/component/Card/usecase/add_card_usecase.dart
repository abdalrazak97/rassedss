import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:rassed_app/features/Constants/usecase.dart';
import 'package:rassed_app/features/add_cards/component/Card/api/add_card_remote.dart';
import 'package:rassed_app/features/add_cards/component/Card/model/add_card_model.dart';



class AddCardUseCase extends UseCase<AddCardResponseModel, addCardParams> {
  final AddCardDataSource addCardDataSource;

  AddCardUseCase({this.addCardDataSource});

  @override
  Future<Either<String, AddCardResponseModel>> call(addCardParams params) async {
    return await addCardDataSource.addCard(params);

  }
}

class addCardParams extends Equatable {
  final String api_token;
  final int type_select;
  final String name;
  final String suplier;
  final String price;
  final String pin_serial;
  final String face_value;
  final String pin_number;
  final String pin_expiry;


  addCardParams({this.api_token, this.type_select, this.price,
      this.pin_serial, this.face_value, this.pin_number, this.pin_expiry,this.name,this.suplier});

  @override
  // TODO: implement props
  List<Object> get props => [api_token];
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['api_token'] = this.api_token;
    data['type_select'] = this.type_select;
    data['price'] = this.price;
    data['pin_serial'] = this.pin_serial;
    data['face_value'] = this.face_value;
    data['pin_number'] = this.pin_number;
    data['pin_expiry'] = this.pin_expiry;
    data['name'] = this.name;
    data['suplier'] = this.suplier;
    return data;
  }
}
