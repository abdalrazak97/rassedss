import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:rassed_app/features/Constants/usecase.dart';
import 'package:rassed_app/features/add_cards/component/Card/api/get_types_remote.dart';
import 'package:rassed_app/features/add_cards/component/Card/model/get_types_model.dart';


class GetTypesUseCase extends UseCase<List<GetAllTypesResponseModel>,getTypesParams > {
  final GetTypesDataSource getTypes;

  GetTypesUseCase({this.getTypes});

  @override
  Future<Either<String, List<GetAllTypesResponseModel>>> call(getTypesParams params) async {
    return await getTypes.getTypes(params);

  }
}

class getTypesParams extends Equatable {
  final String api_token;

  getTypesParams(
      {@required this.api_token,});

  @override
  // TODO: implement props
  List<Object> get props => [api_token];
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['api_token'] = this.api_token;

    return data;
  }
}
