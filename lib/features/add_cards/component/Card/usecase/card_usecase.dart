import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:rassed_app/features/Constants/usecase.dart';
import 'package:rassed_app/features/add_cards/component/Card/api/get_cards_remote.dart';
import 'package:rassed_app/features/add_cards/component/Card/model/card_model.dart';



class GetCardsUseCase extends UseCase<List<GetCardResponseModel>, getCardParams> {
  final GetCardDataSource cardDataSource;

  GetCardsUseCase({this.cardDataSource});

  @override
  Future<Either<String, List<GetCardResponseModel>>> call(getCardParams params) async {
    return await cardDataSource.getCard(params);

  }
}

class getCardParams extends Equatable {
  final String api_token;

  getCardParams(
      {@required this.api_token,});

  @override
  // TODO: implement props
  List<Object> get props => [api_token];
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['api_token'] = this.api_token;
    return data;
  }
}
