
import 'dart:io';

import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:rassed_app/features/Constants/usecase.dart';
import 'package:rassed_app/features/add_cards/component/Card/api/add_card_excel_remote.dart';
import 'package:rassed_app/features/add_cards/component/Card/model/add_card_excel_model.dart';



class AddCardExcelUseCase extends UseCase<AddCardExcelResponseModel, addCardExcelParams> {
  final AddCardExcelDataSource addCardExcelDataSource;

  AddCardExcelUseCase({this.addCardExcelDataSource});

  @override
  Future<Either<String, AddCardExcelResponseModel>> call(addCardExcelParams params) async {
    return await addCardExcelDataSource.addCardExcel(params);

  }
}

class addCardExcelParams extends Equatable {
  final String api_token;
  final int type_select;
  final File file;


  addCardExcelParams({this.api_token, this.type_select, this.file});

  @override
  // TODO: implement props
  List<Object> get props => [api_token];
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['api_token'] = this.api_token;
    data['type_select'] = this.type_select;
    data['file']= this.file;
    return data;
  }
}
