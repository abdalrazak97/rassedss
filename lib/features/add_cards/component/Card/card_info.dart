import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:rassed_app/features/Constants/ApiConstants.dart';
import 'package:rassed_app/features/widget/textauto.dart';

class CardInfo extends StatelessWidget {
  final int count;
  final int index;
  final String name;
  final String value;
  final int is_sale;
  CardInfo({ this.count,this.index,this.name,this.value,this.is_sale});
  @override
  Widget build(BuildContext context) {

    return OrientationBuilder(
        builder: (context, orientation) {
          bool isPotrait = MediaQuery
              .of(context)
              .orientation == Orientation.portrait;
          return is_sale==0?Container(
            decoration: BoxDecoration(
                border: Border.all(color: Colors.grey.shade200)
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  height:isPotrait? ScreenUtil().screenWidth*0.2:ScreenUtil().screenWidth*0.06 ,
                  width:isPotrait? ScreenUtil().screenWidth*0.2:ScreenUtil().screenWidth*0.06 ,
                  decoration: BoxDecoration(
                    color: Colors.grey.shade400,
                  ),
                  child: Padding(
                    padding:  EdgeInsets.symmetric(
                        horizontal: ScreenUtil().setWidth(4),
                        vertical: ScreenUtil().setHeight(4)),
                    child: Container(
                      child: Align(
                          alignment: Alignment.center ,
                          child:value.length>2?
                          AutoTextgrey(
                              isPotrait?ScreenUtil().setSp(14): ScreenUtil().setSp(18),
                              value[0]+value[1], Colors.white):
                          AutoTextgrey(
                              isPotrait?ScreenUtil().setSp(14): ScreenUtil().setSp(18)
                              ,
                              name, Colors.white)),
                    ),
                  ),
                ),
                Container(
                  width:isPotrait?ScreenUtil().screenWidth*0.73:ScreenUtil().screenWidth*0.48,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                          child: AutoTextgrey(
                              isPotrait?ScreenUtil().setSp(14): ScreenUtil().setSp(18), value, Colors.grey.shade800)),

                      Padding(
                        padding:  EdgeInsets.only(right:ScreenUtil().setWidth(15) ),
                        child: Container(
                            child: Text( name,style: TextStyle(
                              fontSize: isPotrait?ScreenUtil().setSp(14):
                              ScreenUtil().setSp(18),
                              color: Colors.grey.shade800,
                            ),)),
                      ),


                    ],
                  ),
                ),
              ],
            ),
          ):SizedBox();}
    );


  }

}
