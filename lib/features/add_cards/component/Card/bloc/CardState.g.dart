// GENERATED CODE - DO NOT MODIFY BY HAND

part of CardTypeState;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$AddCardState extends AddCardState {
  @override
  final bool isSuccess;
  @override
  final bool isLoading;
  @override
  final bool receive;
  @override
  final String api_token;
  @override
  final String name;
  @override
  final String suplier;
  @override
  final int category_id;
  @override
  final int company_id;
  @override
  final int type_select;
  @override
  final String price;
  @override
  final String pin_serial;
  @override
  final String face_value;
  @override
  final String pin_number;
  @override
  final String pin_expiry;
  @override
  final ErrorCode error;
  @override
  final File file;
  @override
  final String errorMessage;
  @override
  final List<GetCardResponseModel> getCardResponseModel;
  @override
  final List<GetAllTypesResponseModel> getAllTypesResponseModel;
  @override
  final AddCardResponseModel addCardResponseModel;
  @override
  final AddCardExcelResponseModel addCardExcelResponseModel;

  factory _$AddCardState([void Function(AddCardStateBuilder) updates]) =>
      (new AddCardStateBuilder()..update(updates)).build();

  _$AddCardState._(
      {this.isSuccess,
      this.isLoading,
      this.receive,
      this.api_token,
      this.name,
      this.suplier,
      this.category_id,
      this.company_id,
      this.type_select,
      this.price,
      this.pin_serial,
      this.face_value,
      this.pin_number,
      this.pin_expiry,
      this.error,
      this.file,
      this.errorMessage,
      this.getCardResponseModel,
      this.getAllTypesResponseModel,
      this.addCardResponseModel,
      this.addCardExcelResponseModel})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        isSuccess, 'AddCardState', 'isSuccess');
    BuiltValueNullFieldError.checkNotNull(
        isLoading, 'AddCardState', 'isLoading');
    BuiltValueNullFieldError.checkNotNull(receive, 'AddCardState', 'receive');
    BuiltValueNullFieldError.checkNotNull(
        api_token, 'AddCardState', 'api_token');
    BuiltValueNullFieldError.checkNotNull(name, 'AddCardState', 'name');
    BuiltValueNullFieldError.checkNotNull(suplier, 'AddCardState', 'suplier');
    BuiltValueNullFieldError.checkNotNull(
        category_id, 'AddCardState', 'category_id');
    BuiltValueNullFieldError.checkNotNull(
        company_id, 'AddCardState', 'company_id');
    BuiltValueNullFieldError.checkNotNull(
        type_select, 'AddCardState', 'type_select');
    BuiltValueNullFieldError.checkNotNull(price, 'AddCardState', 'price');
    BuiltValueNullFieldError.checkNotNull(
        pin_serial, 'AddCardState', 'pin_serial');
    BuiltValueNullFieldError.checkNotNull(
        face_value, 'AddCardState', 'face_value');
    BuiltValueNullFieldError.checkNotNull(
        pin_number, 'AddCardState', 'pin_number');
    BuiltValueNullFieldError.checkNotNull(
        pin_expiry, 'AddCardState', 'pin_expiry');
    BuiltValueNullFieldError.checkNotNull(
        errorMessage, 'AddCardState', 'errorMessage');
    BuiltValueNullFieldError.checkNotNull(
        getCardResponseModel, 'AddCardState', 'getCardResponseModel');
    BuiltValueNullFieldError.checkNotNull(
        getAllTypesResponseModel, 'AddCardState', 'getAllTypesResponseModel');
    BuiltValueNullFieldError.checkNotNull(
        addCardResponseModel, 'AddCardState', 'addCardResponseModel');
    BuiltValueNullFieldError.checkNotNull(
        addCardExcelResponseModel, 'AddCardState', 'addCardExcelResponseModel');
  }

  @override
  AddCardState rebuild(void Function(AddCardStateBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  AddCardStateBuilder toBuilder() => new AddCardStateBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is AddCardState &&
        isSuccess == other.isSuccess &&
        isLoading == other.isLoading &&
        receive == other.receive &&
        api_token == other.api_token &&
        name == other.name &&
        suplier == other.suplier &&
        category_id == other.category_id &&
        company_id == other.company_id &&
        type_select == other.type_select &&
        price == other.price &&
        pin_serial == other.pin_serial &&
        face_value == other.face_value &&
        pin_number == other.pin_number &&
        pin_expiry == other.pin_expiry &&
        error == other.error &&
        file == other.file &&
        errorMessage == other.errorMessage &&
        getCardResponseModel == other.getCardResponseModel &&
        getAllTypesResponseModel == other.getAllTypesResponseModel &&
        addCardResponseModel == other.addCardResponseModel &&
        addCardExcelResponseModel == other.addCardExcelResponseModel;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc(
                        $jc(
                            $jc(
                                $jc(
                                    $jc(
                                        $jc(
                                            $jc(
                                                $jc(
                                                    $jc(
                                                        $jc(
                                                            $jc(
                                                                $jc(
                                                                    $jc(
                                                                        $jc(
                                                                            $jc($jc($jc(0, isSuccess.hashCode), isLoading.hashCode),
                                                                                receive.hashCode),
                                                                            api_token.hashCode),
                                                                        name.hashCode),
                                                                    suplier.hashCode),
                                                                category_id.hashCode),
                                                            company_id.hashCode),
                                                        type_select.hashCode),
                                                    price.hashCode),
                                                pin_serial.hashCode),
                                            face_value.hashCode),
                                        pin_number.hashCode),
                                    pin_expiry.hashCode),
                                error.hashCode),
                            file.hashCode),
                        errorMessage.hashCode),
                    getCardResponseModel.hashCode),
                getAllTypesResponseModel.hashCode),
            addCardResponseModel.hashCode),
        addCardExcelResponseModel.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('AddCardState')
          ..add('isSuccess', isSuccess)
          ..add('isLoading', isLoading)
          ..add('receive', receive)
          ..add('api_token', api_token)
          ..add('name', name)
          ..add('suplier', suplier)
          ..add('category_id', category_id)
          ..add('company_id', company_id)
          ..add('type_select', type_select)
          ..add('price', price)
          ..add('pin_serial', pin_serial)
          ..add('face_value', face_value)
          ..add('pin_number', pin_number)
          ..add('pin_expiry', pin_expiry)
          ..add('error', error)
          ..add('file', file)
          ..add('errorMessage', errorMessage)
          ..add('getCardResponseModel', getCardResponseModel)
          ..add('getAllTypesResponseModel', getAllTypesResponseModel)
          ..add('addCardResponseModel', addCardResponseModel)
          ..add('addCardExcelResponseModel', addCardExcelResponseModel))
        .toString();
  }
}

class AddCardStateBuilder
    implements Builder<AddCardState, AddCardStateBuilder> {
  _$AddCardState _$v;

  bool _isSuccess;
  bool get isSuccess => _$this._isSuccess;
  set isSuccess(bool isSuccess) => _$this._isSuccess = isSuccess;

  bool _isLoading;
  bool get isLoading => _$this._isLoading;
  set isLoading(bool isLoading) => _$this._isLoading = isLoading;

  bool _receive;
  bool get receive => _$this._receive;
  set receive(bool receive) => _$this._receive = receive;

  String _api_token;
  String get api_token => _$this._api_token;
  set api_token(String api_token) => _$this._api_token = api_token;

  String _name;
  String get name => _$this._name;
  set name(String name) => _$this._name = name;

  String _suplier;
  String get suplier => _$this._suplier;
  set suplier(String suplier) => _$this._suplier = suplier;

  int _category_id;
  int get category_id => _$this._category_id;
  set category_id(int category_id) => _$this._category_id = category_id;

  int _company_id;
  int get company_id => _$this._company_id;
  set company_id(int company_id) => _$this._company_id = company_id;

  int _type_select;
  int get type_select => _$this._type_select;
  set type_select(int type_select) => _$this._type_select = type_select;

  String _price;
  String get price => _$this._price;
  set price(String price) => _$this._price = price;

  String _pin_serial;
  String get pin_serial => _$this._pin_serial;
  set pin_serial(String pin_serial) => _$this._pin_serial = pin_serial;

  String _face_value;
  String get face_value => _$this._face_value;
  set face_value(String face_value) => _$this._face_value = face_value;

  String _pin_number;
  String get pin_number => _$this._pin_number;
  set pin_number(String pin_number) => _$this._pin_number = pin_number;

  String _pin_expiry;
  String get pin_expiry => _$this._pin_expiry;
  set pin_expiry(String pin_expiry) => _$this._pin_expiry = pin_expiry;

  ErrorCode _error;
  ErrorCode get error => _$this._error;
  set error(ErrorCode error) => _$this._error = error;

  File _file;
  File get file => _$this._file;
  set file(File file) => _$this._file = file;

  String _errorMessage;
  String get errorMessage => _$this._errorMessage;
  set errorMessage(String errorMessage) => _$this._errorMessage = errorMessage;

  List<GetCardResponseModel> _getCardResponseModel;
  List<GetCardResponseModel> get getCardResponseModel =>
      _$this._getCardResponseModel;
  set getCardResponseModel(List<GetCardResponseModel> getCardResponseModel) =>
      _$this._getCardResponseModel = getCardResponseModel;

  List<GetAllTypesResponseModel> _getAllTypesResponseModel;
  List<GetAllTypesResponseModel> get getAllTypesResponseModel =>
      _$this._getAllTypesResponseModel;
  set getAllTypesResponseModel(
          List<GetAllTypesResponseModel> getAllTypesResponseModel) =>
      _$this._getAllTypesResponseModel = getAllTypesResponseModel;

  AddCardResponseModel _addCardResponseModel;
  AddCardResponseModel get addCardResponseModel => _$this._addCardResponseModel;
  set addCardResponseModel(AddCardResponseModel addCardResponseModel) =>
      _$this._addCardResponseModel = addCardResponseModel;

  AddCardExcelResponseModel _addCardExcelResponseModel;
  AddCardExcelResponseModel get addCardExcelResponseModel =>
      _$this._addCardExcelResponseModel;
  set addCardExcelResponseModel(
          AddCardExcelResponseModel addCardExcelResponseModel) =>
      _$this._addCardExcelResponseModel = addCardExcelResponseModel;

  AddCardStateBuilder();

  AddCardStateBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _isSuccess = $v.isSuccess;
      _isLoading = $v.isLoading;
      _receive = $v.receive;
      _api_token = $v.api_token;
      _name = $v.name;
      _suplier = $v.suplier;
      _category_id = $v.category_id;
      _company_id = $v.company_id;
      _type_select = $v.type_select;
      _price = $v.price;
      _pin_serial = $v.pin_serial;
      _face_value = $v.face_value;
      _pin_number = $v.pin_number;
      _pin_expiry = $v.pin_expiry;
      _error = $v.error;
      _file = $v.file;
      _errorMessage = $v.errorMessage;
      _getCardResponseModel = $v.getCardResponseModel;
      _getAllTypesResponseModel = $v.getAllTypesResponseModel;
      _addCardResponseModel = $v.addCardResponseModel;
      _addCardExcelResponseModel = $v.addCardExcelResponseModel;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(AddCardState other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$AddCardState;
  }

  @override
  void update(void Function(AddCardStateBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$AddCardState build() {
    final _$result = _$v ??
        new _$AddCardState._(
            isSuccess: BuiltValueNullFieldError.checkNotNull(
                isSuccess, 'AddCardState', 'isSuccess'),
            isLoading: BuiltValueNullFieldError.checkNotNull(
                isLoading, 'AddCardState', 'isLoading'),
            receive: BuiltValueNullFieldError.checkNotNull(
                receive, 'AddCardState', 'receive'),
            api_token: BuiltValueNullFieldError.checkNotNull(
                api_token, 'AddCardState', 'api_token'),
            name: BuiltValueNullFieldError.checkNotNull(
                name, 'AddCardState', 'name'),
            suplier: BuiltValueNullFieldError.checkNotNull(
                suplier, 'AddCardState', 'suplier'),
            category_id: BuiltValueNullFieldError.checkNotNull(
                category_id, 'AddCardState', 'category_id'),
            company_id: BuiltValueNullFieldError.checkNotNull(
                company_id, 'AddCardState', 'company_id'),
            type_select: BuiltValueNullFieldError.checkNotNull(
                type_select, 'AddCardState', 'type_select'),
            price: BuiltValueNullFieldError.checkNotNull(
                price, 'AddCardState', 'price'),
            pin_serial: BuiltValueNullFieldError.checkNotNull(pin_serial, 'AddCardState', 'pin_serial'),
            face_value: BuiltValueNullFieldError.checkNotNull(face_value, 'AddCardState', 'face_value'),
            pin_number: BuiltValueNullFieldError.checkNotNull(pin_number, 'AddCardState', 'pin_number'),
            pin_expiry: BuiltValueNullFieldError.checkNotNull(pin_expiry, 'AddCardState', 'pin_expiry'),
            error: error,
            file: file,
            errorMessage: BuiltValueNullFieldError.checkNotNull(errorMessage, 'AddCardState', 'errorMessage'),
            getCardResponseModel: BuiltValueNullFieldError.checkNotNull(getCardResponseModel, 'AddCardState', 'getCardResponseModel'),
            getAllTypesResponseModel: BuiltValueNullFieldError.checkNotNull(getAllTypesResponseModel, 'AddCardState', 'getAllTypesResponseModel'),
            addCardResponseModel: BuiltValueNullFieldError.checkNotNull(addCardResponseModel, 'AddCardState', 'addCardResponseModel'),
            addCardExcelResponseModel: BuiltValueNullFieldError.checkNotNull(addCardExcelResponseModel, 'AddCardState', 'addCardExcelResponseModel'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
