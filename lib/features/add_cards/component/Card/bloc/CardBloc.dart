import 'dart:async';
import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:rassed_app/features/add_cards/component/Card/bloc/CardEvent.dart';
import 'package:rassed_app/features/add_cards/component/Card/bloc/CardState.dart';
import 'package:rassed_app/features/add_cards/component/Card/model/add_card_excel_model.dart';
import 'package:rassed_app/features/add_cards/component/Card/usecase/add_card_excel_usecase.dart';
import 'package:rassed_app/features/add_cards/component/Card/usecase/add_card_usecase.dart';
import 'package:rassed_app/features/add_cards/component/Card/usecase/get_types_usecase.dart';
import 'package:rassed_app/features/add_cards/component/Card/usecase/card_usecase.dart';
import 'package:rassed_app/features/add_cards/component/Card/model/add_card_model.dart';
import 'package:rassed_app/features/error/failures.dart';



const String SERVER_FAILURE_MESSAGE = 'Server Failure';
const String CACHE_FAILURE_MESSAGE = 'Cache Failure';
const String INVALID_INPUT_FAILURE_MESSAGE =
    'Invalid Input - The number must be a positive integer or zero.';

class AddCardBloc extends Bloc<AddCardEvents, AddCardState> {

  final GetCardsUseCase getcardUseCase;
  final GetTypesUseCase getTypesUseCase;
  final AddCardUseCase addCardUseCase;
  final AddCardExcelUseCase addCardExcelUseCase;
  AddCardBloc({this.getcardUseCase,this.getTypesUseCase,this.addCardUseCase,this.addCardExcelUseCase}) : super(AddCardState.initial());

  void onChangeApiToken(String api_token) {
    add(ChangeApiToken(api_token));
  }

  void onCardEvent() {
    add(CardEvent());
  }

  void onAddCardEvent() {
    add(AddCardEvent());
  }
  void onAddCardExcelEvent() {
    add(AddCardExcelEvent());
  }

  void onTypesEvent() {
    add(TypesEvent());
  }

  void onChangeExcelFile(File file) {
    add(ChangeExcelFile(file));
  }

  void onChangeNameCardType(String name) {
    add(ChangeNameCardType(name));
  }

  void onChangePrice(String price) {
    add(ChangePrice(price));
  }
  void onChangeFaceValue(String face_value) {
    add(ChangeFaceValue(face_value));
  }
  void onChangePinNumber(String pin_number) {
    add(ChangePinNumber(pin_number));
  }
  void onChangePinExpiry(String pin_expiry) {
    add(ChangePinExpiry(pin_expiry));
  }
  void onChangePinSerial(String pin_serial) {
    add(ChangePinSerial(pin_serial));
  }
  void onChangeNameCard(String name) {
    add(ChangeNameCard(name));
  }
  void onChangeSupplireNumber(String suplire) {
    add(ChangeSupplireNumber(suplire));
  }
  void onChangeTypeSelect(int type_select) {
    add(ChangeTypeSelect(type_select));
  }

  @override
  Stream<AddCardState> mapEventToState(AddCardEvents event) async* {
    if (event is CardEvent) {
      yield state.rebuild((b) =>
      b
        ..isLoading = true
        ..isSuccess = false

      );
      final result = await getcardUseCase(
          getCardParams(
              api_token: state.api_token

          )
      );

      yield* result.fold((l) async* {
        yield state.rebuild((b) =>
        b
          ..isLoading = false
          ..isSuccess = false
          ..errorMessage = l);
      }, (r) async* {
        print('sucesss');
        yield state.rebuild((b) =>
        b
          ..isLoading = false
          ..isSuccess = true
          ..errorMessage = ''
          ..getCardResponseModel = r);
      });
    }
    else if (event is TypesEvent) {
      yield state.rebuild((b) =>
      b
        ..isLoading = true
        ..isSuccess = false

      );
      final result = await getTypesUseCase(
          getTypesParams(
              api_token: state.api_token
          )
      );

      yield* result.fold((l) async* {
        yield state.rebuild((b) =>
        b
          ..isLoading = false
          ..isSuccess = false
          ..errorMessage = l);
      }, (r) async* {
        print('sucesss');
        yield state.rebuild((b) =>
        b
          ..isLoading = false
          ..isSuccess = true
          ..errorMessage = ''
          ..getAllTypesResponseModel = r);
      });
    }
    else if (event is AddCardEvent) {
      yield* _onAddCardInitiated(
        addCardUseCase(
          addCardParams(
              api_token: state.api_token,
              type_select: state.type_select,
              price: state.price,
              pin_serial: state.pin_serial,
              face_value: state.face_value,
              pin_number: state.pin_number,
              pin_expiry: state.pin_expiry,
              name: state.name,
              suplier: state.suplier


          ),
        ),
        state,
      );
    }
    else if (event is AddCardExcelEvent) {
      yield* _onAddCardExcelInitiated(
        addCardExcelUseCase(
          addCardExcelParams(
              api_token: state.api_token,
              type_select: state.type_select,
            file: state.file
          ),
        ),
        state,
      );
    }
    else if (event is ChangeApiToken) {

      yield state.rebuild((b) => b..api_token = event.api_token);
    }

    else if (event is ChangeNameCardType) {

      yield state.rebuild((b) => b..name = event.name);
    }

    else if (event is ChangeTypeSelect) {
      print('333 category_id');
      yield state.rebuild((b) => b..type_select = event.type_select);
    }
    else if (event is ChangeExcelFile) {
      print('333 category_id');
      yield state.rebuild((b) => b..file = event.file);
    }
    else if (event is ChangePrice) {

      yield state.rebuild((b) => b..price = event.price);
    }
    else if (event is ChangePinSerial) {
      yield state.rebuild((b) => b..pin_serial = event.pin_serial);
    }
    else if (event is ChangePinNumber) {
      yield state.rebuild((b) => b..pin_number = event.pin_number);
    }
    else if (event is ChangePinExpiry) {
      yield state.rebuild((b) => b..pin_expiry = event.pin_expiry);
    }
    else if (event is ChangeFaceValue) {
      yield state.rebuild((b) => b..face_value = event.face_value);
    }
    else if (event is ChangeSupplireNumber) {
      yield state.rebuild((b) => b..suplier = event.suplire);
    }
    else if (event is ChangeNameCard) {
      print('name 3');
      yield state.rebuild((b) => b..name = event.name);
    }
  }

  Stream<AddCardState> _onAddCardInitiated(
      Future<Either<String, AddCardResponseModel>> AddCard,
      AddCardState state,
      ) async* {
    yield state.rebuild(
          (builder) => builder
        ..isLoading = true
        ..isSuccess = false
        ..error = null,

    );

    final result = await AddCard;

    yield* result.fold((l) async* {
      print('===========1');
      if (l is ServerFailure) {
        yield state.rebuild((builder) => builder

          ..isSuccess = false
          ..isLoading = false
          ..errorMessage= l);
      } else {
        print('===========2');
        yield state.rebuild((builder) => builder

          ..isSuccess = false
          ..isLoading = false);
      }
    }, (r) async* {
      print('===========3');
      // the best case
      yield state.rebuild(
            (builder) => builder
          ..isLoading = false
          ..isSuccess = true
          ..receive=true
           ..addCardResponseModel=r

      );

      // navigate to next screen
    });
  }

  Stream<AddCardState> _onAddCardExcelInitiated(
      Future<Either<String, AddCardExcelResponseModel>> AddCard,
      AddCardState state,
      ) async* {
    yield state.rebuild(
          (builder) => builder
        ..isLoading = true
        ..isSuccess = false
        ..error = null,

    );

    final result = await AddCard;

    yield* result.fold((l) async* {
      print('===========1');
      if (l is ServerFailure) {
        yield state.rebuild((builder) => builder

          ..isSuccess = false
          ..isLoading = false
          ..errorMessage= l);
      } else {
        print('===========2');
        yield state.rebuild((builder) => builder

          ..isSuccess = false
          ..isLoading = false);
      }
    }, (r) async* {
      print('===========3');
      // the best case
      yield state.rebuild(
              (builder) => builder
            ..isLoading = false
            ..isSuccess = true
            ..receive=true
            ..addCardExcelResponseModel=r

      );

      // navigate to next screen
    });
  }

}
