library CardTypeState;

import 'dart:io';
import 'package:built_value/built_value.dart';
import 'package:rassed_app/features/Constants/ApiConstants.dart';
import 'package:rassed_app/features/add_cards/component/Card/model/add_card_excel_model.dart';
import 'package:rassed_app/features/add_cards/component/Card/model/get_types_model.dart';
import 'package:rassed_app/features/add_cards/component/Card/model/card_model.dart';
import 'package:rassed_app/features/add_cards/component/Card/model/add_card_model.dart';

part 'CardState.g.dart';

abstract class AddCardState
    implements Built<AddCardState, AddCardStateBuilder> {
  bool get isSuccess;

  bool get isLoading;

  bool get receive;

  String get api_token;

  String get name;

  String get suplier;

  int get category_id;

  int get company_id;

  int get type_select;

  String get price;

  String get pin_serial;

  String get face_value;

  String get pin_number;

  String get pin_expiry;

  @nullable
  ErrorCode get error;

  @nullable
  File get file;

  String get errorMessage;

  List<GetCardResponseModel> get getCardResponseModel;

  List<GetAllTypesResponseModel> get getAllTypesResponseModel;

  AddCardResponseModel get addCardResponseModel;
  AddCardExcelResponseModel get addCardExcelResponseModel;

  AddCardState._();

  factory AddCardState([updates(AddCardStateBuilder b)]) = _$AddCardState;

  factory AddCardState.initial() {
    return AddCardState((b) => b
      ..isLoading = false
      ..isSuccess = false
      ..receive = false
      ..api_token = ''
      ..name = ''
      ..suplier=''
      ..price=''
      ..type_select=0
      ..pin_expiry=''
      ..pin_number=''
      ..face_value=''
      ..pin_serial=''
      ..category_id = 0
      ..company_id = 0
      ..errorMessage = ''
      ..getCardResponseModel = []
      ..getAllTypesResponseModel = []
      ..addCardResponseModel = AddCardResponseModel(message: '', id: 0)
    ..addCardExcelResponseModel = AddCardExcelResponseModel(message: '', id: 0));
  }
}
