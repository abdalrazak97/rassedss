import 'dart:io';

import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class AddCardEvents extends Equatable {
  @override
  List<Object> get props => [];
}

class CardEvent extends AddCardEvents {}
class TypesEvent extends AddCardEvents {}
class AddCardEvent extends AddCardEvents {}
class AddCardExcelEvent extends AddCardEvents {}
class ChangeApiToken extends AddCardEvents{
  final api_token;
  ChangeApiToken(this.api_token);
}
class ChangeImageNetwork extends AddCardEvents {
  final File img;
  ChangeImageNetwork(this.img);
}
class ChangeNameCardType extends AddCardEvents{
  final name;
  ChangeNameCardType(this.name);
}
class ChangeTypeSelect extends AddCardEvents{
  final type_select;
  ChangeTypeSelect(this.type_select);
}
class ChangePrice extends AddCardEvents{
  final price;
  ChangePrice(this.price);
}
class ChangePinSerial extends AddCardEvents{
  final pin_serial;
  ChangePinSerial(this.pin_serial);
}
class ChangeExcelFile extends AddCardEvents{
  final file;
  ChangeExcelFile(this.file);
}
class ChangeFaceValue extends AddCardEvents{
  final face_value;
  ChangeFaceValue(this.face_value);
}
class ChangePinNumber extends AddCardEvents{
  final pin_number;
  ChangePinNumber(this.pin_number);
}
class ChangePinExpiry extends AddCardEvents{
  final pin_expiry;
  ChangePinExpiry(this.pin_expiry);
}

class ChangeNameCard extends AddCardEvents{
  final name;
  ChangeNameCard(this.name);
}

class ChangeSupplireNumber extends AddCardEvents{
  final suplire;
  ChangeSupplireNumber(this.suplire);
}

class ResetError extends AddCardEvents {}

class Enable extends AddCardEvents {}

class Disable extends AddCardEvents {}
