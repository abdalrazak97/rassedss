import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_fonts_arabic/fonts.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:rassed_app/features/add_cards/component/Card/add_info_card.dart';
import 'dart:ui' as ui;

import 'package:rassed_app/features/settings/Preference.dart';
import 'package:rassed_app/features/widget/textauto.dart';
import 'package:rassed_app/features/add_cards/component/Card/card_info.dart';
import '../../../../injection.dart';
import 'bloc/CardBloc.dart';
import 'bloc/CardState.dart';


class AddCard extends StatefulWidget {
  @override
  _AddCardState createState() => _AddCardState();
}

class _AddCardState extends State<AddCard> {


  AddCardBloc bloc = sl<AddCardBloc>();
  String Token ;
  @override
  void initState() {

    Token=Preferences.getUserToken();
    bloc.onChangeApiToken(Token);
    bloc.onTypesEvent();
    bloc.onCardEvent();
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        actions: [
          SizedBox(
            width: ScreenUtil().setWidth(40),
            child: Center(
              child: TextButton(
                style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.all(Colors.blueGrey),
                ),
                onPressed: () {
                  Navigator.pop(context);
                },
                child: SvgPicture.asset(
                  "assets/icons/close3.svg",
                  width: ScreenUtil().screenWidth * 0.1,
                  color: Colors.white,
                ),
              ),
            ),
          ),
        ],
        leading: SizedBox(
          width: 5,
        ),
        backgroundColor: Colors.blueGrey,
        centerTitle: true,
        title: Text(
          'Card'.tr().toString(),
          textDirection: ui.TextDirection.rtl,
          style: TextStyle(
            fontFamily: ArabicFonts.Cairo,
            package: 'google_fonts_arabic',
            fontSize: 20,
          ),
        ),
      ),
      body:  BlocBuilder<AddCardBloc, AddCardState>(
          bloc: bloc,
          builder: (context, state) {
            if (state.isLoading)
              return Center(child: CircularProgressIndicator(),);
            return SingleChildScrollView(
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  textDirection: ui.TextDirection.rtl,
                  children: [
                    SizedBox(height: ScreenUtil().screenHeight * 0.05,),
                    Center(
                      child: SizedBox(
                        height: ScreenUtil().screenHeight * 0.1,
                        width: ScreenUtil().screenWidth * 0.8,
                        child: TextButton(
                            style: ButtonStyle(
                              backgroundColor: MaterialStateProperty.all(
                                  Colors.blueGrey),
                              shape: MaterialStateProperty.all(
                                  RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(15.0),
                                      side:
                                      BorderSide(color: Colors.grey.shade50))),
                            ),
                            child: Padding(
                              padding: EdgeInsets.symmetric(
                                  vertical: ScreenUtil().setHeight(12)),
                              child: Container(
                                  child: AutoTextgrey(
                                      ScreenUtil().setSp(16),
                                      "Add Card".tr().toString(),
                                      Colors.grey.shade50)),
                            ),
                            onPressed: () {

                              Navigator.push(context, MaterialPageRoute(
                                  builder: (context) =>
                                      AddInfoCardType(getAllTypesResponseModel: state.getAllTypesResponseModel,)));

                            }),
                      ),
                    ),
                    SizedBox(height: ScreenUtil().screenHeight * 0.05,),
                    Container(
                      padding: EdgeInsets.symmetric(
                          vertical: ScreenUtil().setHeight(1)),

                      color: Colors.grey.shade50,
                      height: ScreenUtil().screenHeight*0.9,
                      width: ScreenUtil().screenWidth,
                      child: ListView.builder(
                        itemCount: state.getCardResponseModel.length,
                        itemBuilder: (context, index) =>
                            GestureDetector(
                              onTap: () {
                                /* Route route = MaterialPageRoute(builder: (context) =>
                                  EditItem(prdouctEntity: state2.Prdoucts[index],));
                              Navigator.push(context, route).then(onGoBack);*/
                              },
                              child: CardInfo(
                                index: index
                                , name: state.getCardResponseModel[index]
                                    .type,

                                value: state.getCardResponseModel[index]
                                    .price.toString(),is_sale:
                                state.getCardResponseModel[index].is_sale,),

                            ),
                      ),
                    ),

                    SizedBox(
                      height: ScreenUtil().screenHeight * 0.4,
                    ),
                  ]),

            );
          }
      ),
    );


  }
}
