  import 'dart:convert';
  import 'package:flutter/material.dart';
  import 'package:rassed_app/features/Constants/ApiConstants.dart';
  import 'package:dartz/dartz.dart';
  import 'package:data_connection_checker/data_connection_checker.dart';
  import 'package:dio/dio.dart';
  import 'package:meta/meta.dart';
  import 'package:rassed_app/features/add_cards/component/Company/usecase/get_company_usecase.dart';
  import 'package:rassed_app/features/add_cards/component/Company/model/get_company_model.dart';
  abstract class GetCompanyDataSource {
    Future<Either<String, List<GetCompanyResponseModel>>> getCompany(getCompanyParams params);
  }

  class GetCompanyDataSourceImpl extends GetCompanyDataSource {
    final Dio dio;
    final DataConnectionChecker networkInfo;

    GetCompanyDataSourceImpl({@required this.dio, @required this.networkInfo});

    @override
    Future<Either<String, List<GetCompanyResponseModel>>> getCompany(getCompanyParams params) async {
      List<GetCompanyResponseModel> companyResponseModel;
      print("params ${params.api_token}");
      if (await networkInfo.hasConnection) {
        try {
          final result = await dio.get(
            Url.getCompany ,

            options: Options(
              headers:{
                'Authorization': 'Bearer ${params.api_token}',
              } ,
              followRedirects: false,
              validateStatus: (status) {
                return status < 500;
              },
            ),
          );

          print(result.data);
          companyResponseModel=GetCompanyResponseModelFromJson(result.data);

          return Right(companyResponseModel);
        } on DioError catch (ex) {
          if (ex.type == DioErrorType.connectTimeout) {
            return Left(Er.NetworkError);
          } else if (ex.type == DioErrorType.receiveTimeout) {
            return Left(Er.NetworkError);
          }
          else if (companyResponseModel == null) return Left(Er.Error);
        }
      } else {
        return Left(Er.NetworkError);
      }
    }
  }
