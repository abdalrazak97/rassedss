import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:rassed_app/features/Constants/ApiConstants.dart';
import 'package:dartz/dartz.dart';
import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:dio/dio.dart';
import 'package:meta/meta.dart';

import 'package:rassed_app/features/add_cards/component/Company/usecase/get_categories_usecase.dart';
import 'package:rassed_app/features/add_cards/component/Company/model/get_categories_model.dart';
abstract class GetCategoriesDataSource {
  Future<Either<String, List<GetCategoriesResponseModel>>> getCategories(getCategoryParams params);
}

class GetCategoriesDataSourceImpl extends GetCategoriesDataSource {
  final Dio dio;
  final DataConnectionChecker networkInfo;

  GetCategoriesDataSourceImpl({@required this.dio, @required this.networkInfo});

  @override
  Future<Either<String, List<GetCategoriesResponseModel>>> getCategories(getCategoryParams params) async {
    List<GetCategoriesResponseModel> categories;
    print("params ${params.api_token}");
    if (await networkInfo.hasConnection) {
      try {
        final result = await dio.get(
          Url.getCategories ,

          options: Options(
            headers:{
              'Authorization': 'Bearer ${params.api_token}',
            } ,
            followRedirects: false,
            validateStatus: (status) {
              return status < 500;
            },
          ),
        );

        print(result.data);
        categories=GetCategoriesResponseModelFromJson(result.data);

        return Right(categories);
      } on DioError catch (ex) {
        if (ex.type == DioErrorType.connectTimeout) {
          return Left(Er.NetworkError);
        } else if (ex.type == DioErrorType.receiveTimeout) {
          return Left(Er.NetworkError);
        }
        else if (categories == null) return Left(Er.Error);
      }
    } else {
      return Left(Er.NetworkError);
    }
  }
}
