
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:rassed_app/features/Constants/ApiConstants.dart';
import 'package:dartz/dartz.dart';
import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:dio/dio.dart';
import 'package:meta/meta.dart';
import 'package:rassed_app/features/error/exceptions.dart';
import 'dart:core';
import 'package:rassed_app/features/error/failures.dart';
import 'package:rassed_app/features/add_cards/component/Company/model/edit_company_model.dart';
import 'package:rassed_app/features/add_cards/component/Company/usecase/edit_company_usecae.dart';
abstract class EditCompanyDataSource {
  Future<Either<String, EditCompanyResponseModel>> editCompany(editCompanyParams params);
}

class EditCompanyDataSourceImpl extends EditCompanyDataSource {
  final Dio dio;
  final DataConnectionChecker networkInfo;

  EditCompanyDataSourceImpl({@required this.dio, @required this.networkInfo});

  @override
  Future<Either<String, EditCompanyResponseModel>> editCompany(editCompanyParams params) async {
    EditCompanyResponseModel editCompanyResponseModel;
    print("params ${params.api_token}");
    print("params ${params.name}");
    print("params ${params.image}");


    FormData formData;
    if(params.image==null){
       formData = FormData.fromMap({
        'category_select': params.category_select,
        'name': params.name,
      });
    }
    else{
      String fileName = params.image.path.split('/').last;
        formData = FormData.fromMap({
      'category_select': params.category_select,
      'name': params.name,
      "image": await MultipartFile.fromFile(
          params.image.path, filename: fileName),
    });
    }

    if (await networkInfo.hasConnection) {
      try {
        final result = await dio.post(
          Url.editCompany+"/${params.id}",
          data: formData,
          options: Options(
            headers: {
              'Authorization': 'Bearer ${params.api_token}',
            },
            followRedirects: false,
            validateStatus: (status) {
              return status < 500;
            },
          ),
        );
        print(result.data);
        editCompanyResponseModel=EditCompanyResponseModel.fromJson(json.decode(result.data));
        return Right(editCompanyResponseModel);
      } on DioError catch (ex) {
        if (ex.type == DioErrorType.connectTimeout) {
          return Left(Er.NetworkError);
        } else if (ex.type == DioErrorType.receiveTimeout) {
          return Left(Er.NetworkError);
        }
        else if (editCompanyResponseModel == null) return Left(Er.Error);
      }
    } else {
      return Left(Er.NetworkError);
    }
  }
}