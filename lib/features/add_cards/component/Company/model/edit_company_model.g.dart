// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'edit_company_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

EditCompanyResponseModel _$EditCompanyResponseModelFromJson(
    Map<String, dynamic> json) {
  return EditCompanyResponseModel(
    id: json['id'] as int,
    name: json['name'] as String,
    success: json['success'] as bool,
    message: json['message'] as String,
  );
}

Map<String, dynamic> _$EditCompanyResponseModelToJson(
        EditCompanyResponseModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'success': instance.success,
      'message': instance.message,
    };
