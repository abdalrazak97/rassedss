import 'dart:convert';

import 'package:json_annotation/json_annotation.dart';



part 'get_categories_model.g.dart';

@JsonSerializable()
class GetCategoriesResponseModel {
  final int id;
  final String name;
  final bool success;
  final String message;


  Map<String, dynamic> toJson() => _$GetCategoriesResponseModelToJson(this);
  factory GetCategoriesResponseModel.fromJson(Map<String, dynamic> json) =>
      _$GetCategoriesResponseModelFromJson(json);

  GetCategoriesResponseModel({
    this.id, this.name,this.success, this.message});
}
List<GetCategoriesResponseModel> GetCategoriesResponseModelFromJson(String str) =>
    List<GetCategoriesResponseModel>.from(json.decode(str).map((x) => GetCategoriesResponseModel.fromJson(x)));
String GetCategoriesResponseModelToJson(List<GetCategoriesResponseModel> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));