import 'dart:convert';

import 'package:json_annotation/json_annotation.dart';



part 'edit_company_model.g.dart';

@JsonSerializable()
class EditCompanyResponseModel {
  final int id;
  final String name;
  final bool success;
  final String message;


  Map<String, dynamic> toJson() => _$EditCompanyResponseModelToJson(this);
  factory EditCompanyResponseModel.fromJson(Map<String, dynamic> json) =>
      _$EditCompanyResponseModelFromJson(json);

  EditCompanyResponseModel({
    this.id, this.name,this.success, this.message});
}

