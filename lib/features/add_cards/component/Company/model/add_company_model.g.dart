// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'add_company_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AddCompanyResponseModel _$AddCompanyResponseModelFromJson(
    Map<String, dynamic> json) {
  return AddCompanyResponseModel(
    id: json['id'] as int,
    name: json['name'] as String,
    success: json['success'] as bool,
    message: json['message'] as String,
  );
}

Map<String, dynamic> _$AddCompanyResponseModelToJson(
        AddCompanyResponseModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'success': instance.success,
      'message': instance.message,
    };
