import 'dart:convert';

import 'package:json_annotation/json_annotation.dart';



part 'add_company_model.g.dart';

@JsonSerializable()
class AddCompanyResponseModel {
  final int id;
  final String name;
  final bool success;
  final String message;


  Map<String, dynamic> toJson() => _$AddCompanyResponseModelToJson(this);
  factory AddCompanyResponseModel.fromJson(Map<String, dynamic> json) =>
      _$AddCompanyResponseModelFromJson(json);

  AddCompanyResponseModel({
    this.id, this.name,this.success, this.message});
}
List<AddCompanyResponseModel> AddCompanyResponseModelFromJson(String str) =>
    List<AddCompanyResponseModel>.from(json.decode(str).map((x) => AddCompanyResponseModel.fromJson(x)));
String AddCompanyResponseModelToJson(List<AddCompanyResponseModel> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));
