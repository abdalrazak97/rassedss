// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'get_categories_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

GetCategoriesResponseModel _$GetCategoriesResponseModelFromJson(
    Map<String, dynamic> json) {
  return GetCategoriesResponseModel(
    id: json['id'] as int,
    name: json['name'] as String,
    success: json['success'] as bool,
    message: json['message'] as String,
  );
}

Map<String, dynamic> _$GetCategoriesResponseModelToJson(
        GetCategoriesResponseModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'success': instance.success,
      'message': instance.message,
    };
