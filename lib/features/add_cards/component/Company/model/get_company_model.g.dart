// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'get_company_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

GetCompanyResponseModel _$GetCompanyResponseModelFromJson(
    Map<String, dynamic> json) {
  return GetCompanyResponseModel(
    id: json['id'] as int,
    name: json['name'] as String,
    categoryname: json['categoryname'] as String,
    category_id: json['category_id'] as int,
    image: json['image'] as String,
    success: json['success'] as bool,
    message: json['message'] as String,
  );
}

Map<String, dynamic> _$GetCompanyResponseModelToJson(
        GetCompanyResponseModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'category_id': instance.category_id,
      'categoryname': instance.categoryname,
      'image': instance.image,
      'success': instance.success,
      'message': instance.message,
    };
