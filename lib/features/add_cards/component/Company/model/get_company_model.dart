import 'dart:convert';

import 'package:json_annotation/json_annotation.dart';



part 'get_company_model.g.dart';

@JsonSerializable()
class GetCompanyResponseModel {
  final int id;
  final String name;
  final int category_id;
  final String categoryname;
  final String image;
  final bool success;
  final String message;


  Map<String, dynamic> toJson() => _$GetCompanyResponseModelToJson(this);
  factory GetCompanyResponseModel.fromJson(Map<String, dynamic> json) =>
      _$GetCompanyResponseModelFromJson(json);

  GetCompanyResponseModel({
    this.id, this.name,this.categoryname, this.category_id,this.image, this.success, this.message});
}
List<GetCompanyResponseModel> GetCompanyResponseModelFromJson(String str) =>
    List<GetCompanyResponseModel>.from(json.decode(str).map((x) => GetCompanyResponseModel.fromJson(x)));
String GetCompanyResponseModelToJson(List<GetCompanyResponseModel> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));