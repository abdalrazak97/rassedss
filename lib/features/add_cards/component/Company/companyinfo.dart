import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:rassed_app/features/Constants/ApiConstants.dart';
import 'package:rassed_app/features/widget/textauto.dart';

class item extends StatelessWidget {
  final int count;
  final int index;
  final String url;
  final String name;
  final String value;
  item({ this.count,this.index,this.name,this.value,this.url});
  @override
  Widget build(BuildContext context) {

    return OrientationBuilder(
        builder: (context, orientation) {
          bool isPotrait = MediaQuery
              .of(context)
              .orientation == Orientation.portrait;
          return Container(
            decoration: BoxDecoration(
                border: Border.all(color: Colors.grey.shade200)
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  height:isPotrait? ScreenUtil().screenWidth*0.2:ScreenUtil().screenWidth*0.06 ,
                  width:isPotrait? ScreenUtil().screenWidth*0.2:ScreenUtil().screenWidth*0.06 ,
                  decoration: BoxDecoration(
                    color: Colors.grey.shade400,
                  ),
                  child:url!=null? Image.network(Url.BaseUrl+url,fit: BoxFit.fill,):
                  Padding(
                    padding:  EdgeInsets.symmetric(
                        horizontal: ScreenUtil().setWidth(4),
                        vertical: ScreenUtil().setHeight(4)),
                    child: Container(
                      child: Align(
                          alignment: Alignment.center ,
                          child:name.length>2?
                          AutoTextgrey(
                              isPotrait?ScreenUtil().setSp(14): ScreenUtil().setSp(18),
                              name[0]+name[1], Colors.white):
                          AutoTextgrey(
                              isPotrait?ScreenUtil().setSp(14): ScreenUtil().setSp(18)
                              ,
                              name, Colors.white)),
                    ),
                  ),
                ),
                Container(
                  width:isPotrait?ScreenUtil().screenWidth*0.73:ScreenUtil().screenWidth*0.48,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                          child: AutoTextgrey(
                              isPotrait?ScreenUtil().setSp(14): ScreenUtil().setSp(18), name, Colors.grey.shade800)),

                      Padding(
                        padding:  EdgeInsets.only(right:ScreenUtil().setWidth(15) ),
                        child: Container(
                            child: Text( value,style: TextStyle(
                              fontSize: isPotrait?ScreenUtil().setSp(14):
                              ScreenUtil().setSp(18),
                              color: Colors.grey.shade800,
                            ),)),
                      ),


                    ],
                  ),
                ),
              ],
            ),
          );}
    );


  }

}
