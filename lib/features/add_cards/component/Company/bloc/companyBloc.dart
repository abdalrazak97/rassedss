import 'dart:async';
import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:rassed_app/features/Constants/ApiConstants.dart';
import 'package:rassed_app/features/add_cards/component/Company/bloc/companyEvent.dart';
import 'package:rassed_app/features/add_cards/component/Company/model/edit_company_model.dart';
import 'package:rassed_app/features/add_cards/component/Company/usecase/edit_company_usecae.dart';
import 'package:rassed_app/features/add_cards/component/Company/usecase/get_company_usecase.dart';
import 'package:rassed_app/features/add_cards/component/Company/usecase/add_company_usecase.dart';
import 'package:rassed_app/features/add_cards/component/Company/usecase/get_categories_usecase.dart';
import 'companyState.dart';
import 'package:rassed_app/features/add_cards/component/Company/model/add_company_model.dart';
import 'package:rassed_app/features/error/failures.dart';
import 'package:rassed_app/features/error/error_handler.dart';
const String SERVER_FAILURE_MESSAGE = 'Server Failure';
const String CACHE_FAILURE_MESSAGE = 'Cache Failure';
const String INVALID_INPUT_FAILURE_MESSAGE =
    'Invalid Input - The number must be a positive integer or zero.';

class CompanyBloc extends Bloc<CompanyEvents, CompanyState> {
  final GetCompanysUseCase getCompanysUseCase;
  final AddCompanyUseCase addCompanyUseCase;
  final GetCategoryUseCase getCategoriesUseCase;
  final EditCompanyUseCase editCompanyUseCase;
  CompanyBloc({this.getCompanysUseCase,this.addCompanyUseCase,this.getCategoriesUseCase,this.editCompanyUseCase}) : super(CompanyState.initial());

  void onChangeApiToken(String api_token){
    add( ChangeApiToken(api_token));
  }

void onCompanyEvent(){
    add(CompanyEvent());
}
  void onCategoryEvent(){
    add(CategoryEvent());
  }
 void onAddCompanyEvent(){
    add(AddCompanyEvent());
 }
  void onEditCompanyEvent(){
    add(EditCompanyEvent());
  }
  void onChangeImageNetwork(File im) {
    add(ChangeImageNetwork(im));
  }
void onChangeNameCompany(String name){
    add(ChangeNameCompany(name));
}

  void onChangeIdCompany(int id){
    add(ChangeIdCompany(id));
  }
void onChangeCategoryCompany(int category_id){
    add(ChangeCategoryCompany(category_id));
}

  @override
  Stream<CompanyState> mapEventToState(CompanyEvents event) async* {
    if (event is CompanyEvent) {
      yield state.rebuild((b) => b
        ..isLoading = true
        ..isSuccess = false

      );
      final result = await getCompanysUseCase(
          getCompanyParams(
              api_token: state.api_token
          )
      );

      yield* result.fold((l) async* {

        yield state.rebuild((b) => b
          ..isLoading = false
          ..isSuccess = false
          ..errorMessage = l);
      }, (r) async* {
        print('sucesss');
        yield state.rebuild((b) => b
          ..isLoading = false
          ..isSuccess = true
          ..errorMessage = ''
          ..getCompanyResponseModel = r);
      });
    }
    else if (event is AddCompanyEvent) {

      yield* _onAddPrdouctInitiated(
        addCompanyUseCase(
          addCompanyParams(
              name: state.name,
              image: state.image,
            category_id: state.category_id,
            api_token: state.api_token
          ),
        ),
        state,
      );
    }
    else if (event is EditCompanyEvent) {

      yield* _onEditPrdouctInitiated(
        editCompanyUseCase(
          editCompanyParams(
              id: state.id,
              name: state.name,
              image: state.image,
              category_select: state.category_id,
              api_token: state.api_token
          ),
        ),
        state,
      );
    }
   else if (event is CategoryEvent) {
      yield state.rebuild((b) => b
        ..isLoading = true
        ..isSuccess = false
      );

      final result = await getCategoriesUseCase(
          getCategoryParams(
              api_token: state.api_token
          )
      );

      yield* result.fold((l) async* {

        yield state.rebuild((b) => b
          ..isLoading = false
          ..isSuccess = false
          ..errorMessage = l);
      }, (r) async* {
        print('hello from categories');
        yield state.rebuild((b) => b
          ..isLoading = false
          ..isSuccess = true
          ..errorMessage = ''
          ..getCategoriesResponseModel = r);
      });

    }
    else if (event is ChangeApiToken) {
      print('1111');
      yield state.rebuild((b) => b..api_token = event.api_token);

    }

    else if (event is ChangeNameCompany) {
      print('222 name');
      yield state.rebuild((b) => b..name = event.name);

    }
    else if (event is ChangeCategoryCompany) {
      print('333 category_id');
      yield state.rebuild((b) => b..category_id = event.category_id);

    }
    else if (event is ChangeImageNetwork) {
      print('image change ');
      yield state.rebuild((b) => b..image = event.img);

    }
    else if (event is ChangeIdCompany) {
      print('id change ');
      yield state.rebuild((b) => b..id = event.id);

    }
  }
  Stream<CompanyState> _onAddPrdouctInitiated(
      Future<Either<String, AddCompanyResponseModel>> AddPrdouct,
      CompanyState state,
      ) async* {
    yield state.rebuild(
          (builder) => builder
        ..isLoading = true
        ..isSuccess = false
        ..error = null,

    );

    final result = await AddPrdouct;

    yield* result.fold((l) async* {
      print('===========1');
      if (l is ServerFailure) {
        yield state.rebuild((builder) => builder
          ..isSuccess = false
          ..isLoading = false
          ..errorMessage= l);
      } else {
        print('===========2');
        yield state.rebuild((builder) => builder

          ..isSuccess = false
          ..isLoading = false);
      }
    }, (r) async* {
      print('===========3');
      // the best case
      yield state.rebuild(
            (builder) => builder
          ..isLoading = false
          ..isSuccess = true
          ..receive=true
          ..addCompanyResponseModel=r,
                          );

      // navigate to next screen
    });
  }
  Stream<CompanyState> _onEditPrdouctInitiated(
      Future<Either<String, EditCompanyResponseModel>> EditPrdouct,
      CompanyState state,
      ) async* {
    yield state.rebuild(
          (builder) => builder
        ..isLoading = true
        ..isSuccess = false
        ..error = null,
    );

    final result = await EditPrdouct;

    yield* result.fold((l) async* {
      print('===========1');
      if (l is ServerFailure) {
        yield state.rebuild((builder) => builder
          ..errorMessage= l
          ..isSuccess = false
          ..isLoading = false);
      } else {
        print('===========2');
        yield state.rebuild((builder) => builder
          ..error = ErrorCode.SERVER_ERROR
          ..isSuccess = false
          ..isLoading = false);
      }
    }, (r) async* {
      print('===========3');
      // the best case
      yield state.rebuild(
            (builder) => builder
          ..isLoading = false
          ..isSuccess = true
          ..receive=true
          ..editCompanyResponseModel=r,
      );

      // navigate to next screen
    });
  }
}
