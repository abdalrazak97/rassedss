// GENERATED CODE - DO NOT MODIFY BY HAND

part of companyState;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$CompanyState extends CompanyState {
  @override
  final bool isSuccess;
  @override
  final bool receive;
  @override
  final bool isLoading;
  @override
  final String Accept_Language;
  @override
  final String api_token;
  @override
  final String name;
  @override
  final int category_id;
  @override
  final int id;
  @override
  final ErrorCode error;
  @override
  final File image;
  @override
  final String errorMessage;
  @override
  final List<GetCompanyResponseModel> getCompanyResponseModel;
  @override
  final List<GetCategoriesResponseModel> getCategoriesResponseModel;
  @override
  final AddCompanyResponseModel addCompanyResponseModel;
  @override
  final EditCompanyResponseModel editCompanyResponseModel;

  factory _$CompanyState([void Function(CompanyStateBuilder) updates]) =>
      (new CompanyStateBuilder()..update(updates)).build();

  _$CompanyState._(
      {this.isSuccess,
      this.receive,
      this.isLoading,
      this.Accept_Language,
      this.api_token,
      this.name,
      this.category_id,
      this.id,
      this.error,
      this.image,
      this.errorMessage,
      this.getCompanyResponseModel,
      this.getCategoriesResponseModel,
      this.addCompanyResponseModel,
      this.editCompanyResponseModel})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        isSuccess, 'CompanyState', 'isSuccess');
    BuiltValueNullFieldError.checkNotNull(receive, 'CompanyState', 'receive');
    BuiltValueNullFieldError.checkNotNull(
        isLoading, 'CompanyState', 'isLoading');
    BuiltValueNullFieldError.checkNotNull(
        Accept_Language, 'CompanyState', 'Accept_Language');
    BuiltValueNullFieldError.checkNotNull(
        api_token, 'CompanyState', 'api_token');
    BuiltValueNullFieldError.checkNotNull(name, 'CompanyState', 'name');
    BuiltValueNullFieldError.checkNotNull(
        category_id, 'CompanyState', 'category_id');
    BuiltValueNullFieldError.checkNotNull(id, 'CompanyState', 'id');
    BuiltValueNullFieldError.checkNotNull(
        errorMessage, 'CompanyState', 'errorMessage');
    BuiltValueNullFieldError.checkNotNull(
        getCompanyResponseModel, 'CompanyState', 'getCompanyResponseModel');
    BuiltValueNullFieldError.checkNotNull(getCategoriesResponseModel,
        'CompanyState', 'getCategoriesResponseModel');
    BuiltValueNullFieldError.checkNotNull(
        addCompanyResponseModel, 'CompanyState', 'addCompanyResponseModel');
    BuiltValueNullFieldError.checkNotNull(
        editCompanyResponseModel, 'CompanyState', 'editCompanyResponseModel');
  }

  @override
  CompanyState rebuild(void Function(CompanyStateBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  CompanyStateBuilder toBuilder() => new CompanyStateBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is CompanyState &&
        isSuccess == other.isSuccess &&
        receive == other.receive &&
        isLoading == other.isLoading &&
        Accept_Language == other.Accept_Language &&
        api_token == other.api_token &&
        name == other.name &&
        category_id == other.category_id &&
        id == other.id &&
        error == other.error &&
        image == other.image &&
        errorMessage == other.errorMessage &&
        getCompanyResponseModel == other.getCompanyResponseModel &&
        getCategoriesResponseModel == other.getCategoriesResponseModel &&
        addCompanyResponseModel == other.addCompanyResponseModel &&
        editCompanyResponseModel == other.editCompanyResponseModel;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc(
                        $jc(
                            $jc(
                                $jc(
                                    $jc(
                                        $jc(
                                            $jc(
                                                $jc(
                                                    $jc(
                                                        $jc(
                                                            $jc(
                                                                0,
                                                                isSuccess
                                                                    .hashCode),
                                                            receive.hashCode),
                                                        isLoading.hashCode),
                                                    Accept_Language.hashCode),
                                                api_token.hashCode),
                                            name.hashCode),
                                        category_id.hashCode),
                                    id.hashCode),
                                error.hashCode),
                            image.hashCode),
                        errorMessage.hashCode),
                    getCompanyResponseModel.hashCode),
                getCategoriesResponseModel.hashCode),
            addCompanyResponseModel.hashCode),
        editCompanyResponseModel.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('CompanyState')
          ..add('isSuccess', isSuccess)
          ..add('receive', receive)
          ..add('isLoading', isLoading)
          ..add('Accept_Language', Accept_Language)
          ..add('api_token', api_token)
          ..add('name', name)
          ..add('category_id', category_id)
          ..add('id', id)
          ..add('error', error)
          ..add('image', image)
          ..add('errorMessage', errorMessage)
          ..add('getCompanyResponseModel', getCompanyResponseModel)
          ..add('getCategoriesResponseModel', getCategoriesResponseModel)
          ..add('addCompanyResponseModel', addCompanyResponseModel)
          ..add('editCompanyResponseModel', editCompanyResponseModel))
        .toString();
  }
}

class CompanyStateBuilder
    implements Builder<CompanyState, CompanyStateBuilder> {
  _$CompanyState _$v;

  bool _isSuccess;
  bool get isSuccess => _$this._isSuccess;
  set isSuccess(bool isSuccess) => _$this._isSuccess = isSuccess;

  bool _receive;
  bool get receive => _$this._receive;
  set receive(bool receive) => _$this._receive = receive;

  bool _isLoading;
  bool get isLoading => _$this._isLoading;
  set isLoading(bool isLoading) => _$this._isLoading = isLoading;

  String _Accept_Language;
  String get Accept_Language => _$this._Accept_Language;
  set Accept_Language(String Accept_Language) =>
      _$this._Accept_Language = Accept_Language;

  String _api_token;
  String get api_token => _$this._api_token;
  set api_token(String api_token) => _$this._api_token = api_token;

  String _name;
  String get name => _$this._name;
  set name(String name) => _$this._name = name;

  int _category_id;
  int get category_id => _$this._category_id;
  set category_id(int category_id) => _$this._category_id = category_id;

  int _id;
  int get id => _$this._id;
  set id(int id) => _$this._id = id;

  ErrorCode _error;
  ErrorCode get error => _$this._error;
  set error(ErrorCode error) => _$this._error = error;

  File _image;
  File get image => _$this._image;
  set image(File image) => _$this._image = image;

  String _errorMessage;
  String get errorMessage => _$this._errorMessage;
  set errorMessage(String errorMessage) => _$this._errorMessage = errorMessage;

  List<GetCompanyResponseModel> _getCompanyResponseModel;
  List<GetCompanyResponseModel> get getCompanyResponseModel =>
      _$this._getCompanyResponseModel;
  set getCompanyResponseModel(
          List<GetCompanyResponseModel> getCompanyResponseModel) =>
      _$this._getCompanyResponseModel = getCompanyResponseModel;

  List<GetCategoriesResponseModel> _getCategoriesResponseModel;
  List<GetCategoriesResponseModel> get getCategoriesResponseModel =>
      _$this._getCategoriesResponseModel;
  set getCategoriesResponseModel(
          List<GetCategoriesResponseModel> getCategoriesResponseModel) =>
      _$this._getCategoriesResponseModel = getCategoriesResponseModel;

  AddCompanyResponseModel _addCompanyResponseModel;
  AddCompanyResponseModel get addCompanyResponseModel =>
      _$this._addCompanyResponseModel;
  set addCompanyResponseModel(
          AddCompanyResponseModel addCompanyResponseModel) =>
      _$this._addCompanyResponseModel = addCompanyResponseModel;

  EditCompanyResponseModel _editCompanyResponseModel;
  EditCompanyResponseModel get editCompanyResponseModel =>
      _$this._editCompanyResponseModel;
  set editCompanyResponseModel(
          EditCompanyResponseModel editCompanyResponseModel) =>
      _$this._editCompanyResponseModel = editCompanyResponseModel;

  CompanyStateBuilder();

  CompanyStateBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _isSuccess = $v.isSuccess;
      _receive = $v.receive;
      _isLoading = $v.isLoading;
      _Accept_Language = $v.Accept_Language;
      _api_token = $v.api_token;
      _name = $v.name;
      _category_id = $v.category_id;
      _id = $v.id;
      _error = $v.error;
      _image = $v.image;
      _errorMessage = $v.errorMessage;
      _getCompanyResponseModel = $v.getCompanyResponseModel;
      _getCategoriesResponseModel = $v.getCategoriesResponseModel;
      _addCompanyResponseModel = $v.addCompanyResponseModel;
      _editCompanyResponseModel = $v.editCompanyResponseModel;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(CompanyState other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$CompanyState;
  }

  @override
  void update(void Function(CompanyStateBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$CompanyState build() {
    final _$result = _$v ??
        new _$CompanyState._(
            isSuccess: BuiltValueNullFieldError.checkNotNull(
                isSuccess, 'CompanyState', 'isSuccess'),
            receive: BuiltValueNullFieldError.checkNotNull(
                receive, 'CompanyState', 'receive'),
            isLoading: BuiltValueNullFieldError.checkNotNull(
                isLoading, 'CompanyState', 'isLoading'),
            Accept_Language: BuiltValueNullFieldError.checkNotNull(
                Accept_Language, 'CompanyState', 'Accept_Language'),
            api_token: BuiltValueNullFieldError.checkNotNull(
                api_token, 'CompanyState', 'api_token'),
            name: BuiltValueNullFieldError.checkNotNull(
                name, 'CompanyState', 'name'),
            category_id: BuiltValueNullFieldError.checkNotNull(
                category_id, 'CompanyState', 'category_id'),
            id: BuiltValueNullFieldError.checkNotNull(id, 'CompanyState', 'id'),
            error: error,
            image: image,
            errorMessage: BuiltValueNullFieldError.checkNotNull(
                errorMessage, 'CompanyState', 'errorMessage'),
            getCompanyResponseModel: BuiltValueNullFieldError.checkNotNull(
                getCompanyResponseModel, 'CompanyState', 'getCompanyResponseModel'),
            getCategoriesResponseModel:
                BuiltValueNullFieldError.checkNotNull(getCategoriesResponseModel, 'CompanyState', 'getCategoriesResponseModel'),
            addCompanyResponseModel: BuiltValueNullFieldError.checkNotNull(addCompanyResponseModel, 'CompanyState', 'addCompanyResponseModel'),
            editCompanyResponseModel: BuiltValueNullFieldError.checkNotNull(editCompanyResponseModel, 'CompanyState', 'editCompanyResponseModel'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
