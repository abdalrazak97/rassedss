import 'dart:io';

import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class CompanyEvents extends Equatable {
  @override
  List<Object> get props => [];
}

class CompanyEvent extends CompanyEvents {}
class AddCompanyEvent extends CompanyEvents{}
class EditCompanyEvent extends CompanyEvents{}
class CategoryEvent extends CompanyEvents {}
class ChangeApiToken extends CompanyEvents{
  final api_token;
  ChangeApiToken(this.api_token);
}
class ChangeImageNetwork extends CompanyEvents {
  final File img;
  ChangeImageNetwork(this.img);
}
class ChangeNameCompany extends CompanyEvents{
  final name;
  ChangeNameCompany(this.name);
}

class ChangeCategoryCompany extends CompanyEvents{
  final category_id;
  ChangeCategoryCompany(this.category_id);
}
class ChangeIdCompany extends CompanyEvents{
  final id;
  ChangeIdCompany(this.id);
}
class ResetError extends CompanyEvents {}

class Enable extends CompanyEvents {}

class Disable extends CompanyEvents {}
