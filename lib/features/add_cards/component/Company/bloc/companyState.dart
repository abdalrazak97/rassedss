library companyState;
import 'dart:io';
import 'package:built_value/built_value.dart';
import 'package:rassed_app/features/Constants/ApiConstants.dart';
import 'package:rassed_app/features/add_cards/component/Company/model/get_company_model.dart';
import 'package:rassed_app/features/add_cards/component/Company/model/add_company_model.dart';
import 'package:rassed_app/features/add_cards/component/Company/model/get_categories_model.dart';
import 'package:rassed_app/features/add_cards/component/Company/model/edit_company_model.dart';

import 'package:rassed_app/features/error/error_handler.dart';

part 'companyState.g.dart';
abstract class CompanyState implements Built<CompanyState, CompanyStateBuilder> {
  bool get isSuccess;
  bool get receive;
  bool get isLoading;
  String get Accept_Language;
  String get api_token;
  String get name;
  int get category_id;
  int get id;
  @nullable
  ErrorCode get error;
  @nullable
  File get image;
  String get errorMessage;
  List<GetCompanyResponseModel> get getCompanyResponseModel;
  List<GetCategoriesResponseModel> get getCategoriesResponseModel;
  AddCompanyResponseModel get addCompanyResponseModel;
  EditCompanyResponseModel get editCompanyResponseModel;
  CompanyState._();

  factory CompanyState([updates(CompanyStateBuilder b)]) = _$CompanyState;

  factory CompanyState.initial() {
    return CompanyState((b) => b
      ..isLoading = false
      ..isSuccess = false
      ..receive=false
      ..api_token =''
      ..name=''
      ..category_id=0
      ..id=0
      ..errorMessage = ''
      ..Accept_Language = ''
      ..getCompanyResponseModel=[]
        ..getCategoriesResponseModel=[]
        ..addCompanyResponseModel=AddCompanyResponseModel(
          message: ''
        )
      ..editCompanyResponseModel=EditCompanyResponseModel(
          message: ''
      )
    );
  }
}
