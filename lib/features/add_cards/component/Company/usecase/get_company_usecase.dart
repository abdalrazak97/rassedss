import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:rassed_app/features/Constants/usecase.dart';
import 'package:rassed_app/features/add_cards/component/Company/model/get_company_model.dart';
import 'package:rassed_app/features/add_cards/component/Company/api/get_copmany_remote.dart';

class GetCompanysUseCase extends UseCase<List<GetCompanyResponseModel>, getCompanyParams> {
  final GetCompanyDataSource getCompanyApi;

  GetCompanysUseCase({this.getCompanyApi});

  @override
  Future<Either<String, List<GetCompanyResponseModel>>> call(getCompanyParams params) async {
    return await getCompanyApi.getCompany(params);

  }
}

class getCompanyParams extends Equatable {
  final String api_token;

  getCompanyParams(
      {@required this.api_token,});

  @override
  // TODO: implement props
  List<Object> get props => [api_token];
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['api_token'] = this.api_token;

    return data;
  }
}
