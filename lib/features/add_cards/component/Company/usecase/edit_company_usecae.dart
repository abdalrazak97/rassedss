
import 'dart:io';
import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:rassed_app/features/Constants/usecase.dart';
import 'package:rassed_app/features/add_cards/component/Company/api/edit_company_remote.dart';
import 'package:rassed_app/features/add_cards/component/Company/model/edit_company_model.dart';
import 'package:rassed_app/features/error/failures.dart';
import 'package:rassed_app/features/error/error_handler.dart';
class EditCompanyUseCase extends UseCase<EditCompanyResponseModel, editCompanyParams> {
  final EditCompanyDataSource editCompanyDataSource;
  EditCompanyUseCase({this.editCompanyDataSource});

  @override
  Future<Either<String, EditCompanyResponseModel>> call(editCompanyParams params) async {
    return await editCompanyDataSource.editCompany(params);
  }
}


class editCompanyParams extends Equatable {
  final int id;
  final String api_token;
  final String name;
  final int category_select;
  final File image;

  editCompanyParams(
      { this.id,this.api_token, this.category_select, this.name,this.image});

  @override
  // TODO: implement props
  List<Object> get props => [api_token];
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['api_token'] = this.api_token;
    data['category_select'] = this.category_select;
    data['name'] = this.name;
    data['image'] = this.image;

    return data;
  }
}
