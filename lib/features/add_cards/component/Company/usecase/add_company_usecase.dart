
import 'dart:io';
import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:rassed_app/features/Constants/usecase.dart';
import 'package:rassed_app/features/add_cards/component/Company/api/addCompany.dart';
import 'package:rassed_app/features/add_cards/component/Company/model/add_company_model.dart';
import 'package:rassed_app/features/error/failures.dart';
import 'package:rassed_app/features/error/error_handler.dart';
class AddCompanyUseCase extends UseCase<AddCompanyResponseModel, addCompanyParams> {
  final AddCompanyDataSource addCompanyDataSource;
  AddCompanyUseCase({this.addCompanyDataSource});

  @override
  Future<Either<String, AddCompanyResponseModel>> call(addCompanyParams params) async {
    return await addCompanyDataSource.addCompany(params);
  }
}


class addCompanyParams extends Equatable {
  final String api_token;
  final String name;
  final int category_id;
  final File image;

  addCompanyParams(
      {@required this.api_token,@required this.category_id,@required this.name,this.image});

  @override
  // TODO: implement props
  List<Object> get props => [api_token];
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['api_token'] = this.api_token;
    data['category_id'] = this.category_id;
    data['name'] = this.name;
    data['image'] = this.image;

    return data;
  }
}
