import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:rassed_app/features/Constants/usecase.dart';
import 'package:rassed_app/features/add_cards/component/Company/api/get_categories.dart';
import 'package:rassed_app/features/add_cards/component/Company/model/get_categories_model.dart';


class GetCategoryUseCase extends UseCase<List<GetCategoriesResponseModel>,getCategoryParams > {
  final GetCategoriesDataSource getCategories;

  GetCategoryUseCase({this.getCategories});

  @override
  Future<Either<String, List<GetCategoriesResponseModel>>> call(getCategoryParams params) async {
    return await getCategories.getCategories(params);

  }
}

class getCategoryParams extends Equatable {
  final String api_token;

  getCategoryParams(
      {@required this.api_token,});

  @override
  // TODO: implement props
  List<Object> get props => [api_token];
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['api_token'] = this.api_token;

    return data;
  }
}
