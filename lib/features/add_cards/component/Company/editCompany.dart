import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:google_fonts_arabic/fonts.dart';
import 'package:image_picker/image_picker.dart';
import 'package:rassed_app/core/utils/dialog.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter/services.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:rassed_app/features/Constants/ApiConstants.dart';
import 'package:rassed_app/features/settings/Preference.dart';
import 'dart:convert';
import 'package:rassed_app/features/widget/textauto.dart';
import 'package:easy_localization/easy_localization.dart';
import 'dart:ui' as ui;
import '../../../../injection.dart';
import 'bloc/companyBloc.dart';
import 'bloc/companyState.dart';
import 'model/get_categories_model.dart';
import 'model/get_company_model.dart';

class EditCompany extends StatefulWidget {

 final GetCompanyResponseModel companyResponseModel;
 final List<GetCategoriesResponseModel> getCategoriesResponseModel;
  const EditCompany({Key key,this.companyResponseModel,this.getCategoriesResponseModel}) : super(key: key);


  @override
  _EditCompanyState createState() => _EditCompanyState();
}
class categoirsClas{
  int id;
  String name;

  categoirsClas({this.id, this.name});
}
class _EditCompanyState extends State<EditCompany> {


  TextEditingController _namecontroller=new TextEditingController();
  TextEditingController priceControl=new TextEditingController();
  CompanyBloc bloc=sl<CompanyBloc>();
  var isSwitched=true;
  var confirm=false;
  var first=true;
  String Token;
  String dropdownValue ;
  List<categoirsClas> Categories=[];
  categoirsClas selectedUser;
  var _image=null;
  int _user;

  Future getImage(int type) async {

     final pickedFile = await ImagePicker.platform.pickImage(source: ImageSource.gallery, maxHeight: 480, maxWidth: 480,imageQuality: 80);
    setState(() {
      if (pickedFile != null) {
        _image = File(pickedFile.path);
      } else {
        print('No image selected.');
      }
    });
  }

  Color color;

  @override
  void initState() {
    for(int i=0;i<widget.getCategoriesResponseModel.length;i++)
      Categories.add(categoirsClas(name: widget.getCategoriesResponseModel[i].name,id: widget.getCategoriesResponseModel[i].id));
    print(Categories[0]);
    selectedUser=Categories[0];
  print(selectedUser.name);
    Token=Preferences.getUserToken();
    _namecontroller.text=widget.companyResponseModel.name;
    _user=2;
    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    return BlocListener<CompanyBloc,CompanyState>(
      bloc: bloc,
      listener: (context, state) {

        if(state.receive && state.isSuccess){
          if(state.editCompanyResponseModel.message=='Company Details Updated')
          {
            bloc.onCompanyEvent();
            Navigator.pop(context,);
          }
        }
      },
      child: OrientationBuilder(
          builder: (context, orientation) {
            bool isPotrait = MediaQuery
                .of(context)
                .orientation == Orientation.portrait;
            return BlocBuilder<CompanyBloc,CompanyState>(
                bloc: bloc,
                builder: (context, state){
                  return ModalProgressHUD(
                    inAsyncCall:state.isLoading,
                    child: Scaffold(
                      appBar: AppBar(
                        iconTheme: IconThemeData(color: Colors.grey.shade800),
                        backgroundColor: Colors.grey.shade200,
                        elevation: 0,
                        leading: SizedBox(),
                        actions: [
                          SizedBox(
                            width:ScreenUtil().screenWidth ,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Row(
                                  children: [
                                    TextButton(

                                      onPressed: (){
                                        showMyDialog(context,'Unsaved Change'.tr().toString(),'Do you want to resume editing or discard these changes'.tr().toString());
                                      },
                                      child:  Container(
                                        height: ScreenUtil().setHeight(70),
                                        child: SvgPicture.asset(
                                          "assets/icons/close3.svg",
                                        ),
                                      ),
                                    ) ,
                                    Container(
                                        alignment:Alignment.centerLeft,
                                        child: AutoTextgrey(ScreenUtil().setSp(20),"Edit Company".tr().toString(),Colors.black)),
                                  ],
                                ),
                                MaterialButton(
                                    disabledColor: Colors.grey,
                                    minWidth:ScreenUtil().screenWidth*0.2 ,
                                    height: double.maxFinite,
                                    color: Colors.blueGrey,
                                    child: Padding(
                                      padding:  EdgeInsets.symmetric(vertical:ScreenUtil().setHeight(6), ),
                                      child: Container(child:
                                      AutoTextgrey(ScreenUtil().setSp(20),"Save".tr().toString(),Colors.grey.shade200)),
                                    ),
                                    onPressed:() {
                                      bloc.onChangeIdCompany(widget.companyResponseModel.id);
                                      bloc.onChangeNameCompany(_namecontroller.text);
                                      if(_image!=null)
                                      bloc.onChangeImageNetwork(_image);
                                      bloc.onChangeCategoryCompany(_user);
                                      bloc.onEditCompanyEvent();

                                    } ),
                              ],
                            ),
                          ),
                        ],

                      ),
                      body: SafeArea(
                          child: SingleChildScrollView(
                            child: Padding(
                              padding: EdgeInsets.symmetric( horizontal: 1),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                textDirection: ui.TextDirection.rtl,
                                children: [
                                  Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    textDirection: ui.TextDirection.rtl,
                                    children: [
                                      SizedBox(height:isPotrait? ScreenUtil().screenHeight * 0.05:ScreenUtil().screenHeight * 0.1,),
                                      Align(
                                        alignment: Alignment.center,
                                        child: InkWell(
                                          onTap: () async{

                                            await getImage(0);

                                          },
                                          child: Column(
                                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                            children: [
                                              Container(
                                                width:isPotrait? ScreenUtil().screenWidth *0.8:ScreenUtil().screenWidth *0.4,
                                                height:isPotrait? ScreenUtil().screenHeight * 0.2:ScreenUtil().screenHeight * 0.2,
                                                decoration: BoxDecoration(
                                                  color: Colors.blueGrey,
                                                  borderRadius: BorderRadius.circular(8.0),
                                                ),
                                                child: _image==null? ClipRRect(borderRadius: BorderRadius.circular(8.0), child: Image.network(Url.BaseUrl+widget.companyResponseModel.image,fit: BoxFit.cover,)):ClipRRect(borderRadius: BorderRadius.circular(8.0), child: Image.file(_image,fit: BoxFit.cover,)),
                                              ),

                                            ],
                                          ),
                                        ),
                                      ),
                                      SizedBox(height:isPotrait? ScreenUtil().screenHeight * 0.05:ScreenUtil().screenHeight * 0.1,),
                                      Padding(
                                        padding: EdgeInsets.symmetric(
                                            vertical: ScreenUtil().setHeight(10),
                                            horizontal: ScreenUtil().setWidth(30)),
                                        child: Text(
                                          'Company Name'.tr().toString(),
                                          style: TextStyle(
                                              fontFamily: ArabicFonts.Cairo,
                                              package: 'google_fonts_arabic',
                                              fontSize: ScreenUtil().setSp(18),
                                              color: Colors.black),
                                        ),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.symmetric(
                                            horizontal: ScreenUtil().setWidth(12)),
                                        child: Container(
                                          padding: EdgeInsets.symmetric(
                                              vertical:isPotrait?ScreenUtil().setHeight(6):ScreenUtil().setHeight(2),
                                              horizontal: ScreenUtil().setWidth(12)),
                                          decoration: BoxDecoration(
                                              border: Border.all(color: Colors.grey.shade400)
                                          ),
                                          child: TextField(
                                            controller:_namecontroller ,
                                            onChanged:(value){
                                           //   blocPrdouct.onChangeName(value);
                                            } ,
                                            style: TextStyle(
                                              fontSize:isPotrait?ScreenUtil().setSp(16):ScreenUtil().setSp(20),
                                              fontWeight: FontWeight.w500,
                                              fontFamily: 'Raleway',
                                            ),
                                            decoration: InputDecoration(
                                                focusedBorder: InputBorder.none,
                                                border: InputBorder.none,
                                                enabledBorder: InputBorder.none,
                                                hintText: widget.companyResponseModel.name,
                                                hintStyle: TextStyle(
                                                  fontWeight: FontWeight.w500,
                                                  fontFamily: 'Raleway',
                                                  fontSize: isPotrait?ScreenUtil().setSp(16):ScreenUtil().setSp(20),
                                                  color: Colors.grey.shade400,
                                                ),
                                                labelStyle: TextStyle(
                                                  fontSize: isPotrait?ScreenUtil().setSp(16):ScreenUtil().setSp(20),
                                                )
                                            ),
                                          ),
                                        ),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.symmetric(
                                            vertical: ScreenUtil().setHeight(10),
                                            horizontal: ScreenUtil().setWidth(30)),
                                        child: Text(
                                          'Company Category'.tr().toString(),
                                          style: TextStyle(
                                              fontFamily: ArabicFonts.Cairo,
                                              package: 'google_fonts_arabic',
                                              fontSize: ScreenUtil().setSp(18),
                                              color: Colors.black),
                                        ),
                                      ),
                                      Container(
                                          width: ScreenUtil().screenWidth * 0.95,
                                          color: Colors.grey.shade50,
                                          padding: EdgeInsets.symmetric(
                                              horizontal: ScreenUtil().setWidth(5)),
                                          child: Row(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                            textDirection: ui.TextDirection.rtl,
                                            children: [
                                              SizedBox(
                                                width: ScreenUtil().screenWidth * 0.8,
                                                child: Row(
                                                  textDirection: ui.TextDirection.rtl,
                                                  children: [
                                                    Container(
                                                      padding: EdgeInsets.symmetric(horizontal: 10.0),
                                                      decoration: BoxDecoration(
                                                        borderRadius: BorderRadius.circular(15.0),
                                                        border: Border.all(
                                                            color: Colors.blueGrey,
                                                            style: BorderStyle.solid,
                                                            width: 0.80),
                                                      ),
                                                      child: DropdownButton<categoirsClas>(
                                                        style: TextStyle(color: Colors.blueGrey),
                                                        value: selectedUser,
                                                        elevation: 2,
                                                        onChanged: (categoirsClas pt) {
                                                          FocusScope.of(context).requestFocus(new FocusNode());
                                                          setState(() {
                                                            _user=pt.id;
                                                            print(_user);
                                                            selectedUser=pt;
                                                            dropdownValue = Categories[_user].name;
                                                          });
                                                        },
                                                        items: Categories.map((categoirsClas p) {
                                                          return DropdownMenuItem<categoirsClas>(
                                                              value:p,
                                                              child: Center(
                                                                  child: Text(
                                                                    p.name,
                                                                  )));
                                                        }).toList(),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ],
                                          )),
                                      SizedBox(height: ScreenUtil().screenHeight * 0.02,),

                                    ],
                                  ),

                                  SizedBox(height:ScreenUtil().screenHeight*0.04 ,),
                                ],
                              ),
                            ),
                          )),
                    ),
                  );}
            );}
      ),
    );
  }

}
Color _colorFromHex(String hexColor) {
  final hexCode = hexColor.replaceAll('#','');
  return Color(int.parse('FF$hexCode', radix: 16));
}
