import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'dart:io';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_fonts_arabic/fonts.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:image_picker/image_picker.dart';
import 'editCompany.dart';
import 'dart:ui' as ui;
import 'package:rassed_app/features/add_cards/component/Company/addcompnay.dart';
import 'package:rassed_app/features/add_cards/component/Company/bloc/companyState.dart';
import 'package:rassed_app/features/add_cards/component/Company/companyinfo.dart';
import 'package:rassed_app/features/settings/Preference.dart';
import 'package:rassed_app/features/widget/textauto.dart';

import '../../../../injection.dart';
import 'bloc/companyBloc.dart';

class Company extends StatefulWidget {
  @override
  _CompanyState createState() => _CompanyState();
}

class _CompanyState extends State<Company> {

  CompanyBloc bloc = sl<CompanyBloc>();
  String Token ;
  @override
  void initState() {
    Token=Preferences.getUserToken();
    bloc.onChangeApiToken(Token);
    bloc.onCategoryEvent();
    bloc.onCompanyEvent();

    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        actions: [
          SizedBox(
            width: ScreenUtil().setWidth(40),
            child: Center(
              child: TextButton(
                style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.all(Colors.blueGrey),
                ),
                onPressed: () {
                  bloc.onCompanyEvent();
                  Navigator.pop(context);
                },
                child: SvgPicture.asset(
                  "assets/icons/close3.svg",
                  width: ScreenUtil().screenWidth * 0.1,
                  color: Colors.white,
                ),
              ),
            ),
          ),
        ],
        leading: SizedBox(
          width: 5,
        ),
        backgroundColor: Colors.blueGrey,
        centerTitle: true,
        title: Text(
          'Company'.tr().toString(),
          textDirection: ui.TextDirection.rtl,
          style: TextStyle(
            fontFamily: ArabicFonts.Cairo,
            package: 'google_fonts_arabic',
            fontSize: 20,
          ),
        ),
      ),
      body: BlocBuilder<CompanyBloc,CompanyState>(
           bloc: bloc,
           builder: (context, state) {
             if(state.isLoading)
               return Center(child: CircularProgressIndicator(),);
        return SingleChildScrollView(
        child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            textDirection: ui.TextDirection.rtl,
            children: [
              SizedBox(height: ScreenUtil().screenHeight * 0.05,),
              Center(
                child: SizedBox(
                  height: ScreenUtil().screenHeight * 0.1,
                  width: ScreenUtil().screenWidth * 0.8,
                  child: TextButton(
                      style: ButtonStyle(
                        backgroundColor: MaterialStateProperty.all(
                            Colors.blueGrey),
                        shape: MaterialStateProperty.all(
                            RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(15.0),
                                side:
                                BorderSide(color: Colors.grey.shade50))),
                      ),
                      child: Padding(
                        padding: EdgeInsets.symmetric(
                            vertical: ScreenUtil().setHeight(12)),
                        child: Container(
                            child: AutoTextgrey(
                                ScreenUtil().setSp(16),
                                "Add Company".tr().toString(),
                                Colors.grey.shade50)),
                      ),
                      onPressed: () {
                        Navigator.push(context, MaterialPageRoute(
                            builder: (context) => AddCompany(getCategoriesResponseModel: state.getCategoriesResponseModel,)));
                      }),
                ),
              ),
              Container(
                padding: EdgeInsets.symmetric(vertical: ScreenUtil().setHeight(20)),
                color: Colors.grey.shade50,
                height: ScreenUtil().screenHeight * 0.8,
                width: ScreenUtil().screenWidth ,
                child: ListView.builder(
                  itemCount: state.getCompanyResponseModel.length,
                  itemBuilder: (context, index) =>
                      GestureDetector(
                        onTap: () {
                          Route route = MaterialPageRoute(builder: (context) =>
                              EditCompany(companyResponseModel:  state.getCompanyResponseModel[index],getCategoriesResponseModel: state.getCategoriesResponseModel,));
                          Navigator.push(context, route);
                        },
                        child: item(count: state.getCompanyResponseModel.length, index: index
                          , name: state.getCompanyResponseModel[index].name,url:state.getCompanyResponseModel[index].image ,value: state.getCompanyResponseModel[index].categoryname,),
                      ),
                ),
              ),

              SizedBox(
                height: ScreenUtil().screenHeight * 0.02,
              ),
            ]),
      );
     }
      ),
    );
  }
}
