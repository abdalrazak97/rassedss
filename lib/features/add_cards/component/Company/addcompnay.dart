import 'dart:io';

import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts_arabic/fonts.dart';
import 'dart:ui' as ui;
import 'package:google_fonts_arabic/fonts.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:image_picker/image_picker.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:rassed_app/features/add_cards/component/Company/bloc/companyState.dart';
import 'package:rassed_app/features/add_cards/component/Company/comany.dart';
import 'package:rassed_app/features/add_cards/component/Company/model/get_categories_model.dart';
import 'package:rassed_app/features/settings/Preference.dart';
import 'package:rassed_app/features/widget/textauto.dart';

import '../../../../injection.dart';
import 'bloc/companyBloc.dart';
class AddCompany extends StatefulWidget {
  List<GetCategoriesResponseModel> getCategoriesResponseModel;

AddCompany({this.getCategoriesResponseModel});

  @override
  _AddCompanyState createState() => _AddCompanyState();
}

class categoirsClas{
  int id;
  String name;

  categoirsClas({this.id, this.name});
}
class _AddCompanyState extends State<AddCompany> {
  TextEditingController nameController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController phoneController = TextEditingController();
  CompanyBloc bloc = sl<CompanyBloc>();
  String dropdownValue ;
  File _image;
  int _user;
  String Token;


  List<categoirsClas> Categories=[];
  categoirsClas selectedUser;
  final picker = ImagePicker();

  Future getImage() async {
    final pickedFile = await picker.getImage(source: ImageSource.gallery);

    setState(() {
      if (pickedFile != null) {
        _image = File(pickedFile.path);
      } else {
        print('No image selected.');
      }
    });
  }

  @override
  void initState() {
    for(int i=0;i<widget.getCategoriesResponseModel.length;i++)
      Categories.add(categoirsClas(name: widget.getCategoriesResponseModel[i].name,id: widget.getCategoriesResponseModel[i].id));
     selectedUser=Categories[0];
    Token=Preferences.getUserToken();
    _user=2;
    super.initState();
  }
  @override
  void dispose() {
    // TODO: implement dispose
    nameController.dispose();
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar:   AppBar(
      elevation: 0,
      actions: [
        SizedBox(
          width: ScreenUtil().setWidth(40),
          child: Center(
            child: TextButton(
              style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all(Colors.blueGrey),
              ),
              onPressed: () {
                Navigator.pop(context);
              },
              child: SvgPicture.asset(
                "assets/icons/close3.svg",
                width: ScreenUtil().screenWidth * 0.1,
                color: Colors.white,
              ),
            ),
          ),
        ),
      ],
      leading: SizedBox(
        width: 5,
      ),
      backgroundColor: Colors.blueGrey,
      centerTitle: true,
      title: Text(
        'Company'.tr().toString(),
        textDirection: ui.TextDirection.rtl,
        style: TextStyle(
          fontFamily: ArabicFonts.Cairo,
          package: 'google_fonts_arabic',
          fontSize: 20,
        ),
      ),
    ),
     body: BlocListener<CompanyBloc,CompanyState>(
       bloc: bloc,
       listener: (context, state) {
         if(state.isSuccess && state.receive) {
           print(state.receive);
           if(state.addCompanyResponseModel.message=='company Added Successfully')
           {
             bloc.onCompanyEvent();
             Navigator.pop(context,);
           }

           else AwesomeDialog(
               width: ScreenUtil()
                   .setWidth(400),
               context: context,
               buttonsTextStyle: TextStyle(
                   fontSize:
                   ScreenUtil()
                       .setSp(20),
                   color: Colors
                       .white),
               dialogType: DialogType.ERROR,
               animType: AnimType.BOTTOMSLIDE,
               title: "Alert"
                   .toString(),
               desc:
               "ok ${state.addCompanyResponseModel.message.toString()}"
                   .toString(),
               btnOkOnPress: () {},
               btnOkColor:
               Colors.blueGrey)
             ..show();
         }
         else if(state.error !=null) {
           AwesomeDialog(
               width: ScreenUtil()
                   .setWidth(400),
               context: context,
               buttonsTextStyle: TextStyle(
                   fontSize:
                   ScreenUtil()
                       .setSp(20),
                   color: Colors
                       .white),
               dialogType: DialogType.ERROR,
               animType: AnimType.BOTTOMSLIDE,
               title: "Alert"
                   .toString(),
               desc:
               "ok ${state.addCompanyResponseModel.message.toString()}"
                   .toString(),
               btnOkOnPress: () {},
               btnOkColor:
               Colors.blueGrey)
             ..show();
         }
         else CircularProgressIndicator();

       },
       child: BlocBuilder<CompanyBloc,CompanyState>(
        bloc: bloc,
        builder: (context, state) {
          return ModalProgressHUD(
            inAsyncCall:state.isLoading,
            child: SingleChildScrollView(
              child:
              Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  textDirection: ui.TextDirection.rtl,

                  children: [

                    Padding(
                      padding: EdgeInsets.symmetric(
                          vertical: ScreenUtil().setHeight(10),
                          horizontal: ScreenUtil().setWidth(30)),
                      child: Text(
                        'Company name'.tr().toString(),
                        style: TextStyle(
                            fontFamily: ArabicFonts.Cairo,
                            package: 'google_fonts_arabic',
                            fontSize: ScreenUtil().setSp(18),
                            color: Colors.black),
                      ),
                    ),
                    Container(
                        width: ScreenUtil().screenWidth * 0.95,
                        color: Colors.grey.shade50,
                        padding: EdgeInsets.symmetric(
                            horizontal: ScreenUtil().setWidth(5)),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          textDirection: ui.TextDirection.rtl,
                          children: [
                            Expanded(
                              child: TextField(
                                textDirection: ui.TextDirection.rtl,
                                style: TextStyle(
                                  fontSize: ScreenUtil().setSp(14),
                                ),
                                controller: nameController,
                                decoration: InputDecoration(
                                    focusedBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Colors.blueGrey, width: 2.0),
                                    ),
                                    enabledBorder: OutlineInputBorder(
                                      borderSide:
                                      BorderSide(color: Colors.grey, width: 2.0),
                                    ),
                                    prefixIcon: Icon(
                                      Icons.person_rounded,
                                      size: ScreenUtil().setWidth(20),
                                      color: Colors.grey,

                                    ),
                                    hintTextDirection: ui.TextDirection.rtl,
                                    hintText: 'Name'.toString(),
                                    hintStyle: TextStyle(
                                      fontSize: ScreenUtil().setSp(14),
                                    ),
                                    labelStyle: TextStyle(
                                      fontSize: ScreenUtil().setSp(14),
                                    )),
                                onChanged: (text) {

                                },
                                onSubmitted: (string){
                                  FocusScope.of(context).requestFocus(new FocusNode());
                                },
                              ),
                            ),
                          ],
                        )),
                    Padding(
                      padding: EdgeInsets.symmetric(
                          vertical: ScreenUtil().setHeight(10),
                          horizontal: ScreenUtil().setWidth(30)),
                      child: Text(
                        'Company Logo'.tr().toString(),
                        style: TextStyle(
                            fontFamily: ArabicFonts.Cairo,
                            package: 'google_fonts_arabic',
                            fontSize: ScreenUtil().setSp(18),
                            color: Colors.black),
                      ),
                    ),
                    Container(
                        width: ScreenUtil().screenWidth * 0.95,
                        height: ScreenUtil().screenHeight * 0.3,
                        color: Colors.grey.shade50,
                        padding: EdgeInsets.symmetric(
                            horizontal: ScreenUtil().setWidth(5)),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          textDirection: ui.TextDirection.rtl,
                          children: [
                            SizedBox(
                              height: ScreenUtil().screenHeight * 0.1,
                              width: ScreenUtil().screenWidth * 0.4,
                              child: TextButton(
                                  style: ButtonStyle(
                                    backgroundColor: MaterialStateProperty.all(
                                        Colors.grey.shade50),
                                    shape: MaterialStateProperty.all(
                                        RoundedRectangleBorder(
                                            borderRadius: BorderRadius.circular(
                                                15.0),
                                            side:
                                            BorderSide(color: Colors.blueGrey))),
                                  ),
                                  child: Padding(
                                    padding: EdgeInsets.symmetric(
                                        vertical: ScreenUtil().setHeight(12)),
                                    child: Container(
                                        child: AutoTextgrey(
                                            ScreenUtil().setSp(16),
                                            "Select Photo".tr().toString(),
                                            Colors.blueGrey)),
                                  ),
                                  onPressed: () async {
                                    await getImage();
                                  }),
                            ),
                            Padding(
                              padding: EdgeInsets.symmetric(
                                  vertical: ScreenUtil().setHeight(5)),
                              child: SizedBox(
                                height: ScreenUtil().screenHeight * 0.15,
                                width: ScreenUtil().screenWidth * 0.5,
                                child: _image == null
                                    ? SizedBox()
                                    : ClipRRect(
                                    borderRadius: BorderRadius.circular(20.0),
                                    child: Image.file(
                                      _image,
                                      fit: BoxFit.fill,
                                    )),
                              ),
                            ),
                          ],
                        )),
                    Padding(
                      padding: EdgeInsets.symmetric(
                          vertical: ScreenUtil().setHeight(10),
                          horizontal: ScreenUtil().setWidth(30)),
                      child: Text(
                        'Company Category'.tr().toString(),
                        style: TextStyle(
                            fontFamily: ArabicFonts.Cairo,
                            package: 'google_fonts_arabic',
                            fontSize: ScreenUtil().setSp(18),
                            color: Colors.black),
                      ),
                    ),
                    Container(
                        width: ScreenUtil().screenWidth * 0.95,
                        color: Colors.grey.shade50,
                        padding: EdgeInsets.symmetric(
                            horizontal: ScreenUtil().setWidth(5)),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          textDirection: ui.TextDirection.rtl,
                          children: [
                            SizedBox(
                              width: ScreenUtil().screenWidth * 0.8,
                              child: Row(
                                textDirection: ui.TextDirection.rtl,
                                children: [
                                  Container(
                                    padding: EdgeInsets.symmetric(horizontal: 10.0),
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(15.0),
                                      border: Border.all(
                                          color: Colors.blueGrey,
                                          style: BorderStyle.solid,
                                          width: 0.80),
                                    ),
                                    child: DropdownButton<categoirsClas>(
                                      style: TextStyle(color: Colors.blueGrey),
                                      value: selectedUser,
                                      elevation: 2,
                                      onChanged: (categoirsClas pt) {
                                        FocusScope.of(context).requestFocus(new FocusNode());
                                        setState(() {
                                           _user=pt.id;
                                           print(_user);
                                           selectedUser=pt;
                                          dropdownValue = Categories[_user].name;
                                        });
                                      },
                                      items: Categories.map((categoirsClas p) {
                                        return DropdownMenuItem<categoirsClas>(
                                            value:p,
                                            child: Center(
                                                child: Text(
                                                  p.name,
                                                )));
                                      }).toList(),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        )),
                    SizedBox(height: ScreenUtil().screenHeight * 0.05,),
                    Center(
                      child: SizedBox(
                        height: ScreenUtil().screenHeight * 0.1,
                        width: ScreenUtil().screenWidth * 0.8,
                        child: TextButton(
                            style: ButtonStyle(
                              backgroundColor: MaterialStateProperty.all(
                                  Colors.blueGrey),
                              shape: MaterialStateProperty.all(
                                  RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(15.0),
                                      side:
                                      BorderSide(color: Colors.grey.shade50))),
                            ),
                            child: Padding(
                              padding: EdgeInsets.symmetric(
                                  vertical: ScreenUtil().setHeight(12)),
                              child: Container(
                                  child: AutoTextgrey(
                                      ScreenUtil().setSp(16),
                                      "Save".tr().toString(),
                                      Colors.grey.shade50)),
                            ),
                            onPressed: () {

                              bloc.onChangeApiToken(Token);
                              bloc.onChangeNameCompany(nameController.text);
                              bloc.onChangeImageNetwork(_image);
                              bloc.onChangeCategoryCompany(selectedUser.id);
                              bloc.onAddCompanyEvent();

                            }),
                      ),
                    ),
                  ]
              ),
            ),
          );
        }
       ),
     ),
    );
  }
}
