import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:rassed_app/features/add_cards/component/CardType/card_type.dart';
import 'package:rassed_app/features/add_cards/component/Company/comany.dart';
import 'dart:ui' as ui;
import 'package:easy_localization/easy_localization.dart';
import 'package:rassed_app/features/home/home.dart';

import 'package:rassed_app/features/profile/component/Personal_Data.dart';

import 'component/Card/card.dart';
class AddCardsPage extends StatefulWidget {


  @override
  _AddCardsPageState createState() => _AddCardsPageState();
}

class _AddCardsPageState extends State<AddCardsPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        actions: [
          SizedBox(width: ScreenUtil().setWidth(40),
            child: Center(child: TextButton(
              style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all(
                    Colors.blueGrey),
              ),
              onPressed: () {
                Navigator.push(context,MaterialPageRoute(builder: (context)=>Home()));
              },
              child: SvgPicture.asset(
                "assets/icons/close3.svg",width: ScreenUtil().screenWidth*0.1,color: Colors.white,
              ),
            ),),),
        ],
        leading: SizedBox(width: 10,),

        backgroundColor: Colors.blueGrey,
        centerTitle: true,
        title: Text('Dashboard'.tr().toString(),
          textDirection: ui.TextDirection.rtl,
          style: TextStyle(
            fontFamily: 'Raleway',
            fontSize: 20,
          ),),
      ),
      body: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            InkWell(
              onTap: (){
                Navigator.push(context, MaterialPageRoute(builder: (context) =>  Company()));
              },
              child: Container(
                padding: EdgeInsets.symmetric(vertical: ScreenUtil().setHeight(10),horizontal: ScreenUtil().setWidth(20)),
                child: Row(
                  textDirection: ui.TextDirection.rtl,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      textDirection: ui.TextDirection.rtl,
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Center(child: Icon(Icons.person_rounded,size: 30,color:Colors.blueGrey,)),
                        SizedBox(width: ScreenUtil().screenWidth*0.1),
                        Center(
                          child: Text('Companies'.tr().toString(),style: TextStyle(
                              fontSize:ScreenUtil()
                                  .setSp(22),
                              fontFamily: 'Raleway',
                              color: Colors
                                  .blueGrey),


                          ),
                        ),
                      ],
                    ),
                    Icon(Icons.arrow_back_ios_outlined,size: 25,color:Colors.blueGrey , textDirection: ui.TextDirection.ltr),
                  ],
                ),
              ),
            ),
            Divider(),
            InkWell(
              onTap: (){
                Navigator.push(context, MaterialPageRoute(builder: (context) =>  CardType()));
              },
              child: Container(
                padding: EdgeInsets.symmetric(vertical: ScreenUtil().setHeight(10),horizontal: ScreenUtil().setWidth(20)),
                child: Row(
                  textDirection: ui.TextDirection.rtl,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      textDirection: ui.TextDirection.rtl,
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [

                        Center(child: Icon(Icons.library_add_sharp,size: 30,color:Colors.blueGrey,)),
                        SizedBox(width: ScreenUtil().screenWidth*0.1),
                        Center(
                          child: Text('Card Type'.tr().toString(),style: TextStyle(
                              fontSize:ScreenUtil()
                                  .setSp(22),
                              fontFamily: 'Raleway',
                              color: Colors
                                  .blueGrey),
                          ),
                        ),
                      ],
                    ),
                    Icon(Icons.arrow_back_ios_outlined,size: 25,color:Colors.blueGrey , textDirection: ui.TextDirection.ltr,),
                  ],
                ),
              ),
            ),
            Divider(),
            InkWell(
                onTap: (){
                  Navigator.push(context, MaterialPageRoute(builder: (context) =>  AddCard()));
                },
              child: Container(
                padding: EdgeInsets.symmetric(vertical: ScreenUtil().setHeight(10),horizontal: ScreenUtil().setWidth(20)),
                child: Row(
                  textDirection: ui.TextDirection.rtl,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      textDirection: ui.TextDirection.rtl,
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Center(child: Icon(Icons.library_add_sharp,size: 30,color:Colors.blueGrey,)),
                        SizedBox(width: ScreenUtil().screenWidth*0.1),
                        Center(
                          child: Text('Cards'.tr().toString(),style: TextStyle(
                              fontSize:ScreenUtil()
                                  .setSp(22),
                              fontFamily: 'Raleway',
                              color: Colors
                                  .blueGrey),
                          ),
                        ),
                      ],
                    ),
                    Icon(Icons.arrow_back_ios_outlined,size: 25,color:Colors.blueGrey , textDirection: ui.TextDirection.ltr),
                  ],
                ),
              ),
            ),
            Divider(),
          ],
        ),
      ),
    );
  }
}
