library shop_state;

import 'package:built_value/built_value.dart';
import '../model/shop_model.dart';
part 'shop_state.g.dart';

abstract class ShopState implements Built<ShopState, ShopStateBuilder> {
  bool get isSuccess;
  bool get receive;
  bool get isLoading;
  String get Accept_Language;
  String get api_token;
  String get errorMessage;
  int get invoice_id;

  List<ShopResponseModel> get shopResponseModel;

  ShopState._();

  factory ShopState([updates(ShopStateBuilder b)]) = _$ShopState;

  factory ShopState.initial() {
    return ShopState((b) => b
      ..isLoading = false
      ..isSuccess = false
      ..receive=false
      ..invoice_id=0
      ..api_token =''
      ..errorMessage = ''
      ..Accept_Language = ''
      ..shopResponseModel=[]

    );
  }
}
