
import 'package:bloc/bloc.dart';
import 'shop_event.dart';
import 'shop_state.dart';
import 'package:rassed_app/features/shop/usecase/shops_usecase.dart';
const String SERVER_FAILURE_MESSAGE = 'Server Failure';
const String CACHE_FAILURE_MESSAGE = 'Cache Failure';
const String INVALID_INPUT_FAILURE_MESSAGE = 'Invalid Input - The number must be a positive integer or zero.';

class ShopBloc extends Bloc<ShopEvents, ShopState> {
  ShopUseCase shopUseCase;

  ShopBloc({this.shopUseCase,}) : super(ShopState.initial());

  void onChangeApiToken(String api_token){
    add( ChangeApiToken(api_token));
  }
  onShopEvent(){
    add(ShopEvent());
  }

  @override
  Stream<ShopState> mapEventToState(ShopEvents event) async* {
    if (event is ShopEvent) {
      yield state.rebuild((b) => b
        ..isLoading = true
        ..isSuccess = false

      );

      final result = await shopUseCase(
          shopParams(
            api_token: state.api_token,
          )
      );

      yield* result.fold((l) async* {

        yield state.rebuild((b) => b
          ..isLoading = false
          ..isSuccess = false
          ..errorMessage = l);
      }, (r) async* {
        print('sucesss');
        yield state.rebuild((b) => b
          ..isLoading = false
          ..isSuccess = true
          ..errorMessage = ''
          ..shopResponseModel=r
          );
      });

    }

    else if (event is ChangeApiToken) {
      print('1111');
      yield state.rebuild((b) => b..api_token = event.api_token);

    }


  }
}
