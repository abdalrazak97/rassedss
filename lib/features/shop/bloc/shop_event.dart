import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class ShopEvents extends Equatable {
  @override
  List<Object> get props => [];
}

class ShopEvent extends ShopEvents {}

class ChangeApiToken extends ShopEvents{
  final api_token;
  ChangeApiToken(this.api_token);
}


class ResetError extends ShopEvents {}

class Enable extends ShopEvents {}

class Disable extends ShopEvents {}
