// GENERATED CODE - DO NOT MODIFY BY HAND

part of shop_state;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$ShopState extends ShopState {
  @override
  final bool isSuccess;
  @override
  final bool receive;
  @override
  final bool isLoading;
  @override
  final String Accept_Language;
  @override
  final String api_token;
  @override
  final String errorMessage;
  @override
  final int invoice_id;
  @override
  final List<ShopResponseModel> shopResponseModel;

  factory _$ShopState([void Function(ShopStateBuilder) updates]) =>
      (new ShopStateBuilder()..update(updates)).build();

  _$ShopState._(
      {this.isSuccess,
      this.receive,
      this.isLoading,
      this.Accept_Language,
      this.api_token,
      this.errorMessage,
      this.invoice_id,
      this.shopResponseModel})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(isSuccess, 'ShopState', 'isSuccess');
    BuiltValueNullFieldError.checkNotNull(receive, 'ShopState', 'receive');
    BuiltValueNullFieldError.checkNotNull(isLoading, 'ShopState', 'isLoading');
    BuiltValueNullFieldError.checkNotNull(
        Accept_Language, 'ShopState', 'Accept_Language');
    BuiltValueNullFieldError.checkNotNull(api_token, 'ShopState', 'api_token');
    BuiltValueNullFieldError.checkNotNull(
        errorMessage, 'ShopState', 'errorMessage');
    BuiltValueNullFieldError.checkNotNull(
        invoice_id, 'ShopState', 'invoice_id');
    BuiltValueNullFieldError.checkNotNull(
        shopResponseModel, 'ShopState', 'shopResponseModel');
  }

  @override
  ShopState rebuild(void Function(ShopStateBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ShopStateBuilder toBuilder() => new ShopStateBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ShopState &&
        isSuccess == other.isSuccess &&
        receive == other.receive &&
        isLoading == other.isLoading &&
        Accept_Language == other.Accept_Language &&
        api_token == other.api_token &&
        errorMessage == other.errorMessage &&
        invoice_id == other.invoice_id &&
        shopResponseModel == other.shopResponseModel;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc(
                        $jc($jc($jc(0, isSuccess.hashCode), receive.hashCode),
                            isLoading.hashCode),
                        Accept_Language.hashCode),
                    api_token.hashCode),
                errorMessage.hashCode),
            invoice_id.hashCode),
        shopResponseModel.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('ShopState')
          ..add('isSuccess', isSuccess)
          ..add('receive', receive)
          ..add('isLoading', isLoading)
          ..add('Accept_Language', Accept_Language)
          ..add('api_token', api_token)
          ..add('errorMessage', errorMessage)
          ..add('invoice_id', invoice_id)
          ..add('shopResponseModel', shopResponseModel))
        .toString();
  }
}

class ShopStateBuilder implements Builder<ShopState, ShopStateBuilder> {
  _$ShopState _$v;

  bool _isSuccess;
  bool get isSuccess => _$this._isSuccess;
  set isSuccess(bool isSuccess) => _$this._isSuccess = isSuccess;

  bool _receive;
  bool get receive => _$this._receive;
  set receive(bool receive) => _$this._receive = receive;

  bool _isLoading;
  bool get isLoading => _$this._isLoading;
  set isLoading(bool isLoading) => _$this._isLoading = isLoading;

  String _Accept_Language;
  String get Accept_Language => _$this._Accept_Language;
  set Accept_Language(String Accept_Language) =>
      _$this._Accept_Language = Accept_Language;

  String _api_token;
  String get api_token => _$this._api_token;
  set api_token(String api_token) => _$this._api_token = api_token;

  String _errorMessage;
  String get errorMessage => _$this._errorMessage;
  set errorMessage(String errorMessage) => _$this._errorMessage = errorMessage;

  int _invoice_id;
  int get invoice_id => _$this._invoice_id;
  set invoice_id(int invoice_id) => _$this._invoice_id = invoice_id;

  List<ShopResponseModel> _shopResponseModel;
  List<ShopResponseModel> get shopResponseModel => _$this._shopResponseModel;
  set shopResponseModel(List<ShopResponseModel> shopResponseModel) =>
      _$this._shopResponseModel = shopResponseModel;

  ShopStateBuilder();

  ShopStateBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _isSuccess = $v.isSuccess;
      _receive = $v.receive;
      _isLoading = $v.isLoading;
      _Accept_Language = $v.Accept_Language;
      _api_token = $v.api_token;
      _errorMessage = $v.errorMessage;
      _invoice_id = $v.invoice_id;
      _shopResponseModel = $v.shopResponseModel;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ShopState other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$ShopState;
  }

  @override
  void update(void Function(ShopStateBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$ShopState build() {
    final _$result = _$v ??
        new _$ShopState._(
            isSuccess: BuiltValueNullFieldError.checkNotNull(
                isSuccess, 'ShopState', 'isSuccess'),
            receive: BuiltValueNullFieldError.checkNotNull(
                receive, 'ShopState', 'receive'),
            isLoading: BuiltValueNullFieldError.checkNotNull(
                isLoading, 'ShopState', 'isLoading'),
            Accept_Language: BuiltValueNullFieldError.checkNotNull(
                Accept_Language, 'ShopState', 'Accept_Language'),
            api_token: BuiltValueNullFieldError.checkNotNull(
                api_token, 'ShopState', 'api_token'),
            errorMessage: BuiltValueNullFieldError.checkNotNull(
                errorMessage, 'ShopState', 'errorMessage'),
            invoice_id: BuiltValueNullFieldError.checkNotNull(
                invoice_id, 'ShopState', 'invoice_id'),
            shopResponseModel: BuiltValueNullFieldError.checkNotNull(
                shopResponseModel, 'ShopState', 'shopResponseModel'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
