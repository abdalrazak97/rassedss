import 'dart:convert';

import 'package:json_annotation/json_annotation.dart';
part 'shop_model.g.dart';

@JsonSerializable()
class ShopResponseModel {
  final int id;
  final String first_name;
  final String last_name;
  final bool is_verify;
  final String email;
  final String phone;
  final double balance;
  final String email_verified_at;
  final int role_id;
  final String shop_name;
  final String device_number;
  final String card_number;
  final String created_at;
  final String updated_at;


  Map<String, dynamic> toJson() => _$ShopResponseModelToJson(this);
  factory ShopResponseModel.fromJson(Map<String, dynamic> json) =>
      _$ShopResponseModelFromJson(json);

  ShopResponseModel({
      this.id,
      this.first_name,
      this.last_name,
      this.is_verify,
      this.email,
      this.phone,
      this.balance,
      this.email_verified_at,
      this.role_id,
      this.shop_name,
      this.device_number,
      this.card_number,
      this.created_at,
      this.updated_at});
}
List<ShopResponseModel> ShopResponseModelFromJson(String str) =>
    List<ShopResponseModel>.from(json.decode(str).map((x) => ShopResponseModel.fromJson(x)));
String ShopResponseModelToJson(List<ShopResponseModel> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));