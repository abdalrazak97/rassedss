// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'shop_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ShopResponseModel _$ShopResponseModelFromJson(Map<String, dynamic> json) {
  return ShopResponseModel(
    id: json['id'] as int,
    first_name: json['first_name'] as String,
    last_name: json['last_name'] as String,
    is_verify: json['is_verify'] as bool,
    email: json['email'] as String,
    phone: json['phone'] as String,
    balance: (json['balance'] as num)?.toDouble(),
    email_verified_at: json['email_verified_at'] as String,
    role_id: json['role_id'] as int,
    shop_name: json['shop_name'] as String,
    device_number: json['device_number'] as String,
    card_number: json['card_number'] as String,
    created_at: json['created_at'] as String,
    updated_at: json['updated_at'] as String,
  );
}

Map<String, dynamic> _$ShopResponseModelToJson(ShopResponseModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'first_name': instance.first_name,
      'last_name': instance.last_name,
      'is_verify': instance.is_verify,
      'email': instance.email,
      'phone': instance.phone,
      'balance': instance.balance,
      'email_verified_at': instance.email_verified_at,
      'role_id': instance.role_id,
      'shop_name': instance.shop_name,
      'device_number': instance.device_number,
      'card_number': instance.card_number,
      'created_at': instance.created_at,
      'updated_at': instance.updated_at,
    };
