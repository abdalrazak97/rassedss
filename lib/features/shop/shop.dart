import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:google_fonts_arabic/fonts.dart';
import 'package:rassed_app/features/settings/Preference.dart';
import 'dart:ui' as ui;
import 'package:rassed_app/features/widget/drawer.dart';
import 'package:rassed_app/injection.dart';
import 'bloc/shop_bloc.dart';
import 'bloc/shop_state.dart';

class Shop extends StatefulWidget {
  const Shop({Key key}) : super(key: key);

  @override
  _ShopState createState() => _ShopState();
}

class _ShopState extends State<Shop> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  String token;
  ShopBloc bloc = sl<ShopBloc>();

  @override
  void initState() {
    token=Preferences.getUserToken();
    bloc.onChangeApiToken(token);
    bloc.onShopEvent();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      drawer: drawer(),
      backgroundColor: Colors.blueGrey,
      appBar: AppBar(
        elevation: 0,
        actions: [
          SizedBox(width: ScreenUtil().setWidth(40),),
        ],
        leading: SizedBox(
          width: ScreenUtil().setWidth(30),
          child: InkWell(child: Icon(Icons.menu),
            onTap: (){
              _scaffoldKey.currentState.openDrawer();
            },),
        ),
        backgroundColor: Colors.blueGrey,
        centerTitle: true,
        title: AutoSizeText('Shops'.tr().toString(),
          style: TextStyle(
            fontFamily: ArabicFonts.Cairo,
            package: 'google_fonts_arabic',
            fontSize: ScreenUtil().setSp(20),
          ),),
      ),


      body: BlocBuilder<ShopBloc,ShopState>(
        bloc: bloc,
        builder: (context, state) {
         if (state.isSuccess)
        return SingleChildScrollView(
          child: Container(

            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              textDirection: ui.TextDirection.rtl,
              children: [
                for(int i=0;i<state.shopResponseModel.length;i++)
                Center(
                  child: Container(
                    margin: EdgeInsets.symmetric(
                        vertical: ScreenUtil().setHeight(20)),
                    width: ScreenUtil().screenWidth * 0.9,
                    height: ScreenUtil().screenHeight * 0.25,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(8.0),
                    ),
                    child: Column(
                      children: [
                        SizedBox(height: ScreenUtil().screenHeight * 0.01,),
                        Padding(
                          padding: EdgeInsets.symmetric(
                              horizontal: ScreenUtil().setWidth(30),
                              vertical: ScreenUtil().setHeight(2)),
                          child: Row(

                            children: [
                              AutoSizeText('ID Shop'.tr().toString()+": ",

                                style: TextStyle(
                                    fontFamily: ArabicFonts.Cairo,
                                    package: 'google_fonts_arabic',
                                    fontSize: ScreenUtil().setSp(18)),),
                              AutoSizeText('${i+1}',style: TextStyle(
                                  fontFamily: ArabicFonts.Cairo,
                                  package: 'google_fonts_arabic',
                                  fontSize: ScreenUtil().setSp(16)),),
                            ],
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(
                              horizontal: ScreenUtil().setWidth(30),
                              vertical: ScreenUtil().setHeight(2)),
                          child: Row(

                            children: [
                              AutoSizeText('Name Shop'.tr().toString()+": ",
                                style: TextStyle(
                                    fontFamily: ArabicFonts.Cairo,
                                    package: 'google_fonts_arabic',
                                    fontSize: ScreenUtil().setSp(18)),),
                              AutoSizeText("first_name",style: TextStyle(
                                  fontFamily: ArabicFonts.Cairo,
                                  package: 'google_fonts_arabic',
                                  fontSize: ScreenUtil().setSp(16)),),
                            ],
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(
                              horizontal: ScreenUtil().setWidth(30),
                              vertical: ScreenUtil().setHeight(2)),
                          child: Row(

                            children: [
                              AutoSizeText(
                                'Balance'.tr().toString()+": ", textDirection: ui.TextDirection.rtl,
                                style: TextStyle(
                                    fontFamily: ArabicFonts.Cairo,
                                    package: 'google_fonts_arabic',
                                    fontSize: ScreenUtil().setSp(18)),),
                              AutoSizeText(state.shopResponseModel[i].balance.toString(),style: TextStyle(
                                  fontFamily: ArabicFonts.Cairo,
                                  package: 'google_fonts_arabic',
                                  fontSize: ScreenUtil().setSp(16)),),
                            ],
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(
                              horizontal: ScreenUtil().setWidth(30),
                              vertical: ScreenUtil().setHeight(2)),
                          child: Row(

                            children: [
                              AutoSizeText(
                                'Phone Number'.tr().toString()+": ",
                                style: TextStyle(
                                    fontFamily: ArabicFonts.Cairo,
                                    package: 'google_fonts_arabic',
                                    fontSize: ScreenUtil().setSp(18)),),
                              AutoSizeText(state.shopResponseModel[i].phone,textDirection: ui.TextDirection.ltr,
                                style: TextStyle(
                                    fontFamily: ArabicFonts.Cairo,
                                    package: 'google_fonts_arabic',
                                    fontSize: ScreenUtil().setSp(16)),),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                ]
            ),
          ),
        );
        return Center(child: CircularProgressIndicator(  valueColor: new AlwaysStoppedAnimation<Color>(Colors.white),),);
    }
      ),
    );
  }
}
