import 'package:flutter/material.dart';
import 'package:rassed_app/features/Constants/ApiConstants.dart';
import 'package:dartz/dartz.dart';
import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:dio/dio.dart';
import 'package:meta/meta.dart';
import '../usecase/shops_usecase.dart';
import '../model/shop_model.dart';

abstract class ShopRemoteDataSource {
  Future<Either<String, List<ShopResponseModel>>> shops(shopParams params);
}

class ShopRemoteDataSourceImpl extends ShopRemoteDataSource {
  final Dio dio;
  final DataConnectionChecker networkInfo;
  ShopRemoteDataSourceImpl({@required this.dio, @required this.networkInfo});

  @override
  Future<Either<String, List<ShopResponseModel>>> shops(shopParams params) async {
    List<ShopResponseModel> shop;

    if (await networkInfo.hasConnection) {
      try {

        final result = await dio.get(
          Url.showShop,
          options: Options(
            headers:{
              'Authorization': 'Bearer ${params.api_token}',
            } ,
            followRedirects: false,
            validateStatus: (status) {
              return status < 500;
            },
          ),
        );

        shop = ShopResponseModelFromJson(result.data);

        return Right(shop);
      } on DioError catch (ex) {
        if (ex.type == DioErrorType.connectTimeout) {
          return Left(Er.NetworkError);
        } else if (ex.type == DioErrorType.receiveTimeout) {
          return Left(Er.NetworkError);
        } else if (shop != null)
          return Left(Er.Error);
        else if (shop == null) return Left(Er.Error);
      }
    } else {
      return Left(Er.NetworkError);
    }
  }
}
