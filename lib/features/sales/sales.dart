
import 'package:auto_size_text/auto_size_text.dart';
import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:google_fonts_arabic/fonts.dart';
import 'package:rassed_app/features/home/home.dart';
import 'package:rassed_app/features/settings/Preference.dart';
import 'dart:ui' as ui;

import 'package:rassed_app/features/widget/drawer.dart';
import 'bloc/sale_bloc.dart';
import 'bloc/sale_state.dart';
import '../../injection.dart';
import 'invoice_details.dart';
class Sales extends StatefulWidget {
  const Sales({Key key}) : super(key: key);

  @override
  _SalesState createState() => _SalesState();
}

class _SalesState extends State<Sales> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  SaleBloc bloc = sl<SaleBloc>();
  String token;
  bool first=false;

  @override
  void initState() {
    token=Preferences.getUserToken();
     bloc.onChangeApiToken(token);
     bloc.onSaleEvent();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      drawer: drawer(),
      backgroundColor: Colors.blueGrey,
      appBar: AppBar(
        elevation: 0,
        actions: [
          SizedBox(width: ScreenUtil().setWidth(40),
          ),
        ],
        leading: SizedBox(
          width: ScreenUtil().setWidth(30),
          child: InkWell(child: Icon(Icons.menu),
            onTap: (){
              _scaffoldKey.currentState.openDrawer();
            },),
        ),
        backgroundColor: Colors.blueGrey,
        centerTitle: true,
        title: AutoSizeText('Invoices'.tr().toString(),
          style: TextStyle(
            fontFamily: ArabicFonts.Cairo,
            package: 'google_fonts_arabic',
            fontSize: 20,
          ),),
      ),
      body: BlocListener<SaleBloc, SaleState>(
        bloc: bloc,
        listener: (context, state) {
          if(state.isSuccess && first){
            if (state.refundAllInvoiceResponseModel.message.contains("Waite To Admin For Accept"))
            {
              setState(() {
                first=false;
              });
              AwesomeDialog(
                  width: ScreenUtil()
                      .setWidth(400),
                  context: context,
                  buttonsTextStyle: TextStyle(
                      fontSize:
                      ScreenUtil()
                          .setSp(20),
                      color: Colors
                          .white),
                  dialogType: DialogType.SUCCES,
                  animType: AnimType.BOTTOMSLIDE,
                  title: "Alert"
                      .toString(),
                  desc:
                  "${state.refundAllInvoiceResponseModel.message.toString()}"

                      .toString(),
                  btnOkOnPress: () {
                    Navigator.push(context,
                      MaterialPageRoute(builder: (context) => Home()),);
                  },
                  btnOkColor:
                  Colors.blueGrey)..show();

            }
          }
        },
        child: BlocBuilder<SaleBloc,SaleState>(
          bloc: bloc,
          builder: (context, state) {
            if(state.isSuccess)
            return SingleChildScrollView(
              child: Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  textDirection: ui.TextDirection.rtl,
                  children: [
                    for(int i=0;i<state.saleResponseModel.length;i++)
                    Center(
                      child: InkWell(
                        onTap: (){
                          Navigator.push(context, MaterialPageRoute(builder: (context) =>InvoiceDetails(invoice_id: state.saleResponseModel[i].id,)));
                        },
                        child: Container(
                          margin: EdgeInsets.symmetric(
                              vertical: ScreenUtil().setHeight(20)),
                          width: ScreenUtil().screenWidth * 0.9,
                          height: ScreenUtil().screenHeight * 0.25,
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(8.0),
                          ),
                          child: Column(

                            children: [
                              SizedBox(height: ScreenUtil().screenHeight * 0.01,),
                              Padding(
                                padding: EdgeInsets.symmetric(
                                    horizontal: ScreenUtil().setWidth(30),
                                    vertical: ScreenUtil().setHeight(2)),
                                child: Row(

                                  children: [
                                    AutoSizeText('Invoice ID'.tr().toString()+": ",

                                      style: TextStyle(
                                          fontFamily: ArabicFonts.Cairo,
                                          package: 'google_fonts_arabic',
                                          fontSize: ScreenUtil().setSp(18)),),
                                    AutoSizeText("${i+1}".toString(),style: TextStyle(
                                        fontFamily: ArabicFonts.Cairo,
                                        package: 'google_fonts_arabic',
                                        fontSize: ScreenUtil().setSp(14)),)
                                  ],
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.symmetric(
                                    horizontal: ScreenUtil().setWidth(30),
                                    vertical: ScreenUtil().setHeight(2)),
                                child: Row(

                                  children: [
                                    AutoSizeText('Total amount'.tr().toString()+": ",

                                      style: TextStyle(
                                          fontFamily: ArabicFonts.Cairo,
                                          package: 'google_fonts_arabic',
                                          fontSize: ScreenUtil().setSp(18)),),
                                    AutoSizeText(state.saleResponseModel[i].total,style: TextStyle(
                                        fontFamily: ArabicFonts.Cairo,
                                        package: 'google_fonts_arabic',
                                        fontSize: ScreenUtil().setSp(14)),),
                                  ],
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.symmetric(
                                    horizontal: ScreenUtil().setWidth(30),
                                    vertical: ScreenUtil().setHeight(2)),
                                child: Row(

                                  children: [
                                    AutoSizeText('Date Invoice'.tr().toString()+": ",

                                      style: TextStyle(
                                          fontFamily: ArabicFonts.Cairo,
                                          package: 'google_fonts_arabic',
                                          fontSize: ScreenUtil().setSp(18)),),
                                    AutoSizeText(state.saleResponseModel[i].invoice_date,style: TextStyle(
                                        fontFamily: ArabicFonts.Cairo,
                                        package: 'google_fonts_arabic',
                                        fontSize: ScreenUtil().setSp(14)),),
                                  ],
                                ),
                              ),
                              Align(
                                  alignment: Alignment.bottomLeft,
                                  child: Container(
                                    height: ScreenUtil().screenHeight * 0.07,
                                    width: ScreenUtil().screenWidth * 0.4,
                                    color: Colors.white,
                                    child: Container(
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(8.0),
                                      ),
                                      padding: EdgeInsets.symmetric(
                                          horizontal: ScreenUtil().setWidth(20)),

                                      child: InkWell(
                                        onTap: () async {
                                           print(state.saleResponseModel[i].id,);
                                         bloc.onChangeInvoiceId(state.saleResponseModel[i].id);
                                         bloc.onChangeIds(null);
                                         bloc.onRefundInvoiceEvent();
                                         setState(() {
                                           first=true;
                                         });
                                        },
                                        child: Container(
                                          decoration: BoxDecoration(
                                            color: Colors.red,
                                            borderRadius: BorderRadius.circular(8.0),
                                          ),

                                          width: ScreenUtil().screenWidth,
                                          child: Center(child: Text('Refund Invoice'.tr().toString(),
                                            style: TextStyle(
                                                fontSize: ScreenUtil().setSp(14),
                                                color: Colors.white,
                                                fontFamily: 'Raleway-Regular'),)),),
                                      ),

                                    ),
                                  ),
                                ),
                            ],
                          ),
                        ),
                      ),
                    ),

                  ],
                ),
              ),
            );
            return Center(child: CircularProgressIndicator(  valueColor: new AlwaysStoppedAnimation<Color>(Colors.white),),);
          }
        ),
      ),
    );
  }
}
