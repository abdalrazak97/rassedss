
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:rassed_app/features/Constants/usecase.dart';
import 'package:rassed_app/features/sales/api/sale_remote.dart';
import '../model/sale_model.dart';



class SaleUseCase extends UseCase<List<SaleResponseModel>, saleInvoiceParams> {
  final SaleRemoteDataSource saleRemoteDataSource;

  SaleUseCase({this.saleRemoteDataSource});

  @override
  Future<Either<String, List<SaleResponseModel>>> call(saleInvoiceParams params) async {
    return await saleRemoteDataSource.saleInvoice(params);

  }
}

class saleInvoiceParams extends Equatable {
  final String api_token;


  saleInvoiceParams(
      {@required this.api_token,});

  @override
  // TODO: implement props
  List<Object> get props => [api_token];
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['api_token'] = this.api_token;
    return data;
  }
}
