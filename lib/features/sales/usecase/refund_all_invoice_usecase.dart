
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:rassed_app/features/Constants/usecase.dart';
import 'package:rassed_app/features/sales/api/refund_all_invoice_remote.dart';
import '../model/refund_all_invoice_model.dart';



class RefundAllInvoiceUseCase extends UseCase<RefundAllInvoiceResponseModel, refundAllInvoiceParams> {
  final RefundAllIncvoiceRemoteDataSource refundAllIncvoiceRemoteDataSource;

  RefundAllInvoiceUseCase({this.refundAllIncvoiceRemoteDataSource});

  @override
  Future<Either<String, RefundAllInvoiceResponseModel>> call(refundAllInvoiceParams params) async {
    return await refundAllIncvoiceRemoteDataSource.refundAllInvoice(params);

  }
}

class refundAllInvoiceParams extends Equatable {
  final String api_token;
  final int invoice_id;
  final List<int> ids;
  refundAllInvoiceParams(
      {@required this.api_token,this.invoice_id,this.ids});

  @override
  // TODO: implement props
  List<Object> get props => [api_token,invoice_id,ids];
  Map<String, dynamic> toJson() {

      final Map<String, dynamic> data = new Map<String, dynamic>();
      data['api_token'] = this.api_token;
      data['invoice_id'] = this.invoice_id;
      data['ids'] = this.ids;
      return data;


  }
}
