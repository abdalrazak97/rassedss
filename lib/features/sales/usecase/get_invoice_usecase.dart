import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:rassed_app/features/Constants/usecase.dart';
import 'package:rassed_app/features/sales/api/get_invoices_remote.dart';
import '../model/get_invoice_model.dart';



class GetInvoiceUseCase extends UseCase<List<GetInvoiceResponseModel>, getInvoiceParams> {
  final GetInvoiceRemoteDataSource getInvoiceRemoteDataSource;

  GetInvoiceUseCase({this.getInvoiceRemoteDataSource});

  @override
  Future<Either<String, List<GetInvoiceResponseModel>>> call(getInvoiceParams params) async {
    return await getInvoiceRemoteDataSource.getInvoice(params);

  }
}

class getInvoiceParams extends Equatable {
  final String api_token;
  final int invoice_id;

  getInvoiceParams(
      {@required this.api_token,this.invoice_id});

  @override
  // TODO: implement props
  List<Object> get props => [api_token];
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['api_token'] = this.api_token;
    data['invoice_id'] = this.invoice_id;
    return data;
  }
}
