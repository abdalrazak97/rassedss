import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:google_fonts_arabic/fonts.dart';
import 'package:rassed_app/features/Shopping_basket/model/Ids_class_model.dart';
import 'package:rassed_app/features/settings/Preference.dart';
import 'package:rassed_app/features/widget/drawer.dart';
import 'package:easy_localization/easy_localization.dart';
import '../../injection.dart';
import 'bloc/sale_bloc.dart';
import 'bloc/sale_state.dart';
import 'dart:ui' as ui;
class InvoiceDetails extends StatefulWidget {
   InvoiceDetails({Key key,this.invoice_id}) : super(key: key);
   int invoice_id;
  @override
  _InvoiceDetailsState createState() => _InvoiceDetailsState();
 }

class _InvoiceDetailsState extends State<InvoiceDetails> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  SaleBloc bloc = sl<SaleBloc>();
  String token;
  List<bool> _isChecked;
  List<int> ids=[];
  @override
  void initState() {
    _isChecked = List<bool>.filled(10000, false);
    token=Preferences.getUserToken();
    bloc.onChangeApiToken(token);
    bloc.onChangeInvoiceId(widget.invoice_id);
    bloc.onGetInvoiceEvent();
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      drawer: drawer(),
      backgroundColor: Colors.blueGrey,
      appBar: AppBar(
        elevation: 0,
        actions: [
          SizedBox(width: ScreenUtil().setWidth(40),),
        ],
        leading: SizedBox(
          width: ScreenUtil().setWidth(30),
          child: InkWell(child: Icon(Icons.menu),
            onTap: (){
              _scaffoldKey.currentState.openDrawer();
            },),
        ),
        backgroundColor: Colors.blueGrey,
        centerTitle: true,
        title: AutoSizeText('Invoice Details'.tr().toString(),
          style: TextStyle(
            fontFamily: ArabicFonts.Cairo,
            package: 'google_fonts_arabic',
            fontSize: ScreenUtil().setSp(20),
          ),),
      ),


      body: BlocBuilder<SaleBloc,SaleState>(
          bloc: bloc,
          builder: (context, state) {
            if (state.isSuccess)
              return SingleChildScrollView(
                child: Container(
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      textDirection: ui.TextDirection.rtl,
                      children: [
                        for(int i=0;i<state.getInvoiceResponseModel.length;i++)
                          Center(
                            child: Container(
                              margin: EdgeInsets.symmetric(
                                  vertical: ScreenUtil().setHeight(20)),
                              width: ScreenUtil().screenWidth * 0.9,
                              height: ScreenUtil().screenHeight * 0.5,
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(8.0),
                              ),
                              child: Column(
                                children: [
                                  SizedBox(height: ScreenUtil().screenHeight * 0.01,),
                                  Padding(
                                    padding: EdgeInsets.symmetric(
                                        horizontal: ScreenUtil().setWidth(30),
                                        vertical: ScreenUtil().setHeight(2)),
                                    child: Row(

                                      children: [
                                        AutoSizeText('Name Card'.tr().toString()+": ",

                                          style: TextStyle(
                                              fontFamily: ArabicFonts.Cairo,
                                              package: 'google_fonts_arabic',
                                              fontSize: ScreenUtil().setSp(18)),),
                                        AutoSizeText('${state.getInvoiceResponseModel[i].name}',style: TextStyle(
                                            fontFamily: ArabicFonts.Cairo,
                                            package: 'google_fonts_arabic',
                                            fontSize: ScreenUtil().setSp(16)),),
                                      ],
                                    ),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.symmetric(
                                        horizontal: ScreenUtil().setWidth(30),
                                        vertical: ScreenUtil().setHeight(2)),
                                    child: Row(

                                      children: [
                                        AutoSizeText('Price Card'.tr().toString()+": ",
                                          style: TextStyle(
                                              fontFamily: ArabicFonts.Cairo,
                                              package: 'google_fonts_arabic',
                                              fontSize: ScreenUtil().setSp(18)),),
                                        AutoSizeText("${state.getInvoiceResponseModel[i].price}",style: TextStyle(
                                            fontFamily: ArabicFonts.Cairo,
                                            package: 'google_fonts_arabic',
                                            fontSize: ScreenUtil().setSp(16)),),
                                      ],
                                    ),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.symmetric(
                                        horizontal: ScreenUtil().setWidth(30),
                                        vertical: ScreenUtil().setHeight(2)),
                                    child: Row(

                                      children: [
                                        AutoSizeText(
                                          'Pin Serial'.tr().toString()+": ", textDirection: ui.TextDirection.rtl,
                                          style: TextStyle(
                                              fontFamily: ArabicFonts.Cairo,
                                              package: 'google_fonts_arabic',
                                              fontSize: ScreenUtil().setSp(18)),),
                                        AutoSizeText(state.getInvoiceResponseModel[i].pinserial.toString(),style: TextStyle(
                                            fontFamily: ArabicFonts.Cairo,
                                            package: 'google_fonts_arabic',
                                            fontSize: ScreenUtil().setSp(16)),),
                                      ],
                                    ),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.symmetric(
                                        horizontal: ScreenUtil().setWidth(30),
                                        vertical: ScreenUtil().setHeight(2)),
                                    child: Row(

                                      children: [
                                        AutoSizeText(
                                          'Pin Number'.tr().toString()+": ",
                                          style: TextStyle(
                                              fontFamily: ArabicFonts.Cairo,
                                              package: 'google_fonts_arabic',
                                              fontSize: ScreenUtil().setSp(18)),),
                                        AutoSizeText(state.getInvoiceResponseModel[i].pinnumber,textDirection: ui.TextDirection.ltr,
                                          style: TextStyle(
                                              fontFamily: ArabicFonts.Cairo,
                                              package: 'google_fonts_arabic',
                                              fontSize: ScreenUtil().setSp(16)),),
                                      ],
                                    ),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.symmetric(
                                        horizontal: ScreenUtil().setWidth(30),
                                        vertical: ScreenUtil().setHeight(2)),
                                    child: Row(

                                      children: [
                                        AutoSizeText(
                                          'Pin Expiry'.tr().toString()+": ",
                                          style: TextStyle(
                                              fontFamily: ArabicFonts.Cairo,
                                              package: 'google_fonts_arabic',
                                              fontSize: ScreenUtil().setSp(18)),),
                                        AutoSizeText(state.getInvoiceResponseModel[i].pinexpiry.substring(0,10),textDirection: ui.TextDirection.ltr,
                                          style: TextStyle(
                                              fontFamily: ArabicFonts.Cairo,
                                              package: 'google_fonts_arabic',
                                              fontSize: ScreenUtil().setSp(16)),),
                                      ],
                                    ),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.symmetric(
                                        horizontal: ScreenUtil().setWidth(30),
                                        vertical: ScreenUtil().setHeight(2)),
                                    child: Row(

                                      children: [
                                        AutoSizeText(
                                          'Face Value'.tr().toString()+": ",
                                          style: TextStyle(
                                              fontFamily: ArabicFonts.Cairo,
                                              package: 'google_fonts_arabic',
                                              fontSize: ScreenUtil().setSp(18)),),
                                        AutoSizeText(state.getInvoiceResponseModel[i].facevalue,textDirection: ui.TextDirection.ltr,
                                          style: TextStyle(
                                              fontFamily: ArabicFonts.Cairo,
                                              package: 'google_fonts_arabic',
                                              fontSize: ScreenUtil().setSp(16)),),
                                      ],
                                    ),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.symmetric(
                                        horizontal: ScreenUtil().setWidth(30),
                                        vertical: ScreenUtil().setHeight(2)),
                                    child: Row(

                                      children: [
                                        AutoSizeText(
                                          'Number Supplier'.tr().toString()+": ",
                                          style: TextStyle(
                                              fontFamily: ArabicFonts.Cairo,
                                              package: 'google_fonts_arabic',
                                              fontSize: ScreenUtil().setSp(18)),),
                                        AutoSizeText(state.getInvoiceResponseModel[i].suplier_phone.toString(),textDirection: ui.TextDirection.ltr,
                                          style: TextStyle(
                                              fontFamily: ArabicFonts.Cairo,
                                              package: 'google_fonts_arabic',
                                              fontSize: ScreenUtil().setSp(16)),),
                                      ],
                                    ),
                                  ),
                                 Padding(
                                 padding: EdgeInsets.symmetric(
                                  horizontal: ScreenUtil().setWidth(30),
                                  vertical: ScreenUtil().setHeight(2)),
                                      child: Row(
                                          children: [
                                        Checkbox(
                                            activeColor: Colors.red,
                                            value: _isChecked[i],
                                        onChanged: (value){
                                        setState(() {
                                          if(value==true)
                                          ids.add(state.getInvoiceResponseModel[i].id);
                                          else
                                            ids.remove(state.getInvoiceResponseModel[i].id);
                                          _isChecked[i]=value;
                                          print(ids);
                                          });
                                   }),
                                            AutoSizeText(
                                              'Refund Card'.tr().toString(),
                                              style: TextStyle(
                                                color: Colors.red,
                                                  fontFamily: ArabicFonts.Cairo,
                                                  package: 'google_fonts_arabic',
                                                  fontSize: ScreenUtil().setSp(18)),),
                                ]),),
                                ],
                              ),
                            ),
                          ),

                        Align(
                          alignment: Alignment.bottomCenter,
                          child: Container(
                            height: ScreenUtil().screenHeight * 0.1,
                            width: ScreenUtil().screenWidth * 0.7,

                            child: Container(
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(8.0),
                              ),
                              padding: EdgeInsets.symmetric(
                                  horizontal: ScreenUtil().setWidth(20)),

                              child: InkWell(
                                onTap: () async {
                                  bloc.onChangeInvoiceId(widget.invoice_id);
                                  bloc.onChangeIds(ids);
                                  bloc.onRefundInvoiceEvent();
                                },
                                child: Container(
                                  decoration: BoxDecoration(
                                    color: Colors.red,
                                    borderRadius: BorderRadius.circular(8.0),
                                  ),

                                  width: ScreenUtil().screenWidth,
                                  child: Center(child: Text('Refund Cards'.toString(),
                                    style: TextStyle(
                                        fontSize: ScreenUtil().setSp(14),
                                        color: Colors.white,
                                      fontFamily: ArabicFonts.Cairo,
                                      package: 'google_fonts_arabic',),)),),
                              ),

                            ),
                          ),
                        ),
                        SizedBox(height: 20,),
                      ]


                  ),
                ),
              );
            return Center(child: CircularProgressIndicator(  valueColor: new AlwaysStoppedAnimation<Color>(Colors.white),),);
          }
      ),
    );
  }
}
