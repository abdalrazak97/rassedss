import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:rassed_app/features/Constants/ApiConstants.dart';
import 'package:dartz/dartz.dart';
import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:dio/dio.dart';
import 'package:meta/meta.dart';
import '../usecase/refund_all_invoice_usecase.dart';
import '../model/refund_all_invoice_model.dart';

abstract class RefundAllIncvoiceRemoteDataSource {
  Future<Either<String, RefundAllInvoiceResponseModel>> refundAllInvoice(refundAllInvoiceParams params);
}

class RefundAllIncvoiceRemoteDataSourceImpl extends RefundAllIncvoiceRemoteDataSource {
  final Dio dio;
  final DataConnectionChecker networkInfo;
  RefundAllIncvoiceRemoteDataSourceImpl({@required this.dio, @required this.networkInfo});

  @override
  Future<Either<String, RefundAllInvoiceResponseModel>> refundAllInvoice(refundAllInvoiceParams params) async {
    RefundAllInvoiceResponseModel refundAllInvoice;


    if (await networkInfo.hasConnection) {
      try {
        final result = await dio.post(
          Url.refundAllInvoice,
          data: jsonEncode(params.toJson()),
          options: Options(
            headers:{
              'Authorization': 'Bearer ${params.api_token}',
            } ,
            followRedirects: false,
            validateStatus: (status) {
              return status < 500;
            },
          ),
        );

        refundAllInvoice = RefundAllInvoiceResponseModel.fromJson(json.decode(result.data));

        return Right(refundAllInvoice);
      } on DioError catch (ex) {
        if (ex.type == DioErrorType.connectTimeout) {
          return Left(Er.NetworkError);
        } else if (ex.type == DioErrorType.receiveTimeout) {
          return Left(Er.NetworkError);
        } else if (refundAllInvoice != null)
          return Left(Er.Error);
        else if (refundAllInvoice == null) return Left(Er.Error);
      }
    } else {
      return Left(Er.NetworkError);
    }
  }
}
