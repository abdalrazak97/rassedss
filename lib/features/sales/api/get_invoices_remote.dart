import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:rassed_app/features/Constants/ApiConstants.dart';
import 'package:dartz/dartz.dart';
import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:dio/dio.dart';
import 'package:meta/meta.dart';
import '../usecase/get_invoice_usecase.dart';
import '../model/get_invoice_model.dart';

abstract class GetInvoiceRemoteDataSource {
  Future<Either<String, List<GetInvoiceResponseModel>>> getInvoice(getInvoiceParams params);
}

class GetInvoiceRemoteDataSourceImpl extends GetInvoiceRemoteDataSource {
  final Dio dio;
  final DataConnectionChecker networkInfo;
  GetInvoiceRemoteDataSourceImpl({@required this.dio, @required this.networkInfo});

  @override
  Future<Either<String, List<GetInvoiceResponseModel>>> getInvoice(getInvoiceParams params) async {
    List<GetInvoiceResponseModel> getInvoice;

    if (await networkInfo.hasConnection) {
      try {

        final result = await dio.post(
          Url.showInvoiceDetails,
          data: params.toJson(),
          options: Options(

            headers:{
              'Authorization': 'Bearer ${params.api_token}',
            } ,
            followRedirects: false,
            validateStatus: (status) {
              return status < 500;
            },
          ),
        );

        getInvoice = GetInvoiceResponseModelFromJson(result.data);

        return Right(getInvoice);
      } on DioError catch (ex) {
        if (ex.type == DioErrorType.connectTimeout) {
          return Left(Er.NetworkError);
        } else if (ex.type == DioErrorType.receiveTimeout) {
          return Left(Er.NetworkError);
        } else if (getInvoice != null)
          return Left(Er.Error);
        else if (getInvoice == null) return Left(Er.Error);
      }
    } else {
      return Left(Er.NetworkError);
    }
  }
}
