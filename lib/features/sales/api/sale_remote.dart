import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:rassed_app/features/Constants/ApiConstants.dart';
import 'package:dartz/dartz.dart';
import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:dio/dio.dart';
import 'package:meta/meta.dart';
import '../usecase/sales_usecase.dart';
import '../model/sale_model.dart';

abstract class SaleRemoteDataSource {
  Future<Either<String, List<SaleResponseModel>>> saleInvoice(saleInvoiceParams params);
}

class SaleRemoteDataSourceImpl extends SaleRemoteDataSource {
  final Dio dio;
  final DataConnectionChecker networkInfo;
  SaleRemoteDataSourceImpl({@required this.dio, @required this.networkInfo});

  @override
  Future<Either<String, List<SaleResponseModel>>> saleInvoice(saleInvoiceParams params) async {
    List<SaleResponseModel> sale;

    if (await networkInfo.hasConnection) {
      try {

        final result = await dio.get(
          Url.showInvoice,
          options: Options(
            headers:{
              'Authorization': 'Bearer ${params.api_token}',
                     } ,
            followRedirects: false,
            validateStatus: (status) {
              return status < 500;
            },
          ),
        );

        sale = SaleResponseModelFromJson(result.data);

        return Right(sale);
      } on DioError catch (ex) {
        if (ex.type == DioErrorType.connectTimeout) {
          return Left(Er.NetworkError);
        } else if (ex.type == DioErrorType.receiveTimeout) {
          return Left(Er.NetworkError);
        } else if (sale != null)
          return Left(Er.Error);
        else if (sale == null) return Left(Er.Error);
      }
    } else {
      return Left(Er.NetworkError);
    }
  }
}
