// GENERATED CODE - DO NOT MODIFY BY HAND

part of sale_state;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$SaleState extends SaleState {
  @override
  final bool isSuccess;
  @override
  final bool receive;
  @override
  final bool isLoading;
  @override
  final String Accept_Language;
  @override
  final String api_token;
  @override
  final String errorMessage;
  @override
  final int invoice_id;
  @override
  final List<int> ids;
  @override
  final List<SaleResponseModel> saleResponseModel;
  @override
  final List<GetInvoiceResponseModel> getInvoiceResponseModel;
  @override
  final RefundAllInvoiceResponseModel refundAllInvoiceResponseModel;

  factory _$SaleState([void Function(SaleStateBuilder) updates]) =>
      (new SaleStateBuilder()..update(updates)).build();

  _$SaleState._(
      {this.isSuccess,
      this.receive,
      this.isLoading,
      this.Accept_Language,
      this.api_token,
      this.errorMessage,
      this.invoice_id,
      this.ids,
      this.saleResponseModel,
      this.getInvoiceResponseModel,
      this.refundAllInvoiceResponseModel})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(isSuccess, 'SaleState', 'isSuccess');
    BuiltValueNullFieldError.checkNotNull(receive, 'SaleState', 'receive');
    BuiltValueNullFieldError.checkNotNull(isLoading, 'SaleState', 'isLoading');
    BuiltValueNullFieldError.checkNotNull(
        Accept_Language, 'SaleState', 'Accept_Language');
    BuiltValueNullFieldError.checkNotNull(api_token, 'SaleState', 'api_token');
    BuiltValueNullFieldError.checkNotNull(
        errorMessage, 'SaleState', 'errorMessage');
    BuiltValueNullFieldError.checkNotNull(
        invoice_id, 'SaleState', 'invoice_id');
    BuiltValueNullFieldError.checkNotNull(
        saleResponseModel, 'SaleState', 'saleResponseModel');
    BuiltValueNullFieldError.checkNotNull(
        getInvoiceResponseModel, 'SaleState', 'getInvoiceResponseModel');
    BuiltValueNullFieldError.checkNotNull(refundAllInvoiceResponseModel,
        'SaleState', 'refundAllInvoiceResponseModel');
  }

  @override
  SaleState rebuild(void Function(SaleStateBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  SaleStateBuilder toBuilder() => new SaleStateBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is SaleState &&
        isSuccess == other.isSuccess &&
        receive == other.receive &&
        isLoading == other.isLoading &&
        Accept_Language == other.Accept_Language &&
        api_token == other.api_token &&
        errorMessage == other.errorMessage &&
        invoice_id == other.invoice_id &&
        ids == other.ids &&
        saleResponseModel == other.saleResponseModel &&
        getInvoiceResponseModel == other.getInvoiceResponseModel &&
        refundAllInvoiceResponseModel == other.refundAllInvoiceResponseModel;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc(
                        $jc(
                            $jc(
                                $jc(
                                    $jc(
                                        $jc($jc(0, isSuccess.hashCode),
                                            receive.hashCode),
                                        isLoading.hashCode),
                                    Accept_Language.hashCode),
                                api_token.hashCode),
                            errorMessage.hashCode),
                        invoice_id.hashCode),
                    ids.hashCode),
                saleResponseModel.hashCode),
            getInvoiceResponseModel.hashCode),
        refundAllInvoiceResponseModel.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('SaleState')
          ..add('isSuccess', isSuccess)
          ..add('receive', receive)
          ..add('isLoading', isLoading)
          ..add('Accept_Language', Accept_Language)
          ..add('api_token', api_token)
          ..add('errorMessage', errorMessage)
          ..add('invoice_id', invoice_id)
          ..add('ids', ids)
          ..add('saleResponseModel', saleResponseModel)
          ..add('getInvoiceResponseModel', getInvoiceResponseModel)
          ..add('refundAllInvoiceResponseModel', refundAllInvoiceResponseModel))
        .toString();
  }
}

class SaleStateBuilder implements Builder<SaleState, SaleStateBuilder> {
  _$SaleState _$v;

  bool _isSuccess;
  bool get isSuccess => _$this._isSuccess;
  set isSuccess(bool isSuccess) => _$this._isSuccess = isSuccess;

  bool _receive;
  bool get receive => _$this._receive;
  set receive(bool receive) => _$this._receive = receive;

  bool _isLoading;
  bool get isLoading => _$this._isLoading;
  set isLoading(bool isLoading) => _$this._isLoading = isLoading;

  String _Accept_Language;
  String get Accept_Language => _$this._Accept_Language;
  set Accept_Language(String Accept_Language) =>
      _$this._Accept_Language = Accept_Language;

  String _api_token;
  String get api_token => _$this._api_token;
  set api_token(String api_token) => _$this._api_token = api_token;

  String _errorMessage;
  String get errorMessage => _$this._errorMessage;
  set errorMessage(String errorMessage) => _$this._errorMessage = errorMessage;

  int _invoice_id;
  int get invoice_id => _$this._invoice_id;
  set invoice_id(int invoice_id) => _$this._invoice_id = invoice_id;

  List<int> _ids;
  List<int> get ids => _$this._ids;
  set ids(List<int> ids) => _$this._ids = ids;

  List<SaleResponseModel> _saleResponseModel;
  List<SaleResponseModel> get saleResponseModel => _$this._saleResponseModel;
  set saleResponseModel(List<SaleResponseModel> saleResponseModel) =>
      _$this._saleResponseModel = saleResponseModel;

  List<GetInvoiceResponseModel> _getInvoiceResponseModel;
  List<GetInvoiceResponseModel> get getInvoiceResponseModel =>
      _$this._getInvoiceResponseModel;
  set getInvoiceResponseModel(
          List<GetInvoiceResponseModel> getInvoiceResponseModel) =>
      _$this._getInvoiceResponseModel = getInvoiceResponseModel;

  RefundAllInvoiceResponseModel _refundAllInvoiceResponseModel;
  RefundAllInvoiceResponseModel get refundAllInvoiceResponseModel =>
      _$this._refundAllInvoiceResponseModel;
  set refundAllInvoiceResponseModel(
          RefundAllInvoiceResponseModel refundAllInvoiceResponseModel) =>
      _$this._refundAllInvoiceResponseModel = refundAllInvoiceResponseModel;

  SaleStateBuilder();

  SaleStateBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _isSuccess = $v.isSuccess;
      _receive = $v.receive;
      _isLoading = $v.isLoading;
      _Accept_Language = $v.Accept_Language;
      _api_token = $v.api_token;
      _errorMessage = $v.errorMessage;
      _invoice_id = $v.invoice_id;
      _ids = $v.ids;
      _saleResponseModel = $v.saleResponseModel;
      _getInvoiceResponseModel = $v.getInvoiceResponseModel;
      _refundAllInvoiceResponseModel = $v.refundAllInvoiceResponseModel;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(SaleState other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$SaleState;
  }

  @override
  void update(void Function(SaleStateBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$SaleState build() {
    final _$result = _$v ??
        new _$SaleState._(
            isSuccess: BuiltValueNullFieldError.checkNotNull(
                isSuccess, 'SaleState', 'isSuccess'),
            receive: BuiltValueNullFieldError.checkNotNull(
                receive, 'SaleState', 'receive'),
            isLoading: BuiltValueNullFieldError.checkNotNull(
                isLoading, 'SaleState', 'isLoading'),
            Accept_Language: BuiltValueNullFieldError.checkNotNull(
                Accept_Language, 'SaleState', 'Accept_Language'),
            api_token: BuiltValueNullFieldError.checkNotNull(
                api_token, 'SaleState', 'api_token'),
            errorMessage: BuiltValueNullFieldError.checkNotNull(
                errorMessage, 'SaleState', 'errorMessage'),
            invoice_id: BuiltValueNullFieldError.checkNotNull(
                invoice_id, 'SaleState', 'invoice_id'),
            ids: ids,
            saleResponseModel: BuiltValueNullFieldError.checkNotNull(
                saleResponseModel, 'SaleState', 'saleResponseModel'),
            getInvoiceResponseModel: BuiltValueNullFieldError.checkNotNull(
                getInvoiceResponseModel, 'SaleState', 'getInvoiceResponseModel'),
            refundAllInvoiceResponseModel:
                BuiltValueNullFieldError.checkNotNull(refundAllInvoiceResponseModel, 'SaleState', 'refundAllInvoiceResponseModel'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
