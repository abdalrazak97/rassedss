library sale_state;

import 'package:built_value/built_value.dart';
import 'package:rassed_app/features/sales/model/refund_all_invoice_model.dart';
import '../model/sale_model.dart';
import '../model/get_invoice_model.dart';
part 'sale_state.g.dart';

abstract class SaleState implements Built<SaleState, SaleStateBuilder> {
  bool get isSuccess;
  bool get receive;
  bool get isLoading;
  String get Accept_Language;
  String get api_token;
  String get errorMessage;
  int get invoice_id;
  @nullable
  List<int> get ids;
  List<SaleResponseModel> get saleResponseModel;
  List<GetInvoiceResponseModel> get getInvoiceResponseModel;
  RefundAllInvoiceResponseModel get refundAllInvoiceResponseModel;
  SaleState._();

  factory SaleState([updates(SaleStateBuilder b)]) = _$SaleState;

  factory SaleState.initial() {
    return SaleState((b) => b
      ..isLoading = false
      ..isSuccess = false
      ..receive=false
      ..invoice_id=0
      ..api_token =''
      ..errorMessage = ''
      ..Accept_Language = ''
      ..saleResponseModel=[]
      ..getInvoiceResponseModel=[]
      ..ids=[]
        ..refundAllInvoiceResponseModel=RefundAllInvoiceResponseModel(
          message: '',
          success: false
        )
    );
  }
}
