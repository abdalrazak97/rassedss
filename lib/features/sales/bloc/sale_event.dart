import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class SaleEvents extends Equatable {
  @override
  List<Object> get props => [];
}

class SaleEvent extends SaleEvents {}
class GetInvoiceEvent extends SaleEvents {}
class RefundInvoiceEvent extends SaleEvents {}
class ChangeApiToken extends SaleEvents{
  final api_token;
  ChangeApiToken(this.api_token);
}
class ChangeInvoiceId extends SaleEvents{
  final invoice_id;
  ChangeInvoiceId(this.invoice_id);
}
class ChangeIds extends SaleEvents{
  final ids;
  ChangeIds(this.ids);
}
class ResetError extends SaleEvents {}

class Enable extends SaleEvents {}

class Disable extends SaleEvents {}
