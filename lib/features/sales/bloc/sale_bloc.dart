import 'dart:async';
import 'package:bloc/bloc.dart';
import '../bloc/sale_event.dart';
import 'sale_state.dart';
import '../usecase/sales_usecase.dart';
import '../usecase/get_invoice_usecase.dart';
import '../usecase/refund_all_invoice_usecase.dart';
const String SERVER_FAILURE_MESSAGE = 'Server Failure';
const String CACHE_FAILURE_MESSAGE = 'Cache Failure';
const String INVALID_INPUT_FAILURE_MESSAGE = 'Invalid Input - The number must be a positive integer or zero.';

class SaleBloc extends Bloc<SaleEvents, SaleState> {
  SaleUseCase saleUseCase;
  GetInvoiceUseCase getInvoiceUseCase;
  RefundAllInvoiceUseCase refundAllInvoiceUseCase;
  SaleBloc({this.saleUseCase,this.getInvoiceUseCase,this.refundAllInvoiceUseCase}) : super(SaleState.initial());

  void onChangeApiToken(String api_token){
    add( ChangeApiToken(api_token));
  }
   onSaleEvent(){
    add(SaleEvent());
}
   onGetInvoiceEvent(){
    add(GetInvoiceEvent());
    }

  void onChangeInvoiceId(int invoice_id){
    add( ChangeInvoiceId(invoice_id));
  }
  void onRefundInvoiceEvent(){
    add( RefundInvoiceEvent());
  }

  void onChangeIds(List<int> ids){
    add(ChangeIds(ids));
  }

  @override
  Stream<SaleState> mapEventToState(SaleEvents event) async* {
    if (event is SaleEvent) {
      yield state.rebuild((b) => b
        ..isLoading = true
        ..isSuccess = false

      );

      final result = await saleUseCase(
          saleInvoiceParams(
            api_token: state.api_token,
          )
      );

      yield* result.fold((l) async* {

        yield state.rebuild((b) => b
          ..isLoading = false
          ..isSuccess = false
          ..errorMessage = l);
      }, (r) async* {
        print('sucesss');
        yield state.rebuild((b) => b
          ..isLoading = false
          ..isSuccess = true
          ..errorMessage = ''
          ..saleResponseModel = r);
      });

    }
  else if (event is GetInvoiceEvent) {
      yield state.rebuild((b) => b
        ..isLoading = true
        ..isSuccess = false

      );

      final result = await getInvoiceUseCase(
          getInvoiceParams(
            api_token: state.api_token,
            invoice_id:state.invoice_id,

          )
      );

      yield* result.fold((l) async* {

        yield state.rebuild((b) => b
          ..isLoading = false
          ..isSuccess = false
          ..errorMessage = l);
      }, (r) async* {
        print('sucesss');
        yield state.rebuild((b) => b
          ..isLoading = false
          ..isSuccess = true
          ..receive=true
          ..errorMessage = ''
          ..getInvoiceResponseModel = r);
      });

    }
    else if (event is RefundInvoiceEvent) {
      yield state.rebuild((b) => b
        ..isLoading = true
        ..isSuccess = false

      );

      final result = await refundAllInvoiceUseCase(
          refundAllInvoiceParams(
              api_token: state.api_token,
              invoice_id: state.invoice_id,
              ids: state.ids
          )
      );

      yield* result.fold((l) async* {

        yield state.rebuild((b) => b
          ..isLoading = false
          ..isSuccess = false
          ..errorMessage = l);
      }, (r) async* {

        yield state.rebuild((b) => b
          ..isLoading = false
          ..isSuccess = true
          ..receive=true
          ..errorMessage = ''
          ..refundAllInvoiceResponseModel = r);
      });

    }
    else if (event is ChangeApiToken) {
      yield state.rebuild((b) => b..api_token = event.api_token);
    }
    else if (event is ChangeInvoiceId) {
      yield state.rebuild((b) => b..invoice_id = event.invoice_id);

    }

    else if (event is ChangeIds) {
      yield state.rebuild((b) => b..ids = event.ids);
    }

  }
}
