
import 'dart:convert';

import 'package:json_annotation/json_annotation.dart';




part 'refund_all_invoice_model.g.dart';

@JsonSerializable()
class RefundAllInvoiceResponseModel {

  final bool success;
  final String message;


  Map<String, dynamic> toJson() => _$RefundAllInvoiceResponseModelToJson(this);
  factory RefundAllInvoiceResponseModel.fromJson(Map<String, dynamic> json) =>
      _$RefundAllInvoiceResponseModelFromJson(json);

  RefundAllInvoiceResponseModel({this.success, this.message,});
}
List<RefundAllInvoiceResponseModel> RefundAllInvoiceResponseModelFromJson(String str) =>
    List<RefundAllInvoiceResponseModel>.from(json.decode(str).map((x) => RefundAllInvoiceResponseModel.fromJson(x)));
String RefundAllInvoiceResponseModelToJson(List<RefundAllInvoiceResponseModel> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));