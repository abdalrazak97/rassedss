// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'get_invoice_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

GetInvoiceResponseModel _$GetInvoiceResponseModelFromJson(
    Map<String, dynamic> json) {
  return GetInvoiceResponseModel(
    id: json['id'] as int,
    success: json['success'] as bool,
    message: json['message'] as String,
    invoice_id: json['invoice_id'] as int,
    price: json['price'] as String,
    name: json['name'] as String,
    suplier_phone: json['suplier_phone'] as String,
    pinserial: json['pinserial'] as String,
    facevalue: json['facevalue'] as String,
    pinnumber: json['pinnumber'] as String,
    pinexpiry: json['pinexpiry'] as String,
    is_refund: json['is_refund'] as int,
  );
}

Map<String, dynamic> _$GetInvoiceResponseModelToJson(
        GetInvoiceResponseModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'success': instance.success,
      'message': instance.message,
      'invoice_id': instance.invoice_id,
      'price': instance.price,
      'name': instance.name,
      'suplier_phone': instance.suplier_phone,
      'pinserial': instance.pinserial,
      'facevalue': instance.facevalue,
      'pinnumber': instance.pinnumber,
      'pinexpiry': instance.pinexpiry,
      'is_refund': instance.is_refund,
    };
