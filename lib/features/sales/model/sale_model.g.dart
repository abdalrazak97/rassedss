// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'sale_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SaleResponseModel _$SaleResponseModelFromJson(Map<String, dynamic> json) {
  return SaleResponseModel(
    id: json['id'] as int,
    user_id: json['user_id'] as int,
    total: json['total'] as String,
    success: json['success'] as bool,
    message: json['message'] as String,
    notes: json['notes'] as String,
    invoice_date: json['invoice_date'] as String,
  );
}

Map<String, dynamic> _$SaleResponseModelToJson(SaleResponseModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'user_id': instance.user_id,
      'total': instance.total,
      'success': instance.success,
      'message': instance.message,
      'notes': instance.notes,
      'invoice_date': instance.invoice_date,
    };
