// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'refund_all_invoice_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RefundAllInvoiceResponseModel _$RefundAllInvoiceResponseModelFromJson(
    Map<String, dynamic> json) {
  return RefundAllInvoiceResponseModel(
    success: json['success'] as bool,
    message: json['message'] as String,
  );
}

Map<String, dynamic> _$RefundAllInvoiceResponseModelToJson(
        RefundAllInvoiceResponseModel instance) =>
    <String, dynamic>{
      'success': instance.success,
      'message': instance.message,
    };
