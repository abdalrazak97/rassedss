import 'dart:convert';

import 'package:json_annotation/json_annotation.dart';

part 'get_invoice_model.g.dart';

@JsonSerializable()
class GetInvoiceResponseModel {
  final int id;
  final bool success;
  final String message;
  final int invoice_id;
  final String price;
  final String  name;
  final String suplier_phone;
  final String pinserial;
  final String facevalue;
  final String pinnumber;
  final String pinexpiry;
  final int  is_refund;


  Map<String, dynamic> toJson() => _$GetInvoiceResponseModelToJson(this);
  factory GetInvoiceResponseModel.fromJson(Map<String, dynamic> json) =>
      _$GetInvoiceResponseModelFromJson(json);

  GetInvoiceResponseModel({
      this.id,
      this.success,
      this.message,
      this.invoice_id,
      this.price,
      this.name,
      this.suplier_phone,
      this.pinserial,
      this.facevalue,
      this.pinnumber,
      this.pinexpiry,
      this.is_refund});
}
List<GetInvoiceResponseModel> GetInvoiceResponseModelFromJson(String str) =>
    List<GetInvoiceResponseModel>.from(json.decode(str).map((x) => GetInvoiceResponseModel.fromJson(x)));
String GetInvoiceResponseModelToJson(List<GetInvoiceResponseModel> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));