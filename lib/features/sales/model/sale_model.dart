import 'dart:convert';

import 'package:json_annotation/json_annotation.dart';




part 'sale_model.g.dart';

@JsonSerializable()
class SaleResponseModel {
  final int id;
  final int user_id;
  final String total;
  final bool success;
  final String message;
  final String notes;
  final String invoice_date;

  Map<String, dynamic> toJson() => _$SaleResponseModelToJson(this);
  factory SaleResponseModel.fromJson(Map<String, dynamic> json) =>
      _$SaleResponseModelFromJson(json);

  SaleResponseModel({this.id, this.user_id, this.total, this.success,
      this.message, this.notes, this.invoice_date});
}
List<SaleResponseModel> SaleResponseModelFromJson(String str) =>
    List<SaleResponseModel>.from(json.decode(str).map((x) => SaleResponseModel.fromJson(x)));
String SaleResponseModelToJson(List<SaleResponseModel> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));