

import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:rassed_app/features/Constants/usecase.dart';
import 'package:rassed_app/features/card_details/api/card_remote.dart';
import 'package:rassed_app/features/card_details/model/card_model.dart';




class CardUseCase extends UseCase<List<CardResponseModel>, cardParams> {
  final CardRemoteDataSource CardApi;

  CardUseCase({this.CardApi});

  @override
  Future<Either<String, List<CardResponseModel>>> call(cardParams params) async {
    return await CardApi.card(params);

  }
}

class cardParams extends Equatable {
  final String api_token;
  final String company_id;
  cardParams(
      {@required this.api_token,@required this.company_id});

  @override
  // TODO: implement props
  List<Object> get props => [api_token,company_id];
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['api_token'] = this.api_token;
    data['company_id']= this.company_id;

    return data;
  }
}
