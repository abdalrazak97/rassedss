import 'dart:convert';

import 'package:json_annotation/json_annotation.dart';
import 'package:rassed_app/features/card_details/model/type_card_model.dart';

part 'card_model.g.dart';

@JsonSerializable()
class CardResponseModel {
  final int id;
  final int category_id;
  final String name;
  final String image;
  List<TypeResponseModel> types;
  final bool success;
  final String message;


  Map<String, dynamic> toJson() => _$CardResponseModelToJson(this);
  factory CardResponseModel.fromJson(Map<String, dynamic> json) =>
      _$CardResponseModelFromJson(json);

  CardResponseModel({this.id, this.category_id, this.name, this.types,
      this.success, this.message,this.image});
}
List<CardResponseModel> CardResponseModelFromJson(String str) =>
    List<CardResponseModel>.from(json.decode(str).map((x) => CardResponseModel.fromJson(x)));
String CardResponseModelToJson(List<CardResponseModel> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));
