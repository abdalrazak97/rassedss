import 'package:json_annotation/json_annotation.dart';
import 'cards_type_model.dart';

part 'type_model.g.dart';

@JsonSerializable()
class TypesResponseModel {

  final int id;
  final int card_type_id;
  final int count;
  final String price;
  final String name;




  Map<String, dynamic> toJson() => _$TypesResponseModelToJson(this);
  factory TypesResponseModel.fromJson(Map<String, dynamic> json) =>
      _$TypesResponseModelFromJson(json);

  TypesResponseModel({this.count,this.id,this.card_type_id ,this.price,this.name});
}
