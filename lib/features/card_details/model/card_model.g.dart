// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'card_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CardResponseModel _$CardResponseModelFromJson(Map<String, dynamic> json) {
  return CardResponseModel(
    id: json['id'] as int,
    category_id: json['category_id'] as int,
    name: json['name'] as String,
    types: (json['types'] as List)
        ?.map((e) => e == null
            ? null
            : TypeResponseModel.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    success: json['success'] as bool,
    message: json['message'] as String,
    image: json['image'] as String,
  );
}

Map<String, dynamic> _$CardResponseModelToJson(CardResponseModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'category_id': instance.category_id,
      'name': instance.name,
      'image': instance.image,
      'types': instance.types,
      'success': instance.success,
      'message': instance.message,
    };
