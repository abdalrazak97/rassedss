import 'package:json_annotation/json_annotation.dart';


part 'cards_type_model.g.dart';

@JsonSerializable()
class CardsTypeResponseModel {

  final int id;
  final double price;



  Map<String, dynamic> toJson() => _$CardsTypeResponseModelToJson(this);
  factory CardsTypeResponseModel.fromJson(Map<String, dynamic> json) =>
      _$CardsTypeResponseModelFromJson(json);

  CardsTypeResponseModel({this.id, this.price});
}
