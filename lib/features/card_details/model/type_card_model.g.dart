// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'type_card_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TypeResponseModel _$TypeResponseModelFromJson(Map<String, dynamic> json) {
  return TypeResponseModel(
    id: json['id'] as int,
    name: json['name'] as String,
    image: json['image'] as String,
    description: json['description'] as String,
    data: (json['data'] as List)
        ?.map((e) => e == null
            ? null
            : TypesResponseModel.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    success: json['success'] as bool,
    message: json['message'] as String,
    company_id: json['company_id'] as int,
  );
}

Map<String, dynamic> _$TypeResponseModelToJson(TypeResponseModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'image': instance.image,
      'company_id': instance.company_id,
      'description': instance.description,
      'data': instance.data,
      'success': instance.success,
      'message': instance.message,
    };
