// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'cards_type_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CardsTypeResponseModel _$CardsTypeResponseModelFromJson(
    Map<String, dynamic> json) {
  return CardsTypeResponseModel(
    id: json['id'] as int,
    price: (json['price'] as num)?.toDouble(),
  );
}

Map<String, dynamic> _$CardsTypeResponseModelToJson(
        CardsTypeResponseModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'price': instance.price,
    };
