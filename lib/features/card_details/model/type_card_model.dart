import 'package:json_annotation/json_annotation.dart';
import 'package:rassed_app/features/card_details/model/type_model.dart';

part 'type_card_model.g.dart';

@JsonSerializable()
class TypeResponseModel {
  final int id;
  final String name;
  final String image;
  final int company_id;
  final String description;
  List<TypesResponseModel> data;
  final bool success;
  final String message;


  Map<String, dynamic> toJson() => _$TypeResponseModelToJson(this);
  factory TypeResponseModel.fromJson(Map<String, dynamic> json) =>
      _$TypeResponseModelFromJson(json);

  TypeResponseModel({this.id, this.name, this.image, this.description,this.data,
      this.success, this.message,this.company_id});
}
