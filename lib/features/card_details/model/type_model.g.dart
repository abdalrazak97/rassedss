// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'type_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TypesResponseModel _$TypesResponseModelFromJson(Map<String, dynamic> json) {
  return TypesResponseModel(
    count: json['count'] as int,
    id: json['id'] as int,
    card_type_id: json['card_type_id'] as int,
    price: json['price'] as String,
    name: json['name'] as String,
  );
}

Map<String, dynamic> _$TypesResponseModelToJson(TypesResponseModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'card_type_id': instance.card_type_id,
      'count': instance.count,
      'price': instance.price,
      'name': instance.name,
    };
