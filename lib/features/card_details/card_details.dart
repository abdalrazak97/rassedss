import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:google_fonts_arabic/fonts.dart';
import 'package:hive/hive.dart';
import 'package:rassed_app/features/Shopping_basket/cards_basket_model.dart';
import 'package:rassed_app/features/card_details/bloc/CardState.dart';
import 'package:rassed_app/features/Shopping_basket/shopping_basket.dart';
import 'package:rassed_app/features/Constants/ApiConstants.dart';
import 'package:rassed_app/features/card_details/model/type_model.dart';
import 'package:rassed_app/features/settings/Preference.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:rassed_app/core/utils/Boxs.dart';
import 'dart:ui' as ui;

import '../../injection.dart';
import 'bloc/CardBloc.dart';


class CardDetails extends StatefulWidget {
  int company_id;

  CardDetails(this.company_id);

  @override
  _CardDetailsState createState() => _CardDetailsState();
}

class _CardDetailsState extends State<CardDetails> {
  int selected = 0;
  int selectedType = 0;
  bool buy = false;
  bool first;
  double total=0;
  int numberCards = 0;
  String token;
  var box;
  CardBloc bloc = sl<CardBloc>();
  TypesResponseModel card;
  @override
  void initState() {
    //Token='eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiNjc4MjIyYTAyMjU0Y2VmMDhjYTBhZDUzNTYzMzRjMWRlODUyZGQzNmU4MmQ0OGIwZjhjYzA0Y2I2ZWE1NjY5ODgxZjkwZGZjNjJkMTYzMWMiLCJpYXQiOjE2MjIyODg5MTEsIm5iZiI6MTYyMjI4ODkxMSwiZXhwIjoxNjUzODI0OTExLCJzdWIiOiIzMyIsInNjb3BlcyI6W119.Dq3UWg3yu79Adi9MIBL7VRBIwGfq7cgytizs-UDsaNiuzfjmf24h7YO2KIXuZ2XZ6j3X_uOvPaVPEfDT17WH8Q';
    first = true;
     box = Boxes.getTransactions();
    token = Preferences.getUserToken();
    bloc.onChangeApiToken(token);
    bloc.onChangeCompanyId('${widget.company_id}');
    bloc.onCardEvent();
    card=TypesResponseModel(
      price:'',
      id: 0
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    box = Boxes.getTransactions();
    return Scaffold(
      backgroundColor: Colors.grey.shade200,
      appBar: AppBar(
        elevation: 0,

        backgroundColor: Colors.blueGrey,
        actions: [
          SizedBox(
              width: ScreenUtil().setWidth(40),
              child: Center(

                  child: IconButton(
                    icon: new Stack(
                        children: <Widget>[
                          new Icon(Icons.shopping_cart),
                          new Positioned(
                            right: 0,
                            child: new Container(
                              padding: EdgeInsets.all(1),
                              decoration: new BoxDecoration(
                                color: Colors.red,
                                borderRadius: BorderRadius.circular(6),
                              ),
                              constraints: BoxConstraints(
                                minWidth: 12,
                                minHeight: 12,
                              ),
                              child: new Text(
                                '${box.length}',
                                style: new TextStyle(
                                  color: Colors.white,
                                  fontSize: 8,
                                ),
                                textAlign: TextAlign.center,
                              ),
                            ),
                          )

                        ]
                    ),
                    onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => ShoppingBasket()),
                  );
                },
              ))),
        ],
        centerTitle: true,
        title: Text(
          'Buy Card'.tr().toString(),
          textDirection: ui.TextDirection.rtl,
          style: TextStyle(
            fontFamily: ArabicFonts.Cairo,
            package: 'google_fonts_arabic',
            fontSize: 20,
          ),
        ),
      ),

      body: SafeArea(
        child: BlocBuilder<CardBloc, CardState>(
            bloc: bloc,
            builder: (context, state) {
              print(state.cardResponseModel.isEmpty);
              print(state.cardResponseModel.isEmpty);
              if (state.cardResponseModel.isEmpty)
                return Center(
                  child: CircularProgressIndicator(),
                );
              else if (state.isLoading && first)
                return Center(
                  child: CircularProgressIndicator(),
                );
              else if (state.cardResponseModel[0].types.isEmpty)
                return Center(
                  child: Text(
                    'No Cards for this company'.tr().toString(),
                    textDirection: ui.TextDirection.rtl,
                    style: TextStyle(
                      fontFamily: 'Raleway',
                      fontSize: ScreenUtil().setSp(22),
                    ),
                  ),
                );
              return Stack(children: [
                SingleChildScrollView(
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      textDirection: ui.TextDirection.rtl,
                      children: [
                        Stack(
                            alignment: AlignmentDirectional.center,
                            children: [
                              Container(
                                width: ScreenUtil().screenWidth,
                                height: ScreenUtil().screenHeight * 0.22,
                                decoration: BoxDecoration(
                                    color: Colors.blueGrey,
                                    borderRadius: BorderRadius.only(
                                        bottomLeft: Radius.circular(20),
                                        bottomRight: Radius.circular(20))),
                              ),
                              Transform.translate(
                                offset: Offset(0, ScreenUtil().setHeight(30)),
                                child: Container(
                                  width: ScreenUtil().screenWidth * 0.9,
                                  height: ScreenUtil().screenHeight * 0.2,
                                  decoration: BoxDecoration(
                                    color: Colors.blueGrey.shade200,
                                    borderRadius: BorderRadius.circular(20),
                                  ),
                                  child: ClipRRect(
                                      borderRadius: BorderRadius.circular(20.0),
                                      child: Image.network(
                                          '${Url.BaseUrl}${state.cardResponseModel[0].types[selectedType].image.toString()}',
                                          fit: BoxFit.cover)),
                                ),
                              ),
                            ]),
                        Padding(
                          padding: EdgeInsets.only(
                              top: ScreenUtil().setHeight(30),
                              right: ScreenUtil().setWidth(30)),
                          child: Text(
                            'Description'.tr().toString(),
                            textDirection: ui.TextDirection.rtl,
                            style: TextStyle(
                              fontFamily: ArabicFonts.Cairo,
                              package: 'google_fonts_arabic',
                              fontSize: 18,
                            ),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(
                              vertical: 1,
                              horizontal: ScreenUtil().setWidth(30)),
                          child: Text(
                            state.cardResponseModel[0].types[selectedType].description,
                            textDirection: ui.TextDirection.rtl,
                            maxLines: 4,
                            style: TextStyle(
                                height: 1,
                                fontSize: 16,
                                color: Colors.grey,
                              fontFamily: ArabicFonts.Cairo,
                              package: 'google_fonts_arabic',),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(
                              vertical: 0,
                              horizontal: ScreenUtil().setWidth(30)),
                          child: Text(
                            'Type card'.tr().toString(),
                            textDirection: ui.TextDirection.rtl,
                            style: TextStyle(
                              fontFamily: ArabicFonts.Cairo,
                              package: 'google_fonts_arabic',
                              fontSize: ScreenUtil().setSp(18),
                            ),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(
                              vertical: ScreenUtil().setHeight(5),
                              horizontal: ScreenUtil().setWidth(30)),
                          child:  Wrap(
                              spacing: 4.0, // gap between adjacent chips
                              runSpacing: 4.0,
                            children:[
                              for (int i = 0; i < state.cardResponseModel[0].types.length; i++)
                                Container(
                                width: ScreenUtil().screenWidth * 0.3,
                                height: ScreenUtil().screenHeight * 0.07,
                                margin: EdgeInsets.symmetric(
                                    horizontal: ScreenUtil().setWidth(5)),
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(5.0),
                                    border: Border.all(
                                        width: 2,
                                        color: selectedType == i
                                            ? Colors.blueGrey
                                            : Colors.grey)),
                                child: ElevatedButton(
                                  onPressed: () {
                                    setState(() {
                                      selectedType = i;
                                      first = false;
                                    });
                                  },
                                  style: ButtonStyle(
                                    backgroundColor:
                                        MaterialStateProperty.all(
                                            Colors.white),
                                  ),
                                  child: Text(
                                    state.cardResponseModel[0].types[i].name,
                                    textDirection: ui.TextDirection.rtl,
                                    style: TextStyle(
                                        fontSize: ScreenUtil().setSp(12),
                                        fontWeight: FontWeight.w400,
                                        color: Colors.blueGrey,
                                      fontFamily: ArabicFonts.Cairo,
                                      package: 'google_fonts_arabic',),
                                  ),
                                ),
                                  ),
                          ]
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(
                            right: ScreenUtil().setWidth(30),
                            top: 10,
                          ),
                          child: Text(
                            'Card amount'.tr().toString(),
                            textDirection: ui.TextDirection.rtl,
                            style: TextStyle(
                              fontFamily: ArabicFonts.Cairo,
                              package: 'google_fonts_arabic',
                              fontSize: ScreenUtil().setSp(18),
                            ),
                          ),
                        ),
                        Column(
                          children: [
                            for (int i = 0; i < state.cardResponseModel[0].types[selectedType].data.length; i = i + 2)
                              Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceAround,
                                  textDirection: ui.TextDirection.rtl,
                                  children: [
                                    InkWell(
                                      onTap: () {
                                        print(i);
                                        setState(() {

                                          card=state.cardResponseModel[0].types[selectedType].data[i];
                                          selected = i;
                                          total=double.parse(card.price);
                                          first = false;
                                          numberCards=1;
                                          bloc.onChangeNumberCards(numberCards);
                                          box = Boxes.getTransactions();
                                          print(card.price);
                                        });
                                      },
                                      child: Container(
                                        width: ScreenUtil().screenWidth * 0.4,
                                        height: ScreenUtil().screenHeight * 0.1,
                                        margin: EdgeInsets.all(8.0),
                                        decoration: BoxDecoration(
                                            color: Colors.white,
                                            borderRadius:
                                                BorderRadius.circular(5.0),
                                            border: Border.all(
                                              width: 2,
                                              color: selected == i
                                                  ? Colors.blueGrey
                                                  : Colors.grey,
                                            )),
                                        child: Column(
                                          children: <Widget>[
                                            Expanded(
                                              flex: 1,
                                              child: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.center,
                                                mainAxisAlignment:
                                                    MainAxisAlignment.center,
                                                children: <Widget>[
                                                  Center(
                                                    child: Text(
                                                      "${state.cardResponseModel[0].types[selectedType].data[i].name}",
                                                      style: TextStyle(
                                                        color: Colors.blueGrey,
                                                        fontWeight:
                                                            FontWeight.w500,
                                                        fontFamily: ArabicFonts.Cairo,
                                                        package: 'google_fonts_arabic',
                                                        fontSize: ScreenUtil().setSp(14),
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                    i + 1 < state.cardResponseModel[0].types[selectedType].data.length ?InkWell(
                                            onTap: () {
                                              print(i + 1);
                                              setState(() {
                                                selected = i + 1;
                                                card=state.cardResponseModel[0].types[selectedType].data[i+1];
                                                numberCards=1;
                                                bloc.onChangeNumberCards(numberCards);
                                                total= double.parse(card.price);
                                                box = Boxes.getTransactions();
                                                first = false;
                                              });
                                            },
                                            child: Container(
                                              width: ScreenUtil().screenWidth *
                                                  0.4,
                                              height:
                                                  ScreenUtil().screenHeight *
                                                      0.1,
                                              margin: EdgeInsets.all(8.0),
                                              decoration: BoxDecoration(
                                                  color: Colors.white,
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          5.0),
                                                  border: Border.all(
                                                    width: 2,
                                                    color: selected == i + 1
                                                        ? Colors.blueGrey
                                                        : Colors.grey,
                                                  )),
                                              child: Column(
                                                children: <Widget>[
                                                  Expanded(
                                                    flex: 1,
                                                    child: Column(
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .center,
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .center,
                                                      children: <Widget>[
                                                        Center(
                                                          child: Text(
                                                            "${state.cardResponseModel[0].types[selectedType].data[i + 1].name}",
                                                            style: TextStyle(
                                                              color: Colors
                                                                  .blueGrey,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w500,
                                                              fontFamily: ArabicFonts.Cairo,
                                                              package: 'google_fonts_arabic',
                                                              fontSize: ScreenUtil().setSp(14),
                                                            ),
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          )
                                        : SizedBox(
                                            width:
                                                ScreenUtil().screenWidth * 0.4,
                                          ),
                                  ]),
                          ],
                        ),
                        SizedBox(height: ScreenUtil().screenHeight*0.2,),
                      ]),
                ),
                Align(
                  alignment: Alignment.bottomCenter,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Row(
                        children: [
                          Container(
                            height: ScreenUtil().screenHeight * 0.1,
                            width: ScreenUtil().screenWidth * 0.4,
                            color: Colors.white,
                            child: Column(
                              children: [
                                Text(
                                  'Total amount'.tr().toString(),
                                  style: TextStyle(
                                    fontSize: ScreenUtil().setSp(20),
                                    color: Colors.black,
                                    fontFamily: ArabicFonts.Cairo,
                                    package: 'google_fonts_arabic',
                                  ),
                                ),
                                Text(
                                  '${total*numberCards}',
                                  style: TextStyle(
                                    fontSize: ScreenUtil().setSp(16),
                                    color: Colors.black,
                                    fontFamily: ArabicFonts.Cairo,
                                    package: 'google_fonts_arabic',
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Container(
                        color: Colors.white,
                        child: Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(8.0),
                          ),
                          padding: EdgeInsets.symmetric(
                              horizontal: ScreenUtil().setWidth(20)),
                          height: ScreenUtil().screenHeight * 0.1,
                          width: ScreenUtil().screenWidth * 0.6,
                          child: Container(
                            decoration: BoxDecoration(
                              color: Colors.blueGrey,
                              borderRadius: BorderRadius.circular(8.0),
                            ),
                            width: ScreenUtil().screenWidth,
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Padding(
                                  padding: EdgeInsets.symmetric(
                                      horizontal: ScreenUtil().setWidth(5)),
                                  child: SizedBox(
                                    width: ScreenUtil().screenWidth * 0.1,
                                    child: TextButton(
                                        style: ButtonStyle(),
                                        // foreground
                                        onPressed: () {
                                          if (numberCards > 0){
                                            numberCards--;
                                            bloc.onChangeNumberCards(numberCards);
                                          }
                                        },
                                        child: Icon(
                                          Icons.remove,
                                          size: ScreenUtil().setWidth(25),
                                          color: Colors.white,
                                        )),
                                  ),
                                ),
                                 Container(
                                    width: ScreenUtil().screenWidth * 0.2,
                                    child: Center(
                                      child: Text(
                                        state.numberCards.toString(),
                                        style: TextStyle(
                                          fontSize: ScreenUtil().setSp(22),
                                          color: Colors.white,
                                          fontFamily: ArabicFonts.Cairo,
                                          package: 'google_fonts_arabic',),
                                      ),
                                    ),
                                  ),
                                Padding(
                                  padding: EdgeInsets.symmetric(
                                      horizontal: ScreenUtil().setWidth(5)),
                                  child: SizedBox(
                                    width: ScreenUtil().screenWidth * 0.1,
                                    child: TextButton(
                                        style: ButtonStyle(),
                                        // foreground
                                        onPressed: () {
                                          if(numberCards<card.count)
                                          {
                                            numberCards++;
                                            bloc.onChangeNumberCards(numberCards);
                                          }
                                          else
                                          {
                                            ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                                              duration: Duration(milliseconds: 1000),
                                              backgroundColor: Colors.redAccent,
                                              content: Text("No more Cards".tr().toString(),style: TextStyle(
                                                color: Colors
                                                    .white,
                                                fontWeight:
                                                FontWeight
                                                    .w500,
                                                fontFamily: ArabicFonts.Cairo,
                                                package: 'google_fonts_arabic',
                                                fontSize: ScreenUtil().setSp(18),
                                              ),),
                                            ));
                                          }

                                        },
                                        child: Icon(
                                          Icons.add,
                                          size: ScreenUtil().setWidth(25),
                                          color: Colors.white,
                                        )),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                        ],),
                      Row(
                        children: [
                          Container(
                            color: Colors.white,
                            child: Container(
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(8.0),
                              ),
                              padding: EdgeInsets.symmetric(
                                  horizontal: ScreenUtil().setWidth(75),vertical: ScreenUtil().setHeight(5)),
                              height: ScreenUtil().screenHeight * 0.1,
                              width: ScreenUtil().screenWidth ,
                              child: Container(
                                decoration: BoxDecoration(
                                  color: Colors.blueGrey,
                                  borderRadius: BorderRadius.circular(8.0),
                                ),
                                width: ScreenUtil().screenWidth,
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    InkWell(
                                      onTap: () async {
                                        if(numberCards>0){
                                          setState(() {
                                            box = Boxes.getTransactions();
                                          });
                                          Box<CardBasket> itemBox = Hive.box<CardBasket>("CardBasket");
                                          bool found= itemBox.values.where((item) => item.cardId==card.id).isNotEmpty;
                                          if(found==false)
                                            addCardBasket(card.name,numberCards,double.parse(card.price),"${Url.BaseUrl}${state.cardResponseModel[0].types[selectedType].image.toString()}",card.id,true,card.count);
                                          else {
                                            ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                                              duration: Duration(milliseconds: 1000),
                                              backgroundColor: Colors.redAccent,
                                              content: Text("the card is exist".tr().toString(),style: TextStyle(
                                                color: Colors
                                                    .white,
                                                fontWeight:
                                                FontWeight
                                                    .w500,
                                                fontFamily: ArabicFonts.Cairo,
                                                package: 'google_fonts_arabic',
                                                fontSize: ScreenUtil().setSp(18),
                                              ),),),);
                                          }
                                        }
                                      },
                                      child: Container(
                                        width: ScreenUtil().screenWidth * 0.4,
                                        child: Center(
                                          child: Text(
                                            "Add Card".tr().toString(),
                                            style: TextStyle(
                                              fontSize: ScreenUtil().setSp(20),
                                              color: Colors.white,
                                              fontFamily: ArabicFonts.Cairo,
                                              package: 'google_fonts_arabic',),
                                          ),
                                        ),
                                      ),
                                    ),

                                  ],
                                ),
                              ),
                            ),
                          ),
                        ],),

                    ],
                  ),
                ),

              ]);
            }),
      ),
    );
  }
  Future addCardBasket(String name, int count,double price,String image,int cardID,bool isAdd,int maxCount ) async {

    final cardBasket = CardBasket()
      ..name = name
      ..count=count
      ..price=price
      ..image=image
     ..cardId=cardID
      ..isAdd=isAdd
    ..maxCount=maxCount;
    final box = Boxes.getTransactions();

    box.add(cardBasket);
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      duration: Duration(milliseconds: 1000),
      content: Text("Add Card done".tr().toString(),style: TextStyle(
        color: Colors
            .white,
        fontWeight:
        FontWeight
            .w500,
        fontFamily: ArabicFonts.Cairo,
        package: 'google_fonts_arabic',
        fontSize: ScreenUtil().setSp(18),
      ),),
    ));

  }
}
