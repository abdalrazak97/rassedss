library CardState;

import 'package:built_value/built_value.dart';
import 'package:rassed_app/features/card_details/model/card_model.dart';


part 'CardState.g.dart';

abstract class CardState implements Built<CardState, CardStateBuilder> {
  bool get isSuccess;
  bool get isLoading;
  String get Accept_Language;
  String get api_token;
  String get company_id;
  String get errorMessage;
  int get numberCards;

  List<CardResponseModel> get cardResponseModel;


  CardState._();

  factory CardState([updates(CardStateBuilder b)]) = _$CardState;

  factory CardState.initial() {
    return CardState((b) => b
      ..isLoading = false
      ..isSuccess = false
      ..api_token =''
      ..errorMessage = ''
      ..Accept_Language = ''
      ..company_id='26'
      ..numberCards=0
      ..cardResponseModel=[]
    );
  }
}
