import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:rassed_app/features/card_details/bloc/CardEvent.dart';
import 'package:rassed_app/features/card_details/usecase/card_case.dart';


import 'CardState.dart';


const String SERVER_FAILURE_MESSAGE = 'Server Failure';
const String CACHE_FAILURE_MESSAGE = 'Cache Failure';
const String INVALID_INPUT_FAILURE_MESSAGE =
    'Invalid Input - The number must be a positive integer or zero.';

class CardBloc extends Bloc<CardEvents, CardState> {
  final CardUseCase cardUseCase;

  CardBloc({this.cardUseCase}) : super(CardState.initial());

  void onChangeApiToken(String api_token){
    add( ChangeApiToken(api_token));
  }

  void onChangeCompanyId(String company_id){
    add( ChangeCampanyId(company_id));
  }
  void onChangeNumberCards(int numberCards){
    add( ChangeNumberCards(numberCards));
  }
  void onCardEvent()
   {add(CardEvent());}
  @override
  Stream<CardState> mapEventToState(CardEvents event) async* {
    if (event is CardEvent) {
      yield state.rebuild((b) => b
        ..isLoading = true
        ..isSuccess = false

      );

      final result = await cardUseCase(
          cardParams(
              api_token: state.api_token,
              company_id: state.company_id
          )
      );

      yield* result.fold((l) async* {

        yield state.rebuild((b) => b
          ..isLoading = false
          ..isSuccess = false
          ..errorMessage = l);
      }, (r) async* {
        print('sucesss');
        yield state.rebuild((b) => b
          ..isLoading = false
          ..isSuccess = true
          ..errorMessage = ''
          ..cardResponseModel = r);
      });

    }

    else if (event is ChangeApiToken) {
      print('1111');
      yield state.rebuild((b) => b..api_token = event.api_token);

    }
    else if (event is ChangeCampanyId) {
      print('2222');
      yield state.rebuild((b) => b..company_id = event.company_id);

    }
    else if (event is ChangeNumberCards) {
      print('2222');
      yield state.rebuild((b) => b..numberCards = event.numberCards);

    }

  }
}
