// GENERATED CODE - DO NOT MODIFY BY HAND

part of CardState;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$CardState extends CardState {
  @override
  final bool isSuccess;
  @override
  final bool isLoading;
  @override
  final String Accept_Language;
  @override
  final String api_token;
  @override
  final String company_id;
  @override
  final String errorMessage;
  @override
  final int numberCards;
  @override
  final List<CardResponseModel> cardResponseModel;

  factory _$CardState([void Function(CardStateBuilder) updates]) =>
      (new CardStateBuilder()..update(updates)).build();

  _$CardState._(
      {this.isSuccess,
      this.isLoading,
      this.Accept_Language,
      this.api_token,
      this.company_id,
      this.errorMessage,
      this.numberCards,
      this.cardResponseModel})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(isSuccess, 'CardState', 'isSuccess');
    BuiltValueNullFieldError.checkNotNull(isLoading, 'CardState', 'isLoading');
    BuiltValueNullFieldError.checkNotNull(
        Accept_Language, 'CardState', 'Accept_Language');
    BuiltValueNullFieldError.checkNotNull(api_token, 'CardState', 'api_token');
    BuiltValueNullFieldError.checkNotNull(
        company_id, 'CardState', 'company_id');
    BuiltValueNullFieldError.checkNotNull(
        errorMessage, 'CardState', 'errorMessage');
    BuiltValueNullFieldError.checkNotNull(
        numberCards, 'CardState', 'numberCards');
    BuiltValueNullFieldError.checkNotNull(
        cardResponseModel, 'CardState', 'cardResponseModel');
  }

  @override
  CardState rebuild(void Function(CardStateBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  CardStateBuilder toBuilder() => new CardStateBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is CardState &&
        isSuccess == other.isSuccess &&
        isLoading == other.isLoading &&
        Accept_Language == other.Accept_Language &&
        api_token == other.api_token &&
        company_id == other.company_id &&
        errorMessage == other.errorMessage &&
        numberCards == other.numberCards &&
        cardResponseModel == other.cardResponseModel;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc(
                        $jc($jc($jc(0, isSuccess.hashCode), isLoading.hashCode),
                            Accept_Language.hashCode),
                        api_token.hashCode),
                    company_id.hashCode),
                errorMessage.hashCode),
            numberCards.hashCode),
        cardResponseModel.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('CardState')
          ..add('isSuccess', isSuccess)
          ..add('isLoading', isLoading)
          ..add('Accept_Language', Accept_Language)
          ..add('api_token', api_token)
          ..add('company_id', company_id)
          ..add('errorMessage', errorMessage)
          ..add('numberCards', numberCards)
          ..add('cardResponseModel', cardResponseModel))
        .toString();
  }
}

class CardStateBuilder implements Builder<CardState, CardStateBuilder> {
  _$CardState _$v;

  bool _isSuccess;
  bool get isSuccess => _$this._isSuccess;
  set isSuccess(bool isSuccess) => _$this._isSuccess = isSuccess;

  bool _isLoading;
  bool get isLoading => _$this._isLoading;
  set isLoading(bool isLoading) => _$this._isLoading = isLoading;

  String _Accept_Language;
  String get Accept_Language => _$this._Accept_Language;
  set Accept_Language(String Accept_Language) =>
      _$this._Accept_Language = Accept_Language;

  String _api_token;
  String get api_token => _$this._api_token;
  set api_token(String api_token) => _$this._api_token = api_token;

  String _company_id;
  String get company_id => _$this._company_id;
  set company_id(String company_id) => _$this._company_id = company_id;

  String _errorMessage;
  String get errorMessage => _$this._errorMessage;
  set errorMessage(String errorMessage) => _$this._errorMessage = errorMessage;

  int _numberCards;
  int get numberCards => _$this._numberCards;
  set numberCards(int numberCards) => _$this._numberCards = numberCards;

  List<CardResponseModel> _cardResponseModel;
  List<CardResponseModel> get cardResponseModel => _$this._cardResponseModel;
  set cardResponseModel(List<CardResponseModel> cardResponseModel) =>
      _$this._cardResponseModel = cardResponseModel;

  CardStateBuilder();

  CardStateBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _isSuccess = $v.isSuccess;
      _isLoading = $v.isLoading;
      _Accept_Language = $v.Accept_Language;
      _api_token = $v.api_token;
      _company_id = $v.company_id;
      _errorMessage = $v.errorMessage;
      _numberCards = $v.numberCards;
      _cardResponseModel = $v.cardResponseModel;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(CardState other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$CardState;
  }

  @override
  void update(void Function(CardStateBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$CardState build() {
    final _$result = _$v ??
        new _$CardState._(
            isSuccess: BuiltValueNullFieldError.checkNotNull(
                isSuccess, 'CardState', 'isSuccess'),
            isLoading: BuiltValueNullFieldError.checkNotNull(
                isLoading, 'CardState', 'isLoading'),
            Accept_Language: BuiltValueNullFieldError.checkNotNull(
                Accept_Language, 'CardState', 'Accept_Language'),
            api_token: BuiltValueNullFieldError.checkNotNull(
                api_token, 'CardState', 'api_token'),
            company_id: BuiltValueNullFieldError.checkNotNull(
                company_id, 'CardState', 'company_id'),
            errorMessage: BuiltValueNullFieldError.checkNotNull(
                errorMessage, 'CardState', 'errorMessage'),
            numberCards: BuiltValueNullFieldError.checkNotNull(
                numberCards, 'CardState', 'numberCards'),
            cardResponseModel: BuiltValueNullFieldError.checkNotNull(
                cardResponseModel, 'CardState', 'cardResponseModel'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
