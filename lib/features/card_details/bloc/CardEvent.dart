import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class CardEvents extends Equatable {
  @override
  List<Object> get props => [];
}

class CardEvent extends CardEvents {}

class ChangeApiToken extends CardEvents{
  final api_token;
  ChangeApiToken(this.api_token);
}
class ChangeCampanyId extends CardEvents{
  final company_id;
  ChangeCampanyId(this.company_id);
}
class ChangeNumberCards extends CardEvents{
  final numberCards;
  ChangeNumberCards(this.numberCards);
}

class ResetError extends CardEvents {}

class Enable extends CardEvents {}

class Disable extends CardEvents {}
