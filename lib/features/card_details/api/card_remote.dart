import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:rassed_app/features/Constants/ApiConstants.dart';
import 'package:dartz/dartz.dart';
import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:dio/dio.dart';
import 'package:meta/meta.dart';
import 'package:rassed_app/features/card_details/model/card_model.dart';
import 'package:rassed_app/features/card_details/usecase/card_case.dart';


abstract class CardRemoteDataSource {
  Future<Either<String, List<CardResponseModel>>> card(cardParams params);
}

class CardRemoteDataSourceImpl extends CardRemoteDataSource {
  final Dio dio;
  final DataConnectionChecker networkInfo;

  CardRemoteDataSourceImpl({@required this.dio, @required this.networkInfo});

  @override
  Future<Either<String, List<CardResponseModel>>> card(cardParams params) async {
    List<CardResponseModel> card;

    if (await networkInfo.hasConnection) {
      try {
      print(params.company_id);
        final result = await dio.post(
          Url.show_company_card ,
          data:params.toJson(),
          options: Options(
            headers:{
              'Authorization': 'Bearer ${params.api_token}',
            } ,
            followRedirects: false,
            validateStatus: (status) {
              return status < 500;
            },
          ),
        );
        print('8888888888888888888888888');
        print(result.data);
        card=CardResponseModelFromJson(result.data);

        return Right(card);
      } on DioError catch (ex) {
        if (ex.type == DioErrorType.connectTimeout) {
          return Left(Er.NetworkError);
        } else if (ex.type == DioErrorType.receiveTimeout) {
          return Left(Er.NetworkError);
        } else if (card != null)
          return Left(card[0].message);
        else if (card == null) return Left(Er.Error);
      }
    } else {
      return Left(Er.NetworkError);
    }
  }
}
