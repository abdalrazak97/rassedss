import 'package:flutter/material.dart';

class ValueItemCard extends StatelessWidget {

  final String price;
  final int id;
  final int selected;
  ValueItemCard({this.price,this.id,this.selected});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding:  EdgeInsets.all(0),
      child: Container(
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(5.0),
            border: Border.all(
                width: 2,
                color:id==selected?Colors.deepPurple:Colors.grey
            )
        ),
        child: Column(
          children: <Widget>[
            Expanded(
              flex: 1,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                   Center(
                      child: Text("$price \$", style: TextStyle(
                        color: Colors.deepPurple,
                        fontWeight: FontWeight.w500,
                        fontFamily: 'Poppins',
                        fontSize: 18,
                      ),),
                    ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }



}
