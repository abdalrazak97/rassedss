import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:rassed_app/features/profile/bloc/ProfileBloc.dart';
import 'package:rassed_app/features/profile/bloc/ProfileState.dart';
import 'package:rassed_app/features/settings/Preference.dart';
import 'package:rassed_app/features/widget/textauto.dart';

import '../../injection.dart';

class drawerItem extends StatefulWidget {
  String text;
  bool counter;
  VoidCallback press;
  int myindex;

  drawerItem(String Text1, bool counter1, VoidCallback press1, int index) {
    text = Text1;
    counter = counter1;
    press = press1;
    myindex = index;
  }

  @override
  _drawerItemState createState() => _drawerItemState();
}

class _drawerItemState extends State<drawerItem> {
  ProfileBloc bloc = sl<ProfileBloc>();

  int num;
  String token;

  @override
  void initState() {
    if(widget.counter){
      token = Preferences.getUserToken();
      bloc.onChangeApiToken(token);
      bloc.onNotifyProfile();
    }

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return OrientationBuilder(builder: (context, orientation) {
      bool isPotrait =
          MediaQuery.of(context).orientation == Orientation.portrait;
      return Container(
        height: MediaQuery.of(context).size.height * 0.065,
        child: TextButton(
          style: ButtonStyle(
            padding: MaterialStateProperty.all(EdgeInsets.zero),
            shape: MaterialStateProperty.all(RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(15))),
          ),
          onPressed: widget.press,
          child: Container(
            height: MediaQuery.of(context).size.height * 0.1,
            child: Padding(
              padding:
                  EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(12)),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    children: [
                      SizedBox(
                        width: 10,
                      ),
                      AutoTextgrey(
                          isPotrait
                              ? ScreenUtil().setSp(18)
                              : ScreenUtil().setSp(20),
                          widget.text,
                          Colors.grey.shade300),
                    ],
                  ),
                  widget.counter == true
                      ? BlocBuilder<ProfileBloc, ProfileState>(
                          bloc: bloc,
                          builder: (context, state) {
                            num = state.notifyResponseModel.length;
                            print(num);
                            return IconButton(
                              icon: new Stack(children: <Widget>[
                                new Icon(
                                  Icons.notifications_active,
                                  color: Colors.white,
                                  size: ScreenUtil().setWidth(25),
                                ),
                                new Positioned(
                                  right: 0,
                                  child: new Container(
                                    padding: EdgeInsets.all(1),
                                    decoration: new BoxDecoration(
                                      color: Colors.red,
                                      borderRadius: BorderRadius.circular(6),
                                    ),
                                    constraints: BoxConstraints(
                                      minWidth: 12,
                                      minHeight: 12,
                                    ),
                                    child: new Text(
                                      '$num',
                                      style: new TextStyle(
                                        color: Colors.white,
                                        fontSize: 8,
                                      ),
                                      textAlign: TextAlign.center,
                                    ),
                                  ),
                                )
                              ]),
                            );
                          })
                      : Padding( padding:  EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(12)),
                        child: Icon(
                            widget.myindex == 1
                                ? Icons.person_rounded
                                : widget.myindex == 2
                                    ? Icons.home
                                    : widget.myindex == 3
                                        ? Icons.shopping_bag
                                        : widget.myindex == 4
                                            ? Icons.storefront
                                            : widget.myindex == 5
                                                ? Icons.shopping_cart
                                                : widget.myindex == 6
                                                    ? Icons.dashboard_outlined
                                                    : Icons.notifications_active,
                            size: ScreenUtil().setWidth(25),
                            color: Colors.white,
                          ),
                      ),
                ],
              ),
            ),
          ),
        ),
      );
    });
  }
}
