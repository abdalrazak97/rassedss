
import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:rassed_app/features/Shopping_basket/shopping_basket.dart';
import 'package:rassed_app/features/add_cards/add_cards.dart';
import 'package:rassed_app/features/home/home.dart';
import 'package:rassed_app/features/profile/profile.dart';
import 'package:rassed_app/features/settings/Globals.dart';
import 'package:rassed_app/features/shop/shop.dart';
import 'drawerItem.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:rassed_app/features/sales/sales.dart';
import 'package:rassed_app/features/profile/component/notify.dart';
class drawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    print(role);
    return
      SafeArea(
        child: Drawer(
          child: Container(
            color: Colors.blueGrey,
            child: Column(
                children: [
                SizedBox(height: ScreenUtil().setHeight(20),),
                drawerItem('Profile'.tr().toString(),false,(){
                  Navigator.pushReplacement(context, MaterialPageRoute(builder: (BuildContext context) => Profile()));
                },1),
                SizedBox(height: ScreenUtil().setHeight(20),),
                drawerItem('Home'.tr().toString(),false,(){
                  Navigator.pushReplacement(context, MaterialPageRoute(builder: (BuildContext context) => Home()));
                },2),
                SizedBox(height: ScreenUtil().setHeight(20),),
                drawerItem('Invoices'.tr().toString(),false,(){
                  Navigator.pushReplacement(context, MaterialPageRoute(builder: (BuildContext context) => Sales()));
                },3),
                SizedBox(height: ScreenUtil().setHeight(20),),
                drawerItem('Shops'.tr().toString(),false,(){
                  Navigator.pushReplacement(context, MaterialPageRoute(builder: (BuildContext context) => Shop()));
                 },4),
                SizedBox(height: ScreenUtil().setHeight(20),),
                drawerItem('Shop Cart'.tr().toString(),false,(){
                  Navigator.pushReplacement(context, MaterialPageRoute(builder: (BuildContext context) => ShoppingBasket()));
                },5),
                role==10? SizedBox(height: ScreenUtil().setHeight(20),):SizedBox(),
                role==10? drawerItem('Dashboard'.tr().toString(),false,(){
                  Navigator.pushReplacement(context, MaterialPageRoute(builder: (BuildContext context) => AddCardsPage()));
                },6):SizedBox(),
                SizedBox(height: ScreenUtil().setHeight(20),),
                drawerItem('Notify'.tr().toString(),true,(){
                  Navigator.pushReplacement(context, MaterialPageRoute(builder: (BuildContext context) => NotifyPage()));
                },7),
              ],
            ),
          ),
          ),
      );
  }
}
